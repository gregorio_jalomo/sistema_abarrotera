<?php



	function validate($context) {
		$output = array();
		$output['status'] = false;
		$context->form_validation->set_error_delimiters('', '');
		$validated = $context->form_validation->run();
		if ($validated)
		{
			$output['status'] = true;
			$output =  $output;
		}
		else
		{
			$output['errors'] = validation_errors();
		}
		if (array_key_exists('errors', $output)) {
			$errors = explode("\n", $output['errors']);
			foreach ($errors as $key => $error) {
				$errors[$key] = $error;//json_decode($error);
			}
			$output['errors'] = $errors;
		}
		/*
		if (defined('PHPUNIT_TEST')) {
			return json_encode(array('output' => $output));
		} else {
			$context->load->view('json', array('output' => $output));
		}
		*/
		//return json_encode(array('output' => $output));
		return $output;
	}

	function validar_permiso($rol_usuario,$id_modulo,$id_funcion){//(2,1,1)
		// ej. funciones: Agregar,Listar,Eliminar,Editar
		$CI =&get_instance();
		$CI->load->model('usuarios/Roles_model');
		return $CI->Roles_model->validar_permiso($rol_usuario,$id_modulo,$id_funcion);
	}


	function crear_foliodocumento($prefijo_nombre,$FUNI){
		$CI =&get_instance();
		$CI->load->model('catalogos/Foliodocumento');
		return $CI->Foliodocumento->nuevo_foliodocumento($prefijo_nombre,$FUNI);
	}

	function get_miliseconds($HH_MM_SS){
		//devuelve el tiempo en milisegundos dada una cadena "HH:MM:SS"
		$time   = explode(":", $HH_MM_SS);
		$hour   = $time[0] * 60 * 60 * 1000;
        $minute = $time[1] * 60 * 1000;
        $sec    = $time[2] * 1000;
        return $hour + $minute + $sec;
    };