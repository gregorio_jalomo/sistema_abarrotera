<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}
      public function get_where_in_row($campo,$array,$tabla){
        return $this->db->where_in($campo,$array)->get($tabla)->row();
      }

      public function get_where_in_result($campo,$array,$tabla){
        return $this->db->where_in($campo,$array)->get($tabla)->result();
      }

      public function update_table_row($tabla,$data,$id_table,$id){
    		return $this->db->update($tabla, $data, array($id_table=>$id));
    	}

      public function obtenerProductosPorPedido($id_pedido){
        $this->db->select(['d.nombre_producto', 'p.producto_id_productosat',
                           'p.producto_id_unidad_sat', 'p.producto_id', 
                           'd.cantidad', 'd.costo_unitario', 'd.costo_total',
                           'd.IVA', 'd.IEPS']);                           
        $this->db->from('pedidosaduana_detalle d');
        $this->db->where('id_pedidosaduana', $id_pedido );
        $this->db->join('productos p', 'd.id_producto = p.producto_id');
        return $query = $this->db->get()->result();
      }
      
}
