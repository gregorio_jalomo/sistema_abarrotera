<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_agenteventa').submit(function(event){
    event.preventDefault();
    console.log($('#agenteventa_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/agentesventa/guardar";
    ajaxJson(url,{
        'agenteventa_nombre' : $('#agenteventa_nombre').val(),
        'agenteventa_a_paterno' : $('#agenteventa_a_paterno').val(),
        'agenteventa_a_materno' : $('#agenteventa_a_materno').val(),
        'agenteventa_celular' : $('#agenteventa_celular').val(),
        'agenteventa_comision' : $('#agenteventa_comision').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/agentesventa/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                        if(result=="true"){
                          $("#borrar_"+id).slideUp();
                          ExitoCustom("Eliminado");
                        }else{
                          ErrorCustom("No tienes permiso para eliminar esto");
                        }
                      });

                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Agente De Venta</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_agenteventa">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Nombre</label>
                  <input type="text" class="form-control" name="agenteventa_nombre" id="agenteventa_nombre">
                </div>
                <div class="col-md-4">
                  <label for="">Apellido Paterno</label>
                  <input type="text" class="form-control" name="agenteventa_a_paterno" id="agenteventa_a_paterno">
                </div>
                <div class="col-md-4">
                  <label for="">Apellido Materno</label>
                  <input type="text" class="form-control" name="agenteventa_a_materno" id="agenteventa_a_materno">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Celular</label>
                  <input type="number" class="form-control" name="agenteventa_celular" id="agenteventa_celular">
                </div>
                <div class="col-md-3">
                  <label for="">Comisión %</label>
                  <input type="number" class="form-control" name="agenteventa_comision" id="agenteventa_comision">
                </div>


              </div>






              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agentes de Venta Agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre Completo</th>
                  <th>Celular</th>
                  <th>Comisión</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($agentesventa as $row ) {
                      $agenteventa_id = $row->agenteventa_id;
                      $agenteventa_nombre = $row->agenteventa_nombre;
                      $agenteventa_a_paterno = $row->agenteventa_a_paterno;
                      $agenteventa_a_materno = $row->agenteventa_a_materno;
                      $agenteventa_celular = $row->agenteventa_celular;
                      $agenteventa_comision = $row->agenteventa_comision;

                      echo '
                        <tr id="borrar_'.$agenteventa_id.'">
                          <td>'.$agenteventa_nombre.' '.$agenteventa_a_paterno.' '.$agenteventa_a_materno.'</td>
                          <td>'.$agenteventa_celular.'</td>
                          <td>'.$agenteventa_comision.'</td>


                          <td>
                            <a href="'.base_url().'index.php/agentesventa/editar/'.$agenteventa_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$agenteventa_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/agentesventa/cambiar_estatus/'.$agenteventa_id.'/3" class="eliminar_relacion" flag="'.$agenteventa_id.'" id="delete'.$agenteventa_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuAgentesventa').addClass('active-link');
  });
</script>
