<?php
  $agenteventa_id = $agentesventa->agenteventa_id;
  $agenteventa_nombre = $agentesventa->agenteventa_nombre;
  $agenteventa_a_paterno = $agentesventa->agenteventa_a_paterno;
  $agenteventa_a_materno = $agentesventa->agenteventa_a_materno;
  $agenteventa_celular = $agentesventa->agenteventa_celular;
  $agenteventa_comision = $agentesventa->agenteventa_comision;

?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_agenteventa').submit(function(event){
    event.preventDefault();
    console.log($('#agenteventa_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/agentesventa/actualizar";
    ajaxJson(url,{
        'agenteventa_id' : '<?php echo $agenteventa_id; ?>',
        'agenteventa_nombre' : $('#agenteventa_nombre').val(),
        'agenteventa_a_paterno' : $('#agenteventa_a_paterno').val(),
        'agenteventa_a_materno' : $('#agenteventa_a_materno').val(),
        'agenteventa_celular' : $('#agenteventa_celular').val(),
        'agenteventa_comision' : $('#agenteventa_comision').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/agentesventa/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Agente de Venta</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_agenteventa">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Nombre</label>
                  <input type="text" class="form-control" name="agenteventa_nombre" id="agenteventa_nombre" value="<?php echo $agenteventa_nombre; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Apellido Paterno</label>
                  <input type="text" class="form-control" name="agenteventa_a_paterno" id="agenteventa_a_paterno" value="<?php echo $agenteventa_a_paterno; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Apellido Materno</label>
                  <input type="text" class="form-control" name="agenteventa_a_materno" id="agenteventa_a_materno" value="<?php echo $agenteventa_a_materno; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Celular</label>
                  <input type="text" class="form-control" name="agenteventa_celular" id="agenteventa_celular" value="<?php echo $agenteventa_celular; ?>">
                </div>
                <div class="col-md-3">
                  <label for="">Comisión</label>
                  <input type="number" class="form-control" name="agenteventa_comision" id="agenteventa_comision" value="<?php echo $agenteventa_comision; ?>">
                </div>

              </div>







              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/agentesventa/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuAgentesventa').addClass('active-link');
    $('#agenteventacredito_id>option:selected').text();
  });
</script>
