<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Choferes extends MX_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = "only_admin_choferes";//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,"only_admin_choferes") || $this->is_admin ){

      $data['titulo_seccion'] = "Agregar chofer";
      $data['flecha_ir_atras'] = "catalogos/Catalogos/menu";

      $data['choferes']=$this->Mgeneral->get_table("choferes");

      $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
      $this->cargar_vista('choferes/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar choferes","catalogos/Catalogos/menu");
    }

  }//...alta

  public function ver_chofer($id_chofer = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,"only_admin_choferes") || $this->is_admin ){

      $data['titulo_seccion'] = "Editar chofer";
      $data['flecha_ir_atras'] = "catalogos/choferes/alta";

      $data['chofer']=$this->Mgeneral->get_row("choferes_id",$id_chofer,"choferes");

      $contenido = $this->load->view('choferes/editar', $data, TRUE);
        $archivos_js=array(
            'statics/js/bootbox.min.js',
            'statics/js/general.js?v='.time(),
            'statics/bootstrap4/js/bootstrap.min.js'
          );
      
        $this->cargar_vista('choferes/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar choferes","catalogos/choferes/alta");
    }

  }//...ver_chofer

  public function guardar_chofer(){
    $this->form_validation->set_rules('choferes_nombre', 'Nombre', 'required');
    $this->form_validation->set_rules('choferes_celular', 'Celular', 'required');
    $this->form_validation->set_rules('choferes_licencia', 'Licencia', 'required');

    $response = validate($this);
    if($response['status']){
      $data['choferes_nombre']      = $this->input->post('choferes_nombre');
      $data['choferes_celular']     = $this->input->post('choferes_celular');
      $data['choferes_licencia']    = $this->input->post('choferes_licencia');
      $data["fecha"]                = date("Y-m-d H:i:s");
      //$data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('choferes', $data);
    }

    echo json_encode(array('output' => $response));
  }//...guardar_chofer

  public function editar_chofer(){
    $this->form_validation->set_rules('choferes_nombre', 'Nombre', 'required');
    $this->form_validation->set_rules('choferes_celular', 'Celular', 'required');
    $this->form_validation->set_rules('choferes_licencia', 'Licencia', 'required');

    $response = validate($this);
    if($response['status']){

        $choferes_id = $this->input->post('choferes_id');
        $data['choferes_nombre'] = $this->input->post('choferes_nombre');
        $data['choferes_celular'] = $this->input->post('choferes_celular');
        $data['choferes_licencia'] = $this->input->post('choferes_licencia');
        
        //$data['cat_estatus_id'] = 1;
        $this->Mgeneral->update_table_row('choferes',$data,'choferes_id',$choferes_id);
    }

    echo json_encode(array('output' => $response));

  }//...editar_chofer

  public function cambiar_estatus_chofer($chofer_id, $estatus_id) {
    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,"only_admin_choferes") || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,"only_admin_choferes") || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['chofer_id'] = $chofer_id;
      $this->Mgeneral->update_table_row('choferes',$data,'chofer_id',$data['chofer_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus


}
