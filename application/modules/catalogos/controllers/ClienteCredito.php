<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class ClienteCredito extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 30;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,106) || $this->is_admin ){

      $data['titulo_seccion'] = "Crédito de Cliente";
      $data['flecha_ir_atras'] = "clientes/catalogos";
      $data['clientescredito'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescredito');
      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/clientes_credito/alta',$data,$archivos_js);    

    }else{
      $this->p_denied_view("agregar credito de clientes","clientes/catalogos");
    }

  }

  public function guardar(){
    $this->form_validation->set_rules('cliente_id', 'Cliente', 'required');
    $this->form_validation->set_rules('clientecredito_descripcion', 'Descripción', 'required');
    $this->form_validation->set_rules('clientecredito_limitecredito', 'Límite Crédito', 'required');
    $this->form_validation->set_rules('clientecredito_limitedias', 'Límite Días', 'required');
    $response = validate($this);
    if($response['status']){
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['clientecredito_descripcion'] = $this->input->post('clientecredito_descripcion');
      $data['clientecredito_limitecredito'] = $this->input->post('clientecredito_limitecredito');
      $data['clientecredito_limitedias'] = $this->input->post('clientecredito_limitedias');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('clientescredito', $data);
    }
    echo json_encode(array('output' => $response));
  }//...guardar

  public function editar($id_clientecredito = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,130) || $this->is_admin ){

      $data['titulo_seccion'] = "Crédito de Cliente";
      $data['flecha_ir_atras'] = "catalogos/clientecredito/alta";
      $get_row = $this->Mgeneral->get_row('clientecredito_id',$id_clientecredito,'clientescredito');
      $data['clientescredito'] = $get_row;
      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/clientes_credito/editar',$data,$archivos_js);    
      
    }else{
      $this->p_denied_view("editar credito de clientes","catalogos/clientecredito/alta");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('cliente_id', 'cliente_id', 'required');
    $this->form_validation->set_rules('clientecredito_descripcion', 'clientecredito_descripcion', 'required');
    $this->form_validation->set_rules('clientecredito_limitecredito', 'clientecredito_limitecredito', 'required');
    $this->form_validation->set_rules('clientecredito_limitedias', 'clientecredito_limitedias', 'required');
    $response = validate($this);
    if($response['status']){
      $data['clientecredito_id'] = $this->input->post('clientecredito_id');
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['clientecredito_descripcion'] = $this->input->post('clientecredito_descripcion');
      $data['clientecredito_limitecredito'] = $this->input->post('clientecredito_limitecredito');
      $data['clientecredito_limitedias'] = $this->input->post('clientecredito_limitedias');
      //...col-md-10
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('clientescredito',$data,'clientecredito_id',$data['clientecredito_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar

  public function cambiar_estatus($clientecredito_id, $estatus_id) {
    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,130) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,131) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['clientecredito_id'] = $clientecredito_id;
      $this->Mgeneral->update_table_row('clientescredito',$data,'clientecredito_id',$data['clientecredito_id']);

    }
    echo json_encode($go);


  }//...cambiar_estatus
}
?>
