<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Clientelugar extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproductos', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('catalogos/Clientelugar_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 30;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,106) || $this->is_admin ){

      $data['titulo_seccion'] = "Lugares";
      $data['flecha_ir_atras'] = 'catalogos/clientelugar/listar_lugares';

      $data['lugares'] = $this->Clientelugar_model->get_lugares();
      $data['clientes']= $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/clientelugar/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar lugares","catalogos/clientelugar/listar_lugares");
    }


  }//...alta

  public function guardar(){
    $this->form_validation->set_rules('cliente_id', 'cliente_id', 'required');
    $this->form_validation->set_rules('cliente_calle', 'cliente_calle', 'required');
    $this->form_validation->set_rules('cliente_no_exterior', 'cliente_no_exterior', 'required');
    $this->form_validation->set_rules('cliente_colonia', 'cliente_colonia', 'required');
    $this->form_validation->set_rules('cliente_cp', 'cliente_cp', 'required');
    $this->form_validation->set_rules('cliente_ciudad', 'cliente_ciudad', 'required');
    $this->form_validation->set_rules('cliente_municipio', 'cliente_municipio', 'required');
    $this->form_validation->set_rules('cliente_estado', 'cliente_estado', 'required');
    $this->form_validation->set_rules('cliente_pais', 'cliente_pais', 'required');
    $this->form_validation->set_rules('cliente_latitud', 'cliente_latitud', 'required');
    $this->form_validation->set_rules('cliente_longitud', 'cliente_longitud', 'required');

    $response = validate($this);
    if($response['status']){
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['cliente_calle'] = $this->input->post('cliente_calle');
      $data['cliente_no_interior'] = $this->input->post('cliente_no_interior');
      $data['cliente_no_exterior'] = $this->input->post('cliente_no_exterior');
      $data['cliente_colonia'] = $this->input->post('cliente_colonia');
      $data['cliente_cp'] = $this->input->post('cliente_cp');
      $data['cliente_ciudad'] = $this->input->post('cliente_ciudad');
      $data['cliente_municipio'] = $this->input->post('cliente_municipio');
      $data['cliente_estado'] = $this->input->post('cliente_estado');
      $data['cliente_pais'] = $this->input->post('cliente_pais');

      $data['cliente_latitud'] = $this->input->post('cliente_latitud');
      $data['cliente_longitud'] = $this->input->post('cliente_longitud');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");
      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->save_register('lugares', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar

  public function listar_lugares(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,129) || $this->is_admin ){

      $data['titulo_seccion'] = "Lugares";
      $data['lugares'] = $this->Clientelugar_model->get_lugares();
      
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/clientelugar/listar_lugares',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar lugares","");
    }

  }//...listar_lugares

  public function ver_detalle($lugar_id){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,107) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del cliente";
      $data['flecha_ir_atras'] = "catalogos/clientelugar/listar_lugares";
      
      $data['lugar'] = $this->Clientelugar_model->get_lugar_by_id($lugar_id);

      $data['cat_estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/clientelugar/ver_detalle',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("ver detalle de lugares","catalogos/clientelugar/listar_lugares");
    }

  }//...ver_detalle

  public function editar($lugar_id = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,130) || $this->is_admin ){

      $data['titulo_seccion'] = "lugares";
      $data['flecha_ir_atras'] = "catalogos/clientelugar/listar_lugares";

      $data['clientes']= $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      $data['lugar'] = $this->Clientelugar_model->get_lugar_by_id($lugar_id);

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/clientelugar/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar lugares","catalogos/clientelugar/listar_lugares");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('cliente_id', 'cliente_id', 'required');
    $this->form_validation->set_rules('cliente_calle', 'cliente_calle', 'required');
    $this->form_validation->set_rules('cliente_no_exterior', 'cliente_no_exterior', 'required');
    $this->form_validation->set_rules('cliente_colonia', 'cliente_colonia', 'required');
    $this->form_validation->set_rules('cliente_cp', 'cliente_cp', 'required');
    $this->form_validation->set_rules('cliente_ciudad', 'cliente_ciudad', 'required');
    $this->form_validation->set_rules('cliente_municipio', 'cliente_municipio', 'required');
    $this->form_validation->set_rules('cliente_estado', 'cliente_estado', 'required');
    $this->form_validation->set_rules('cliente_pais', 'cliente_pais', 'required');

    $this->form_validation->set_rules('cliente_latitud', 'cliente_latitud', 'required');
    $this->form_validation->set_rules('cliente_longitud', 'cliente_longitud', 'required');

    $response = validate($this);
    if($response['status']){
      $lugar_id = $this->input->post('lugar_id');
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['cliente_calle'] = $this->input->post('cliente_calle');
      $data['cliente_no_interior'] = $this->input->post('cliente_no_interior');
      $data['cliente_no_exterior'] = $this->input->post('cliente_no_exterior');
      $data['cliente_colonia'] = $this->input->post('cliente_colonia');
      $data['cliente_cp'] = $this->input->post('cliente_cp');
      $data['cliente_ciudad'] = $this->input->post('cliente_ciudad');
      $data['cliente_municipio'] = $this->input->post('cliente_municipio');
      $data['cliente_estado'] = $this->input->post('cliente_estado');
      $data['cliente_pais'] = $this->input->post('cliente_pais');

      $data['cliente_latitud'] = $this->input->post('cliente_latitud');
      $data['cliente_longitud'] = $this->input->post('cliente_longitud');
      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->update_table_row('lugares',$data,'lugar_id',$lugar_id);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar


  public function cambiar_estatus($lugar_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,130) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,131) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['lugar_id'] = $lugar_id;
      $this->Mgeneral->update_table_row('lugares',$data,'lugar_id',$data['lugar_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus

  public function get_lugares($cliente_id){
    echo json_encode($this->Clientelugar_model->get_lugares($cliente_id,null));
  }
}
