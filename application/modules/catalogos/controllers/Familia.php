<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class familia extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 22;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,84) || $this->is_admin ){

      $data['titulo_seccion'] = "Familias";
      $data['flecha_ir_atras'] = "catalogos/Catalogos/menu";
      $data['familias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('familia/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar familias","catalogos/Catalogos/menu");
    }

  }//...alta

  public function ver_familia($id_familia = null){//vista editar
    if(validar_permiso($this->rol_usuario,$this->id_modulo,82) || $this->is_admin ){

      $data['titulo_seccion'] = "familia";
      $data['flecha_ir_atras'] = "catalogos/familia/alta";
      $get_row = $this->Mgeneral->get_row('familia_id',$id_familia,'familia');
      $data['familias'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('familia/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar familias","catalogos/familia/alta");
    }

}//...ver_familia

  public function guardar_familia(){
    $this->form_validation->set_rules('familia_tipo', 'Nombre', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['familia_tipo'] = $this->input->post('familia_tipo');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('familia', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_familia

  public function editar_familia(){
    $this->form_validation->set_rules('familia_tipo', 'familia_tipo', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['familia_id'] = $this->input->post('familia_id');
      $data['familia_tipo'] = $this->input->post('familia_tipo');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('familia',$data,'familia_id',$data['familia_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_familia

  public function cambiar_estatus_familia($familia_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,9) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,10) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['familia_id'] = $familia_id;
      $this->Mgeneral->update_table_row('familia',$data,'familia_id',$data['familia_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus_familia

}
