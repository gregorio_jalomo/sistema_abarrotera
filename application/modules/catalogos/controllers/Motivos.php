<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Motivos extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 28;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista
  
  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,101) || $this->is_admin ){

      $data['titulo_seccion'] = "Motivos";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['motivos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'motivos');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('motivos/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar motivos","catalogos/catalogos/menu");
    }

  }

  public function editar($motivo_id){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,99) || $this->is_admin ){

      $data['titulo_seccion'] = "Editar motivo";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['motivo'] = $this->Mgeneral->get_row('motivo_id',$motivo_id,'motivos');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('motivos/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar motivos","");
    }

  }

  public function guardar_motivo(){
    $this->form_validation->set_rules('motivo_descripcion', 'motivos', 'required');
    $this->form_validation->set_rules('motivo_accion', 'Accion', 'required');

    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['motivo_descripcion'] = $this->input->post('motivo_descripcion');
      $data['motivo_accion'] = $this->input->post('accion');

      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('motivos', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_motivo

  public function editar_motivo(){//update database
    $this->form_validation->set_rules('motivo_descripcion', 'Motivo', 'required');
    $this->form_validation->set_rules('motivo_accion', 'Accion', 'required');

    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['motivo_id'] = $this->input->post('motivo_id');
      $data['motivo_descripcion'] = $this->input->post('motivo_descripcion');
      $data['motivo_accion'] = $this->input->post('motivo_accion');

      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('motivos',$data,'motivo_id',$data['motivo_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_motivo


  public function cambiar_estatus_motivo($motivo_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,99) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,100) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['motivo_id'] = $motivo_id;
      $this->Mgeneral->update_table_row('motivos',$data,'motivo_id',$data['motivo_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus_motivo

  public function get_motivos_by_accion($motivo_accion){
    echo json_encode($this->Mgeneral->get_result("motivo_accion",$motivo_accion,"motivos"));
  }//...get_motivos_by_accion

}
