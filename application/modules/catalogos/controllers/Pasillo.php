<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Pasillo extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 23;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modul,88) || $this->is_admin ){

      $data['titulo_seccion'] = "Pasillo";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['pasillos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'pasillo');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/pasillo/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar pasillo","catalogos/catalogos/menu");
    }

  }//...alta

  public function ver_pasillo($id_pasillo = null){//vista editar
    if(validar_permiso($this->rol_usuario,$this->id_modulo,86) || $this->is_admin ){

      $data['titulo_seccion'] = "Pasillo";
      $data['flecha_ir_atras'] = "catalogos/pasillo/alta";
      $get_row = $this->Mgeneral->get_row('pasillo_id',$id_pasillo,'pasillo');
      $data['pasillos'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/pasillo/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar pasillos","catalogos/pasillo/alta");
    }

  }//...ver_pasillo

  public function guardar_pasillo(){
    $this->form_validation->set_rules('pasillo_descripcion', 'Nombre', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['pasillo_descripcion'] = $this->input->post('pasillo_descripcion');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('pasillo', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_pasillo

  public function editar_pasillo(){
    $this->form_validation->set_rules('pasillo_descripcion', 'pasillo_descripcion', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['pasillo_id'] = $this->input->post('pasillo_id');
      $data['pasillo_descripcion'] = $this->input->post('pasillo_descripcion');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('pasillo',$data,'pasillo_id',$data['pasillo_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_pasillo

  public function cambiar_estatus_pasillo($pasillo_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,86) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,87) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['pasillo_id'] = $pasillo_id;
      $this->Mgeneral->update_table_row('pasillo',$data,'pasillo_id',$data['pasillo_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus_pasillo

}
