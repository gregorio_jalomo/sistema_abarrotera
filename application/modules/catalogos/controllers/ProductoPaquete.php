<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class ProductoPaquete extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 38;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,135) || $this->is_admin ){

      $data['titulo_seccion'] = "Producto Paquete";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['productospaquetes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'detalle_producto');
      $data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/productos_paquetes/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar paquetes","catalogos/catalogos/menu");
    }

  }

  public function guardarDetallePaquete(){
    $this->form_validation->set_rules('producto_id', 'producto_id', 'required');
    $this->form_validation->set_rules('producto_agregado_id', 'producto_agregado_id', 'required');
    $this->form_validation->set_rules('cantidad', 'cantidad', 'required');
    $response = validate($this);
    if($response['status']){
      $data['producto_id'] = $this->input->post('producto_id');
      $data['producto_agregado_id'] = $this->input->post('producto_agregado_id');
      $data['cantidad'] = $this->input->post('cantidad');
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('detalle_producto', $data);
    }
    echo json_encode(array('output' => $response));
  }//...guardarPaquete
  
  public function editar($producto_id = null){//vista editar
    if(validar_permiso($this->rol_usuario,$this->id_modulo,137) || $this->is_admin ){

      $data['titulo_seccion'] = "Producto Paquete";
      $data['flecha_ir_atras'] = "catalogos/productopaquete/alta";
      $data['detalle_producto'] = json_encode($this->Mgeneral->get_result('producto_id',$producto_id,'detalle_producto'));
      $data['detalle_producto_row'] = $this->Mgeneral->get_row('producto_id',$producto_id,'detalle_producto');
      $data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');
      $data['producto_row'] = $this->Mgeneral->get_row('producto_id',$producto_id,'productos');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/productos_paquetes/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar paquetes","catalogos/productopaquete/alta");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('producto_id', 'producto_id', 'required');
    $this->form_validation->set_rules('producto_agregado_id', 'producto_agregado_id', 'required');
    $this->form_validation->set_rules('cantidad', 'cantidad', 'required');
    $response = validate($this);
    if($response['status']){
      $data['producto_id'] = $this->input->post('producto_id');
      $data['producto_agregado_id'] = $this->input->post('producto_agregado_id');
      $data['cantidad'] = $this->input->post('cantidad');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('detalle_producto', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar

  public function ver_detalle($producto_id=false){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,136) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del Producto Paquete";
      $data['flecha_ir_atras'] = "catalogos/productopaquete/alta";
      $data['detalle_producto'] = $this->Mgeneral->get_result('producto_id',$producto_id,'detalle_producto');
      $data['detallesproductos'] = $this->Mgeneral->get_row('producto_id',$producto_id,'detalle_producto');
      $data['estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('catalogos/productos_paquetes/ver_detalle',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar paquetes","catalogos/productopaquete/alta");
    }

  }//...ver_detalle

  public function cambiar_estatus($producto_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,137) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,138) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['producto_id'] = $producto_id;
      $this->Mgeneral->update_table_row('detalle_producto',$data,'producto_id',$data['producto_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus

  function mostrarProductoCostoReal() {
    $producto_id = $this->input->post('producto_id');
    $consulta = $this->Mgeneral->get_row("producto_id",$producto_id,"productos");
    echo json_encode(array('output' => $consulta->producto_costo_real));
  }

  function eliminar() {
    $producto_id = $this->input->post('producto_id');
    $this->Mgeneral->delete_row('detalle_producto','producto_id',$producto_id);
  }
}
?>
