<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class ProveedorCredito extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 11;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    $data['titulo_seccion'] = "Crédito de proveedor";
    $data['flecha_ir_atras'] = "proveedores/catalogos";
    $data['proveedorescredito'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedorescredito');
    $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');

    $archivos_js=array(
      'statics/js/bootbox.min.js',
      'statics/js/general.js?v='.time(),
      'statics/bootstrap4/js/bootstrap.min.js'
    );

    $this->cargar_vista('proveedores/proveedores_credito/alta',$data,$archivos_js);
  }

  public function guardar(){
    $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'required');
    $this->form_validation->set_rules('proveedorcredito_descripcion', 'proveedorcredito_descripcion', 'required');
    $this->form_validation->set_rules('proveedorcredito_limitecredito', 'proveedorcredito_limitecredito', 'required');
    $this->form_validation->set_rules('proveedorcredito_limitedias', 'proveedorcredito_limitedias', 'required');
    $response = validate($this);
    if($response['status']){
      $data['proveedor_id'] = $this->input->post('proveedor_id');
      $data['proveedorcredito_descripcion'] = $this->input->post('proveedorcredito_descripcion');
      $data['proveedorcredito_limitecredito'] = $this->input->post('proveedorcredito_limitecredito');
      $data['proveedorcredito_limitedias'] = $this->input->post('proveedorcredito_limitedias');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('proveedorescredito', $data);
    }
    echo json_encode(array('output' => $response));
  }//...guardar

  public function editar($id_proveedorcredito = null){
    $data['titulo_seccion'] = "Crédito de proveedor";
    $data['flecha_ir_atras'] = "catalogos/proveedorcredito/alta";
    $get_row = $this->Mgeneral->get_row('proveedorcredito_id',$id_proveedorcredito,'proveedorescredito');
    $data['proveedorescredito'] = $get_row;
    $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
    $archivos_js=array(
      'statics/js/bootbox.min.js',
      'statics/js/general.js?v='.time(),
      'statics/bootstrap4/js/bootstrap.min.js'
    );

    $this->cargar_vista('proveedores/proveedores_credito/editar',$data,$archivos_js);
  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'required');
    $this->form_validation->set_rules('proveedorcredito_descripcion', 'proveedorcredito_descripcion', 'required');
    $this->form_validation->set_rules('proveedorcredito_limitecredito', 'proveedorcredito_limitecredito', 'required');
    $this->form_validation->set_rules('proveedorcredito_limitedias', 'proveedorcredito_limitedias', 'required');
    $response = validate($this);
    if($response['status']){
      $data['proveedorcredito_id'] = $this->input->post('proveedorcredito_id');
      $data['proveedor_id'] = $this->input->post('proveedor_id');
      $data['proveedorcredito_descripcion'] = $this->input->post('proveedorcredito_descripcion');
      $data['proveedorcredito_limitecredito'] = $this->input->post('proveedorcredito_limitecredito');
      $data['proveedorcredito_limitedias'] = $this->input->post('proveedorcredito_limitedias');
      //...col-md-10
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('proveedorescredito',$data,'proveedorcredito_id',$data['proveedorcredito_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar

  public function cambiar_estatus($proveedorcredito_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,51) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,54) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['proveedorcredito_id'] = $proveedorcredito_id;
      $this->Mgeneral->update_table_row('proveedorescredito',$data,'proveedorcredito_id',$data['proveedorcredito_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus
}
?>
