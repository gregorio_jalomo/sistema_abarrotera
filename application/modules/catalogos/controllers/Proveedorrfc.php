<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Proveedorrfc extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 11;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,53) || $this->is_admin ){

      $data['titulo_seccion'] = "RFC";
      $data['flecha_ir_atras'] = "proveedores/catalogos";
      $data['proveedoresrfc'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedoresrfc');
      $contenido = $this->load->view('proveedores/proveedores_rfc/alta', $data, TRUE);
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedores/proveedores_rfc/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar RFC(proveedor)","proveedores/catalogos");
    }

  }//...alta

  public function guardar(){
    $this->form_validation->set_rules('proveedorrfc_rfc', 'RFC', 'required');
    $this->form_validation->set_rules('proveedorrfc_correo', 'Correo', 'required');

    $response = validate($this);
    if($response['status']){
      $data['proveedorrfc_rfc'] = $this->input->post('proveedorrfc_rfc');
      $data['proveedorrfc_correo'] = $this->input->post('proveedorrfc_correo');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('proveedoresrfc', $data);
    }
    echo json_encode(array('output' => $response));
  }//...guardar

  public function editar($id_proveedorrfc = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,51) || $this->is_admin ){

      $data['titulo_seccion'] = "RFC";
      $data['flecha_ir_atras'] = "catalogos/proveedorrfc/alta";
      $get_row = $this->Mgeneral->get_row('proveedorrfc_id',$id_proveedorrfc,'proveedoresrfc');
      $data['proveedoresrfc'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedores/proveedores_rfc/editar',$data,$archivos_js);     
    
    }else{
      $this->p_denied_view("editar RFC(proveedor)","catalogos/proveedorrfc/alta");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('proveedorrfc_rfc', 'RFC', 'required');
    $this->form_validation->set_rules('proveedorrfc_correo', 'Correo', 'required');

    $response = validate($this);
    if($response['status']){
      $data['proveedorrfc_id'] = $this->input->post('proveedorrfc_id');
      $data['proveedorrfc_rfc'] = $this->input->post('proveedorrfc_rfc');
      $data['proveedorrfc_correo'] = $this->input->post('proveedorrfc_correo');

      //...col-md-10
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('proveedoresrfc',$data,'proveedorrfc_id',$data['proveedorrfc_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar

  public function cambiar_estatus($proveedorrfc_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,51) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,54) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['proveedorrfc_id'] = $proveedorrfc_id;
      $this->Mgeneral->update_table_row('proveedoresrfc',$data,'proveedorrfc_id',$data['proveedorrfc_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus
}
?>
