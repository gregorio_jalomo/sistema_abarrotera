<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Tipodeprecio extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 25;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,96) || $this->is_admin ){

      $data['titulo_seccion'] = "Tipos de Precio";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['tipodeprecios'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('tipodeprecio/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar tipos de precio","catalogos/catalogos/menu");
    }
    
  }//...alta

  public function ver_tipodeprecio($id_tipodeprecio = null){//vista editar
    if(validar_permiso($this->rol_usuario,$this->id_modulo,94) || $this->is_admin ){

      $data['titulo_seccion'] = "Tipo de Precio";
      $data['flecha_ir_atras'] = "catalogos/tipodeprecio/alta";
      $get_row = $this->Mgeneral->get_row('tipodeprecio_id',$id_tipodeprecio,'tipodeprecio');
      $data['tipodeprecios'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('tipodeprecio/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar tipos de precio","catalogos/tipodeprecio/alta");
    }

  }//...ver_tipodeprecio

  public function guardar_tipodeprecio(){
    $this->form_validation->set_rules('tipodeprecio_descripcion', 'Nombre', 'required');
      $this->form_validation->set_rules('tipodeprecio_porcentaje', 'Porcentaje', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['tipodeprecio_descripcion'] = $this->input->post('tipodeprecio_descripcion');
      $data['tipodeprecio_porcentaje'] = $this->input->post('tipodeprecio_porcentaje');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('tipodeprecio', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_tipodeprecio

  public function editar_tipodeprecio(){
    $this->form_validation->set_rules('tipodeprecio_descripcion', 'tipodeprecio_descripcion', 'required');
    $this->form_validation->set_rules('tipodeprecio_porcentaje', 'tipodeprecio_porcentaje', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['tipodeprecio_id'] = $this->input->post('tipodeprecio_id');
      $data['tipodeprecio_descripcion'] = $this->input->post('tipodeprecio_descripcion');
      $data['tipodeprecio_porcentaje'] = $this->input->post('tipodeprecio_porcentaje');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('tipodeprecio',$data,'tipodeprecio_id',$data['tipodeprecio_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_tipodeprecio

  public function cambiar_estatus_tipodeprecio($tipodeprecio_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,94) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,97) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['tipodeprecio_id'] = $tipodeprecio_id;
      $this->Mgeneral->update_table_row('tipodeprecio',$data,'tipodeprecio_id',$data['tipodeprecio_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus_tipodeprecio

}
