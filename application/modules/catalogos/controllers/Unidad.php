<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Unidad extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 17;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }


  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,78) || $this->is_admin ){

      $data['titulo_seccion'] = "Unidades";
      $data['flecha_ir_atras'] = "catalogos/catalogos/menu";
      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('unidad/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar unidades","catalogos/catalogos/menu");
    }

  }//...alta

  public function ver_unidad($id_unidad = null){//vista editar
    if(validar_permiso($this->rol_usuario,$this->id_modulo,74) || $this->is_admin ){

      $data['titulo_seccion'] = "Unidades";
      $data['flecha_ir_atras'] = "catalogos/unidad/alta";
      $get_row = $this->Mgeneral->get_row('unidad_id',$id_unidad,'unidad');
      $data['unidades'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('unidad/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar unidades","");
    }

}//...ver_unidad

  public function guardar_unidad(){
    $this->form_validation->set_rules('unidad_tipo', 'Nombre', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['unidad_tipo'] = $this->input->post('unidad_tipo');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('unidad', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_unidad

  public function editar_unidad(){
    $this->form_validation->set_rules('unidad_tipo', 'unidad_tipo', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['unidad_id'] = $this->input->post('unidad_id');
      $data['unidad_tipo'] = $this->input->post('unidad_tipo');
      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('unidad',$data,'unidad_id',$data['unidad_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_unidad

  public function cambiar_estatus_unidad($unidad_id, $estatus_id) {
    
    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,74) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,75) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['unidad_id'] = $unidad_id;
      $this->Mgeneral->update_table_row('unidad',$data,'unidad_id',$data['unidad_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus_unidad

}
