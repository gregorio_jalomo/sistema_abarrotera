<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Vehiculos extends MX_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 43;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,160) || $this->is_admin ){

      $data['titulo_seccion'] = "Agregar vehiculo";
      $data['flecha_ir_atras'] = "catalogos/Catalogos/menu";

      $data['vehiculos']=$this->Mgeneral->get_table("vehiculos");

      $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
      $this->cargar_vista('vehiculos/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar vehiculos","catalogos/Catalogos/menu");
    }

  }//...alta

  public function ver_vehiculo($id_vehiculo = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,161) || $this->is_admin ){

      $data['titulo_seccion'] = "Editar vehiculo";
      $data['flecha_ir_atras'] = "catalogos/vehiculos/alta";

      $data['vehiculo']=$this->Mgeneral->get_row("vehiculos_id",$id_vehiculo,"vehiculos");

      $contenido = $this->load->view('vehiculos/editar', $data, TRUE);
        $archivos_js=array(
            'statics/js/bootbox.min.js',
            'statics/js/general.js?v='.time(),
            'statics/bootstrap4/js/bootstrap.min.js'
          );
      
        $this->cargar_vista('vehiculos/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar vehiculos","catalogos/vehiculos/alta");
    }

  }//...ver_vehiculo

  public function guardar_vehiculo(){
    $this->form_validation->set_rules('vehiculos_marca', 'Nombre', 'required');
    $this->form_validation->set_rules('vehiculos_linea', 'Celular', 'required');
    $this->form_validation->set_rules('vehiculos_modelo', 'Licencia', 'required');

    $response = validate($this);
    if($response['status']){
      $data['vehiculos_marca']      = $this->input->post('vehiculos_marca');
      $data['vehiculos_linea']     = $this->input->post('vehiculos_linea');
      $data['vehiculos_modelo']    = $this->input->post('vehiculos_modelo');
      $data['vehiculos_caracteristicas']    = $this->input->post('vehiculos_caracteristicas');

      //$data["fecha"]                = date("Y-m-d H:i:s");
      //$data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('vehiculos', $data);
    }

    echo json_encode(array('output' => $response));
  }//...guardar_vehiculo

  public function editar_vehiculo(){
    $this->form_validation->set_rules('vehiculos_marca', 'Nombre', 'required');
    $this->form_validation->set_rules('vehiculos_linea', 'Celular', 'required');
    $this->form_validation->set_rules('vehiculos_modelo', 'Licencia', 'required');

    $response = validate($this);
    if($response['status']){

        $vehiculos_id = $this->input->post('vehiculos_id');
        $data['vehiculos_marca'] = $this->input->post('vehiculos_marca');
        $data['vehiculos_linea'] = $this->input->post('vehiculos_linea');
        $data['vehiculos_modelo'] = $this->input->post('vehiculos_modelo');
        $data['vehiculos_caracteristicas']    = $this->input->post('vehiculos_caracteristicas');
        
        //$data['cat_estatus_id'] = 1;
        $this->Mgeneral->update_table_row('vehiculos',$data,'vehiculos_id',$vehiculos_id);
    }

    echo json_encode(array('output' => $response));

  }//...editar_vehiculo

}
