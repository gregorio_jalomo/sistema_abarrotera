<?php
/**

 **/
class Clientelugar_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }
    public function get_lugares($cliente_id=null,$lugar_id=null){
        /* "SELECT `lugares`.*, `clientes`.`cliente_nombre`
            FROM `lugares`
            JOIN `clientes` ON `lugares`.`cliente_id`=`clientes`.`cliente_id`" 
        */
        
        $db=$this->db;
        $db->select("lugares.*,clientes.cliente_nombre");
        $db->join("clientes","lugares.cliente_id=clientes.cliente_id");
        if(isset($lugar_id)){
            $db->where("lugar_id",$lugar_id);
        }
        if(isset($cliente_id)){
            $db->where("lugares.cliente_id",$cliente_id);
        }
        $lugares=$db->get("lugares")->result();

        if(isset($cliente_id)){
            array_push($lugares,$this->get_lugar_from_clientes($cliente_id));
        }
        return $lugares;
    }
    public function get_lugar_by_id($lugar_id){
        $lugar=$this->get_lugares(null,$lugar_id);
        return $lugar[0];
    }
    public function get_lugar_from_clientes($cliente_id){
        $lugar=$this->Mgeneral->get_row("cliente_id",$cliente_id,"clientes");
        return $lugar;
    }
}//...class
