<?php
/**

 **/
class Foliodocumento extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function consulta_folio_prefijo($prefijo){
        $query="SELECT foliosdocumentos.*, prefijos.prefijo_nombre, prefijos.prefijo_id
        FROM foliosdocumentos
        INNER JOIN prefijos ON foliosdocumentos.prefijo_id = prefijos.prefijo_id
        WHERE foliosdocumentos.foliodocumento_fechainicio <= CURRENT_TIMESTAMP
        AND foliosdocumentos.foliodocumento_fechafinal >= CURRENT_TIMESTAMP
        AND prefijos.prefijo_nombre = ('$prefijo')
        AND foliosdocumentos.cat_estatus_id = ('1')
        ORDER by foliosdocumentos.foliodocumento_folio DESC";
        $folio= $this->db->query($query)->row();
        return $folio;
    }
    public function nuevo_foliodocumento($prefijo_nombre,$FUNI){
        //echo json_encode($this->db->last_query());
        //echo "||||".$foliodocumento_row;
        if($FUNI != ""){//Si se recibe un FUNI de parametro, el mismo se asigna al documento saliente
            $FUNI_row=(object)array("foliodocumento_folio"=>$FUNI,"foliodocumento_id"=>false);
        }else{//Si NO se recibe un FUNI de parametro se obtiene el consecutivo de base de datos
            $FUNI_row=$this->consulta_folio_prefijo("FUNI"); 
        }
        
        if($FUNI_row != NULL){
            if($FUNI != ""){//Si se recibe un FUNI de parametro se asigna al funi del documento saliente
                $FUNI_final=$FUNI;
            }else{//Si NO se recibe un FUNI de parametro se obtiene el consecutivo de base de datos
                $folioFUNI_folio=$FUNI_row->foliodocumento_folio;
                $folioFUNI_folio++;
                $zeroFUNI="";
                    /* if($foliodoFUNI_folio <10){$zeroFUNI="00";}
                    if($foliodoFUNI_folio > 10 && $foliodoFUNI_folio < 100){$zeroFUNI="0";} */
                $valueFUNI=strval($folioFUNI_folio);
                $folioFUNI_folio = "$zeroFUNI$valueFUNI";
                $FUNI_final=$FUNI_row->prefijo_nombre.$FUNI_row->foliodocumento_ano.$folioFUNI_folio;
            }
            $foliodocumento_row=$this->consulta_folio_prefijo($prefijo_nombre);
            if($foliodocumento_row != NULL){
                $foliodocumento_folio=$foliodocumento_row->foliodocumento_folio;
                $foliodocumento_folio++;
                $zero="";
                /* if($foliodocumento_folio <10){$zero="00";}
                if($foliodocumento_folio > 10 && $foliodocumento_folio < 100){$zero="0";} */
                $value=strval($foliodocumento_folio);
                $foliodocumento_folio = "$zero$value";
                $foliodocumento["folio_string"]=$foliodocumento_row->prefijo_nombre.$foliodocumento_row->foliodocumento_ano.$foliodocumento_folio;
                $foliodocumento["funi"]=$FUNI_final;
                $foliodocumento["id_foliosdocumentos"]=$foliodocumento_row->foliodocumento_id;
                $foliodocumento["id_FUNI_foliosdocumentos"]=$FUNI_row->foliodocumento_id;
                $foliodocumento["errorPrefijo"]="FALSE";
                $foliodocumento["errorFUNI"]="FALSE";

            }else{
                $foliodocumento["errorFUNI"]="FALSE";
                $foliodocumento["errorPrefijo"]="TRUE";
            }//...if foliodocumento_row

        }else{
            $foliodocumento["errorFUNI"]="TRUE";
        }//...if FUNI_row
  

return $foliodocumento;
    }


}
