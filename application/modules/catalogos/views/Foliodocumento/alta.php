<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_foliodocumento').submit(function(event){
    event.preventDefault();
    console.log($('#foliodocumento_folio').val());
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/foliodocumento/guardar";
    ajaxJson(url,
      {
        "foliodocumento_id" : $('#foliodocumento_id').val(),
        "foliodocumento_folio" : $('#foliodocumento_folio').val(),
        "foliodocumento_ano" : $('#foliodocumento_ano').val(),
        "foliodocumento_fechainicio" : $('#foliodocumento_fechainicio').val(),
        "foliodocumento_fechafinal" : $('#foliodocumento_fechafinal').val(),
        "prefijo_id" : $('#prefijo_id').val(),
        "prefijo_nombre" : $('#prefijo_nombre').val()


      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/foliodocumento/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });

                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Folio Documento</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_foliodocumento">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-md-4">
                  <label for="">Prefijo</label>
                  <select class="form-control select2" name="prefijo_id" id="prefijo_id">
                    <option selected disabled>-  -</option>
                    <?php
                      foreach ($prefijos as $row) {
                        echo
                          '<option value="'.$row->prefijo_id.'">'.$row->prefijo_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="foliodocumento_ano">Año</label>
                  <input type="number" class="form-control mr-sm-2" id="foliodocumento_ano">
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="foliodocumento_folio">Folio</label>
                  <input type="number" class="form-control mr-sm-2" id="foliodocumento_folio">
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="foliodocumento_fechainicio">Fecha Inicio</label>
                  <input type="datetime-local" class="form-control mr-sm-2" id="foliodocumento_fechainicio">
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="foliodocumento_fechafinal">Fecha Final</label>
                  <input type="datetime-local" class="form-control mr-sm-2" id="foliodocumento_fechafinal">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Folios Documentos Agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Prefijo</th>
                  <th>Año</th>
                  <th>Folio</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($foliosdocumentos as $row ) {
                      $foliodocumento_id = $row->foliodocumento_id;
                      $foliodocumento_folio = $row->foliodocumento_folio;
                      $foliodocumento_ano = $row->foliodocumento_ano;
                      $foliodocumento_fechainicio = $row->foliodocumento_fechainicio;
                      $foliodocumento_fechafinal = $row->foliodocumento_fechafinal;
                      $prefijo_id = $row->prefijo_id;

                      $datos_prefijo = $this->Mgeneral->get_row('prefijo_id',$prefijo_id,'prefijos');
                      $prefijo_nombre = $datos_prefijo->prefijo_nombre;



                      echo '
                        <tr id="borrar_'.$foliodocumento_id.'">
                          <td>'.$prefijo_nombre.'</td>
                          <td>'.$foliodocumento_ano.'</td>
                          <td>'.$foliodocumento_folio.'</td>
                          <td>'.$foliodocumento_fechainicio.'</td>
                          <td>'.$foliodocumento_fechafinal.'</td>

                          <td>
                            <a href="'.base_url().'index.php/catalogos/foliodocumento/editar/'.$foliodocumento_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$foliodocumento_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/foliodocumento/cambiar_estatus/'.$foliodocumento_id.'/3" class="eliminar_relacion" flag="'.$foliodocumento_id.'" id="delete'.$foliodocumento_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
