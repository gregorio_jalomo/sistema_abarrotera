<?php
  $foliodocumento_id = $foliosdocumentos->foliodocumento_id;
  $foliodocumento_folio = $foliosdocumentos->foliodocumento_folio;
  $foliodocumento_ano = $foliosdocumentos->foliodocumento_ano;
  $foliodocumento_fechainicio = $foliosdocumentos->foliodocumento_fechainicio;
  $foliodocumento_fechafinal = $foliosdocumentos->foliodocumento_fechafinal;
  $prefijo_id = $foliosdocumentos->prefijo_id;

?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_foliodocumento').submit(function(event){
    event.preventDefault();
    console.log($('#foliodocumento_folio').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/foliodocumento/actualizar";
    ajaxJson(url,{
        'foliodocumento_id' : '<?php echo $foliodocumento_id; ?>',
        'foliodocumento_folio' : $('#foliodocumento_folio').val(),
        'foliodocumento_ano' : $('#foliodocumento_ano').val(),
        'foliodocumento_fechainicio' : $('#foliodocumento_fechainicio').val(),
        'foliodocumento_fechafinal' : $('#foliodocumento_fechafinal').val(),


        'prefijo_id' : $('#prefijo_id').val()


      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/foliodocumento/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Folio Documentos</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_foliodocumento">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-3">
                  <label for="">Prefijo</label>
                  <select class="form-control select2" name="prefijo_id" id="prefijo_id">


                    <?php
                      foreach ($prefijos as $row) {
                        echo
                          '<option value="'.$row->prefijo_id.'">'.$row->prefijo_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="">Año</label>
                  <input type="number" class="form-control" name="foliodocumento_ano" id="foliodocumento_ano" value="<?php echo $foliodocumento_ano; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Folio</label>
                  <input type="text" class="form-control" name="foliodocumento_folio" id="foliodocumento_folio" value="<?php echo $foliodocumento_folio; ?>">
                </div>

                <div class="col-md-4">
                  <label for="">Fecha Inicio</label>
                  <input type="text" class="form-control" name="foliodocumento_fechainicio" id="foliodocumento_fechainicio" value="<?php echo $foliodocumento_fechainicio; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Fecha Final</label>
                  <input type="text" class="form-control" name="foliodocumento_fechafinal" id="foliodocumento_fechafinal" value="<?php echo $foliodocumento_fechafinal; ?>">
                </div>
              </div>



              </div>



              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/catalogos/foliodocumento/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>


            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
