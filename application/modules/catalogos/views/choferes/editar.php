<?php
  $chofer_id = $chofer->choferes_id;
  $nombre = $chofer->choferes_nombre;
  $celular = $chofer->choferes_celular;
  $licencia = $chofer->choferes_licencia;

?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar chofer</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_chofer">
            <div class="card-body">
              <div class="form-row align-items-center">

                <div class="col-md-1">
                  <label class="mr-sm-2" for="choferes_nombre">Nombre</label>
                </div>
                <div class="col-2 my-1">
                  <input type="text" class="form-control mr-sm-2" id="choferes_nombre" value="<?=$nombre?>">
                </div>
                <div class="col-md-1">
                  <label class="mr-sm-2" for="choferes_celular">Celular</label>
                </div>
                <div class="col-2 my-1">
                  <input type="text" class="form-control mr-sm-2" maxlength="10" id="choferes_celular" value="<?=$celular?>">
                </div>
                <div class="col-md-1">
                  <label class="mr-sm-2" for="choferes_licencia">Licencia</label>
                </div>
                <div class="col-2 my-1">
                  <input type="text" class="form-control mr-sm-2" id="choferes_licencia" value="<?=$licencia?>">
                </div>

                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                  <a href="<?php echo base_url().'index.php/catalogos/chofer/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>

  $(document).ready(function(){
  $('#menuCatalogos').addClass('active-link');
  $("#cargando").hide();
    $('#editar_chofer').submit(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $("#cargando").show();
      var url ="<?php echo base_url()?>index.php/catalogos/choferes/editar_chofer";
      ajaxJson(url,
        {
          "choferes_id" : '<?=$chofer_id; ?>',
          "choferes_nombre" : $('#choferes_nombre').val(),
          "choferes_celular" : $('#choferes_celular').val(),
          "choferes_licencia" : $('#choferes_licencia').val()
        },
        "POST","",function(result){

        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        if(obj_status == false){
          aux = "";
          $.each( obj_output.errors, function( key, value ) {
            aux +=value+"<br/>";
          });
          exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
          $("#enviar").show();
          $("#cargando").hide();
        }
        if(obj_status == true){
          exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/choferes/alta");
          $("#enviar").show();
          $("#cargando").hide();
        }
      });
    });
  });
</script>
