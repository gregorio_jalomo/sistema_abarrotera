<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Cliente</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_cliente">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-8">
                  <label for="">Nombre Comercial</label>
                  <select name="cliente_id" id="cliente_id" class="select2">
                    <?php
                      foreach($clientes as $cliente){
                        echo "<option value='".$cliente->cliente_id."'>".$cliente->cliente_nombre."</option>";

                      }
                    ?>
                  </select>
                </div>


              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Calle</label>
                  <input type="text" class="form-control" name="cliente_calle" id="cliente_calle">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. Interior</label>
                  <input type="text" class="form-control" name="cliente_no_interior" id="cliente_no_interior">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. exterior</label>
                  <input type="number" class="form-control" name="cliente_no_exterior" id="cliente_no_exterior">
                </div>
                <div class="col-md-4">
                  <label for="">Colonia</label>
                  <input type="text" class="form-control" name="cliente_colonia" id="cliente_colonia">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-2">
                  <label for="">Código Postal</label>
                  <input type="text" class="form-control" name="cliente_cp" id="cliente_cp">
                </div>
                <div class="col-md-5">
                  <label for="">Ciudad</label>
                  <input type="text" class="form-control" name="cliente_ciudad" id="cliente_ciudad">
                </div>
                <div class="col-md-5">
                  <label for="">Municipio</label>
                  <input type="text" class="form-control" name="cliente_municipio" id="cliente_municipio">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Estado</label>
                  <input type="text" class="form-control" name="cliente_estado" id="cliente_estado">
                </div>
                <div class="col-md-4">
                  <label for="">País</label>
                  <input type="text" class="form-control" name="cliente_pais" id="cliente_pais">
                </div>

              </div>

              <div class="row">
                  <input id="cliente_latitud" name="cliente_latitud" type="hidden" class="form-control cc-exp form-control-sm" >
                  <input id="cliente_longitud" name="cliente_longitud" type="hidden" class="form-control cc-exp form-control-sm" >
                  <div class="col-12">
                    <div class="form-group">
                      <label for="">Ubicación</label>
                      <div id="mapa-direccion" style="height: 350px; "></div>
                      <img class="hidden" id="imagen_mapa" src="<?php echo base_url();?>ubicacion.png" style="visibility: hidden;" />
                    </div>
                  </div>
              </div>

              <div align="right">
                <div id="alta_cliente_lugar" class="btn btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                </div>

                  <div  id="enviando" class="btn btn-primary btn-lg cargando"><i class="fa fa-spinner fa-spin"></i> Enviando</div>
                  <a href="<?php echo base_url().'index.php/caatalogos/clientelugar/listar_lugares'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Lugares agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre Comercial</th>
                  <th>Direccion</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($lugares as $row ) {
                      $cliente_id = $row->cliente_id;
                      $cliente_nombre = $row->cliente_nombre;
                      $cliente_direccion="calle ".$row->cliente_calle." int.".$row->cliente_no_interior.", ext.".$row->cliente_no_exterior.", colonia ".$row->cliente_colonia;
                      echo '
                        <tr id="borrar_'.$cliente_id.'">
                          <td>'.$cliente_nombre.'</td>
                          <td>'.$cliente_direccion.'</td>
                          <td>
                            <a href="'.base_url().'index.php/catalogos/clientelugar/editar/'.$cliente_id.'" class="btn btn-primary btn-sm" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <div class="delete_lugar btn btn-danger btn-sm" data-content="Eliminar" data-cliente_id='.$cliente_id.'><i class="fas fa-trash-alt"></i> Eliminar</div>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $(document).ready(function(){
    $('#menuClientes').addClass('active-link');
    $(".cargando").hide();

    $(".delete_lugar").click(function(event){
        event.preventDefault();
        var cliente_id=$(this).data("cliente_id");
        bootbox.dialog({
        message: "Desea eliminar el registro?",
        closeButton: true,
        buttons:
          {
            "danger":
                      {
                        "label": "<i class='icon-remove'></i>Eliminar ",
                        "className": "btn-danger",
                        "callback": function () {
                        url = base_url+"index.php/clientes/cambiar_estatus/"+cliente_id+"/3";
                        $("#borrar_"+cliente_id).slideUp();
                          $.get(url);
                        }
                        },
                          "cancel":
                          {
                              "label": "<i class='icon-remove'></i> Cancelar",
                              "className": "btn-sm btn-info",
                              "callback": ""
                          }

                      }
                  });
    });

    $('#alta_cliente_lugar').click(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $(".cargando").show();
      var url ="<?php echo base_url()?>index.php/catalogos/clientelugar/guardar";
      ajaxJson(url,{
          'cliente_id' : $('#cliente_id').val(),
          'cliente_calle' : $('#cliente_calle').val(),
          'cliente_no_interior' : $('#cliente_no_interior').val(),
          'cliente_no_exterior' : $('#cliente_no_exterior').val(),
          'cliente_colonia' : $('#cliente_colonia').val(),
          'cliente_cp' : $('#cliente_cp').val(),
          'cliente_ciudad' : $('#cliente_ciudad').val(),
          'cliente_municipio' : $('#cliente_municipio').val(),
          'cliente_estado' : $('#cliente_estado').val(),
          'cliente_pais' : $('#cliente_pais').val(),
          'clientecredito_id' : $('#clientecredito_id').val(),
          'cliente_latitud' : $('#cliente_latitud').val(),
          'cliente_longitud' : $('#cliente_longitud').val()
          },
          "POST","",function(result){
              json_response = JSON.parse(result);
              obj_output = json_response.output;
              obj_status = obj_output.status;
              if(obj_status === false){
                aux = "";
                $.each( obj_output.errors, function( key, value ) {
                  aux +=value+"<br/>";
                });
                exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger auto_close");
                $("#enviar").show();
                $(".cargando").hide();
              }else{
                exito_redirect("DATOS GUARDADOS CON EXITO","success auto_close","<?php echo base_url()?>index.php/catalogos/clientelugar/alta");
                $("#enviar").show();
                $(".cargando").hide();
              }
            }//... callback function
        );//... ajaxJson
    });//... alta_clientelugar.click

  });//... document_ready

</script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAHHWSkUcqyeERY0E2KCxx_Fggbo1BZN5I"></script>
<script type="text/javascript">
  var geoCode = new google.maps.Geocoder();
  function initialize() {

    <?php //if($negocios->negociocliente_Latitud == '' && $negocios->negociocliente_Longitud == ''): ?>
            var latLng = new google.maps.LatLng(19.264146,-103.718579);
    <?php //else: ?>
            //var latLng = new google.maps.LatLng('','');
    <?php //endif; ?>
    var markerImage = new google.maps.MarkerImage($("#imagen_mapa").attr('src'),
                                                          new google.maps.Size(34, 32),
                                                          new google.maps.Point(0, 0),
                                                          new google.maps.Point(0, 32));
    var map = new google.maps.Map(document.getElementById('mapa-direccion'),{
                zoom: 15,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
    var marker = new google.maps.Marker({
                                        position: latLng,
                                        title: 'Seleccione su ubicacion',
                                        map: map,
                                        icon: markerImage,
                                        draggable: true
                                      });

      //ACTUALIZAR LA INFORMACION DE LA POSICION ACTUAL
      updateMarkerPosition(latLng);
      geocodePosition(latLng);

      //AGREGAR EL EVENTO DE ARRASTRAR LOS DATOS
      google.maps.event.addListener(marker, 'dragstart', function(){
          updateMarkerAddress('...');
      });

      google.maps.event.addListener(marker, 'drag', function(){
          updateMarkerStatus('.....');
          updateMarkerPosition(marker.getPosition());
      });

      google.maps.event.addListener(marker, 'dragend', function(){
          updateMarkerStatus('....');
          geocodePosition(marker.getPosition());
      });
  }
    /**
     * METODO QUE SE USA PARA EL CODIGO
     * DE LA POSICION
     **/
    function geocodePosition(pos){
      geoCode.geocode({
        latLng: pos
        }, function(responses){
            if(responses && responses.lenght > 0){
              updateMarkerAddress(responses[0].formatted_address);
            } else {
              updateMarkerAddress('');
            }
          }
      );
    }
    /**
     * ACTUALIZAR EL DATO DE LOS MARKERS
     * EN EL GOOGLE MAPS
     **/
    function updateMarkerStatus(str){
        //document.getElementById('markerStatus').innerHTML = str;
    }

    /**
     * ACTUALIZA LA POSICION DEL MARCADOR PARA
     * QUE CAPTURAR SU cliente_LATITUD Y cliente_LONGITUD
     **/
    function updateMarkerPosition(latLng){
        var uno = latLng.lng();
        var dos = latLng.lat();
        document.getElementById('cliente_longitud').value = uno;
        document.getElementById('cliente_latitud').value = dos;
       // alert('hola: ' + latLng.lng())
        /*document.getElementById('info').innerHTML = [
            latLng.lat(),
            latLng.lng()
            ].join(', ');*/
    }

    /**
     * ACTUALIZA LA DIRECCION DEL MARCADOR PARA
     * MOSTRARLA LA USUARIO
     **/
    function updateMarkerAddress(str){
        //document.getElementById('address').innerHTML = str;
    }

    //EVENTO ONLOAD PARA ACTIVAR LA APLICACION
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
