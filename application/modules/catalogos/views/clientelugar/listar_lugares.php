<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Lugares de clientes</h3>
    </div>
    <div class="col-md-6">
      <a href="<?php echo base_url().'index.php/catalogos/clientelugar/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar lugares</button></a>

    </div>

    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
      <tr>
        <th>Nombre Comecial</th>
        <th>Direccion</th>
        <th>Fecha de Registro</th>
        <th>Opciones</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($lugares as $lugar) {
        $lugar_id = $lugar->lugar_id;
        $cliente_id = $lugar->cliente_id;
        $cliente_nombre = $lugar->cliente_nombre;
        $cliente_calle = $lugar->cliente_calle;
        $cliente_no_interior = $lugar->cliente_no_interior;
        $cliente_no_exterior = $lugar->cliente_no_exterior;
        $cliente_colonia = $lugar->cliente_colonia;
        $cliente_cp = $lugar->cliente_cp;
        $cliente_ciudad = $lugar->cliente_ciudad;
        $cliente_municipio = $lugar->cliente_municipio;
        $cliente_estado = $lugar->cliente_estado;
        $cliente_pais = $lugar->cliente_pais;
        $clientecredito_id = $lugar->clientecredito_id;
        $cliente_latitud = $lugar->cliente_latitud;
        $cliente_longitud = $lugar->cliente_longitud;
        $fecha_registro = $lugar->fecha_registro;
        $cat_estatus_id = $lugar->cat_estatus_id;
        $cliente_direccion="calle ".$lugar->cliente_calle." int.".$lugar->cliente_no_interior.", ext.".$lugar->cliente_no_exterior.", colonia ".$lugar->cliente_colonia;

        echo
        '<tr id="borrar_'.$lugar_id.'">
          <td>'.$cliente_nombre.'</td>
          <td>'.$cliente_direccion.'</td>
          <td>'.$fecha_registro.'</td>
          <td>
          <a href="'.base_url().'index.php/catalogos/clientelugar/ver_detalle/'.$lugar_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
            <a href="'.base_url().'index.php/catalogos/clientelugar/editar/'.$lugar_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>

              <a href="'.base_url().'index.php/catalogos/clientelugar/cambiar_estatus/'.$lugar_id.'/3" class="eliminar_relacion" flag="'.$lugar_id.'" id="delete'.$lugar_id.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>
<script>
  $(document).ready(function(){

    $('#menuClientes').addClass('active-link');
    $('[data-toggle="popover"]').popover();

    $(".eliminar_relacion").click(function(event){
        event.preventDefault();
        bootbox.dialog({
        message: "Desea eliminar el registro?",
        closeButton: true,
        buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
      });
  });

  function estatuscliente(cliente_id, cat_estatus_id) {
    $.post('<?php echo base_url()."index.php/clientes/cambiar_estatus_cliente"; ?>',{
      cliente_id : cliente_id,
      cat_estatus_id : cat_estatus_id
      }, function(data){}
    );
  }
</script>
