<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_clientecorreo').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/clientecorreo/guardar";
    ajaxJson(url,
      {
        "cliente_id" : $('#cliente_id').val(),
        "cliente_correo" : $('#cliente_correo').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/clientecorreo/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Correo de Cliente</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_clientecorreo">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $cliente) {
                        echo
                          '<option value="'.$cliente->cliente_id.'">'.$cliente->cliente_nombre.' '.$cliente->cliente_a_paterno.' '.$cliente->cliente_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="cliente_correo">Correo</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-at"></i></span>
                    </div>
                    <input type="email" class="form-control" name="cliente_correo" id="cliente_correo">
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Correos de Cliente agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Cliente</th>
                  <th>Correo</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($clientecorreo as $row ) {
                      $clientecorreo_id = $row->clientecorreo_id;
                      $cliente_id = $row->cliente_id;
                      $cliente_correo = $row->cliente_correo;
                      $datos_cliente = $this->Mgeneral->get_row('cliente_id',$cliente_id,'clientes');
                      $cliente_nombre = $datos_cliente->cliente_nombre;

                      echo '
                        <tr id="borrar_'.$clientecorreo_id.'">
                          <td>'.$cliente_id.'</td>
                          <td>'.$cliente_nombre.' </td>
                          <td>'.$cliente_correo.'</td>
                          <td>
                            <a href="'.base_url().'index.php/catalogos/clientecorreo/editar/'.$clientecorreo_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$clientecorreo_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/clientecorreo/cambiar_estatus/'.$clientecorreo_id.'/3" class="eliminar_relacion" flag="'.$clientecorreo_id.'" id="delete'.$clientecorreo_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');
  });
</script>
