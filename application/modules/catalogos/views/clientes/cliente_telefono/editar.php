<?php
  $clientetelefono_id = $clientetelefono->clientetelefono_id;
  $cliente_id = $clientetelefono->cliente_id;
  $cliente_telefono = $clientetelefono->cliente_telefono;
?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_clientetelefono').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/clientetelefono/actualizar";
    ajaxJson(url,
      {
        "clientetelefono_id" : '<?php echo $clientetelefono_id; ?>',
        "cliente_id" : $('#cliente_id').val(),
        "cliente_telefono" : $('#cliente_telefono').val().replace(/-|_/g," ")
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/clientetelefono/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                        $.get(url);
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Teléfono de Cliente</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_clientetelefono">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $row) {
                        echo
                          '<option value="'.$row->cliente_id.'">'.$row->cliente_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-3 my-1">
                  <label class="mr-sm-2" for="cliente_telefono">Teléfono</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [99999]', '+099 99 99 9999[9]-9999']" data-mask="" im-insert="true"  id="cliente_telefono" value="<?php echo $cliente_telefono; ?>">
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/clientetelefono/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
$(document).ready(function() {
  $('#menuClientes').addClass('active-link');
  $('#cliente_id').val('<?= $cliente_id; ?>');
});
</script>
