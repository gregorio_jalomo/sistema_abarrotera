<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_clientecondicion').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/ClienteCondicion/guardar";
    ajaxJson(url,
      {
        "cliente_id" : $('#cliente_id').val(),
        "tipodeprecio_id" : $('#tipodeprecio_id').val(),
        "metodopago_id" : $('#metodopago_id').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/ClienteCondicion/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });

                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Nueva Condición</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_clientecondicion">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $row) {
                        echo
                          '<option value="'.$row->cliente_id.'">'.$row->cliente_nombre.' '.$row->cliente_a_paterno.' '.$row->cliente_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Método Pago</label>
                  <select class="form-control select2" name="metodopago_id" id="metodopago_id">
                    <option disabled selected>- Cheque -</option>
                    <?php
                      foreach ($metodopago as $row) {
                        echo
                          '<option value="'.$row->metodopago_id.'">'.$row->metodopago_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Tipo de Precio</label>
                  <select class="form-control select2" name="tipodeprecio_id" id="tipodeprecio_id">
                    <option disabled selected>- Mayoreo -</option>
                    <?php
                      foreach ($tipodeprecio as $row) {
                        echo
                          '<option value="'.$row->tipodeprecio_id.'">'.$row->tipodeprecio_descripcion.' </option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Condiciones Agregadas</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Cliente</th>
                  <th>Método Pago</th>
                  <th>Tipo de Precio</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($clientescondicion as $row ) {
                      $clientescondicion_id = $row->clientescondicion_id;
                      $cliente_id = $row->cliente_id;
                      $tipodeprecio_id = $row->tipodeprecio_id;
                      $metodopago_id = $row->metodopago_id;

                      $datos_cliente = $this->Mgeneral->get_row('cliente_id',$cliente_id,'clientes');
                      $cliente_nombre = $datos_cliente->cliente_nombre;
                    

                      $metodopago = $this->Mgeneral->get_row('metodopago_id',$metodopago_id,'metodopago');
                      $metodopago_descripcion = $metodopago->metodopago_descripcion;

                      $tipodeprecio = $this->Mgeneral->get_row('tipodeprecio_id',$tipodeprecio_id,'tipodeprecio');

                      $tipodeprecio_descripcion = $tipodeprecio->tipodeprecio_descripcion;

                      echo '
                        <tr id="borrar_'.$clientescondicion_id.'">
                          <td>'.$clientescondicion_id.'</td>
                          <td>'.$cliente_nombre.'</td>
                          <td>'.$tipodeprecio_descripcion.'</td>
                          <td>'.$metodopago_descripcion.'</td>
                          <td>
                            <a href="'.base_url().'index.php/catalogos/ClienteCondicion/editar/'.$clientescondicion_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$clientescondicion_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/ClienteCondicion/cambiar_estatus/'.$clientescondicion_id.'/3" class="eliminar_relacion" flag="'.$clientescondicion_id.'" id="delete'.$clientescondicion_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
