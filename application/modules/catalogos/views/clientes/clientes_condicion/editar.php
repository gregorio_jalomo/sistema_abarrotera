<?php
  $clientescondicion_id = $clientescondicion->clientescondicion_id;
  $cliente_id = $clientescondicion->cliente_id;
  $tipodeprecio_id = $clientescondicion->tipodeprecio_id;
  $metodopago_id = $clientescondicion->metodopago_id;

?>
<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#editar_clientecondicion').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/ClienteCondicion/actualizar";
    ajaxJson(url,
      {
        "clientescondicion_id" : '<?php echo $clientescondicion_id; ?>',
        "cliente_id" : '<?php echo $cliente_id; ?>',
        "tipodeprecio_id" : $('#tipodeprecio_id').val(),
        "metodopago_id" : $('#metodopago_id').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/ClienteCondicion/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Editar Condición</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_clientecondicion">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $row) {
                        echo
                          '<option value="'.$row->cliente_id.'">'.$row->cliente_nombre.' '.$row->cliente_a_paterno.' '.$row->cliente_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Tipo de Precio</label>
                  <select class="form-control select2" name="tipodeprecio_id" id="tipodeprecio_id">
                    <option disabled selected>- Mayoreo -</option>
                    <?php
                      foreach ($tipodeprecio as $row) {
                        echo
                          '<option value="'.$row->tipodeprecio_id.'">'.$row->tipodeprecio_descripcion.' </option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">Método Pago</label>
                  <select class="form-control select2" name="metodopago_id" id="metodopago_id">
                    <option disabled selected>- Cheque -</option>
                    <?php
                      foreach ($metodopago as $row) {
                        echo
                          '<option value="'.$row->metodopago_id.'">'.$row->metodopago_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>



              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/ClienteCondicion/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');

    $('#cliente_id').val(<?= $cliente_id; ?>);
    $('#tipodeprecio_id').val(<?= $tipodeprecio_id; ?>);
    $('#metodopago_id').val(<?= $metodopago_id; ?>);
  });
</script>
