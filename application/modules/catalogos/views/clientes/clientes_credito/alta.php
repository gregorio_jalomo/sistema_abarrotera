<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_clientecredito').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/clientecredito/guardar";
    ajaxJson(url,
      {
        "cliente_id" : $('#cliente_id').val(),
        "clientecredito_descripcion" : $('#clientecredito_descripcion').val(),
        "clientecredito_limitecredito" : $('#clientecredito_limitecredito').val(),
        "clientecredito_limitedias" : $('#clientecredito_limitedias').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/clientecredito/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Crédito de Cliente</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_clientecredito">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="clientecredito_descripcion">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $row) {
                        echo
                          '<option value="'.$row->cliente_id.'">'.$row->cliente_nombre.' '.$row->cliente_a_paterno.' '.$row->cliente_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="clientecredito_descripcion">Descripción</label>
                  <input type="text" class="form-control mr-sm-2" id="clientecredito_descripcion">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="clientecredito_limitecredito">Límite de crédito</label>
                  <input type="number" class="form-control mr-sm-2" id="clientecredito_limitecredito">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="clientecredito_limitedias">Límite de días</label>
                  <input type="number" class="form-control mr-sm-2" id="clientecredito_limitedias">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Créditos de Cliente agregadas</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Descripción</th>
                  <th>Límite de crédito</th>
                  <th>Límite de días</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($clientescredito as $row ) {
                      $cliente_id = $row->cliente_id;

                      $consulta_cliente = $this->Mgeneral->get_row('cliente_id',$cliente_id,'clientes');
                      $cliente_nombre = $consulta_cliente->cliente_nombre;

                      $clientecredito_id = $row->clientecredito_id;
                      $clientecredito_descripcion = $row->clientecredito_descripcion;
                      $clientecredito_limitecredito = $row->clientecredito_limitecredito;
                      $clientecredito_limitedias = $row->clientecredito_limitedias;
                      echo '
                        <tr id="borrar_'.$clientecredito_id.'">
                          <td>'.$cliente_nombre.'</td>
                          <td>'.$clientecredito_descripcion.'</td>
                          <td>'.$clientecredito_limitecredito.'</td>
                          <td>'.$clientecredito_limitedias.'</td>
                          <td>
                            <a href="'.base_url().'index.php/catalogos/clientecredito/editar/'.$clientecredito_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$clientecredito_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/clientecredito/cambiar_estatus/'.$clientecredito_id.'/3" class="eliminar_relacion" flag="'.$clientecredito_id.'" id="delete'.$clientecredito_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');
  });
</script>
