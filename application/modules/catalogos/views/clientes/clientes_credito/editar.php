<?php
  $clientecredito_id = $clientescredito->clientecredito_id;
  $cliente_id = $clientescredito->cliente_id;
  $clientecredito_descripcion = $clientescredito->clientecredito_descripcion;
  $clientecredito_limitecredito = $clientescredito->clientecredito_limitecredito;
  $clientecredito_limitedias = $clientescredito->clientecredito_limitedias;
?>
<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_clientecredito').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/clientecredito/actualizar";
    ajaxJson(url,
      {
        "clientecredito_id" : '<?php echo $clientecredito_id; ?>',
        "cliente_id" : $('#cliente_id').val(),
        "clientecredito_descripcion" : $('#clientecredito_descripcion').val(),
        "clientecredito_limitecredito" : $('#clientecredito_limitecredito').val(),
        "clientecredito_limitedias" : $('#clientecredito_limitedias').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/clientecredito/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Actualizar Crédito del Cliente</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_clientecredito">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="clientecredito_descripcion">Cliente</label>
                  <select class="form-control select2" name="cliente_id" id="cliente_id">
                    <option disabled selected>- Cliente -</option>
                    <?php
                      foreach ($clientes as $row) {
                        echo
                          '<option value="'.$row->cliente_id.'">'.$row->cliente_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="clientecredito_descripcion">Descripción</label>
                  <input type="text" class="form-control mr-sm-2" id="clientecredito_descripcion" value="<?php echo $clientecredito_descripcion; ?>">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="clientecredito_limitecredito">Límite de crédito</label>
                  <input type="text" class="form-control mr-sm-2" id="clientecredito_limitecredito" value="<?php echo $clientecredito_limitecredito; ?>">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="clientecredito_limitedias">Límite de días</label>
                  <input type="number" class="form-control mr-sm-2" id="clientecredito_limitedias" value="<?php echo $clientecredito_limitedias; ?>">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/clientecredito/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');
    $('#cliente_id').val('<?= $cliente_id; ?>');
    $('#cliente_id>option:selected').text();
  });
</script>
