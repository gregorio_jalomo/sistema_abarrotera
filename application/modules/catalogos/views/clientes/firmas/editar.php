<?php
  $firma_id = $firmas->firma_id;
  $nombre = $firmas->nombre;
  $firma_digital = $firmas->firma_digital;

?>
<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_firmas').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/Firmas/actualizar";


    var form = $(this);
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(form[0]);
    }

    ajaxJsonFiles(url,
      formdata ? formdata : form.serialize(),
      "POST","",function(result){
      
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/Firmas/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Actualizar Firma</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_firmas">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="nombre">Nombre</label>
                  <input type="text" class="form-control mr-sm-2" id="nombre" value="<?php echo $nombre; ?>" name="nombre">

                  <input type="hidden"  value="<?php echo $firma_id; ?>" name="firma_id">
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="firma_digital">Firma Digital</label>
                  <input type="file" class=" mr-sm-2" id="firma_digital" value="" name="firma_digital">
                </div>
                <div class="col-4 my-1">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">Firma Digital Actual</h3>
                    </div>
                  </div>
                  <div class="card-body">
                      <img name="firma_actual" id="firma_actual" src="<?php echo base_url().$firmas->firma_digital;?>" width="200" height="114">
                  </div>  
                </div>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/Firmas/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
