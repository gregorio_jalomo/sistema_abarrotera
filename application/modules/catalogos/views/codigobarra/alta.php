<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_codigobarra').submit(function(event){
    event.preventDefault();
    console.log($('#codigobarra_codigo').val());
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/codigobarra/guardar";
    ajaxJson(url,
      {
        "codigobarra_id" : $('#codigobarra_id').val(),
        "codigobarra_codigo" : $('#codigobarra_codigo').val(),
        "codigobarra_ano" : $('#codigobarra_ano').val(),
        "codigobarra_fechainicio" : $('#codigobarra_fechainicio').val(),
        "codigobarra_fechafinal" : $('#codigobarra_fechafinal').val(),
        "producto_id" : $('#producto_id').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/codigobarra/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Nuevo Código De Barras</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_codigobarra">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-md-4">
                  <label for="">Producto</label>
                  <select class="form-control select2" name="producto_id" id="producto_id">
                    <option selected disabled>-  -</option>
                    <?php
                      foreach ($productos as $row) {
                        echo
                          '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="codigobarra_codigo">Código de barras</label>
                  <input type="number" class="form-control mr-sm-2" id="codigobarra_codigo">
                </div>




              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Código de Barras Agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Productos</th>
                  <th>Códigos de barras</th>

                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($codigosbarras as $row ) {
                      $codigobarra_id = $row->codigobarra_id;
                      $codigobarra_codigo = $row->codigobarra_codigo;

                      $producto_id = $row->producto_id;

                      $datos_producto = $this->Mgeneral->get_row('producto_id',$producto_id,'productos');
                      $producto_nombre = $datos_producto->producto_nombre;



                      echo '
                        <tr id="borrar_'.$codigobarra_id.'">
                        <td>'.$producto_nombre.'</td>
                          <td>'.$codigobarra_codigo.'</td>



                          <td>
                            <a href="'.base_url().'index.php/catalogos/codigobarra/editar/'.$codigobarra_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$codigobarra_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/codigobarra/cambiar_estatus/'.$codigobarra_id.'/3" class="eliminar_relacion" flag="'.$codigobarra_id.'" id="delete'.$codigobarra_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
