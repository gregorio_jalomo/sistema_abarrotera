<?php
  $codigobarra_id = $codigosbarras->codigobarra_id;
  $codigobarra_codigo = $codigosbarras->codigobarra_codigo;

  $producto_id = $codigosbarras->producto_id;

?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_codigobarra').submit(function(event){
    event.preventDefault();
    console.log($('#codigobarra_codigo').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/codigobarra/actualizar";
    ajaxJson(url,{
        'codigobarra_id' : '<?php echo $codigobarra_id; ?>',
        'codigobarra_codigo' : $('#codigobarra_codigo').val(),
        'codigobarra_ano' : $('#codigobarra_ano').val(),
        'codigobarra_fechainicio' : $('#codigobarra_fechainicio').val(),
        'codigobarra_fechafinal' : $('#codigobarra_fechafinal').val(),


        'producto_id' : $('#producto_id').val()


      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/codigobarra/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Folio Documentos</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_codigobarra">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-3">
                  <label for="">Producto</label>
                  <select class="form-control select2" name="producto_id" id="producto_id">
                    <option selected disabled>- Galletas -</option>

                    <?php
                      foreach ($productos as $row) {
                        echo
                          '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="">Código de Barras</label>
                  <input type="text" class="form-control" name="codigobarra_codigo" id="codigobarra_codigo" value="<?php echo $codigobarra_codigo; ?>">
                </div>



              </div>



              </div>



              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/catalogos/codigobarra/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>


            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
