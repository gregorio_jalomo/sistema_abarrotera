<?php

$rol_usuario=$this->session->userdata['infouser']['rol'];
$is_admin=($rol_usuario==2 || $rol_usuario==1)?true:false;
?>
  <div class="">
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Catálogos Productos</h1>
            </div><!-- /.col -->

          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content" >
        <div class="container-fluid" >
          <!-- Info boxes -->
          <div class="row" >


            <!-- <?php
                //categorias
                /* if(validar_permiso($rol_usuario,1,1) || $is_admin){ ?>


                <?php
                } */
            ?> -->

            <?php
                //categorias
                if(validar_permiso($rol_usuario,2,6) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/categoria/alta">

                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img   height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/categoria.png" alt=""> </span>

                      <div class="info-box-content">

                        <span class="info-box-text">Categorías</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->

                <?php
                }
            ?>


            <?php
                //subcategorias
                if(validar_permiso($rol_usuario,16,73) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                  <a href="<?php echo base_url()?>index.php/catalogos/subcategoria/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/subcategoria.png" alt=""></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Subcategorías</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>


              <?php
                //unidades
                if(validar_permiso($rol_usuario,17,78) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/unidad/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img width="60px" height="60px" src="<?php echo base_url()?>statics/tema/dist/img/unidad6.png" alt=""></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Unidades</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->

                <?php
                }
            ?>


            <?php
                //familias
                if(validar_permiso($rol_usuario,22,84) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/familia/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Familias</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->

                <?php
                }
            ?>


            <?php
                //pasillo
                if(validar_permiso($rol_usuario,23,88) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                  <a href="<?php echo base_url()?>index.php/catalogos/pasillo/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/pasillo.svg" alt=""></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Pasillo</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>


            <?php
                //area
                if(validar_permiso($rol_usuario,24,92) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                  <a href="<?php echo base_url()?>index.php/catalogos/area/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/area.png" alt=""></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Área</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>


            <?php
                //tipodeprecio
                if(validar_permiso($rol_usuario,25,96) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/tipodeprecio/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1">
                      <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/precio10.png" alt=""></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Tipos de Precio</span>
                        <span class="info-box-number"></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>


            <?php
                //paquete(ProductoPaquete)
                if(validar_permiso($rol_usuario,38,135) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/ProductoPaquete/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cubes"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Paquetes</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>

            <?php
                //codigos de barra
                if(validar_permiso($rol_usuario,29,105) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/codigobarra/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cubes"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Código de Barra</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>

            <?php
                //motivos
                if(validar_permiso($rol_usuario,28,101) || $is_admin){ ?>

                  <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?php echo base_url()?>index.php/catalogos/motivos/alta">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="nav-icon fas fa-comment-alt"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Motivos</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>

                <?php
                }
            ?>


          </div><!-- row -->
        </div><!-- container-fluid -->
      </section>
  </div>
      <div class="content-header">
        <a href="#"></a>
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Catálogos</h1>
              </div><!-- /.col -->

            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content" >
          <div class="container-fluid" >
            <!-- Info boxes -->
            <div class="row" >

                <?php
                    //condicion
                    if(validar_permiso($rol_usuario,39,140) || $is_admin){ ?>

                      <div class="col-12 col-sm-6 col-md-3">
                      <a href="<?php echo base_url()?>index.php/catalogos/ClienteCondicion/alta">
                        <div class="info-box mb-3">
                          <span class="info-box-icon bg-success elevation-1">
                          <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/condicion.png" alt=""></span>

                          <div class="info-box-content">
                            <span class="info-box-text">Condición</span>
                            <span class="info-box-number"></span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>

                    <?php
                    }
                ?>

                <?php
                    //metodo de pago
                    if(validar_permiso($rol_usuario,40,145) || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/metododepago/alta">
                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1">
                            <img  height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/pago5.png" alt=""></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Método Pago</span>
                              <span class="info-box-number"></span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                    <?php
                    }
                ?>

                <?php
                    //Firmas
                    if(validar_permiso($rol_usuario,41,150) || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/firmas/alta">
                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-pencil-alt"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Firmas</span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>

                    <?php
                    }
                ?>

                <?php
                    //prefijos
                    if(validar_permiso($rol_usuario,"only_admin_prefijo","only_admin_prefijo") || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/prefijo/alta">

                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1">
                          </span>

                            <div class="info-box-content">
                              <span class="info-box-text">Prefijos</span>
                              <span class="info-box-number"></span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>

                    <?php
                    }
                ?>

                <?php
                    //subcategorias
                    if(validar_permiso($rol_usuario,"only_admin_foliodocumento","only_admin_foliodocumento") || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/foliodocumento/alta">

                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="icofont-file-document"></i></span>

                            <div class="info-box-content">

                              <span class="info-box-text">Folios Documentos</span>
                              <span class="info-box-number"></span>
                            </div><!-- /.info-box-content -->
                          </div><!-- /.info-box -->
                        </div>

                    <?php
                    }
                ?>

                <?php
                    //subcategorias
                    if(validar_permiso($rol_usuario,"only_admin_choferes","only_admin_choferes") || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/choferes/alta">

                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-address-card"></i></span>

                            <div class="info-box-content">

                              <span class="info-box-text">Choferes</span>
                              <span class="info-box-number"></span>
                            </div><!-- /.info-box-content -->
                          </div><!-- /.info-box -->
                        </div>

                    <?php
                    }
                ?>

                <?php
                    //vehiculos
                    if(validar_permiso($rol_usuario,43,160) || $is_admin){ ?>

                        <div class="col-12 col-sm-6 col-md-3">
                          <a href="<?php echo base_url()?>index.php/catalogos/vehiculos/alta">

                          <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-truck"></i></span>

                            <div class="info-box-content">

                              <span class="info-box-text">Vehiculos</span>
                              <span class="info-box-number"></span>
                            </div><!-- /.info-box-content -->
                          </div><!-- /.info-box -->
                        </div>

                    <?php
                    }
                ?>

            </div><!-- row -->
          </div><!-- container-fluid -->
        </section>
        <br>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
