<?php
  $motivo_id          = $motivo->motivo_id;
  $motivo_descripcion = $motivo->motivo_descripcion;
  $motivo_accion      = $motivo->motivo_accion;

?>
<style>
  .select2-container {
      display: inline-block!important;
      min-width: 200px!important;
  }
</style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar Motivos</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_motivo">
            <div class="card-body">
              <div class="form-row align-items-center">
              <div class="col-1">
                  <label class="mr-sm-2" for="motivo_descripcion">Descripcion</label>
                </div>
                <div class="col-4 my-1">
                  <input type="text" class="form-control mr-sm-2" id="motivo_descripcion" value="<?=$motivo_descripcion?>">
                </div>

                  <label class="mr-sm-2" for="accion">Accion:</label>
                  <select name="" id="accion" class="select2 ">
                      <?php
                        $selected="selected='selected'";
                      ?>
                      <option value="1" <?=($motivo_accion==1)?$selected:"";?>>Aumento</option>
                      <option value="2" <?=($motivo_accion==2)?$selected:"";?>>Decremento</option>
                  </select>

                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Guardar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
    $("#cargando").hide();

    $('#editar_motivo').submit(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $("#cargando").show();
      var url ="<?php echo base_url()?>index.php/catalogos/motivos/editar_motivo";
      ajaxJson(url,
        {
          "motivo_id" : '<?=$motivo_id?>',
          "motivo_descripcion" : $('#motivo_descripcion').val(),
          "motivo_accion" : $('#accion').val()

        },
        "POST","",function(result){
        correoValido = false;
        console.log(result);
        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        if(obj_status == false){
          aux = "";
          $.each( obj_output.errors, function( key, value ) {
            aux +=value+"<br/>";
          });
          exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
          $("#enviar").show();
          $("#cargando").hide();
        }
        if(obj_status == true){
          exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/motivos/alta");
          $("#enviar").show();
          $("#cargando").hide();
        }
      });
    });
  });
</script>
