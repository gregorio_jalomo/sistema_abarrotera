<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_paquete').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    $('select[id^="producto_agregado_id-"]').each(function() {
      let url ="<?php echo base_url()?>index.php/catalogos/productopaquete/guardardetallepaquete";
      let id = this.id;
      let split = id.split("-");
      let nProducto = split[1];
      let producto_agregado_id = $('#producto_agregado_id-'+nProducto).val();
      let cantidad = $('#cantidad-'+nProducto).val();
      // console.log(idPaquete);
      ajaxJson(url,
        {
          "producto_id" : $('#producto_id').val(),
          "producto_agregado_id" : producto_agregado_id,
          "cantidad" : cantidad
        },
        "POST","",function(result){
        correoValido = false;
        console.log(result);
        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        aResults = [];
        if(obj_status == false){
          aResults.push("false");
        }
        if(obj_status == true){
          aResults.push("true");
        }
      });
    });
    if(aResults.includes("false")){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    } else {
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/productopaquete/alta");
      $("#enviar").show();
      $("#cargando").hide();
    }
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Agregar Producto Paquete</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_paquete">
            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-5 my-1">
                  <label class="mr-sm-2" for="nombre-">Producto Paquete</label>
                  <select class="form-control select2" name="producto_id" id="producto_id" onchange="mostrarCostoReal(this.value)">
                    <option selected disabled>- Producto Paquete -</option>
                    <?php
                      foreach ($productos as $row) {

                        if ($row->check_paquete === '1') {
                          echo '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
                <div class="col-2">
                  <label class="mr-sm-2" for="producto_costo_real">Producto Paquete Costo Real</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                    </div>
                    <input type="text" class="form-control" id="producto_costo_real">
                  </div>
                </div>
              </div>
              <div class="form-row my-3 mb-3">
                <div class="col-12">
                  <button type="button" class="btn btn-success" onclick="agregarDivProducto()">Agregar Producto <i class="fas fa-plus"></i></button>
                </div>
              </div>
              <div class="form-row align-items-center">
                <div class="col-5 my-1">
                  <label class="mr-sm-2" for="producto_agregado_id-">Producto</label>
                  <select class="form-control select2" name="producto_agregado_id-" id="producto_agregado_id-">
                    <option selected disabled>- Producto -</option>
                    <?php
                      foreach ($productos as $row) {
                        if ($row->check_paquete === '0') {
                          echo '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="cantidad">Cantidad</label>
                  <input type="text" class="form-control" name="cantidad-" id="cantidad-">
                </div>
              </div>
              <div class="form-row align-items-center">
                <div class="col-12" id="divProductosAgregados"></div>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Tipos de precio</h3>
						<div class="card-tools">
							<div class="input-group input-group-sm" style="width: 150px;">
								<input type="text" name="table_search" class="form-control float-right" placeholder="Buscar...">

								<div class="input-group-append">
									<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body table-responsive p-0 table-productos-precios">
						<table class="table table-head-fixed text-nowrap">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Porcentaje %</th>
									<th>Total $</th>
								</tr>
							</thead>
							<tbody id="tipos-de-precio">
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
		</div>
    <div class="row">
      <div class="col-12">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Productos Paquetes agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Producto Paquete</th>
                  <th>Costo Real</th>
                  <th>Fecha de Registro</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $aProductosPaquetesRepetidos = [];
                    $bExiste;
                    foreach ($productospaquetes as $row ) {
                      $producto_id = $row->producto_id;
                      if (!in_array($producto_id,$aProductosPaquetesRepetidos)) {
                        $aProductosPaquetesRepetidos [] = $producto_id;
                        $bExiste = 1;
                      } else {
                        $bExiste = 0;
                      }
                      if ($bExiste === 1) {
                        $consulta_productos = $this->Mgeneral->get_row('producto_id',$producto_id,'productos');
                        $producto_nombre = $consulta_productos->producto_nombre;
                        $producto_costo_real = $consulta_productos->producto_costo_real;
                        $fecha_registro = $row->fecha_registro;
                        echo '
                          <tr id="borrar_'.$producto_id.'">
                            <td>'.$producto_nombre.'</td>
                            <td>'.$producto_costo_real.'</td>
                            <td>'.$fecha_registro.'</td>
                            <td>
                              <a href="'.base_url().'index.php/catalogos/productopaquete/ver_detalle/'.$producto_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
                              <a href="'.base_url().'index.php/catalogos/productopaquete/editar/'.$producto_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                              <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$producto_id.',2);"><i class="fa fa-times"></i></button> -->
                              <a href="'.base_url().'index.php/catalogos/productopaquete/cambiar_estatus/'.$producto_id.'/3" class="eliminar_relacion" flag="'.$producto_id.'" id="delete'.$producto_id.'">
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                              </a>
                            </td>
                          </tr>
                        ';
                      }
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
    $('#producto_costo_real').attr('disabled',true);
	});
	function mostrarCostoReal(idProducto){
    let url ="<?php echo base_url()?>index.php/catalogos/productopaquete/mostrarProductoCostoReal";
    ajaxJson(url,
      {
        "producto_id" : idProducto,
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_consulta = JSON.parse(result);
      costoReal = json_consulta.output;
    });
    $('#producto_costo_real').val(costoReal);

    let valueCostoReal = $('#producto_costo_real').val();

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });
  }
	let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
  let tbody = '';
  let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
  $('#tipos-de-precio').html(tbody);

  let nProducto = 1;
  function agregarDivProducto() {
    nProducto++;

    let div = `<div class="form-row" id="divProducto-${nProducto}">
                <div class="col-5 my-1">
                  <label class="mr-sm-2" for="tipodeprecio_porcentaje">Producto</label>
                  <select class="form-control select2" name="producto_agregado_id-${nProducto}" id="producto_agregado_id-${nProducto}">
                    <option selected disabled>- Producto -</option>
                    <?php
                      foreach ($productos as $row) {
                        if ($row->check_paquete == 0) {
                          echo '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="tipodeprecio_porcentaje">Cantidad</label>
                  <input type="text" class="form-control" name="cantidad-${nProducto}" id="cantidad-${nProducto}">
                </div>
                <div class="col-2 my-1">
                  <button type="button" class="btn btn-danger mt-2em"><i class="fas fa-times" onclick="eliminarDivProducto('${nProducto}')"></i></button>
                </div>
              </div>`;
    $('#divProductosAgregados').append(div);
		// $('#producto_id-'+nProducto).addClass('select2');
		$('#producto_id-'+nProducto).select2();
  }
  function eliminarDivProducto(divProducto) {
    $("#divProducto-" + divProducto).html('');
	}//...eliminarDivProducto

</script>
