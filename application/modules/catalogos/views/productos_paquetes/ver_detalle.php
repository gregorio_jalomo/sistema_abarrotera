<?php 
  $producto_id = $detallesproductos->producto_id;
  $consulta_producto = $this->Mgeneral->get_row('producto_id',$producto_id,'productos');
  //...
  $producto_nombre = ($detallesproductos->producto_id === $consulta_producto->producto_id) ? $consulta_producto->producto_nombre : '';
  $producto_costo_real = $consulta_producto->producto_costo_real;
  
  // var_dump($productos);
?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <div class="row">
            <div class="col-6">
              <h5><i class="fas fa-info"></i> Producto: <span><?php echo $producto_nombre?></span> # <?php echo $producto_id; ?></h5>
            </div>
            <div class="col-6">
            <h5 class="float-right"><i class="fas fa-dollar-sign"></i> Costo real: <?php echo $producto_costo_real; ?></h5>
            </div>
          </div>  
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-3 mb-3">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-6 border-right">
                  <!-- title row -->
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Producto</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <?php
                  $nProducto = 0;
                    foreach($detalle_producto as $row) {
                      $producto_id = $row->producto_id;

                      $consulta_producto = $this->Mgeneral->get_row('producto_id',$row->producto_agregado_id,'productos');

                      $producto_agregado_nombre = $consulta_producto->producto_nombre;
                      $cantidad = $row->cantidad;

                      echo '
                      <div class="row">
                        <div class="col-sm-6 invoice-col">
                          <ul>
                            <li class="font-weight-bolder">Producto '.++$nProducto.'</li>
                            <li>'.$producto_agregado_nombre.'</li>
                          </ul>
                        </div>
                        <div class="col-sm-2">
                          <ul>
                            <li class="font-weight-bolder">Cantidad</li>
                            <li>'.$cantidad.'</li>
                          </ul>
                        </div>
                      </div>';
                    }
                  ?>
                </div>
                <!-- /.col-6 -->
                <div class="col-6">
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Otros datos</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <?php 
                    $fecha_registro = $detallesproductos->fecha_registro;
                    $consulta_estatus = $this->Mgeneral->get_row('cat_estatus_id',$detallesproductos->cat_estatus_id,'cat_estatus');
                    $cat_estatus_nombre = $consulta_estatus->cat_estatus_nombre;
                  ?>
                  <div class="row">
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Estatus</li>
                        <li class="font-weight-bolder"><span class="badge bg-success"><?php echo $cat_estatus_nombre; ?></span></li>
                      </ul>
                    </div>
                    <div class="col-sm-9">
                      <ul>
                        <li class="font-weight-bolder">Fecha de Registro</li>
                        <li><?php echo $fecha_registro; ?></li>
                      </ul>
                    </div>
                  </div>
                  
                </div>
                <!-- /.col-6 -->
              </div>
            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Descripción</th>
                  <th>Porcentaje %</th> 
                  <th>Total $</th>
                </tr>
                </thead>
                <tbody id="tipos-de-precio">      
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');

    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
    $('#tipos-de-precio').html(tbody);

    const valueCostoReal = '<?php echo $producto_costo_real; ?>';

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });
  });
</script>