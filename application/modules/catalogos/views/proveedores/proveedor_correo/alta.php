<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_proveedorcorreo').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/proveedorcorreo/guardar";
    ajaxJson(url,
      {
        "proveedor_id" : $('#proveedor_id').val(),
        "proveedor_correo" : $('#proveedor_correo').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/proveedorcorreo/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                        if(result=="true"){
                          $("#borrar_"+id).slideUp();
                          ExitoCustom("Eliminado");
                        }else{
                          ErrorCustom("No tienes permiso para eliminar esto");
                        }
                      });
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar correo de proveedor</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_proveedorcorreo">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="telefono">proveedor</label>
                  <select class="form-control select2" name="proveedor_id" id="proveedor_id">
                    <option disabled selected>- proveedor -</option>
                    <?php
                      foreach ($proveedores as $proveedor) {
                        echo
                          '<option value="'.$proveedor->proveedor_id.'">'.$proveedor->proveedor_nombre.' '.$proveedor->proveedor_a_paterno.' '.$proveedor->proveedor_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedor_correo">correo</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-at"></i></span>
                    </div>
                    <input type="email" class="form-control" name="proveedor_correo" id="proveedor_correo">
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">correos de proveedor agregados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>proveedor</th>
                  <th>correo</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($proveedorcorreo as $row ) {
                      $proveedorcorreo_id = $row->proveedorcorreo_id;
                      $proveedor_id = $row->proveedor_id;
                      $proveedor_correo = $row->proveedor_correo;
                      $datos_proveedor = $this->Mgeneral->get_row('proveedor_id',$proveedor_id,'proveedores');
                      $proveedor_nombre = $datos_proveedor->proveedor_nombre;

                      echo '
                        <tr id="borrar_'.$proveedorcorreo_id.'">
                          <td>'.$proveedor_id.'</td>
                          <td>'.$proveedor_nombre.' </td>
                          <td>'.$proveedor_correo.'</td>
                          <td>
                            <a href="'.base_url().'index.php/catalogos/proveedorcorreo/editar/'.$proveedorcorreo_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$proveedorcorreo_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/proveedorcorreo/cambiar_estatus/'.$proveedorcorreo_id.'/3" class="eliminar_relacion" flag="'.$proveedorcorreo_id.'" id="delete'.$proveedorcorreo_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');
  });
</script>
