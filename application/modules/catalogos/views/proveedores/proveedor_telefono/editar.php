<?php
  $proveedortelefono_id = $proveedortelefono->proveedortelefono_id;
  $proveedor_id = $proveedortelefono->proveedor_id;
  $proveedor_telefono = $proveedortelefono->proveedor_telefono;
?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#alta_proveedortelefono').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/proveedortelefono/actualizar";
    ajaxJson(url,
      {
        "proveedortelefono_id" : '<?php echo $proveedortelefono_id; ?>',
        "proveedor_id" : $('#proveedor_id').val(),
        "proveedor_telefono" : $('#proveedor_telefono').val().replace(/-|_/g," ")
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/proveedortelefono/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                        $.get(url);
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Teléfono de proveedor</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_proveedortelefono">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcredito_descripcion">proveedor</label>
                  <select class="form-control select2" name="proveedor_id" id="proveedor_id">
                    <option disabled selected>- proveedor -</option>
                    <?php
                      foreach ($proveedores as $row) {
                        echo
                          '<option value="'.$row->proveedor_id.'">'.$row->proveedor_nombre.' '.$row->proveedor_a_paterno.' '.$row->proveedor_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-3 my-1">
                  <label class="mr-sm-2" for="proveedor_telefono">Teléfono</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [99999]', '+099 99 99 9999[9]-9999']" data-mask="" im-insert="true"  id="proveedor_telefono" value="<?php echo $proveedor_telefono; ?>">
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/proveedortelefono/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
$(document).ready(function() {
  $('#menuProveedores').addClass('active-link');
  $('#menuProveedores').val('<?= $proveedor_id; ?>');
});
</script>
