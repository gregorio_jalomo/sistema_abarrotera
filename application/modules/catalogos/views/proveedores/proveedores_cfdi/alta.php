<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_proveedorcfdi').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/proveedorcfdi/guardar";
    ajaxJson(url,
      {
        "proveedorcfdi_clave" : $('#proveedorcfdi_clave').val(),
        "proveedorcfdi_descripcion" : $('#proveedorcfdi_descripcion').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/proveedorcfdi/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar cfdi</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_proveedorcfdi">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcfdi_clave">Clave</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcfdi_clave">
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcfdi_descripcion">Descripción</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcfdi_descripcion">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Guardar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="ccard card-info">
          <div class="card-header">
            <h3 class="card-title">cfdi Agregadas</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Clave</th>
                  <th>Descripción</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($proveedorescfdi as $row ) {
                      $proveedorcfdi_id = $row->proveedorcfdi_id;
                      $proveedorcfdi_clave = $row->proveedorcfdi_clave;
                      $proveedorcfdi_descripcion = $row->proveedorcfdi_descripcion;

                      echo '
                        <tr id="borrar_'.$proveedorcfdi_id.'">
                          <td>'.$proveedorcfdi_clave.'</td>
                          <td>'.$proveedorcfdi_descripcion.'</td>

                          <td>
                            <a href="'.base_url().'index.php/catalogos/proveedorcfdi/editar/'.$proveedorcfdi_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$proveedorcfdi_id.',2);"><i class="fa fa-times"></i></button> -->
                            <a href="'.base_url().'index.php/catalogos/proveedorcfdi/cambiar_estatus/'.$proveedorcfdi_id.'/3" class="eliminar_relacion" flag="'.$proveedorcfdi_id.'" id="delete'.$proveedorcfdi_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');
  });
</script>
