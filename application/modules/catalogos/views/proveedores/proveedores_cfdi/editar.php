<?php
  $proveedorcfdi_id = $proveedorescfdi->proveedorcfdi_id;
  $proveedorcfdi_clave = $proveedorescfdi->proveedorcfdi_clave;
  $proveedorcfdi_descripcion = $proveedorescfdi->proveedorcfdi_descripcion;

?>
<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_proveedorcfdi').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/proveedorcfdi/actualizar";
    ajaxJson(url,
      {
        "proveedorcfdi_id" : '<?php echo $proveedorcfdi_id; ?>',
        "proveedorcfdi_clave" : $('#proveedorcfdi_clave').val(),
        "proveedorcfdi_descripcion" : $('#proveedorcfdi_descripcion').val()

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/proveedorcfdi/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Actualizar cfdi</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_proveedorcfdi">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcfdi_clave">Clave</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcfdi_clave" value="<?php echo $proveedorcfdi_clave; ?>">
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcfdi_descripcion">Descripción</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcfdi_descripcion" value="<?php echo $proveedorcfdi_descripcion; ?>">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/proveedorcfdi/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');
  });
</script>
