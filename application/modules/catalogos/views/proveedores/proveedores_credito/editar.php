<?php
  $proveedorcredito_id = $proveedorescredito->proveedorcredito_id;
  $proveedor_id = $proveedorescredito->proveedor_id;
  $proveedorcredito_descripcion = $proveedorescredito->proveedorcredito_descripcion;
  $proveedorcredito_limitecredito = $proveedorescredito->proveedorcredito_limitecredito;
  $proveedorcredito_limitedias = $proveedorescredito->proveedorcredito_limitedias;
?>
<script>
$(document).ready(function(){
$("#cargando").hide();
  $('#alta_proveedorcredito').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/proveedorcredito/actualizar";
    ajaxJson(url,
      {
        "proveedorcredito_id" : '<?php echo $proveedorcredito_id; ?>',
        "proveedor_id" : $('#proveedor_id').val(),
        "proveedorcredito_descripcion" : $('#proveedorcredito_descripcion').val(),
        "proveedorcredito_limitecredito" : $('#proveedorcredito_limitecredito').val(),
        "proveedorcredito_limitedias" : $('#proveedorcredito_limitedias').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/proveedorcredito/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar Crédito del proveedor</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_proveedorcredito">

            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcredito_descripcion">proveedor</label>
                  <select class="form-control select2" name="proveedor_id" id="proveedor_id">
                    <option disabled selected>- proveedor -</option>
                    <?php
                      foreach ($proveedores as $row) {
                        echo
                          '<option value="'.$row->proveedor_id.'">'.$row->proveedor_nombre.' '.$row->proveedor_a_paterno.' '.$row->proveedor_a_materno.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="proveedorcredito_descripcion">Descripción</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcredito_descripcion" value="<?php echo $proveedorcredito_descripcion; ?>">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="proveedorcredito_limitecredito">Límite de crédito</label>
                  <input type="text" class="form-control mr-sm-2" id="proveedorcredito_limitecredito" value="<?php echo $proveedorcredito_limitecredito; ?>">
                </div>
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="proveedorcredito_limitedias">Límite de días</label>
                  <input type="number" class="form-control mr-sm-2" id="proveedorcredito_limitedias" value="<?php echo $proveedorcredito_limitedias; ?>">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/proveedorcredito/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');
    $('#proveedor_id').val('<?= $proveedor_id; ?>');
    $('#proveedor_id>option:selected').text();
  });
</script>
