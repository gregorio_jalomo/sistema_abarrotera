<?php
  $subcategoria_id = $subcategorias->subcategoria_id;
  $subcategoria_descripcion = $subcategorias->subcategoria_descripcion;
?>
<script>
 //menu_activo = "proveedores";
//$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
  $('#editar_subcategoria').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/subcategoria/editar_subcategoria";
    ajaxJson(url,
      {
        "subcategoria_id" : '<?php echo $subcategoria_id; ?>',
        "subcategoria_descripcion" : $('#subcategoria_descripcion').val()
      },
      "POsT","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JsON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOs GUARDADOs CON EXITO","success","<?php echo base_url()?>index.php/catalogos/subcategoria/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar subcategoria</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_subcategoria">
            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-md-1">
                  <label class="mr-sm-2" for="subcategoria_descripcion">Nombre</label>
                </div>
                <div class="col-6 my-1">
                  <input type="text" class="form-control mr-sm-2" id="subcategoria_descripcion" value="<?php echo $subcategoria_descripcion; ?>">
                </div>
                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                  <a href="<?php echo base_url().'index.php/catalogos/subcategoria/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
