<?php
  $tipodeprecio_id = $tipodeprecios->tipodeprecio_id;
  $tipodeprecio_descripcion = $tipodeprecios->tipodeprecio_descripcion;
  $tipodeprecio_porcentaje = $tipodeprecios->tipodeprecio_porcentaje;
?>
<script>
 //menu_activo = "proveedores";
//$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
  $('#editar_tipodeprecio').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/tipodeprecio/editar_tipodeprecio";
    ajaxJson(url,
      {
        "tipodeprecio_id" : '<?php echo $tipodeprecio_id; ?>',
        "tipodeprecio_descripcion" : $('#tipodeprecio_descripcion').val(),
        "tipodeprecio_porcentaje" : $('#tipodeprecio_porcentaje').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/tipodeprecio/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Actualizar Tipo de Precio</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_tipodeprecio">
            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-4 my-1">
                  <label class="mr-sm-2" for="tipodeprecio_descripcion">Nombre</label>
                  <input type="text" class="form-control mr-sm-2" id="tipodeprecio_descripcion" value="<?php echo $tipodeprecio_descripcion; ?>">
                </div>

                <div class="col-3 my-1">
                  <label class="mr-sm-2" for="tipodeprecio_porcentaje">Porcentaje</label>
                  <input type="number" class="form-control mr-sm-2" id="tipodeprecio_porcentaje" value="<?php echo $tipodeprecio_porcentaje; ?>">
                </div>

              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              <a href="<?php echo base_url().'index.php/catalogos/tipodeprecio/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
