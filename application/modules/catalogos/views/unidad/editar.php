<?php
  $unidad_id = $unidades->unidad_id;
  $unidad_tipo = $unidades->unidad_tipo;
?>
<script>
 //menu_activo = "proveedores";
//$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
  $('#editar_unidad').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/catalogos/unidad/editar_unidad";
    ajaxJson(url,
      {
        "unidad_id" : '<?php echo $unidad_id; ?>',
        "unidad_tipo" : $('#unidad_tipo').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/unidad/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Editar unidad</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_unidad">
            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-md-1">
                  <label class="mr-sm-2" for="unidad_tipo">Nombre</label>
                </div>
                <div class="col-6 my-1">
                  <input type="text" class="form-control mr-sm-2" id="unidad_tipo" value="<?php echo $unidad_tipo; ?>">
                </div>
                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                  <a href="<?php echo base_url().'index.php/catalogos/unidad/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuCatalogos').addClass('active-link');
  });
</script>
