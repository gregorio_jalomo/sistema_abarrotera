<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Agregar Vehiculo</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_vehiculos">
            <div class="card-body">
              <div class="form-row align-items-center">
                <!-- <div class="col-md-1">
                </div> -->
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_marca">Marca</label>
                  <input type="text" class="form-control mr-sm-2" id="vehiculos_marca">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_linea">Linea</label>
                  <input type="text" class="form-control mr-sm-2" maxlength="10" id="vehiculos_linea">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_modelo">Modelo</label>
                  <input type="text" class="form-control mr-sm-2" id="vehiculos_modelo">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_caracteristicas">Características</label>
                  <textarea id="vehiculos_caracteristicas" cols="30"></textarea>
                </div>

                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Guardar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Vehiculos agregados</h3>


          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Marca</th>
                  <th>Linea</th>
                  <th>Modelo</th>
                  <th>Características</th>
                  <th>Opciones</th>

                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($vehiculos as $row) {
                      $vehiculos_id = $row->vehiculos_id;
                      $marca = $row->vehiculos_marca;
                      $linea = $row->vehiculos_linea;
                      $modelo = $row->vehiculos_modelo;
                      $caracteristicas = $row->vehiculos_caracteristicas;

                      echo '
                        <tr id="borrar_'.$vehiculos_id.'">
                          <td>'.$vehiculos_id.'</td>

                          <td>'.$marca.'</td>
                          <td>'.$linea.'</td>
                          <td>'.$modelo.'</td>
                          <td>'.$caracteristicas.'</td>

                          <td>
                            <a href="'.base_url().'index.php/catalogos/vehiculos/ver_vehiculo/'.$vehiculos_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusProducto('.$vehiculos_id.',2);"><i class="fa fa-times"></i></button> -->
                            <!--<a href="'.base_url().'index.php/catalogos/vehiculos/cambiar_estatus_vehiculos/'.$vehiculos_id.'/3" class="eliminar_relacion" flag="'.$vehiculos_id.'" id="delete'.$vehiculos_id.'">
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                            </a>-->
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script>

  $(document).ready(function(){
    $('#menuCatalogos').addClass('active-link');
    $("#cargando").hide();
    $('#alta_vehiculos').submit(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $("#cargando").show();
      var url ="<?php echo base_url()?>index.php/catalogos/vehiculos/guardar_vehiculo";
      ajaxJson(url,
        {
          "vehiculos_marca" : $('#vehiculos_marca').val(),
          "vehiculos_linea" : $('#vehiculos_linea').val(),
          "vehiculos_modelo" : $('#vehiculos_modelo').val(),
          "vehiculos_caracteristicas" : $('#vehiculos_caracteristicas').val()

        },
        "POST","",function(result){

        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        if(obj_status == false){
          aux = "";
          $.each( obj_output.errors, function( key, value ) {
            aux +=value+"<br/>";
          });
          exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
          $("#enviar").show();
          $("#cargando").hide();
        }
        if(obj_status == true){
          exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/vehiculos/alta");
          $("#enviar").show();
          $("#cargando").hide();
        }
      });
    });
    $(".eliminar_relacion").click(function(event){
          event.preventDefault();
          bootbox.dialog({
          message: "Desea eliminar el registro?",
          closeButton: true,
          buttons:
                  {
                    "danger":
                              {
                                "label": "<i class='icon-remove'></i>Eliminar ",
                                "className": "btn-danger",
                                "callback": function () {
                                id = $(event.currentTarget).attr('flag');
                                url = $("#delete"+id).attr('href');
                                $("#borrar_"+id).slideUp();
                                  $.get(url);
                                }
                                },
                                  "cancel":
                                  {
                                      "label": "<i class='icon-remove'></i> Cancelar",
                                      "className": "btn-sm btn-info",
                                      "callback": function () {

                                      }
                                  }

                              }
                          });
        });
    });
</script>
