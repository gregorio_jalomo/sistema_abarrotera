<?php
$vehiculos_id = $vehiculo->vehiculos_id;
$marca = $vehiculo->vehiculos_marca;
$linea = $vehiculo->vehiculos_linea;
$modelo = $vehiculo->vehiculos_modelo;
$caracteristicas = $vehiculo->vehiculos_caracteristicas;

?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar vehiculo</h3>
          </div>
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_vehiculo">
            <div class="card-body">
              <div class="form-row align-items-center">
                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_marca">Marca</label>
                  <input type="text" class="form-control mr-sm-2" id="vehiculos_marca" value="<?=$marca?>">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_linea">Linea</label>
                  <input type="text" class="form-control mr-sm-2" maxlength="10" id="vehiculos_linea" value="<?=$linea?>">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_modelo">Modelo</label>
                  <input type="text" class="form-control mr-sm-2" id="vehiculos_modelo" value="<?=$modelo?>">
                </div>

                <div class="col-2 my-1">
                  <label class="mr-sm-2" for="vehiculos_caracteristicas">Características</label>
                  <textarea id="vehiculos_caracteristicas" cols="30"><?=$caracteristicas?></textarea>
                </div>

                <div class="col-5 my-1">
                  <button type="submit" class="btn btn-primary" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                  <a href="<?php echo base_url().'index.php/catalogos/vehiculo/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
                </div>

              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.div-->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>

  $(document).ready(function(){
  $('#menuCatalogos').addClass('active-link');
  $("#cargando").hide();
    $('#editar_vehiculo').submit(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $("#cargando").show();
      var url ="<?php echo base_url()?>index.php/catalogos/vehiculos/editar_vehiculo";
      ajaxJson(url,
        {
          "vehiculos_marca" : $('#vehiculos_marca').val(),
          "vehiculos_linea" : $('#vehiculos_linea').val(),
          "vehiculos_modelo" : $('#vehiculos_modelo').val(),
          "vehiculos_caracteristicas" : $('#vehiculos_caracteristicas').val()
        },
        "POST","",function(result){

        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        if(obj_status == false){
          aux = "";
          $.each( obj_output.errors, function( key, value ) {
            aux +=value+"<br/>";
          });
          exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
          $("#enviar").show();
          $("#cargando").hide();
        }
        if(obj_status == true){
          exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/catalogos/vehiculos/alta");
          $("#enviar").show();
          $("#cargando").hide();
        }
      });
    });
  });
</script>
