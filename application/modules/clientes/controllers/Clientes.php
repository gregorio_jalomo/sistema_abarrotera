<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Clientes extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproductos', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 6;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

/* 
    if(validar_permiso($this->rol_usuario,$this->id_modulo,26) || $this->is_admin ){

      //code
    
    }else{
      $this->p_denied_view("listar clientes","");
    }
*/

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,26) || $this->is_admin ){

      $data['titulo_seccion'] = "Clientes";
      $data['flecha_ir_atras'] = 'clientes/listar_clientes';

      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      $data['CFDI'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescfdi');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar clientes","");
    }


  }//...alta


  public function catalogos(){
    //clientes_catalogo->listar
    if(validar_permiso($this->rol_usuario,30,129) || $this->is_admin ){

      $data['titulo_seccion'] = "";
      $data['flecha_ir_atras'] = 'clientes/listar_clientes';

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/catalogos',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar clientes","clientes/listar_clientes");
    }


  }//...alta
  public function guardar(){
    $this->form_validation->set_rules('cliente_nombre', 'cliente_nombre', 'required');
    $this->form_validation->set_rules('cliente_cfdi', 'cliente_cfdi', 'required');

    $this->form_validation->set_rules('cliente_calle', 'cliente_calle', 'required');
    $this->form_validation->set_rules('cliente_no_exterior', 'cliente_no_exterior', 'required');
    $this->form_validation->set_rules('cliente_colonia', 'cliente_colonia', 'required');
    $this->form_validation->set_rules('cliente_cp', 'cliente_cp', 'required');
    $this->form_validation->set_rules('cliente_ciudad', 'cliente_ciudad', 'required');
    $this->form_validation->set_rules('cliente_municipio', 'cliente_municipio', 'required');
    $this->form_validation->set_rules('cliente_estado', 'cliente_estado', 'required');
    $this->form_validation->set_rules('cliente_pais', 'cliente_pais', 'required');
    // $this->form_validation->set_rules('clientecredito_id', 'clientecredito_id', 'required');
    $this->form_validation->set_rules('cliente_latitud', 'cliente_latitud', 'required');
    $this->form_validation->set_rules('cliente_longitud', 'cliente_longitud', 'required');

    $response = validate($this);
    if($response['status']){
      $data['cliente_nombre'] = $this->input->post('cliente_nombre');
      $data['id_clientecfdi'] = $this->input->post('cliente_cfdi');

      $data['cliente_calle'] = $this->input->post('cliente_calle');
      $data['cliente_no_interior'] = $this->input->post('cliente_no_interior');
      $data['cliente_no_exterior'] = $this->input->post('cliente_no_exterior');
      $data['cliente_colonia'] = $this->input->post('cliente_colonia');
      $data['cliente_cp'] = $this->input->post('cliente_cp');
      $data['cliente_ciudad'] = $this->input->post('cliente_ciudad');
      $data['cliente_municipio'] = $this->input->post('cliente_municipio');
      $data['cliente_estado'] = $this->input->post('cliente_estado');
      $data['cliente_pais'] = $this->input->post('cliente_pais');

      $data['cliente_latitud'] = $this->input->post('cliente_latitud');
      $data['cliente_longitud'] = $this->input->post('cliente_longitud');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");
      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->save_register('clientes', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar

  public function ver_detalle($cliente_id=false){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,28) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del cliente";
      $data['flecha_ir_atras'] = "clientes/clientes/listar_clientes";
      
      $data['clientes'] = $this->Mgeneral->get_row("cliente_id",$cliente_id,'clientes');
      $data['CFDI'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescfdi');


      $data['estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/ver_detalle',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar clientes","clientes/clientes/listar_clientes");
    }

  }//...ver_detalle

  public function guardarDetallePaquete(){
    $this->form_validation->set_rules('cliente_id', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_nombre', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_calle', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_colonia', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_no_exterior', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_no_interior', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_ciudad', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_cp', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_municipio', 'Cliente', 'required');
    $this->form_validation->set_rules('cliente_pais', 'Cliente', 'required');

    $response = validate($this);
    if($response['status']){
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['cliente_id'] = $this->input->post('cliente_nombre');
      $data['cliente_id'] = $this->input->post('cliente_calle');
      $data['cliente_id'] = $this->input->post('cliente_colonia');
      $data['cliente_id'] = $this->input->post('cliente_no_exterior');
      $data['cliente_id'] = $this->input->post('cliente_no_interior');
      $data['cliente_id'] = $this->input->post('cliente_ciudad');
      $data['cliente_id'] = $this->input->post('cliente_cp');
      $data['cliente_id'] = $this->input->post('cliente_municipio');
      $data['cliente_id'] = $this->input->post('cliente_pais');

      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->save_register('clientes', $data);
    }
    echo json_encode(array('output' => $response));
  }//...guardarPaquete

  public function listar_clientes($pagina_inicio = 1, $pagina_fin = 10){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,27) || $this->is_admin ){

      $data['titulo_seccion'] = "clientes";
      $data['con_clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1, 'clientes');
      $data['clientes_CFDI'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescfdi');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/listar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar clientes","");
    }

  }//...listar_clientes


  public function editar($id_cliente = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,29) || $this->is_admin ){

      $data['titulo_seccion'] = "Clientes";
      $data['flecha_ir_atras'] = "clientes/listar_clientes";

      $get_row = $this->Mgeneral->get_row('cliente_id',$id_cliente,'clientes');
      $data['CFDI'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescfdi');

      $data['clientes'] = $get_row;

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientes/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar clientes","clientes/listar_clientes");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('cliente_nombre', 'cliente_nombre', 'required');
    $this->form_validation->set_rules('cliente_cfdi', 'cliente_cfdi', 'required');

    $this->form_validation->set_rules('cliente_calle', 'cliente_calle', 'required');
    $this->form_validation->set_rules('cliente_no_exterior', 'cliente_no_exterior', 'required');
    $this->form_validation->set_rules('cliente_colonia', 'cliente_colonia', 'required');
    $this->form_validation->set_rules('cliente_cp', 'cliente_cp', 'required');
    $this->form_validation->set_rules('cliente_ciudad', 'cliente_ciudad', 'required');
    $this->form_validation->set_rules('cliente_municipio', 'cliente_municipio', 'required');
    $this->form_validation->set_rules('cliente_estado', 'cliente_estado', 'required');
    $this->form_validation->set_rules('cliente_pais', 'cliente_pais', 'required');

    $this->form_validation->set_rules('cliente_latitud', 'cliente_latitud', 'required');
    $this->form_validation->set_rules('cliente_longitud', 'cliente_longitud', 'required');

    $response = validate($this);
    if($response['status']){
      $data['cliente_id'] = $this->input->post('cliente_id');
      $data['cliente_nombre'] = $this->input->post('cliente_nombre');
      $data['id_clientecfdi'] = $this->input->post('cliente_cfdi');

      $data['cliente_calle'] = $this->input->post('cliente_calle');
      $data['cliente_no_interior'] = $this->input->post('cliente_no_interior');
      $data['cliente_no_exterior'] = $this->input->post('cliente_no_exterior');
      $data['cliente_colonia'] = $this->input->post('cliente_colonia');
      $data['cliente_cp'] = $this->input->post('cliente_cp');
      $data['cliente_ciudad'] = $this->input->post('cliente_ciudad');
      $data['cliente_municipio'] = $this->input->post('cliente_municipio');
      $data['cliente_estado'] = $this->input->post('cliente_estado');
      $data['cliente_pais'] = $this->input->post('cliente_pais');

      $data['cliente_latitud'] = $this->input->post('cliente_latitud');
      $data['cliente_longitud'] = $this->input->post('cliente_longitud');
      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->update_table_row('clientes',$data,'cliente_id',$data['cliente_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar


  public function cambiar_estatus($cliente_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;

    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,49) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,50) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['cliente_id'] = $cliente_id;
      $this->Mgeneral->update_table_row('clientes',$data,'cliente_id',$data['cliente_id']);   

    }
    echo json_encode($go);

  }//...cambiar_estatus
}
