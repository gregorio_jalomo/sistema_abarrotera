<div class="">
<div class="content-header">

  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content" >
    <div class="container-fluid" >
      <!-- Info boxes -->
      <div class="row" >

        <!-- /.col -->

        <!-- /.col -->

        <!-- fix for small devices only -->






      </div>
      <div class="content-header">
        <a href="#"></a>
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Catálogos Clientes</h1>
              </div><!-- /.col -->

            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content" >
          <div class="container-fluid" >
            <!-- Info boxes -->
            <div class="row" >

              <!-- /.col -->

              <!-- /.col -->

              <!-- fix for small devices only -->



              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clienterfc/alta">


                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1">
                  <img height="60px" width="60px" src="<?php echo base_url()?>statics/tema/dist/img/rfc.png" alt=""> </span>

                  <div class="info-box-content">

                    <span class="info-box-text">RFC</span>
                    <span class="info-box-number"></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clientecfdi/alta">


                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"> </span>


                  <div class="info-box-content">

                    <span class="info-box-text">CFDI</span>



                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->




              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clientecredito/alta">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"> <i class="far fa-credit-card"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Crédito</span>
                    <span class="info-box-number"></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->






              <!-- /.col -->



              <!-- /.col -->

              <!-- /.col -->


              <!-- /.col -->

              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clientecorreo/alta">


                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-at"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Correo</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clientetelefono/alta">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-phone-alt"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Teléfono</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo base_url()?>index.php/catalogos/clientelugar/listar_lugares">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-map-marker-alt"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Lugares</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div><!--  -->

            </div>
              </div>
              </div></div>

                      <!-- /.col -->
              <br>
<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');
  });
</script>
