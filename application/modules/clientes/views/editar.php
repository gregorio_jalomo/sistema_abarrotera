<?php
  $cliente_id = $clientes->cliente_id;
  $cliente_nombre = $clientes->cliente_nombre;
  $cliente_cfdi = $clientes->id_clientecfdi;

  $cliente_calle = $clientes->cliente_calle;
  $cliente_no_interior = $clientes->cliente_no_interior;
  $cliente_no_exterior = $clientes->cliente_no_exterior;
  $cliente_colonia = $clientes->cliente_colonia;
  $cliente_cp = $clientes->cliente_cp;
  $cliente_ciudad = $clientes->cliente_ciudad;
  $cliente_municipio = $clientes->cliente_municipio;
  $cliente_estado = $clientes->cliente_estado;
  $cliente_pais = $clientes->cliente_pais;
  $clientecredito_id = $clientes->clientecredito_id;
  $cliente_latitud = $clientes->cliente_latitud;
  $cliente_longitud = $clientes->cliente_longitud;

?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_cliente').submit(function(event){
    event.preventDefault();
    console.log($('#cliente_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/clientes/actualizar";
    var cfdi= $('#CFDI').val();
    ajaxJson(url,{
        'cliente_id' : '<?php echo $cliente_id; ?>',
        'cliente_nombre' : $('#cliente_nombre').val(),
        'cliente_cfdi' :(cfdi==-1)?"":cfdi,

        'cliente_calle' : $('#cliente_calle').val(),
        'cliente_no_interior' : $('#cliente_no_interior').val(),
        'cliente_no_exterior' : $('#cliente_no_exterior').val(),
        'cliente_colonia' : $('#cliente_colonia').val(),
        'cliente_cp' : $('#cliente_cp').val(),
        'cliente_ciudad' : $('#cliente_ciudad').val(),
        'cliente_municipio' : $('#cliente_municipio').val(),
        'cliente_estado' : $('#cliente_estado').val(),
        'cliente_pais' : $('#cliente_pais').val(),
        'clientecredito_id' : $('#clientecredito_id').val(),
        'cliente_latitud' : $('#cliente_latitud').val(),
        'cliente_longitud' : $('#cliente_longitud').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/clientes/listar_clientes");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Cliente</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_cliente">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-8">
                  <label for="">Nombre</label>
                  <input type="text" class="form-control" name="cliente_nombre" id="cliente_nombre" value="<?php echo $cliente_nombre; ?>">
                </div>
                <div class="col-md-4">
                    <label for="">CFDI:</label>
                    <select name="CFDI" id="CFDI" 
                    class="form-control select2 select-CFDI">
                    <option value="-1">-Seleccione CFDI-</option>
                    <?php
                      foreach ($CFDI as $row) {
                        $selected="";
                        if($cliente_cfdi==$row->clientecfdi_id){
                          $selected="selected='selected'";
                        }
                          $a="(".$row->clientecfdi_clave.")".$row->clientecfdi_descripcion;
                          echo 
                            '<option value="'.$row->clientecfdi_id.'" '.$selected.' >'.$a.' </option>
                          ';
                        }
                    ?>
                    </select>
                  </div> 

              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Calle</label>
                  <input type="text" class="form-control" name="cliente_calle" id="cliente_calle" value="<?php echo $cliente_calle; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. Interior</label>
                  <input type="number" class="form-control" name="cliente_no_interior" id="cliente_no_interior" value="<?php echo $cliente_no_interior; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. exterior</label>
                  <input type="number" class="form-control" name="cliente_no_exterior" id="cliente_no_exterior" value="<?php echo $cliente_no_exterior; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Colonia</label>
                  <input type="text" class="form-control" name="cliente_colonia" id="cliente_colonia" value="<?php echo $cliente_colonia; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-2">
                  <label for="">Código Postal</label>
                  <input type="text" class="form-control" name="cliente_cp" id="cliente_cp" value="<?php echo $cliente_cp; ?>">
                </div>
                <div class="col-md-5">
                  <label for="">Ciudad</label>
                  <input type="text" class="form-control" name="cliente_ciudad" id="cliente_ciudad" value="<?php echo $cliente_ciudad; ?>">
                </div>
                <div class="col-md-5">
                  <label for="">Municipio</label>
                  <input type="text" class="form-control" name="cliente_municipio" id="cliente_municipio" value="<?php echo $cliente_municipio; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Estado</label>
                  <input type="text" class="form-control" name="cliente_estado" id="cliente_estado" value="<?php echo $cliente_estado; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">País</label>
                  <input type="text" class="form-control" name="cliente_pais" id="cliente_pais" value="<?php echo $cliente_pais; ?>">
                </div>

              </div>

              <div class="row">
                  <input id="cliente_latitud" name="cliente_latitud" type="hidden" class="form-control cc-exp form-control-sm" value="<?php echo $cliente_latitud; ?>">
                  <input id="cliente_longitud" name="cliente_longitud" type="hidden" class="form-control cc-exp form-control-sm" value="<?php echo $cliente_longitud; ?>">
                <div class="col-12">
                  <div class="form-group">
                    <label for="">Ubicación</label>
                    <div id="mapa-direccion" style="height: 350px; "></div>
                    <img class="hidden" id="imagen_mapa" src="<?php echo base_url();?>ubicacion.png" style="visibility: hidden;" />
                  </div>
                </div>
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/clientes/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');
    $('#clientecredito_id').val('<?= $clientecredito_id; ?>');
    $('#clientecredito_id>option:selected').text();
  });
</script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAHHWSkUcqyeERY0E2KCxx_Fggbo1BZN5I"></script>
<script type="text/javascript">
  var geoCode = new google.maps.Geocoder();
  function initialize() {

    <?php //if($negocios->negociocliente_Latitud == '' && $negocios->negociocliente_Longitud == ''): ?>
            var latLng = new google.maps.LatLng(19.264146,-103.718579);
    <?php //else: ?>
            //var latLng = new google.maps.LatLng('','');
    <?php //endif; ?>
    var markerImage = new google.maps.MarkerImage($("#imagen_mapa").attr('src'),
                                                          new google.maps.Size(34, 32),
                                                          new google.maps.Point(0, 0),
                                                          new google.maps.Point(0, 32));
    var map = new google.maps.Map(document.getElementById('mapa-direccion'),{
                zoom: 15,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
    var marker = new google.maps.Marker({
                                        position: latLng,
                                        title: 'Seleccione su ubicacion',
                                        map: map,
                                        icon: markerImage,
                                        draggable: true
                                      });

      //ACTUALIZAR LA INFORMACION DE LA POSICION ACTUAL
      updateMarkerPosition(latLng);
      geocodePosition(latLng);

      //AGREGAR EL EVENTO DE ARRASTRAR LOS DATOS
      google.maps.event.addListener(marker, 'dragstart', function(){
          updateMarkerAddress('...');
      });

      google.maps.event.addListener(marker, 'drag', function(){
          updateMarkerStatus('.....');
          updateMarkerPosition(marker.getPosition());
      });

      google.maps.event.addListener(marker, 'dragend', function(){
          updateMarkerStatus('....');
          geocodePosition(marker.getPosition());
      });
  }
    /**
     * METODO QUE SE USA PARA EL CODIGO
     * DE LA POSICION
     **/
    function geocodePosition(pos){
      geoCode.geocode({
        latLng: pos
        }, function(responses){
            if(responses && responses.lenght > 0){
              updateMarkerAddress(responses[0].formatted_address);
            } else {
              updateMarkerAddress('');
            }
          }
      );
    }
    /**
     * ACTUALIZAR EL DATO DE LOS MARKERS
     * EN EL GOOGLE MAPS
     **/
    function updateMarkerStatus(str){
        //document.getElementById('markerStatus').innerHTML = str;
    }

    /**
     * ACTUALIZA LA POSICION DEL MARCADOR PARA
     * QUE CAPTURAR SU cliente_LATITUD Y cliente_LONGITUD
     **/
    function updateMarkerPosition(latLng){
        var uno = latLng.lng();
        var dos = latLng.lat();
        document.getElementById('cliente_longitud').value = uno;
        document.getElementById('cliente_latitud').value = dos;
       // alert('hola: ' + latLng.lng())
        /*document.getElementById('info').innerHTML = [
            latLng.lat(),
            latLng.lng()
            ].join(', ');*/
    }

    /**
     * ACTUALIZA LA DIRECCION DEL MARCADOR PARA
     * MOSTRARLA LA USUARIO
     **/
    function updateMarkerAddress(str){
        //document.getElementById('address').innerHTML = str;
    }

    //EVENTO ONLOAD PARA ACTIVAR LA APLICACION
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
