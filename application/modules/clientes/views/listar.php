<script>
$(document).ready(function(){
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });

                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">clientes registrados</h3>
    </div>
    <div class="col-md-6">
      <a href="<?php echo base_url().'index.php/clientes/clientes/catalogos'?>"><button class="btn btn-dark float-right"><i class="fas fa-archive"></i> Catálogos Cliente</button></a>
      <a href="<?php echo base_url().'index.php/clientes/clientes/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar cliente</button></a>

    </div>

    </div>
    <!-- /.card-header -->
    <div class="card-body">

      <table id="example1" class="table table-bordered table-striped">
        <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Nombre Comecial</th>
        <th>CFDI</th>
        <th>Fecha de Registro</th>
        <th>Opciones</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($con_clientes as $clientes) {
        $cliente_id = $clientes->cliente_id;
        $cliente_nombre = $clientes->cliente_nombre;
        $cliente_cfdi = $clientes->id_clientecfdi;

        $cliente_calle = $clientes->cliente_calle;
        $cliente_no_interior = $clientes->cliente_no_interior;
        $cliente_no_exterior = $clientes->cliente_no_exterior;
        $cliente_colonia = $clientes->cliente_colonia;
        $cliente_cp = $clientes->cliente_cp;
        $cliente_ciudad = $clientes->cliente_ciudad;
        $cliente_municipio = $clientes->cliente_municipio;
        $cliente_estado = $clientes->cliente_estado;
        $cliente_pais = $clientes->cliente_pais;
        $clientecredito_id = $clientes->clientecredito_id;
        $cliente_latitud = $clientes->cliente_latitud;
        $cliente_longitud = $clientes->cliente_longitud;
        $fecha_registro = $clientes->fecha_registro;
        $cat_estatus_id = $clientes->cat_estatus_id;
        
        foreach ($clientes_CFDI as $row) {

          if($cliente_cfdi==$row->clientecfdi_id){
            $CFDI="(".$row->clientecfdi_clave.")".$row->clientecfdi_descripcion;
          }else{
            $CFDI="no guardado";
          }
        
          }

        echo
        '<tr id="borrar_'.$cliente_id.'">
          <td>'.$cliente_nombre.'</td>
          <td>'.$CFDI.'</td>

          <td>'.$fecha_registro.'</td>
          <td>
          <a href="'.base_url().'index.php/clientes/clientes/ver_detalle/'.$cliente_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
            <a href="'.base_url().'index.php/clientes/clientes/Editar/'.$cliente_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>

              <a href="'.base_url().'index.php/clientes/clientes/cambiar_estatus/'.$cliente_id.'/3" class="eliminar_relacion" flag="'.$cliente_id.'" id="delete'.$cliente_id.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>
<script>
  $(document).ready(function() {

    $('#menuClientes').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  });

</script>
