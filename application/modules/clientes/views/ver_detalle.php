<?php
$cliente_id = $clientes->cliente_id;
$cliente_nombre = $clientes->cliente_nombre;

$cliente_cfdi = $clientes->id_clientecfdi;
foreach ($CFDI as $cfdi) {

  if($cliente_cfdi==$cfdi->clientecfdi_id){
    $CFDI="(".$cfdi->clientecfdi_clave.")".$cfdi->clientecfdi_descripcion;
  }else{
    $CFDI="no guardado";
  }

  }
$cliente_calle = $clientes->cliente_calle;
$cliente_no_interior = $clientes->cliente_no_interior;
$cliente_no_exterior = $clientes->cliente_no_exterior;
$cliente_colonia = $clientes->cliente_colonia;
$cliente_cp = $clientes->cliente_cp;
$cliente_ciudad = $clientes->cliente_ciudad;
$cliente_municipio = $clientes->cliente_municipio;
$cliente_estado = $clientes->cliente_estado;
$cliente_pais = $clientes->cliente_pais;

$cliente_latitud = $clientes->cliente_latitud;
$cliente_longitud = $clientes->cliente_longitud;
$fecha_registro = $clientes->fecha_registro;
$cat_estatus_id = $clientes->cat_estatus_id;

  // var_dump($clientes);
?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> cliente: <?php echo $cliente_nombre; ?></h5>
          ID # <?php echo $cliente_id; ?>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-6 mb-6">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-12 border-right">
                  <!-- title row -->
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">cliente</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">

                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">CFDI</li>
                        <li><?php echo $CFDI; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Calle</li>
                        <li><?php echo $cliente_calle; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Número Exterior</li>
                        <li><?php echo $cliente_no_exterior; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->


                    <div class="col-sm-2">
                      <ul>
                        <li class="font-weight-bolder">Número Interior</li>
                        <li><?php echo $cliente_no_interior; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 ">
                      <ul>
                        <li class="font-weight-bolder">Colonia</li>
                        <li><?php echo $cliente_colonia; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Municipio</li>
                        <li><?php echo $cliente_municipio; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->

                  <div class="row">
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Estado</li>
                        <li><?php echo $cliente_estado; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">País</li>
                        <li><?php echo $cliente_pais; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-5">
                      <ul>
                        <li class="font-weight-bolder">Fecha de Registro</li>
                        <li><?php echo $fecha_registro; ?></li>
                      </ul>
                    </div>

                    <!-- /.col -->



            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->

          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {


      $('#menuclientes').addClass('active-link');



    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
    $('#tipos-de-precio').html(tbody);

    const valueCostoReal = '<?php echo $cliente_costo_real; ?>';

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });
  });
</script>

<script>
  $('document').ready(function() {
    $('#menuclientes').addClass('active-link');
  });
</script>
