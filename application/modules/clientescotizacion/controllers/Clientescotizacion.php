<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Clientescotizacion extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mclientescotizacion', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('clientescotizacion/Clientescotizacion_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 9;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;
 
    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

/*       
    if(validar_permiso($this->rol_usuario,$this->id_modulo,44) || $this->is_admin ){

	    //code

    }else{
      $this->p_denied_view("editar cotizaciones(clientes)","");
    }
*/

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,41) || $this->is_admin ){

      //recibir post del ajax
      $dataClientescotizacion['folio_documento'] = $this->input->post('nuevo_folio_documento');
      $dataClientescotizacion['folio_unico_documento'] = $this->input->post('nuevo_FUNI_documento');
      $dataClientescotizacion['perfil_usuario'] = $this->session->userdata['infouser']['id'];
      $dataClientescotizacion['descripcion_id_usuario'] = $this->session->userdata['infouser']['nombre'];

      $foliosdocumentos_id= $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI= $this->input->post('id_FUNI_foliosdocumentos');


      //insert a clientescotizacion
      $clientescotizacion_id=$this->guardar($dataClientescotizacion);

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que una cotizacion se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }
      echo json_encode($clientescotizacion_id);

    }else{
      echo json_encode(false);
    }

  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('clientescotizacion', $data);
    //  echo $response;
    //echo json_encode(array('output' => $response));
  }//...guardar

  public function editar($id_cotizacion){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,44) || $this->is_admin ){

      $data['titulo_seccion']   = "Cotizacion de cliente";
      $data['flecha_ir_atras']  = "clientescotizacion/listar_clientescotizacion";
      $data['force_landscape']  =true;
      
      $data['clientescotizacion'] = $this->Clientescotizacion_model->get_clientescotizacion_by_id($id_cotizacion);
      $data['clientescondicion'] =$this->Clientescotizacion_model->get_clientes_condicion($data['clientescotizacion']->id_clientes);

      //$data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['productos'] = $this->Clientescotizacion_model->get_productos('productos.cat_estatus_id',1);

      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      $data["fecha_creacion"]         = date("Y-m-d H:i:s");
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js'
      
      );

      $this->cargar_vista('clientescotizacion/editar',$data,$archivos_js);

    }else{
      $this->p_denied_view("editar cotizaciones(clientes)","clientescotizacion/listar_clientescotizacion");
    }

  }//...editar

  public function ver_detalle($id_clientescotizacion){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,43) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de la cotizacion";
      $data['flecha_ir_atras'] = "clientescotizacion/clientescotizacion/listar_clientescotizacion";

      $data['clientescotizacion'] = $this->Clientescotizacion_model->get_clientescotizacion_by_id($id_clientescotizacion);
      $data['clientescotizacion_detalle'] =$this->Clientescotizacion_model->get_clientescotizacion_detalle($id_clientescotizacion);

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientescotizacion/ver_detalle',$data,$archivos_js);

    }else{
      $this->p_denied_view("ver detalle de cotizaciones(clientes)","clientescotizacion/clientescotizacion/listar_clientescotizacion");
    }

  }//...ver_detalle

  public function listar_clientescotizacion(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,42) || $this->is_admin ){

      $data['titulo_seccion'] = "Cotizaciónes de Clientes ";
      $data['con_clientescotizacion'] = $this->Clientescotizacion_model->listar_clientescotizacion();
      $data['force_landscape']=true;

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('clientescotizacion/listar',$data,$archivos_js);

    }else{
      $this->p_denied_view("listar cotizaciones(clientes)","");
    }

  }//...listar_clientescotizacion

  public function get_clientescotizacion_detalle($id_clientescotizacion){
    echo json_encode($this->Clientescotizacion_model->get_clientescotizacion_detalle($id_clientescotizacion));

  }//...get_clientescotizacion_detalle

  public function get_clientespedido_by_FUNI($folio_unico_documento){
    echo json_encode($this->Clientescotizacion_model->get_clientespedido_by_FUNI($folio_unico_documento));
  }//...get_clientespedido_by_FUNI

  public function get_info_cliente(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Clientescotizacion_model->get_info_cliente($cliente_id));
  }//...get_info_cliente

  public function get_condicion_pago_by_cliente_id(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Clientescotizacion_model->get_clientes_condicion($cliente_id));
  }//...get_condicion_pago_by_cliente_id

  public function cambiar_status($id_cotizacion,$estatus_id) {
    $data['status'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,44) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,45) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
      $this->Mgeneral->update_table_row('clientescotizacion',$data,'id',$id_cotizacion);  

    }
    echo json_encode($go);

  }//...cambiar_status

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_cliente_nombre_domicilio(){
    $id_cotizacion=$this->input->post('id_cotizacion');
    $data['id_clientes']=$this->input->post('id_clientes');
    $data['nombre_cliente']=$this->input->post('nombre_cliente');
    $data['domicilio_cliente']=$this->input->post('domicilio_cliente');
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    $this->Mgeneral->update_table_row("clientescotizacion",$data,"id",$id_cotizacion);
  }//...update_cliente_nombre_domicilio

  public function update_info_agentesventa(){
    $data['id_agentes_venta']=$this->input->post('agenteventa_id');
    $data['nombre_agentes_venta']=$this->input->post('nombre_agentes_venta');
    $id_cotizacion=$this->input->post('id_cotizacion');
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    echo json_encode($this->Mgeneral->update_table_row("clientescotizacion",$data,"id",$id_cotizacion));

  }//...update_info_agentesventa

  public function update_info_clientes_condicion(){
    $data['id_clientes_condicion']=$this->input->post('id_clientes_condicion');
    $data['descripcion_clientes_condicion']=$this->input->post('descripcion_clientes_condicion');
    $id_cotizacion=$this->input->post('id_cotizacion');
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    echo json_encode($this->Mgeneral->update_table_row("clientescotizacion",$data,"id",$id_cotizacion));

  }//...update_info_clientes_condicion

  public function agregar_producto(){

    $this->form_validation->set_rules('id_clientescotizacion', ' ID de la cotización', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    $this->form_validation->set_rules('costo_unitario', ' Precio del producto', 'required');
    $this->form_validation->set_rules('costo_total', ' Importe', 'required');
    
    $id_clientescotizacion=$this->input->post('id_clientescotizacion');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total');

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    if($this->Clientescotizacion_model->buscar_producto_cCotizacion_detalle($id_clientescotizacion,$id_producto)){

      $dataUpdate['id_clientescotizacion']  = $id_clientescotizacion;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      $dataUpdate['costo_unitario']         = $costo_unitario;
      $dataUpdate['costo_total']            = $costo_total;

      echo json_encode($this->Clientescotizacion_model->actualizar_producto_cCotizacion_detalle($dataUpdate));

    }else{

    $dataAdd['id_clientescotizacion']  = $id_clientescotizacion;
    $dataAdd['id_producto']            = $id_producto;
    $dataAdd['nombre_producto']        = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']   = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']               = $cantidad;
    $dataAdd['costo_unitario']         = $costo_unitario;
    $dataAdd['IVA']                    = $this->input->post('IVA');
    $dataAdd['IEPS']                   = $this->input->post('IEPS');
    $dataAdd['costo_total']            = $costo_total;

    echo json_encode($this->Clientescotizacion_model->agregar_producto_cCotizacion_detalle($dataAdd));
    }
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_clientescotizacion);
  }//...agregar_producto

  public function eliminar_producto($id_cotizacion,$id_producto){
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    echo json_encode($this->Clientescotizacion_model->eliminar_producto_cCotizacion_detalle($id_cotizacion,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Clientescotizacion_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_cotizacion=$this->input->post('id_cotizacion');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Clientescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    $this->Mgeneral->update_table_row('clientescotizacion',$data,'id',$id_cotizacion);

  }//...agregar_comentarios

  public function update_costo_by_condicion_pago($id_clientescotizacion,$id_clientescondicion){
      echo json_encode($this->Clientescotizacion_model->update_costo_by_condicion_pago($id_clientescotizacion,$id_clientescondicion));
  }//...update_costo_by_condicion_pago

  public function calcular_totales($id_clientescotizacion){
    echo json_encode($this->Clientescotizacion_model->calcular_totales($id_clientescotizacion));
  }//...calcular_totales

  public function get_fecha_modificacion(){
    $id_cotizacion=$this->input->post('id_cotizacion');
    echo json_encode($this->Clientescotizacion_model->get_fecha_modificacion($id_cotizacion));
  }//...get_fecha_modificacion

}
