<?php
/**

 **/
class Clientescotizacion_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        
    }

    public function listar_clientescotizacion(){
        $db=$this->db;
        $db->select("clientescotizacion.*,cat_estatus.cat_estatus_nombre");
        $db->from("clientescotizacion");
        $db->join("cat_estatus","clientescotizacion.status=cat_estatus.cat_estatus_id");
        $db->where_in("status",[4,5,6]);
        $db->order_by("fecha_creacion","DESC");
        return $db->get()->result();

    }//...listar_clientescotizacion

    public function get_clientescotizacion_by_id($id_clientescotizacion){
        $db=$this->db;
        $db->select("clientescotizacion.*,cat_estatus.cat_estatus_nombre");
        $db->from("clientescotizacion");
        $db->join("cat_estatus","clientescotizacion.status=cat_estatus.cat_estatus_id");
        $db->where("id",$id_clientescotizacion);
        return $db->get()->row();
    }//...get_clientescotizacion_by_id

    public function get_clientescotizacion_detalle($id_clientescotizacion){
        $result=$this->db
                ->select("clientescotizacion_detalle.*, codigosbarras.codigobarra_codigo, productos.familia_id, familia.familia_tipo")
                ->join("productos","clientescotizacion_detalle.id_producto = productos.producto_id")
                ->join("familia","productos.familia_id = familia.familia_id")
                ->join("codigosbarras","clientescotizacion_detalle.id_producto = codigosbarras.producto_id")
                ->where("id_clientescotizacion",$id_clientescotizacion)
                ->order_by("id","DESC")
                ->get("clientescotizacion_detalle")->result();
        return $result;
    }//...get_clientescotizacion_detalle
    
    public function get_clientescotizacion_detalle_no_join($id_clientescotizacion){

        $result=$this->db
                ->where("id_clientescotizacion",$id_clientescotizacion)
                ->order_by("id","DESC")
                ->get("clientescotizacion_detalle")->result();
        return $result;
    }//...get_clientescotizacion_detalle_no_join

    public function get_clientespedido_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->select("clientespedido.*,cat_estatus.cat_estatus_nombre");
        $db->from("clientespedido");
        $db->join("cat_estatus","clientespedido.status=cat_estatus.cat_estatus_id");
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get()->row();
    }//...get_clientespedido_by_FUNI

    public function get_clientes_condicion($cliente_id){
        /* SELECT clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion 
            FROM `clientescondicion`
            join tipodeprecio ON clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id
            join metodopago ON clientescondicion.metodopago_id= metodopago.metodopago_id
            WHERE clientescondicion.cat_estatus_id='1' */
        $db=$this->db;
        $db->select("clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion");
        $db->from("clientescondicion");
        $db->join("tipodeprecio","clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id");
        $db->join("metodopago","clientescondicion.metodopago_id= metodopago.metodopago_id");
        $db->where("clientescondicion.cat_estatus_id",1);
        $db->where("clientescondicion.cliente_id",$cliente_id);

        return $db->get()->result();
    }//...get_clientes_condicion

    public function get_info_cliente($cliente_id){
        /* SELECT *
            FROM clientes
            WHERE cliente_id = $cliente_id*/
        $db=$this->db;
        $db->where("cliente_id",$cliente_id);
        
        return $db->get("clientes")->row();

    }//...get_info_cliente

    public function get_porcentaje_tipo_precio($id_clientescondicion){
        /* SELECT tipodeprecio_porcentaje 
            FROM `tipodeprecio`
            WHERE tipodeprecio_id=
                (
                    SELECT clientescondicion.tipodeprecio_id FROM clientescondicion WHERE clientescondicion.clientescondicion_id=12
                ) 
        */
        $db=$this->db;
        $db->select("tipodeprecio_porcentaje");
        $db->from("tipodeprecio");
        $db->where("tipodeprecio_id= 
                    (SELECT clientescondicion.tipodeprecio_id 
                        FROM clientescondicion 
                        WHERE clientescondicion.clientescondicion_id=$id_clientescondicion)"
                    );
        $porcentaje=$db->get()->row();
        $porcentaje=$porcentaje->tipodeprecio_porcentaje/100;

        return $porcentaje;
    }//...get_porcentaje_tipo_precio

    public function get_precio_producto($producto_id){
        $precio=$this->db->get_where("productos",array("producto_id"=>$producto_id))->row()->producto_costo_venta;
        return $precio;
    }//...get_precio_producto

    public function get_productos($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_cant_producto($id_pedido,$id_producto){

        $product_row=$this->get_existencias_producto($id_producto);

        $detalle_row = $this->db->select("cantidad")
                         ->where(array("id_clientescotizacion"=>$id_pedido,"id_producto"=>$id_producto))
                         ->get("clientescotizacion_detalle")->row();
        $arr=array(
            "existencias"=>round($product_row->producto_existencias, 3, PHP_ROUND_HALF_EVEN),
            "comprometidos"=>round($product_row->producto_existencias_comprometidas_pedido, 3, PHP_ROUND_HALF_EVEN),
            "por_comprar"=>round($product_row->producto_existencias_comprometidas_compras, 3, PHP_ROUND_HALF_EVEN),
        );
        if(isset($detalle_row)){
            $arr["cantidad"]=round($detalle_row->cantidad, 3, PHP_ROUND_HALF_EVEN);
            $arr["status"]=true;
        }else{
            $arr["status"]=false;
        }
        return $arr;

    }//...get_cant_producto

    public function buscar_producto_cCotizacion_detalle($id_clientescotizacion,$id_producto){
        $db=$this->db;
        $db->where("id_producto",$id_producto);
        $db->where("id_clientescotizacion",$id_clientescotizacion);

        return $db->get("clientescotizacion_detalle")->row();
    }//...buscar_producto_cCotizacion_detalle
      
    public function actualizar_producto_cCotizacion_detalle($dataUpdate){
        /* 
        $dataUpdate['id_clientescotizacion']  = $id_clientescotizacion;
        $dataUpdate['id_producto']            = $id_producto;
        $dataUpdate['cantidad']               = $cantidad;
        $dataUpdate['costo_unitario']         = $costo_unitario;
        $dataUpdate['costo_total']            = $costo_total; 
        */
        $db=$this->db;
        $id_cot=$dataUpdate['id_clientescotizacion'];
        $id_prod=$dataUpdate['id_producto'];
        return $db->update("clientescotizacion_detalle", $dataUpdate, 
                            array("id_clientescotizacion"=>$id_cot,"id_producto"=>$id_prod)
                          );
    }//...actualizar_producto_cCotizacion_detalle

    public function agregar_producto_cCotizacion_detalle($data){
        $agregado= $this->Mgeneral->save_register("clientescotizacion_detalle",$data);
        if($agregado){
            return true;
        }else{
            return false;
        }
    }//...agregar_producto_cCotizacion_detalle

    public function get_existencias_producto($id_producto){
        return $this->db->select("producto_existencias,producto_existencias_comprometidas_pedido,producto_existencias_comprometidas_compras")
                        ->where("producto_id",$id_producto)
                        ->get("productos")->row();
    }//...get_existencias_producto

    public function eliminar_producto_cCotizacion_detalle($id_clientescotizacion,$id_producto){
        
        $eliminado=$this->db->delete("clientescotizacion_detalle",
                         array("id_clientescotizacion"=>$id_clientescotizacion,"id_producto"=>$id_producto
                                ));
        if($eliminado){
            return true;
        }else{
            return false;
        }
    }//...eliminar_producto_cCotizacion_detalle

    public function update_costo_by_condicion_pago($id_clientescotizacion,$id_clientescondicion){

        $db=$this->db;
        //obtener porcentaje de tipodeprecio
        $porcentaje=$this->get_porcentaje_tipo_precio($id_clientescondicion);
        //obtener los productos de la cotizacion
        $productos=$this->get_clientescotizacion_detalle($id_clientescotizacion);//viene como result

        $return_productos=array();
        //a cada producto hacer update a costo_unitario e importe en base al nuevo porcentaje
        foreach ($productos as $producto) {
            $id_producto            = $producto->id_producto;
            $costo_venta_producto   = $this->get_precio_producto($producto->id_producto);
            $iva                    = ($producto->IVA)/100;
            $ieps                   = ($producto->IEPS)/100;
            $cantidad               = $producto->cantidad;

            $costo_con_condicion    = $costo_venta_producto + ($costo_venta_producto*$porcentaje);
            $costo_iva              = $costo_con_condicion * ($iva);
            $costo_ieps             = $costo_con_condicion * ($ieps);
            $costo_con_impuestos    = $costo_con_condicion + $costo_iva + $costo_ieps;
            $nuevo_importe          = ($costo_con_impuestos) * $cantidad;

            $data['costo_unitario']=$costo_con_condicion;
            $data['costo_total']=$nuevo_importe;
            
            $exec=$db->update("clientescotizacion_detalle", $data, array("id_clientescotizacion"=>$id_clientescotizacion,"id_producto"=>$id_producto));
            
            $return_productos[$id_producto]=array();
            $return_productos[$id_producto]['exec']=($exec==true) ? true : false;
            $return_productos[$id_producto]['costo_venta_producto']=$costo_venta_producto;
            $return_productos[$id_producto]['costo_con_condicion']=$costo_con_condicion;
            $return_productos[$id_producto]['costo_iva']=$costo_iva;
            $return_productos[$id_producto]['costo_ieps']=$costo_ieps;
            $return_productos[$id_producto]['costo_con_impuestos']=$costo_con_impuestos;
            $return_productos[$id_producto]['nuevo_importe']=$nuevo_importe;

        }
        return $return_productos;

    }//...update_costo_by_condicion_pago

    public function calcular_totales($id_clientescotizacion){
        $productos=$this->get_clientescotizacion_detalle_no_join($id_clientescotizacion);//viene como result
        
        $sum_subtotal=0;
        $sum_iva=0;
        $sum_ieps=0;
        $sum_impuestos=0;
        $sum_total=0;
        $rastreo=array();
        $cont=0;
        foreach ($productos as $producto) {
            $id_producto=$producto->id_producto;
            $rastreo[$id_producto]=array();
            $cantidad           = $producto->cantidad;
            $costo_unitario     = $producto->costo_unitario;
            $iva                = ($producto->IVA)/100;
            $ieps               = ($producto->IEPS)/100;

            $subtotal_producto  = round($cantidad*$costo_unitario, 4, PHP_ROUND_HALF_EVEN);
            $sum_subtotal       += $subtotal_producto;
            $costo_iva          = $subtotal_producto * ($iva);
            $costo_ieps         = $subtotal_producto * ($ieps);

            $impuestos_producto = round($costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $iva_producto       = round($costo_iva, 4, PHP_ROUND_HALF_EVEN);
            $ieps_producto      = round($costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $total_producto     = round($subtotal_producto + $costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);

            $sum_impuestos      += $impuestos_producto;
            $sum_iva            += $iva_producto;
            $sum_ieps           += $ieps_producto;
            $sum_total          += $total_producto;

            $rastreo[$id_producto]["costo_unitario"]=$costo_unitario;
            $rastreo[$id_producto]["cantidad"]=$cantidad;
            $rastreo[$id_producto]["subtotal_producto"]=$subtotal_producto;
            $rastreo[$id_producto]["impuestos_producto"]=$impuestos_producto;
            $rastreo[$id_producto]["iva_producto"]=$iva_producto;
            $rastreo[$id_producto]["ieps_producto"]=$ieps_producto;
            $rastreo[$id_producto]["total_producto"]=$total_producto;

            $cont++;
        }
        $totales=array(
            "rastreo"=>$rastreo,
            "subtotal"=>$sum_subtotal,
            "impuestos"=>$sum_impuestos,
            "iva"=>$sum_iva,
            "ieps"=>$sum_ieps,
            "total"=>$sum_total,
        );
        $this->actualizar_totales($id_clientescotizacion,$totales["subtotal"],$totales["iva"],$totales["ieps"],$totales["total"]);
        return $totales;
        /* SELECT 
            SUM(`costo_unitario`) as subtotal,
            SUM(`costo_total`) AS total,
            (SUM(`costo_total`)-SUM(`costo_unitario`)) as impuestos, 
            SUM(`costo_unitario`)*(AVG(`IVA`)/100) AS iva,
            SUM(`costo_unitario`)*(AVG(`IEPS`)/100) AS ieps 
            FROM `clientescotizacion_detalle` 
            return $this->db->query("SELECT 
                SUM(`costo_unitario`) as subtotal,
                SUM(`costo_total`) AS total,
                (SUM(`costo_total`)-SUM(`costo_unitario`)) as impuestos, 
                SUM(`costo_unitario`)*(AVG(`IVA`)/100) AS iva,
                SUM(`costo_unitario`)*(AVG(`IEPS`)/100) AS ieps 
                FROM `clientescotizacion_detalle`
                WHERE `id_clientescotizacion`=$id_clientescotizacion")->row();*/

    }//...calcular_totales

    public function actualizar_totales($id_clientescotizacion,$subtotal,$iva,$ieps,$total){
        $data["subtotal"]=$subtotal;
        $data["IVA"]=$iva;
        $data["IEPS"]=$ieps;
        $data["TOTAL"]=$total;

        return $this->Mgeneral->update_table_row("clientescotizacion",$data,"id",$id_clientescotizacion);
    }//...actualizar_totales

    public function actualizar_fecha_modificacion($id_cotizacion){
        $query="UPDATE `clientescotizacion` SET `fecha_modificacion` = CURRENT_TIMESTAMP WHERE `id` = '$id_cotizacion'";
           $this->db->query($query);
    }//...actualizar_fecha_modificado

    public function get_fecha_modificacion($id_cotizacion){
        $this->db->select("fecha_modificacion");
        $this->db->where("id",$id_cotizacion);
        $row_cotizacion=$this->db->get("clientescotizacion")->row();
        $timestamp=$row_cotizacion->fecha_modificacion;
        $fecha=date('d/m/Y,H:i',strtotime($timestamp));
        return $fecha;
    }//...get_fecha_modificacion
}
