<?php
//consultas Controller
/* $data['productos'];
$data['usuarios'];
$data['familia'];
$data['agentesventa'];
$data['clientes'];
$data['cat_estatus'];  */
//...consultas Controler


//echo json_encode($clientescotizacion);

//...datos_cotizacion
  $id_cotizacion              = $clientescotizacion->id;
  //$fecha_creacion             = explode(" ",$clientescotizacion->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
  //$fecha_modificacion         = explode(" ",$clientescotizacion->fecha_modificacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
  $fecha_creacion             = $clientescotizacion->fecha_creacion;
  $fecha_modificacion         = $clientescotizacion->fecha_modificacion;
  $folio_documento            = $clientescotizacion->folio_documento;
  $folio_unico_documento      = $clientescotizacion->folio_unico_documento;
  $comentario                 = $clientescotizacion->comentario;
  $status                     = $clientescotizacion->status;
  $subtotal                   = $clientescotizacion->subtotal;
  $IVA                        = $clientescotizacion->IVA;
  $IEPS                       = $clientescotizacion->IEPS;
  $TOTAL                      = $clientescotizacion->TOTAL;

  $id_agentes_venta           = $clientescotizacion->id_agentes_venta;
  $nombre_agentes_venta       = $clientescotizacion->nombre_agentes_venta;
  $descr_agentes_venta        = $clientescotizacion->descripcion_agentes_venta;

  $perfil_usuario             = $clientescotizacion->perfil_usuario;
  $descr_id_usuario           = $clientescotizacion->descripcion_id_usuario;

  $id_clientes                = $clientescotizacion->id_clientes;
  $nombre_cliente             = $clientescotizacion->nombre_cliente;
  $id_clientes_condicion      = $clientescotizacion->id_clientes_condicion;
  $domicilio_cliente          = $clientescotizacion->domicilio_cliente;
  $descr_clientes_condicion   = $clientescotizacion->descripcion_clientes_condicion;

  /* INNERS */

    $cat_estatus_nombre                = $clientescotizacion->cat_estatus_nombre;
  /* ...INNERS */
//...datos_cotizacion

//echo $this->db->last_query();
?>
<style>
  ::placeholder{
    color: black;
  }
  /* .main-sidebar {
    height: 109%;
  } */
  .text-center{
    text-align:center;
  }
  .div-as-br{
    font-size: 5px;
  }
  .btn{
    font-size: 13px;
  }
  /* datos status, FUNI,totales etc */
  .datos_cotizacion_right{
    padding-top:5px;
    max-width: 20%;
  }
  /* tabla */
  .datos_cotizacion_left{
    max-width: 80%
  }
  .mr-sm-2{
    margin-bottom: unset;
  }
  .label-center{
    text-align: center;
  }
  .div-info-cot{
    text-align-last: justify;
  }
  .div-info-cot-total{
    background-color: rgba(23,162,184,.3);
    border-radius: 5px;
  }
  .div-info-cot span{
    -webkit-text-stroke-width: thin;
    color:blue;
  }
  .card-hr-top-bold{
    border-top: 8px solid rgba(23,162,184,1);
    border-top-style: outset;
  }
  .card-hr-top{
    border-top: 1px solid rgba(23,162,184,1);
  }
  .card-hr-bottom{
    border-bottom: 1px solid rgba(23,162,184,1);
  }
  .btn-export-pdf{
    color: #fff!important;
    background-color: #d9534f!important;
    border-color: #d43f3a!important;
    font-size: 13px!important;
  }
  .btn-enviar-a-pedidos{
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
    font-size: 13px;
  }
  .card{
    margin-bottom: .5rem;
  }
  .card-header{
    padding: 0.1rem 1rem;
  }
  .card-title{
    text-align: center;
    float: none;
    width: 100%;
  }
  .card-body{
    padding-bottom: unset;
    padding-top: 1vh;
  }
  .content-header {
    padding: 1vh .8vw;
    height: 8vh;
  }
  .content-overflow {
    overflow-y: hidden;
    max-height: 100%;
  }
  .table-responsive {
    max-width: 100%;
    overflow-y: auto;
  }
  .pd-lft-0{
    padding-left:unset;
  }
  .pd-rgt-0{
    padding-right:unset;
  }

  .select2-clientes-container .select2-container .select2-dropdown,.select2-results,.select2-search--dropdown{
    width:260px;
  }
  .select2-clientes-container .select2-container--default .select2-selection--single .select2-selection__rendered {
    display:none;
  }
  .select2-container--default .select2-dropdown.select2-dropdown--below {
    width:260px!important;
  }

  .select2-search{
    padding:unset;
  }
  .context-menu-item{
    width: 80%;
  }
  .context-menu-item:hover{
    background-color: rgba(23,162,184,.3);
    color:black;
  }
  .context-menu-item span{
    font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
  }
  .context-menu-list {
    z-index: 500!important;
  }
  .hover-row:hover{
    background-color: rgba(23,162,184,.3);
  }
  .dataTables_wrapper{
    max-height: 350px;
  }
  .main-footer{
    margin-left: unset!important;
    text-align: center;
  }
  .dataTables_scrollBody{
    overflow-y:hidden!important;
  }
  thead tr {
    font-size: 17px;
    text-align: center;
  }
  .hidden-flex-btn{
    display: flex;
    background-color: transparent;
    border: transparent;
    color: transparent;
  }
  /* .select2-container--default .select2-dropdown--below{
    width:260px;
  }
  .select2-container--default .select2-selection--single{
    width:26px;
  }
  .select2-container--default .select2-selection__arrow{
    left:6px;
  }
  .select2-container--default .select2-selection__rendered{
    display:none;
  } */
</style>

<section class="content">
  <form action="" id="agregar_producto">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Cotización</h3>
            </div>
            <div class="card-body">
              <div class="form-row align-items-center">

                  <div class="col-4">
                    <!--Clientes-->
                    <div class="col-12 label-center">
                      <label class="mr-sm-2" for="cliente_nombre">Cliente:</label>
                      <div class="row">
                        <div class="col-10 pd-rgt-0">
                          <input type="text" placeholder="Seleccione cliente" disabled class="form-control" id="cliente_nombre" value="<?=$nombre_cliente?>">
                        </div>
                        <div class="col-1 pd-lft-0 select2-clientes-container">
                          <select class="form-control form_select_rt_autoupdate select2 select-clientes " name="id_clientes" id="id_clientes" data-left_input="cliente_nombre">
                            <option selected disabled value="-1">-Seleccione cliente-</option>
                            <?php
                                  foreach ($clientes as $row) {
                                    $selected="";
                                    if ($row->cliente_id==$id_clientes){
                                      $selected="selected='selected'";
                                    }
                                    echo
                                      '<option value="'.$row->cliente_id.'" '.$selected.' data-text="'.$row->cliente_nombre.'">'.$row->cliente_nombre.' </option>
                                    ';
                                  }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div><!--...Clientes-->
                  </div>

                  <div class="col-2 label-center">
                    <!--Condicion de pago-->
                    <div class="col-12 label-center">
                      <label class="mr-sm-2" for="descripcion_clientes_condicion">Condición pago:</label>
                      <div class="row">
                        <div class="col-10 pd-rgt-0">
                          <input type="text" placeholder="Seleccione condición" disabled class="form-control text-center" id="descripcion_clientes_condicion" value="<?=$descr_clientes_condicion?>">
                        </div>
                        <div class="col-1 pd-lft-0">
                          <select class="form-control form_select_rt_autoupdate" name="id_clientes_condicion" id="id_clientes_condicion" data-left_input="descripcion_clientes_condicion">
                            <option selected disabled>-Seleccione condición-</option>
                            <?php
                                  foreach ($clientescondicion as $row) {
                                    $selected="";
                                    if ($row->clientescondicion_id==$id_clientes_condicion){
                                      $selected="selected='selected'";
                                    }
                                    echo
                                      '<option value="'.$row->clientescondicion_id.'" '.$selected
                                      .' data-text="'.$row->metodopago_descripcion.'"'
                                      .' data-porcentaje="'.$row->tipodeprecio_porcentaje.'">'
                                      .$row->metodopago_descripcion
                                      .' </option>
                                    ';
                                  }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div><!--...Condicion de pago-->
                  </div>

                  <div class="col-4 label-center">
                    <!--Vendedores-->
                    <div class="col-12 label-center">
                      <label class="mr-sm-2" for="nombre_agentes_venta">Vendedor:</label>
                      <div class="row">
                        <div class="col-10 pd-rgt-0">
                          <input type="text" placeholder="Seleccione Vendedor" disabled class="form-control text-center" id="nombre_agentes_venta" value="<?=$nombre_agentes_venta?>">
                        </div>
                        <div class="col-1 pd-lft-0">
                          <select class="form-control form_select_rt_autoupdate" name="id_agentes_venta" id="id_agentes_venta" data-left_input="nombre_agentes_venta">
                            <option selected disabled>-Seleccione Vendedor-</option>
                            <?php
                                  foreach ($agentesventa as $row) {
                                    $selected="";
                                    if ($row->agenteventa_id==$id_agentes_venta){
                                      $selected="selected='selected'";
                                    }
                                    $agenteventa_fullname=$row->agenteventa_nombre." ".$row->agenteventa_a_paterno." ".$row->agenteventa_a_materno;
                                    echo
                                      '<option value="'.$row->agenteventa_id.'" '.$selected.' data-text="'.$agenteventa_fullname.'">'.$agenteventa_fullname.' </option>
                                    ';
                                  }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div><!--...Vendedores-->
                  </div>

                <div class="col-2 label-center">
                    <label class="mr-sm-2" for="">Usuario</label>
                    <input class="form-control text-center" type="text" readonly name="usuario" value="<?php echo $usuario = ($this->session->userdata['infouser']['nombre']);?>">
                </div>

              </div><!--...form-row-->

              <div class="form-row align-items-center">

                <div class="col-8 label-center">
                  <label class="mr-sm-2">Domicilio</label>
                  <input type="text"class="form-control" id="domicilio_cliente" readonly value="<?=$domicilio_cliente?>" placeholder="Domicilio no disponible">
                </div>

                <div class="col-2 my-1 label-center">
                  <label class="mr-sm-2" for="fecha_registro">Fecha creado</label>
                  <input type="text" class="form-control" id="fecha_registro" disabled name="fecha_registro" value=<?=date('d/m/Y,H:i',strtotime($fecha_creacion))?>>
                </div>
                <div class="col-2 my-1 label-center">
                  <label class="mr-sm-2" for="fecha_registro">Fecha modificado</label>
                  <input type="text" class="form-control" id="fecha_modificacion" disabled name="fecha_modificacion" value=<?=date('d/m/Y,H:i',strtotime($fecha_modificacion))?>>
                </div>

              </div>

              <br>
              <!-- <span class="context-menu-one btn btn-neutral">on_right_click</span> -->
            </div><!--...card-body-->

          </div>
        </div>
      </div>

      <div class="row"><!-- left column -->

        <div class="col-md-12"><!-- general form elements -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Productos</h3>
            </div>

            <div class="form-row">
              <div class="datos_cotizacion_left">
                <div class="card-body">

                  <div class="row">
                    <div class="col-4 label-center">
                      <label class="mr-sm-2" for="">Buscar producto:</label>
                          <select name="select_productos" id="select_productos" 
                          class="form-control select2 select-productos text-center">
                          <option value="-1"></option>
                          <?php
                            $last_id_prod=[];
                            $new_arr_prod=[];
                            foreach ($productos as $a) {
                              foreach($unidades as $unidad){//obtener descripcion de la unidad
                                if($unidad->unidad_id==$a->unidad_id){
                                  $descripcion_de_la_unidad=$unidad->unidad_tipo;
                                  $a->descr_unidad=$descripcion_de_la_unidad;
                                }
                              }
                              if(in_array($a->producto_id,$last_id_prod)){
                                $new_arr_prod[$a->producto_id]->codigobarra_codigo .= ",".$a->codigobarra_codigo;
                              }else{
                                $new_arr_prod[$a->producto_id]=$a;
                                array_push($last_id_prod,$a->producto_id);
                              }
                            }
                            foreach ($new_arr_prod as $producto) {
                                echo 
                                  "<option value='".$producto->codigobarra_codigo
                                  ."' data-id_producto='".$producto->producto_id
                                  ."' data-unidad='".$descripcion_de_la_unidad
                                  ."' data-existencia='".$producto->producto_existencias
                                  ."' data-precio='".$producto->producto_costo_venta
                                  ."' data-iva='".$producto->producto_iva
                                  ."' data-ieps='".$producto->producto_ieps
                                  ."' >".$producto->producto_nombre
                                  ." </option>";
                              }

                          ?>
                          </select>                 
                    </div>
                    <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">Cantidad</label>
                          <input class="form-control  text-center" type="text" autocomplete="off" name="cantidad" id="cantidad" value="">
                          
                      </div>
                  </div> <!-- //...row -->

                  <div class="row">

                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">Unidad</label>
                          <input class="form-control text-center" type="text" readonly name="unidad" id="unidad" value="">
                      </div>
                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">Existencia</label>
                          <input class="form-control text-center" type="text" readonly name="existencia" id="existencia" value="">
                      </div>
                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">Precio</label>
                          <input class="form-control text-center" type="text" autocomplete="off" name="precio" id="precio" value="">
                      </div>
                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">IVA</label>
                          <input class="form-control text-center" type="text" readonly name="iva" id="iva" value="">
                      </div>
                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">IEPS</label>
                          <input class="form-control text-center" type="text" readonly name="ieps" id="ieps" value="">
                      </div>

                      <div class="col-2 label-center">
                          <label class="mr-sm-2" for="">Importe</label>
                          <input class="form-control text-center" type="text" readonly name="importe" id="importe" value="">
                      </div>

                      
                  </div>
                  <br>

                  <div class="table-responsive p-0">
                    <table  class="table table-head-fixed text-nowrap" id="scrolltable" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Código</th>
                          <th>Nombre</th>
                          <th>Unidad</th>
                          <th>Cant.</th>
                          <th>Precio</th>
                          <th>IVA</th>
                          <th>IEPS</th>
                          <th>Importe</th>
                          <th>Familia</th>

                        </tr>
                      </thead>
                      <tbody id="tbody_productos">
 
                      </tbody>
                    </table>
                  </div>

                </div><!--...card-body-->
              </div>
              <div class="col-3 datos_cotizacion_right">
                <div class="card-info">
                  <div class="card-header">
                    <h3 class="card-title">Datos de la cotización</h3>
                  </div>
                  <div class="card-body">
                    <div class="form-row align-items-center">

                    <!--Status-->
                    <div class="col-12 label-center">
                      <label class="mr-sm-2" for="cat_estatus_nombre">Status:</label>
                      <div class="row">
                        <div class="col-10 pd-rgt-0">
                          <input type="text" disabled placeholder="Seleccione estatus" class="form-control text-center" id="cat_estatus_nombre" value="<?=$cat_estatus_nombre?>">
                        </div>
                        <div class="col-1 pd-lft-0">
                          <select class="form-control form_select_rt_autoupdate" name="status" id="status" data-left_input="cat_estatus_nombre">
                            <option selected disabled>-Seleccione estatus-</option>
                            <?php
                                  foreach ($cat_estatus as $row) {
                                    $selected="";
                                    if ($row->cat_estatus_id==$status){
                                      $selected="selected='selected'";
                                    }
                                    echo
                                      '<option value="'.$row->cat_estatus_id.'" '.$disabled.' '.$selected.' data-text="'.$row->cat_estatus_nombre.'">'.$row->cat_estatus_nombre.' </option>
                                    ';
                                  }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div><!--...Status-->

                    <div class="div-as-br">&nbsp</div>

                    <div class="col-12 div-info-cot card-hr-top" >Documento:  <span ><?=$folio_documento?></span></div>
                    <div class="col-12 div-info-cot card-hr-bottom" >Referencia:  <span ><?=$folio_unico_documento?></span></div>

                    <div class="div-as-br">&nbsp</div>

                    <div class="col-12">
                      <textarea id="comentarios" name="comentarios" class="form-control" placeholder="Comentarios..." ><?=$comentario?></textarea>
                    </div>

                      <!-- <div class="col-6 my-1">
                        <center><div id="exportar_a_pdf" class="btn btn-export-pdf"> <i class="fa fa-file-pdf"></i> PDF</div></center>
                      </div> -->
                      <div class="col-3"></div>
                      <div class="col-6 my-1">
                      <center><div id="enviar_a_pedidos" class="btn btn-enviar-a-pedidos"> Enviar</div></center>
                        <!-- <button type="button" id="enviar_a_pedidos" title="Enviar a pedidos" class="btn btn-success"> Enviar</button> -->
                      </div>
                      <div class="col-3"></div>
                      <div class="col-12 div-info-cot card-hr-top-bold" >
                              Subtotal:  $<span id="infocot-subtotal"></span>
                      </div>
                      <div class="col-12 div-info-cot" >IVA: $<span id="infocot-iva"></span></div>
                      <div class="col-12 div-info-cot" >IEPS: $<span id="infocot-ieps"></span></div>
                      <div class="col-12 div-info-cot div-info-cot-total card-hr-bottom" >
                              TOTAL:  $<span id="infocot-total"></span>
                      </div>

                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- ...general form elements -->

      </div>
    </div>
    <center><button type="submit" id="agregar_productos_form-btn" class="hidden-flex-btn"></button></center>
  </form>
</section>



<script>
$(document).ready(function(){
    $("#cargando").hide();
    $('#menuclientes').addClass('active-link');
    var status_ped=$("#status option:selected").val();
    if(status_ped==5 || status_ped==6){
      cerrarDocumento();
      var FUNI="<?=$folio_unico_documento?>";
      ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_clientespedido_by_FUNI/"+FUNI,
      {},
      "POST",
      "",
      function(result){
        //alert(result+"||"+typeof(result)+"||"+result.length);
        var pedido_existente=JSON.parse(result);
        //alert(JSON.stringify(pedido_existente,0,4));
        if(result != "null"){
          $("#status").attr("disabled","");
          $("#enviar_a_pedidos").removeAttr("id");
          ConfirmCustom("Esta cotización ya se ha enviado a pedidos anteriormente ("+pedido_existente.folio_documento+")",
                function(){
                  window.location.href=site_url+("/clientespedido/Clientespedido/editar/"+pedido_existente.id);
                }, "","Ir al pedido", "cerrar"
          ); 

        }
        
      });
      
    }else{
      //la cotizacion está abierta
      setTimeout(function(){init_contextMenu();},1000);//agregar menu contextual a los rows de datatables
    }

    $('.select_productos').select2({
        dropdownParent: $('.form-group'),
        placeholder: {
          id: '-1', // the value of the option
          text: '-Elige un producto-'
        },
        allowClear: true
    });

    $('.select-clientes').select2({
        dropdownParent: $('.form-group'),
        placeholder: "Elige un cliente...",
    });
    
    //counter almacena el numero de productos existentes en la cotizacion
    var productos_count=init_tabla_productos(<?=$id_cotizacion?>);
    var tabla_productos=$('#scrolltable').DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
                extend: 'pdfHtml5',
                download: 'open',
                title:'<?=$folio_documento?>',
                exportOptions: {
                      // Any other settings used
                      grouped_array_index: [8],
                      columns: [ 0,1,2,3,4,5,6,7]
                },
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        width:50,
                        height:52,
                        alignment: 'center',
                        image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAIFAfcDASIAAhEBAxEB/8QAHgABAAAGAwEAAAAAAAAAAAAAAAECAwQICQUGBwr/xABsEAABAwIDBAUEBRESCQoHAQABAAIDBBEFBgcIEiExCRNBUWEUInGBMmORotIVFxgjQlJXYnJ1kpWhsbKz0RYZJDM2NzhDVHN0gpSjtOHi4zQ1U3aFk6XB5CVFR1ZkZYSkwtMmJ0RGg8PUZv/EABwBAQACAwEBAQAAAAAAAAAAAAABAgMEBgUHCP/EAEMRAAIBAwIDAwgGCAYBBQAAAAABAgMEEQUSBiExE0FRIjJhcYGhsdEUFlORotIVMzVDUsHh8AcjJTRCcmIkNkSC8f/aAAwDAQACEQMRAD8A2i+Qe2+9TyD233qu0U7mRgtPIPbfep5B7b71XaJuYwWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibmMFp5B7b71PIPbfeq7RNzGC08g9t96nkHtvvVdom5jBaeQe2+9TyD233qu0TcxgtPIPbfep5B7b71XaJuYwWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibmMFp5B7b71PIPbfeq7RNzGC08g9t96nkHtvvVdom5jBaeQe2+9TyD233qu0TcxgtPIPbfep5B7b71XaJuYwWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibxhFp5B7b71PIPbfeq7RNzGEWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibmMFp5B7b71PIPbfeq7RNzGC08g9t96nkHtvvVdom5jBaeQe2+9TyD233qu0TcxgtPIPbfep5B7b71XaJuYwWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibmMFp5B7b71PIPbfeq7RNzGC08g9t96nkHtvvVdom5jBaeQe2+9TyD233qu0TcxgtPIPbfep5B7b71XaJuYwWnkHtvvU8g9t96rtE3MYLTyD233qeQe2+9V2ibmMFp5B7b71Fdom5jAREUEhERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEUpeA7dN10HVrW7TXRrAHY9qBmmlwuEX6qN7ryzkC+6xg4uPgFMITqSUILLfgD0AmwuvIdRtq7QnSrMdJlTO+oGHUOJ1kgZ1O8XmC/Iy7t+rHi6y16bRvSW5+1ANRlvSKGbKuBSXjdXudeunb3gjhED3C58VhZX1c+I1MlbW1slVUTOL5JJpC573HiSSeZuus0/hSrWW+6ltXh3mOVSMeS5n0WYNjmGY/QU+KYNXU9bR1TBJFPBIHxvaeRa4Egj0LkFof0H2qNYNAK9jsnZgkqsIL96fB6xxkpZe8gXux3iPurZ9s5bfOj+tzYMExOsGWM0S2acNrpRuyv9pl4B/osD4LztS0C507ykt0PFFoyjPzTKJFTiqIpgDG8ODuIIVReEnksERFICIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIgNxdU5aiOFrnyENa0Ekk8LAKMjqTk25kALi8x5nwDKeEzY5mLGKTDaGnbvS1FVM2NjB4klYt7SHSIaTaPPny9lKSPN2Zo96M09JKDTU7/bZRw4Hm0G/oWsnW3aR1Z17xV9fnzMs3kQdeDC6UmOkhHYNweyPi658V7+ncPXV+98/Jh6epEpKHUzf2julEw7DH1WVtAqNmJVIBifjtVHanjdyvEw8XnxcAPStd2d9Qc5akY9PmnPmYazF8RmJvNUyF+6L33Wjk0eA4K/0z0m1C1ix+PLOnGVqvGKpx+WOhZaKAfPSSexYPEkLZFs39GHk7JLqbM+tNZHmfGG7sjMMZcUFOeBs4cDKb35nd8F1DnpvDsMLnP3/ANDG99T0IwY0C2R9ZdoPEIn5ZwF+H4DcddjeINMdM1t/mB7KQ8/YgjvIWx7S/o2tn7JeVajCM2YEc14nXxblTiNaN10ZPPqGj9Kt3jj3lZW4XhOHYLQQ4XhVBTUdJTtDIoKeIRxsaOQDRwAV2BZcnqHEF5fS5PbHwXzMijGC5Gp/aN6MzPORPK80aLVEuZsHaXSPwqQ7tbTt5+YT5soA8Q7ssVhRX0eIYRXOoMUoamgr6R9nwzxmOWJ4PaDxBX0bPbvtLb2uvDtfdjzR7aCoJHZnwOOgxsNPUYxQNEVSw2+aPKQeDgfUvS03iipRxSu1uj495SUFLmuTNcOzt0h2rmjbqbBM4yS5xy1FuxiKrlvV08d/2uV3FwA5NcbcALhbOdE9pnSfXrCGYhkXM8EtXugz4bUERVdOe50Z5+ltx4rVDtB7C2sOg8lRizaGbM+WIyS3FKCFznRMH+WjFyzhzd7HxXg2Xcw49lfFoMdy5jFZhVfTO3oqmklMUjCPEfeXrXOiWGsw7aze2Xo/mu4jfKHKZ9Fwc08nD3VFaxdnLpQ8RwV1NlfX6hNdTebEzH6OK0rB3zxt4O9LQPQVsUyPqPkzUjAqfMmSMw0OMYdUgFk9LO14BPzLrexcO0HiFxV7ptzp0ttde3uMuU+aOzIiLRTyAiIpAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREUN4AJtzUC9rRcuAXUdRNV8haVYFPmPPmZaHCKGBpJdPJ57yPmWMF3PPgAVrf2i+k7zVmuSqy1obQy4Fh1zE7GKgDyqYd8beIYO433vQvQsNMutRlijHl4voHhc2Z0a8bWGkGz/hrpM2ZijnxVzd6DCaMiSpl/i380eJ4LWBtE7eusGuEtRhGGVbsr5YkuwUFFKeunYf8tKLb3oAA7OKxuxXF8Tx/E58WxzEKvEcQq5C+WeeR0ssrz2lxuSVkts6bAeruuEtPjmPQS5QyvIQ41dfERUVDfaoefH5526O0XXaW2k6fokO3upJy9P8AJGJTlPlBGNuEYNjOY8UgwnAcNqsTxCskDIaenjMkkjzyAA4krOrZy6L3NGYjTZr16rX4RQHdkZgVIf0TKOdpZOUfDmACePMLOXQfZU0f2f8ADRT5Iy/G7EnsDanFqlokq5zbjeQi7W/StsPBextG6AO5eNqXFFav/l2q2x8e8tGnGPPqdU080vyLpdgEOWsjZZosHoIRZsVPHbePaXHm4ntJXawAOxRRcrJucnKTy2XCIiAIiIChUUlPUxOgngZJHILOa8XBHiFiDtH9HFpjqz5TmPT9rco5lfvSF0Ed6OqeePyyLhuk/PNPbyKzFRZ7a6rWU+0oyaY7sM0Day7PmrGhOMuwzUTLEtLTl5ZT18IMlLUAdrJLAcuw2KsdKtadS9FsdjzBpxmqpw2XeBlpy8up5wPmZI+RH3VvtzXk/LWdsEqMuZrwajxTDatpZNTVULZI3jxaeC18bRnRcUx8qzLoBiQgfxkdgNdKdx/aRDKb2Pc11h4hdpZcS297HsL9Ln3939DH2e3nA9B2cekxyDqH5NljV1seU8edaMVm9egqHd4ceMZPc648VmvQYjRYlSxVlFVw1EUrQ5skTg5rge0EL53825NzNkTGp8tZ2y5X4RiEDi18FZCWH0g8nDxBIXr+gG2VrHs/1MNHgmKuxfLjCN/Bq+QviDfancTEfqeHeCsV/wALwqp17CS593yYVXumsM3kgg8iorHPZ324NH9fKOGio8SbgWZCB1uD4k8RyF3b1T/YyC/Kxv3gLIsEOAINwRfguNrUatvPs6scMyEURFjAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAW2I4hRYXQz4hiFVHTU1MwyTTSODWRsHEucTwAA7VgjtF9JzlPKflWWNFKRmYcTbeJ2LS73kUDu0ssQZT3Eeb4lZc6++bonnlw5jAa38U5fP855Y21jYX++um4b0q3v5Tq3HNRxy7vaVnJxWV1O16k6qag6s45LmXUDNVZjFVI4uAmfaKEHsjYLNYPQAu0aH7M2sO0BibKTImW5W4a14ZUYvVtMdHD3+eeDiPnW3PgsytkDo5cmY7l/BNVdYcRONx4rSxYhRYNBdlO1j2hzOudzebEHdG6Ae0rYdgWWMAyzhsGDZfwqmw+hpoxFDT08TWMY0cgAAvU1DiSnap29hHpyz3Ir2bbzPmzGHZy6PPSnRVtPj2ZqePNuaWgONZWxg08DuZ6mE+aONrOdd3DgRyWV0MEcMTYmRta1osABYAKoi4qvdVbqbqVpZZkXgLDuRWtbidDh7DJWVkEDGguc6R4aGgdvFdFqNeNOjNLR4Ji0mYKuE2dT4PA+rkB7juCw9ZWJLPRA9ERecYbrZgkmIxYXmbA8ZyrUVbwyiGN0wgbVE8gxwc5u99KSHeC9FY9r2B7XAhwuCDwKNNAmREQBEXE5jzRgWVMOkxXMGK01BSRC75Z5A1oUdegOWReZ0+qebsekdVZL0xrsRwmPj5bXVIoDUDvgjc1xePF24O66qR6zNpXujzLp/m7Bdz2U0uG9fAPRJC53D0gK21g9IUC1p4loPqXS8C1k00zJV+QYPnTCp6oGzqczhkoPduusbruMNTTzi8M8b/qXAqrWOqB57rHoHphrngZwPUDKtJXjdIiqg3cqICe1krbOHovY9oWs3aM6NvUvS51ZmbTJ0+b8uxF0nk7GA19MzxY23W272i/gtvFx3qR0LHgh/EHmDyK9HTtYudOlinLK8H0IaUuTPnI/5Qw+tuwz0dbSycLExywvafUWkELMLZz6SbUzS8U+W9U2TZvy9HZjZ3G1dTt8H8pAO51z4rOvaL2ItGde4ZsSrcL+IWZC35XjGHsDXlwHASs5SDl3HxC1Ca1aV4ronqXjWmmL4jDX1GDSMYamJha2Rr2B7TY8jZwuOPFdxa3thxHDsqsMTXd8mYmpUuafI3jaM656ba55bGZNO8yQ4jAzdFRCfNnpnHiGyRnzmnnzHG3BehLW/0QrAKbURo4DrqA2H1My2QLhNRtI2N1OhF5SMy5rIREWkAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgOg6/frJZ5+sNb+Kcvn/LgGuvy4/fX0Aa+/rJ55+sNb+Kcvn+f7F3r++u44P5Uq3s+BjqdEb6tm2ogo9njIFXUyNjhiyzQOe9xsGtFOwknwV5Lr9kOrl8lyoMUzRUOJa1mEUEk0ZI7OvIEI48OLwrbZnjjm2e9PopWB7HZboAWkXBHk7FDF9L8Wynib8w6QVcWGSTPMtXgs4Jw6sJNyQ0cYJD88ywPzTXLjau3tpp+L+JmljJWbmrWTMEgGAad0WBwci/MGIgSjxEdMJWn0F4VUaf6k4y8vzPqvUQwv9lS4LQspAPATEuk9YIV9krVDDMyV78u43RS4BmSBpdLhVcQ2RzQbF8LvYzR3+aYSBcXseC7yHMIuHC3pWJtx7sFTzql0C02bMKrGcFOY6lp3hUY/O/EZWnvDpy7d9Vl3yhw6kwynjpKClhp4IhZkcTQ1rR4AK5uBzIXEZgzflbK1M6qzHmTDMLiaLmSsqmQtA9LiExKpyXMFfHMBwnMlBLhOOYdTV9FO3dlp6iISRvHi08CvOpciZ608a6q01xhuI4XGL/mexWZ7mNHdTzkOdFw5MILfQupZ028NmDJJkZWaoUOISMBs3C2urA49wdEHD7q8Dzf0tmntGHwZM06xjFXj2EtZKyCM+oXK3rfSr6vyp037eXxIyl1Mxcoas5fzBVHAcThqsCx+L9NwrE2iKb0scCWSjxjc4Ls2NZnwHLlE/EcdxSmoKZnOSokDBfuF+Z8BzWoLVrpFdVtUqV1AMp5aweFjt6lmghe+qgPY9krnEsePnm2XVMi7buseVcYhxvM0eE53qaVrW0xx9kkwgDRYFjWua1rvpwN4nmSvVXC184bmkn4ZG+HibaHZ6z3nyoNNpvl92HYa67TjuNRmJjh89BT/AKZIf3wMbyIJXJYFo7glLiMWYs2V1RmnHYbOjrsTs9sDu0wQ+wg/iAHvJWFGTulwwncjizzpJVQPAAdNhdYxzB3kMfxA8Lle45O6SPZizX1cVTm2rwSpeeMOI0MjA30yAFn3V51fSb+h1pPHo5llz6MyoAsAEXTMpaxaV56YyTKGo2XcY3wLNo8RhlcCewta4kHwPFdyDmkXDh7q82VOVN4ksMjGDhsfydlnNUPk2ZcAw7FIf8nV07Zm+44FdRk0B0/hl63L1PiOWiLWGAYjNh7PWyEtafWF6PvN57w91N5vzw91Rux3g85fp5qNhrw7Lur1aY28oMWw6GsDh2AyDdf67lUanFNdcDHWVOXMrY7TRezfR4hNTVD/ABEb4zH7si5/O2pWWslGCkrauSpxOrv5LhlFGZ6uoP0sTQXW73Ebo7SF1U5b1H1Ke2bOlXPlfApAHDBcPmBqpxztUVLfYjl5sRb23cQrLL5y6Ar5H10oc45rnyX+Y7HqDEaVhfPJIIJ6aO3NrpoJXsa6/wAy4h3gtTPSCcdrXPH1VH/Ro1ugy7lbL+VsNiwnAMHpsPpYRZsUEYYPEm3MntJuSVpf6QT9lrne3LepP6NGul4Uw9QeFy2spV8xmTPRD8INRP36g/BmWx5a4eiH/SNQ/wB+oPwZlseWjxB+0anrXwRZeavUERF4xIREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQHQdfv1ks8/WGt/FOXz/P9g71r6Adfv1ks8/WGt/FOXz+v9g71rueD/wBXW9nwMdXojfhsxfsftPf828P/ABDF6geS8u2YyBs/aek/9W8P/EMXK531Pw/AqyPLuAU8+O5iqh8owyhILm/TzPPmxM+mcR4AnguKr5daaXi/iZX1KupWXci4lgMlbngwU1LQ/L21xk6mSkc3lIyQEFrh3rFnU7V/bKynlx9Zo5kVucMuxbzKXF8Sw9wxGWIcpDAx7d8dokDW3HEtF+OR+D6Y4tmDEafNGq9dHi9fTuEtJhkRJw6gd2FjHfpsg7JHAEcbBt16U2GOwvG2/oVqNWNGabSl6H0Bo6z9tjbVWO1lRhmadR8bwiRj3Nlo6eIUhYe1pAAIty5rxnFsx49js76nGsexGulkO899RUueXHvNyt8+quzzo/rNRuptQMi4ZichG62qMIbUxj6SUec31FYUaudE1A7ynFNFs6CnPFzMLxnecz0NmaC70XafSu203iDTliE6ag/VyMU4Sl0ZrcLGXuGqa1l6Vqfs462aO1clPnzT7EaSBnsa2FnX0zx2ESR3A9DrHwXmwe25a6wI4WXX0a9K4ipUpJr0GtKLh1IIDZQuCph6FfvJ7hc9yG3a1QvY3Qv3iGsBc5xsGt4k+pS8JZZCeehVp6irpXiSknlgeDcOilLD9xeiZQ2jteMhRsjypqtmGgij4Ni8r6yIDu3Xghc5pHshbQGs0rHZWyFV0eHvIDsRxUeTQNB+aG95zh9S0rNzSTon8kYOIcS1gzVU5hqQAX0FBenpQe0b/B7x6mrwdR1XS6Cca2JPwxkywjU69DwfSnpGNrSoxODL1JgeHZ6nldYQ/E2Q1LvqXROAHjdpWZ1JrHtD47gGHT6g6YyaaYXVNPl+MQS/FKopm25mANb5PcX+WO6wN5lq95090m060sw1uEZByZhWB0waGuFHTMjdIR2ucBdx8TxXbTFGQR1beIseHNcDfXttcSzQoqK/v2GwvSdG03yfkTD6AY5lOaPE5MSa2abF5ZvKJ6w24OdIefgBYDsAXe2izQO5ebYppD8Ta+fMGmWMSZVxWod1k8cDN+gq39pmp7hpce17bO5cVRwnVeswHEY8v6sYQ/L9bK4tixBjzLhlSR85NYGMn52VrL9l15mM+VnIPUFpI6QP9lpnj6qk/o0a3YxVdLPG2WCdkjHjea5huCO8ELSd0gl/ktc7+mj/AKNGum4Sf/rn6mUqeYzJnoh/0jUP9+oPwZlseWuHoh/0jUT9+oPwZlseWlxB+0ans+CLLogiIvGJCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA6Dr9+slnn6w1v4py+f1/sHetfQFr9+slnn6w1v4py+f5/sHetdzwf+rrez4GOr0Rvm2dKU1uzfkSkbUy05myvQxiWIjfZenYN5t+0c1xuWdMNQNKDUHJOI4XmWmqpDNVDGWdViU7z2mqYLPtyG+0m1uK5vZi/Y/ae/5t4f+IYvUFxdebVWaXi/iZX1PMoNcsMwuQU2oeXMVyhNvbrpa6EyUQ8fK47wgd284HwXfcMx3DMao48QwfEaWupZgHRzU8okjcD2hzSQVdVVLBVxGCogjlY7m17Q4H1FYCbYldjGk+rVBW6Y43V5UmqsNE0/xNduRzP33C74/YHh3hbukaVU1m6VrQ5Sfj05HmapqVPSqHb1VlZxyM/2uLijgHeaStdunu35qplvqaLULL2H5mpGHddVUbjSVW7884HeY93gAwLJnTvbR0Hz46Kkfmc4BiDyB5HjDRTvLj2NcTuu9RW1qPDOqaW//UUnt8VzRhstcsb/AJUp8/B8j3CtwuhxGB9NiFNFUwSAtfFNGHscO4gixWNGsnR6bO2qrZK6ly27K+KPJPlmCkQAnxi4x+4269yzlq7ptp/hQxnN+cMNw2keLxvlnb8s4Xswc3HwCxU1K6Q6kAkw3SHKE2ISAkDEsVvFC3uLYh5zx6XNWPSrDU7qqlYRlnx7vabF3qlpYx3VppGK2svRoazab01TjmUMQoc2YTThz3hkjaepjYONyx5s7+KSfBYrYXljMON43DlzC8KnlxGeo8ljh3bF0l7Ftzw5rK3PmrOqeqsz5M/53rKqne8vGHwXgpWeAjaeI+qJK6j5DQFgjbSRgAeyAsV9i0vQNRjQf06a392F8TjLvjWiqiVvTzHxfI9O0i6LDUzNbaXFNU8yUeWsPls91HSObU1ZZ9UCWNJ9foWb+j2xBs86PMjqsHyXT4rikVicRxUCpmLhycA7zGH6loWCmnmu+smlUkbMpZ1mqMPi5YZiYNTTW7gLh7fU4BZS6b9IZlGtbFhuq+WavLtU6zDXUx8ppHH551gHRj1Ot3rhuIdE1+k3Kflw/wDH5dT3dN4nsLzEE9kvB/MzBp6WGmY2OFoYxgs1oFgB3BVC6xsutZa1LyFm/BRmDLObMNxLDt25qYKhrmCw43N+BC82z1tlbP8AkfrYJs5xYxWx3BpMIb5VICOw7p3R6yFw1OzuK89lODcvUz3qt1RpR3ymkvWe3F1lEmwueSwMzt0iWYq10lJpvkCGkYB5lZi85e4+PUstb7Ndn2V8dzhtGTZjrNXc01+JwUb4mxYdSSmkow1wNwWMO870OcV61fhnUbO1d5cw2xXj15+g8yjxBY3FyrajLdJ/cZN5i1d0/wAsVJoMRzLSSV97Nw+kd5TWP+pgj3pD6mrrVfnLNWe6SfDMu6Szz4bVMLDVZkLaSB4PfTvBmI8CwLveXckZSynTNpMu5eoKCJvECGEA3778yVzlh3LwU0u49tHk+kmlmbMhVtfW4znBstBWcYsEomP8ho3dpidMXygfShwYOxoWp/pBDfa1zx4Oo/6NGt3HC1rLSR0gf7LXPB+mpP6NGum4Ublftv8AhZSp5jMmeiG/SNRP36g/BmWx5a4eiG/SNRP36g/BmWx5aXEH7Rqez4IsuiCIi8YkIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIDoOv36yWefrDW/inL5/n+wd619AOv36yWefrDW/inL5/n+wd613PB/6ut7PgY6vRG+/Zi/Y/ae/5t4f+IYvUF5fsxfsftPf828P/EMXqC4uv+tn638TK+oWvvpBv10cG+tQ/GFbBFr76Qb9dHBvrUPxhXXcA/tun6n8DkeM/wBmP1oxaIde4tYEqhPTNqGlk0EbmO4EEXuFcs5u9KiV9+lhrEllHyJZTyj2PZI0kyHqbqVU4Fm/CRX0lLhb5YmFzmljg9oFiONuJ4LNAbGegAH6kpAB2Csl+EsYtgL9eHEPrPL+MYskdsDUzOOl2QcOxjJWJCiq6jEWwSSGFkl2FjjazgR2BfEuJJXtTX/oNjUcN2EsPCzj0H0jRI2kdJleXkN+1vrzfd4l0djPZ/PLJ7z/AOLk/KofIZbP3bk938qk/KsN2bZOvrhwziP5DB8BPkyte+3Ojf5DT/AW4+E+J4edcfjZgWvaFL/43uRmT8hns/duUXfyqT8q8l1z052UNG8NIqcreXYzM0+TYdFWyF7j3v8AOsxvifUCvEHbY2vErTGc6Ns4WuKGAf8AoXkuM4/iuYcTnxbGsQkrq2qdvy1E8m89x9f3lv6Zwrq7rp39y9i7lJvJo3+t6dKlts7dKT72uhx+LU2FV1dWVEWGQ4fFVP3jSwklgaDwaSfZW4cSqdNTx08YZHTsAHLgFVc2/ndii3kvpdC3pW8Eqa9vechUrVKnKT5Etyb3aAFmt0dQ+UZuPdJB94rCmX2KzX6On/Bc3fvlP94rlOPH/os/Wvie9wqv9Tp+34GaKIi+AH2gLSR0gf7LTPH1VJ/Ro1u3WkjpA/2WmePqqT+jRrqOEv8AfP1MpU/Vv++8yZ6If9I1D/fqD8GZbHlrh6If9I1D/fqD8GZbHlocQftGp618EWXmr1BEReMSEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREB0HX79ZLPP1hrfxTl8/z/YO9a+gHX79ZLPP1hrfxTl8/wA7iHD0rueD/wBVW9nwMdXojffsxfsftPf828P/ABDF6gvL9mL9j/p7/m3h/wCIYvUFxdf9bP1v4mV9QtfXSC/ro4P9ah+MK2CrFHbD2dM46o4pS5zydLBU1GHUnUSUDzuPkaHF12OPAnjyNvSuj4NvaGn6tTrXMtseay/SczxVa1rvTnCjHLyngwFZe59KmVzimD4tgGITYZjWG1FDVQuLZIZ4yx7T6CrV0ke8Awn1hfoWNWFZbqbyn4HxxwlB4ksGTGwF+vBiH1nl/GMXs/SAuI0wwex/53Z+LcvGNgLhrBiH1nl/GMXs+3+AdL8IJNv+V2H+bcvkGpf+8afrj8Dv7H/21V9b/kYE4PDDNidFFK0OZJPG1wPIguFwto2GbPGiEuH000unWDOc6FhcTDzO6PFasaad1NIydpLZInBzD4g3BWTua9tXM+K6T4fl3Bop8MzN5sFbXQmzRG23nxkcQ53b3XPguj4z0nU9Qr0foLaT5PDxj0s8bh2+sbONR3cU3jlyyZdfI6aG2LvjbYJb96/rUsmznod1L3N03wa4aTfqeX3Vrqbr/rQ82dqXmHdPZ5a/8q2YaW4lV4rpbl/FcSqXz1VXhME0srzdz3GMEkntPFfP9e0nU+H+zde4ctz7pPuOw0i/sNYc4U7dR2rPRGq3P1FS4dnfM2H0MLYaekxSqhhjbyYxsrg1o9AAXXxyHoXZdR3sk1BzS9hNpMYrD/POXWm+cd0dnBfddPblaU2/BfBHy24SVWSXiyD+LVmx0dX+DZt/fKf7xWH+W8pZjzlikeB5YweqxGslNmsgZe3Hm48mjxJstheyRoXmPRnA8QnzPWU767GjG91PDciANHAFxAuePGwt4lcTx9qVotOdpvW9tcvadNwlZ1538ayi9qzz9hkMiIvh59eC0kdIH+y0zx9VSf0aNbt1pI6QP9lpnj6qk/o0a6fhL/fP1MpU8xmTPRD/AKRqH+/UH4My2PLXD0Q/6RqH+/UH4My2PLR4g/aNT1r4IsvNXqCIi8YkIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIDoGvxHxks8/WGt/FOXz+uI4m/A3++voiz5liHOeTMbynPUOgjxihmonSNFywSNLbj0XWmTaF2HdY9BHz4m7Dn5ly1G5xbitBC75Wzvmj4mP03I8V1/Cd5RoOpRqyw3jBSom4+SbM9i/WfTXP2jOVMt5XzXSVeLYFg1JRV9CXblRDJHE1rrxus4tuD5wFj3rIYEO5FfOhl3MuYMp4tBjuVsXqsLxGleHxVFNIWPY4eI+8s+9m/pRcSwptLlPX+g8sgFo2Y/RM+WNHDjNFyd9U231J5rHqvDVak3WtnuT547/AOojUU/QzZqqcjN6/m3XX8i6g5R1IwSDMeS8wUOL4bUN3o56WUPHoNuR8CuyLk5RaltksNF2u5nnOqeheQtWsONHmfA4XVAaRFWRDcniPeHjifQbjwWDesux3qFpu6bFsvRSZhwSMl2/AwGpib9PGOJ9LRb0LZUqMsEcjSJG7wdwIPEL39G4mv8ARJ/5M8w/hfQ8HVOHrTVFmUdsvFGufYcx7CMu6v1HxdxGnoPKsNlpofKJBHvyl7SGC/zXA8PBZebRWjMuuWR4svYdjDaGppaltXBI9u9G8gEbptxsb8wuG1j2TNPdTXvxXDoDgWOjz21tIyzXu7DIzk70gg8uK8Uw7UTaF2V65mGah4VNmfKYcI4qphc4Mbe3my280/SuB8CF7V1d/pu8WqWE9ldYex+K70+j9R4VCg9It5WF9DdRb85fzRjxqZobqXpXVGHNOX5W0bXbrK+nBkp393nj2PodYrojd4ENLrtHJbV9P9X9M9bMIc3CKulqi9g8ow+paBNGDzDmG/Dx5KhhmzPolg+ZnZrockUbKwnfaHEuhjd881h80H73Yuhtv8RK9rB0dRof5iXdyz68nmVOD6dy1UsaycH4933GCWkmy5qdqpLFiMdCcGwZxBNdXMLd8e1s9k708vFbFcHosM07yHSYRX4nFHSYPQtp3VM7gxu6xtt4k8ByXner+0vp3o7Ty4VHI3E8YjbaPDaR4u3hw3yPYD1E+CxwgwLaI2usQZiOJzy4BlTrA6MOa5lPufSM5yut2kgeheBf3F9xM1dajJUqEemf5Lqz0bSNroebeyTq1pcnjoY85tf8Wc8407CWOrBV4pUug6lpeZQ6Vxbugc7gi1l79ozsQ5yzk2DGtQXvwHCnnfbSi3lcrfEG/Vj08fALKrR/Zp040lgZLh2HfFDFi20uI1YD5Ce3dFrMHgO4XJXr7GhrN0AADuWxq/HdapTVpp3kwSxu738idM4Pgp9vfPLbztOoaeaVZK0yw1uF5SwGnoY90CSQN3pJSO1zz5zj6Su4boB4BRRfP6k5VZOdR5b72dvSo06MFCmsJeAQkDiVTmmZA0vke1rQLknkFihtFdIbpTo6yry9laRubM0RAsFPSSDyeB44fLZRexHzoBPoWS3ta93NU6EctmYygx3M+X8sYZU41mHF6XDaCjYZJ6mqlEccbRzJc6wWjvbHz7lXU3aNzdnHJWJtxHCKyWBkFS1jmtkLIWNcRvAG1wRft7FxOtu0rq7r5irq3PmY5DQteXQYXSkxUsI7AG384+JJK6xptpVqFq/mFmWdOMq1eLVhIDzEw9VCD81I/kxvie5d7o+jLR83VzNJ46dyMMpb/IhzM9eiHc3qtRBfiJqD8GZbH1irsNbKGO7M2WsWmzTj0GIYxmOSGWqhp2ERUvVhwDWuPF/szc2HLksqlx2r3FO6valWm8psy4wkgiIvNAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBUKqljq43QzMY+N7S1zXC4IPMEKuiAw02jOjb0x1RFVmXTt0GT8xy3eRTw2oql/08bfYEnm5ov2kFazdYtANVtCsZdhGomWJ6WJxIgrogZKWob3skA4eh1j4Lf45ocLOFwVw2acmZWztgtRl7NmA0WLYdVMLJaerhbIxw9Du3xXQabxFc2LUKnlQ9PUrKCl1NCGk+tGp2ieOszFprmypw5+8DPTBxdT1A7pIzwd6+S2VbOPSVZA1Fko8tarwsyljz7RiqLt6hqX2twfzjJPY4WF/ZLzraJ6LofozNGz9VmN1jK7AK2U7p+lgldyPg8+ta+83ZQzRkPHKjLOcsAr8HxOnO7JTVkDonekXHnA9hFweYXSujpfEUN0Hif3Mq3On15o+h6gxKixKlZW0FTHPBKN5kkTw5rge0EcCrocRdaONnzbM1m2e6mKiwnF3Y1l1pHWYNiLy+MNvx6px86M+g28FtA2eNtnSDaAjgw7D8ZZgmYi35bg2IPayUm3Hq3HhIPqST3gLk9R0K607m1uj4r+ZaMlJcjIdWWK4XQYxQy4diNLFU087S2SOVoc14PMEFXfWM7HD3VHdaV4sZc8x7hKCmsS6GKGp+xdA3EXZy0VxuTLeMU5MrKaOR0cTndzHt4x9vCxHHsXQpM8bZOa76SswWoosSi+V1eK9V1RMPzxlHmgW7W3J7r3WdhY038VL1Md77vH0r3qOv14wUbiEajj5rkstfP2ngVuHqMpuVvN00+qT5P5GM+kWxhlPKs8eZM/zNzJjz3da8TNLqeOQ8zZ3F5v2u9wLJSlpoaKFsEETWMaAGtaLAD0KsGtHIKK8u7vbi/n2leWX7vYepZ6fb2EdtCOPj94RQJA5kLqWo+qOSNKsBlzJnnM9Dg9BECd+okAdIfnWN9k93g0ErWgpVGowWX4G6dse8MFyvHNdtq7SDQHC3zZwzAyXE3MJp8KoyJaqV1uHm3s0eLiAsGNo/pP8AM+amVOVdCqM4Phzt6N+M1UYNVK3leNh4Rg95G96FgvjON4rj+JTYvj2JVVfiNW8ulnqZHSySOPO7nEkldXpvC1Ss+1u3tj4d/wDQpKpGPJc2ZJbRG31q9rd5TgWD1MuVMsyOLRR0UpE07PbZRYm/zo4elY2YVhWKY9iMWFYFh9ViVfUv3IqenidLLI49gAF1kls8bAmsOuXk+OY1RzZSys8tca6uiLaioZ29TCfO5cnOAbx4XWz3QXZR0f2fsNZBk7LUUuKOZu1OLVjRLVzH6s+xF/mW2HgvXuNXsNGh2FnFOS8P5sqoSlzmYNbOPRgZnzUKbNWu9a/BMPNpG4JSyXqpm8wJZB5sY8G7x9C2P6caW5G0owCHLWQst0OEYfCLdXTRBpebW3nHm5x7SeJXa2sa0WAUVxd7qd1qEs1pcvDuMqxFYQREWiAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgIW8V5vrDs+aW65YM/CNQssU1bZpENUxu5UwG3smSDiCPc7wvSUUwqSoyU4PD8R0NQG0V0b+pulb6nMembqjOOXWXf1LGfo+nb3Fg4SgD5poHoWIrZKvDK0TQyT0dXSv5sJjljeD7oN19G7o2O4OaD6ljltEbDukOvrKjFanCm4HmV4cY8XoI2te53Z1reAkHp49xXX6bxTKK7K9WY+PzMcqafNcmYMbOfSR6jaZmmy5qrDNm/L7S2NtUX7tfTM5X3jwlAHYQD9MtmWkWvmmGt+BsxzTzMtNiDN0Gan3w2enJ+ZkjPFpWnbaA2OtZ9nqrllx/CPivl8utDjGHNc+It7OsbbejPfcWvyJXlGTc7Zs08x2DMmSsw1uEYpTuBZU0kxYRx5G3Md4PBejdaFZarDt7KWG/Dp9xHaSjymj6I2m4uorXHs6dKNTzupMrbQNGKaUkRtx6ij+VO8Zohxb6W39AWwTLecMr5vweDMGWMfosTw6pYJI6immD2EW7xy9a4q+0+5sJ7K0cenuMi5rKOYVliuM4bgdHNiOLVsFJSU7DJLPNIGMY0cySeACxx2i9u7SHQxk+EUmJDMmZWA7mGYe8ObG7s62T2LB4C58FrD112t9Y9oKslbmzHZaLBQ8mHBqKRzKZovw3h+2OHzzvVZb+m6Ddag1J+THxf8hKSj1M5to/pOcm5QFXlfRSlizHjDbxuxOUnyKnd3tA4yn0EDxK1v6kas5+1bx2TMeoOaKzFq15JaJZLRRD51jB5rR6Fx+Rsg541Jx6LLOQctVuNYjO6zYqaMkN8XuPmtHiStiWzn0XOEYYKTNWvtXHitZZsrcDpHHyaI87SycDIe9oAHi5dYv0Zw7Dxn97/AKGLy6noRhDons06t6/Yj5LkLLcpomP3KjFakGOkg9L7cT9KLlbOdnDo8NJtGo6bMOaWDN2aGWk8rrYh5PA/2qHiB6XEn0LKHAMt4DljCqfBMv4PSYdQUjBHBT00TY442jkA0cAuTtbgFyepcQ3V+9sfJh4Lv9ZkjGMOhSp6eOmYI4mtYxoAa1osAPAKqiLxCwREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAW1fhtDilNJR4jSQ1MEzSySKWMPY9pFiC08CFhHtHdGXkLPRqczaN1MWU8cku99EQTQVDvBv7UfqbN4clnKlh3LYtLuvZT7ShLA68mfPlqnozqJovj78ualZWrMMqA4iOZzN6nnA7Y5B5rx6DwvxVtlLVXUfI2F1+CZMztjOEYfiTDHU09JVvjY8HnYA2afEWPit9ufdNcmamYNPl7O2XqLFsPqG7r4qiMOt4tPNp8QQVgXqj0TIrs209ZpLnxmFYBVz3rKTEYjNJSMPEmFwI3x2BruI7XFdrZcT21xDZfRw16MpmF03F5gzXFTw1mI1zKSjpqmvrqt+62OJjpZpXk8AALkklZo7O3RnZ+z82lzPrHPNlbA5LSNw1lvihO3gbOHERDwd53gFnfs+7G2kGz7SxVGBYK3Esd3AKjGK8Nknee3c4WY3wAv3kr3mwHIBefqPFFSquysltj495eNNLm+bOi6V6J6aaM4BHl/TvK1JhNMAOsexu9NM63spJHXe8+kld7AA4BEXJylKcnKbyy4REUAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAKXeINrgqYrXDqrr7tfZh2u8e0F0W1CoMMihIdRQ1eHUjmMaIGyPvI+JzjzPaVtWtlO9clBpYWW2SsGx24PahI7CFgPiOU+lSw7D6nEZdXMqOjponTODKKhuQ0XIF6fnwXStOdtrXXM+zBqdmnFMcpWZuyTUUcVPirKKDzxNKGuDot3qyQN4X3e0dyzrSas45pTjLmlyb7ycI2WBxv4KNx3ha4NJcb6THWbIeG6g5Q1dy3HhWKB5gbVYfQsk815abjyc24tK4LVLXfb32YM1ZVl1hz3gOOYbjdSWCkp6Gk3ZWtcwPDnRxMe02eLEFStIqdo6UakXJZ5ZeeXsGEbPN7jYkISB2hYi7ee0hqDoTpnlzENOJ6ekxfMdW2nbVywNl6hnVl5LWvBaTyHEFdAwfLfSnY1hVJi9Fq/lTqK2COojDqKhDg17Q4XHk3OxWOnp05UlWnNRTbxl+ASM+Q4d4S47wtZ2s+p/SJbNuE4VnTUzUnLuJYXU4jHR+TU9BSOMji1zrOLYWuAsx3EEHkvftqzaB1CyRspYDrBp/ibMGxrGIcMqC808c4jFQ1jnNDZGuB9kRyurS0yqnDZJSU3hNPlkhxwZaAg8ioX7ysadinaspNobIzqLMMjIM64GGxYtSlojMo+ZnazhZru0AWBB4DguR2xdpzDNnjTqSegkjmzXjF6bBqOwe7rLcZSzjdrOB5cyB2rWdlXVyrXHlZx/foGMmQtx3hLgdqxR2KNdtQ9UdnXHNRdSMYZi+L4XU1u7IKaKAGOKPfa3dja1vZztdY8aQay9IJtOTZgzBpVqLl7CsNwyuNMaSow+kAjvxAa58LnO4W4krZjpdbdUjOSWx4bb5E7cvkbNd4d6bw7wsDKvJ3SpUdLNWT6vZU3II3SOtRUV7AXNv0N4Lp+hu0Btn7SGn+O5fyFm7BaPOOV8XhbU4nUUVO1tRSua8OaWGJzAQ5osQ0cPQp/Rk5Qc4VItLrz6Z9hOzBsiv3FRuO9audetX+kS2dMOwnEs/atYK6HGKh1NT+RYZQyHfaATe9OLDiF6xlrLPSf182GYrWas5Vfh0zoppY3UdEHGE2Lhwp+duHNWqaVKlSVV1Y4fTm+ePYRt9JndcWvcJvDvWuTMe0Ntg6ubSmZ9GtCM14Rl9mWxK3q6mjp3tkERax7y+WN5uXE2AsLLtpyN0qo/wCmDKX8jof/AOZVlpc6aXaVIptZw2+/2E7DO6470LmjmVgTsubR+0adpvFdnbXnHsMx6ekpZZDUUtLDF1UrNwgNMTGBzSHG9xe9lw20DrxtX1G19NoJojn6hwmKqghfRwVWH0j2NPkolkJkkic7jZ3ai0mu6zpblyjuz3YKuPM2HhzTxum808A4LAypyb0qlNC+d+ruU3NY0us2jobm3/h12fo+NpXVLXAZyy7qxV01dimVpqdrayKCOF0gk6wFrmxgM4GPgQO1Y6mnTjRlWhOMox64fQnCMzCQ3iXW9KBwdyKxC23tpLUHIGJ5Z0j0Kna7UDM87XMDIIqh0FOD2skDmjeIPEjgA48Oa5HYX2m8e1owDGMoalzgZ+yrUugxNroWQOmjLiGv3GgNBBDmkAcLDvVHYV4230p+b7/X6iNrMrUUGu3he1lB72ssXFab5ojBEuaOZCAg8jda8tq7a+1xj1XxrJGzXXBtDkXDX1+ZamOhgqQSOLheRjrBosPNsbl3cssdmLXHC9etJMIz5QuYKqZnUYjTtcCYKpoG+wjs4m48CFuV7Gtb0I3E+j+9esnaeu3HeoE9ywT27doHX3TPV3I+n+jWcoMG/NNAyLcloKedrp5J+raS6WNxaOIvZUWZL6VWRgcNYMokOFx+g6H/APnWWOmVHTjVnUjFS6Zf9CdmTPIHvcEuO8LBnZI2gtojE9oXM+z/AK843huOVmD0clSKulpoYurex8bbAxMaHNIk7RfgvedrTXyl2fNIMUzdDIw4zUt8iwiEgEvqn8Gu3TzDb7xHc2yxVLKrSrK35NvGPDmRtw8HtoPeQo37SsAtkXay1rn1gfo1tN1lsTx6hhrsCfJRwUxBc0uDPlTWg7zbWvxBaRz4LMHXHM2M5P0czbmrLtWKbE8MwmeqpJixrwyRrbg7rgWn1iyivY1basqM31xjHR59Ix4He94d6bw71rI0H1B6SDaFyjNnTIWrOANoIat9I8VmG0Mb+saATYCnPCxCn1r1R6RXZvwLD876i6k5exHCp61tL1NPh9G4OeQXAO3YWuAIB4grc/Q1Ttex7WO/wy8/AnbnmbNLjsITe8VhhtVbSmpmTdlPJOseneLR4Di+ZX0EsxFNFUBrJqd8jmWla4cwONr8F0jJtF0oWeMrYVm/B9XMsNosYo4q2nEtDQtf1cjQ5tx5PwNiFipabUnT7Wc4xWcc2+72E7TYPcd6gCO0ha2qHaL2y9ENo/JmlWu+a8Ix6izPU0kT46ajpmNbHPMYg5r4o2ODgQeB4cF6dt37SmsGlWb8kaa6P4jR4Tieaid6vnpo5t1xkZGxoEjXNAu43NrqXpVbtY0oyT3LKfdgrtyzNj0KFwObgsD25G6VZzA9usOUiCLj9B0X/wDMvOtRNdtu3ZnztlGLWXPGAY9QZgqTGKOnoaYB7GuYH+dHExzTZ4tYqaelyrS2U6kW/DL+Q2mzS553QOHbwWD+bcs9JhJjmK4plPVPK9NgT55ZqGGSiozJHTXJY071OSSG2HEn0rxXRHWfpD9fsXzFgmSNW8FZUZZm6it8swuhjaXbzm+baA34tKilpM505VY1Y4XXm+XuJ29xtK3x2FN8LAbVTP8AttaA7POYs5am6j4RU5iOKUcGF1NFh9I9sULt7rGub1IaSSBxIJXC6eTdJzqbkrCM9Za1eywzDcZpWVdM2egoWyCNwuLjyfgUWlTlT7V1IqOcZy+fuCiuhsT3gOajcHkQsPNJsp9IhRagYRWasal5axHKkcjvilS01LSNkkZumwaWQtIN7ciF5rtCa97VZ2uPjCaKagUWEx1lPC6kgqsOpZIw8xlzi6SSJzuzvVKWm1KtV0qdSLwst55JfcTtNhyXHesCZsndKvFTyVB1eym5sbC+zaOhuQBf9zrs3R67S+q+tlZnXKGrVZS4himV5Iw2thp44S+73MLXNjAYbFhsQOST0+caMq0Jxko9cMq1gzPL2g2JCj61hftm7SuqWXM84Fofs61Afniva6trSymhqDFAAd1lpWuYCd1zjcXsG967zsO7S9br5pnK3OFTG7OGXphR4u0MbE5547s24AAA6x5CwIIWOWn14W6uX5vvXs8BgyXRAbi6LVICIiAIiIAiIgCIiAHktSupenebdUukTzLlDJGfqjJuLTnrY8WgidI+JraVhc2zXtPEC3Nbal4ThuyNkjDNoaq2joscxV2PVTHMdSks8nAMYjNhbe5DvXoWF7Gy7RvrKLSJwn1PAJtgbadraaSmqNtjHJIZWlj43YbPYtI4g/oldb1M2TaHZY2MtS6F+Z349ieOyUU9ZV+T9RH5k8Ya1rC5xHMm5cb3WxeNgjbugk+ldH1r0kwTXDTvE9N8w11VSUGKtY2WWmIEjd14cLX4c2hWo6pWjKKm/JTTeEl0Yya0dFsi7eMOzxh+eNG9UhFleGnmnocCpxGancbI/fDN6I3cXBxA3u31Lr2kmR9W9uDOmGUWquu9NJPlWrL5sIxGAMr44w9u/wBWxrWB1y0Am/m9o79pmjWkGB6K6dYVptl6vq6qgwhr2xS1O6ZHbz3PN7ADm5eV6nbDWmeftSqbVrCMbxrKOZYXiWSqwR7YuvkB9m4Eey5gntvxW5+mqcpVMpJtvEklnn4kpI8H6WaBtPkTTuniJ3IsX3B6BEQs39OsRw/8wWXf0ZAD8S6W/njn1TV0TW7ZhybtBZDw/JOouL4lUPw2Rs8GIwFsU/WhpaXkAbpuCbi1uKx7HRK6NAWZqJnEDu62L4K1VXtbi1hRqzcXFvuz1HJoj0r1VTzaJZcbBURyEZlguGuBt8onXFbZO67o/ciNHbR4B+LiXPUHRO6IU9dBU1+dM210UMjXugknjDX2PIkNushNZNmvJWs2l9HpNi9XW4bg2Hml8nNEWiRjYN3cbdwItZoCzK+tqEaNKm3JQk23jHUcjDXXHTHNez1+Yja+0YpXNEWGUbM0UEdxHNF1TbyOA+ZcPNceywdzuVX02yHmnaSZnba61foHRUTMHrIMn4TId+KCMRu+XWI4kcge0knhYLPulyRgbMo0+Sq6mZX4ZDRsoHxVUbXtmia0Ns8HgbgcVDEsi4HW5Oqcj00EdBhdRQvoGRU0YY2GJzd2zByFhyWJ6tugk15WfO79ueg38zDDo6zvbGecN/8Ay2LfiCuM6JaqpqfJ2f8AyiojjLscFt5wHzAWVmgmzhk/QLTms0zy7iNfiWGVs888z64tL3daLOb5oAtZY9Yv0T2huJYtV4nQZxzVhzKqV0op4pmFsdzchpLb29KzO/tq7rxm2lNpp4z0JWHyZmLmHEcOOAYlaugJ8kmt8sHzhWv7omWh2M6rEi7fL4fwpF3H86U0avZ2o2cCO0dbF8FZEbOGy7p5sy4FiGD5GkrqmTFZmz1lXWvD5ZS0WaLgAAAX4eJWv29vb21SlTlucsd2OhDSXQxe6W4Wydp+I+fxXqOX1DFnPk1rHZUwk2BPkUN/sAvNdo7ZfydtLYbg2F5vxjEqGLBap9XCaItBc5wAIO8Dw4L1vCcNjwjDabDYXueymibE1zuZDRYXWtXuIVLWlRj1jnPtIfM1xbLE8UPSO6pullaxtsSF3Gw/TmrY+cQw+3+HQfZhYq6w9HBo1q/n7EdRKzHcwYPiOKuElUyhmZ1b32ALgHA2Jtx9a6WeiY0b+iJnL/WxfBW9cVLO82zlUcWkljbnp7ScLvOgaayMl6VDNb4ntcww1Vi03FtyPtXTNpDANScy9IfJg2kWZYsBzXUU0BoK+S27DahBfe7Xc2hw5dqzG2ethLSfZ1zXNnfLeK4ziuMSU7qWObEJWlsUbiC7da0Did0cfSux1eyhkqs2iqfaRfjeKNx2ni6oUgLPJiOo6niLb3sePPmtj9K0KVZzp80qe1ZXV+onKZgBrlnfb50Gx7DcC1M1rrKfDcXIjhxqGnikorng5riIQ4EcyLXsbhZLbHej2GbLGlGb9Y83ahYZmBmP00eJT1mHnep2wxB5buvPsy4ydw48LLKzU3S7JmrmUa3JGecJir8LrmFr2PHnMPY9h5tcOwhY70PR5ZQwzT7ENKaPVzO7cqYpOyefDXTxuj3mkkBt28BfmO2w7lgqapRuLdUppQ5rdhdUFgwx0u2kc4fH8zDtKYxoXmLPMuKCSnwR1NI6KGghvuANIheHEMG7cW5u71xtVtAZh0/2oaTaMoNIceyHhGKzxw43QVb3Piq2uNpi1xiYAS0BwFvZNv2rbZp1pxlnTDJeE5FyxRtjw3CKZlNCHNG87dFi53e5xuSe8lcFrZobk3XfINdp/m+N8dJWObI2eBrRLBI03D2EggH/AHXWWOs0HVeaXktbXz7vUTlYwdyy3mLCcz4DQZhwetiqaHEYGVEErHAtex4uCD6F5Rtb68YdoFo5i2bi9r8UqGGhwqDeF5KqQbrT4ht94juaV2rRLR6g0RyBQ6d4TmHEsWw/Dd5tLJXlpljjJvuXaBwHYupa47KeTtf80Zax/PGP4saTLE7KiDC4SwU0zw8OJkBBJvbdPHkvIpSoRr5nzgmU7zXxsz68Zm0Uy3mIYxswZozhjGcal9RiWLPmfEKiFw82PdNO/wA3znG+9x3irvYl1vrNE9oCryPmHLWKZUyjnyoIo8OxPe3qKcuPUnfc1u8LFzCd0XJb3La/Bh9HR00dLDTxiONoY0Bo4NAsF4xtE7I+ne0f8RanM1bX4XX4DKZKWtw/dbKAbEtNweFw0+keler+lqFZ1I1KeIzXN5z06ci7afIw76TDB25p2htLcuNxB1GMVhho/KmGzoesqt3fHiL3HoXdYejPjkha87TOamgtHATnu/fF7VrpsLZE1+dlyozlnTMMdVlzDm4dFUU5jD5w39sku0+eeZIsvLj0SujTvY6i5xAHfLEf/Srw1Gl9Gp0o1XFxWPNyFg6FsW4bSaK7Yee9GzVU2YmjDnzjMM7SawBj4yY3P3iN07/HxaPQvP8Aad16m1f2naGtwrIOMZ5yTptWNjGG4aXBtVVMN3Pe8MeAOsba1jdrfFZkaRbAulWi1PjdRlPH8fONY3h8uGOxaeZrpqeGQgu6sAAB3Aedz4L0DZ52acjbOeWKzLmUp6uuOIVjq2rrK3ddPM8/PEAcAOSrPUraNaVdJylhJZ5etjKRrV2pdc81axYtlnUTBNnnM+QswZOIkbi75HysMDCHMa/5SywY65Bv80Vm6dbcO162JMyZ6opYxVS5cqYMQgab9RVNjs9luY48RfsIWS2PZbwnMWE1WCYnRwzUVbC+CoifGC17HCxB9S8D0u2Isk6UZVznkjL+c8wy4JnSB0NVSVDo3NpyQRvxWHB1nEePDuWGeoW9WlCLhtcHy7+WeZGVnKMENkjT/bMzJpDiWYdnnVKnwXA6aumD8LO4JZqhrAXbu9G4XIsBdw4rg8v0e0Jtd5tk0U1b1ybhlbg1bvvwrHYWxymQXDnRNYxu+8C/mlw4G62jbOez1lbZsyRNkTKWK4hiFHNWPrXS1paZA9wAI80AW4LqOvOxTpXrtmCgzniNTiWX8yUDw4YthDmxTygex3yRxLew8wtv9OQdacnFLPSWOaITXQ8H6RfKdHkLZEyZkqimdLTYFidDQRPdzcyKmlaCfcXk/wCZnpDMhaF4PqJlTVl9blmLCaappsNw1kT6mlozG0t810XnBjLX4k2Czg1T2VME1l0pwvSrPuecdroMLqI6kYl8rFVMWMc0b5tY8HG5tdeoZLyTh2SsnYTkqgnlno8HooqCJ0wBc+ONgYC7svYcVq0tWjQt408KTUm3ldSco1kbKmlmedqzVXL+tmpmt1Dj1RlGpgmlw+RoGIRGGQvjjcwBjWN3rnesb8Qu7dIpIwbTuixcfNbPDcnl/hcayNn2EtOcP1cdrLkLNOP5NxiR/WSwYTIxlM917uvGQQWu7W8iuf2idj7TraWpsJOeMQxOmr8Ga5lPW0LmsfuutvBwIIIuAfBZVqtF3kazfkJNYx0yVye2Q4hh/UR/o2Di0ftg7lrx6U6enqM5aRinmZKRWVPsHA8d+BdxHRK6Odmo2cbfvsXwVzOUOix0PyvmXDsx1eaMzYwcOnZUspquZgje5pBAdutuRw5LDZVLOyr9upt4zy2/1LcjLeTc/M29xtfyQ2+wWAXRb7rtQ9Yw43PxSH4+VbDKmhjqKV1JfdY9hYbDsIsvGtn7ZRyXs741mbHMqY3idbLmmo8pqm1hYQx2+51mboHC7jz8FqULqELatRa5yxj2Fc5R5l0oBbHswVZtx+K1J99y8A0N2Bmaj6UZXzt8fzMeDnF6CKrNDTzER0+82+40bw4BZ46/6F5b2hsgS6eZpxKuoaKWeOoM1GWiQOZew84EW4rGEdEroy0brdRM4Cwt+mRfBW/Z39OFmrdzcHnPTJKwZf5AwShyhlLB8pDGfihJhVFDR+USyAyTdWwN33ceZtcrWftMYHqNmTpDGYLpLmKLAs1VNLTjD8QltuQnqXXJu13MX7Fk1pf0aulmlefMH1AwjPOaayswao8ohhqZIzE9wBFnADlxXpeJbJOSsS2hqPaPlx3FWY9RMaxlI0s8mIawt4i1+R71htLuhY1p1Iy3botc13v0DkYC645z2/NB8aw/L+o+tlZDheMkQxYzTwRSUQJ9k1xEO8CO0Wva5C9+2V9N8N2QNIc66+Zy1DwrMj8epBXGbDX3hkDbmNjZCfPc97udhxd4LMLUfS/KGquUq3JWd8MixHDK9m7Ix7BvNd2PafmXDsIWNUfRsZDiybPp2zVfPH5mJ6xta7CzURGESAm1vN4cz6TxWZ6pRuKCpTShzW7avOQ5ZyYf6E7QmdspapZp17zDoJmbPOL5suKOtgkfFHS05cbtj+UvDgRui4IsGhW2RdoDFtJdqsawO0zxvIeVM4VZp8VwuuLnMIkcC57HGNgO6+zrW4C47Vtvypk/A8nZew3K+C0kcVDhdOylp2BgFmNFgula/wCztkTaKyV+YvOTZ6eKOdlTT1VJutmgkbcXaSDzBII8VdazbyqS3UsRksPm+ndyJbyek4diNFitDT4hQVDJoKmJssb2G4c1wuCPUVcrp2k+nEGlOR8LyLSY9X4vTYRD5PT1FcQZurB81riOe6LAeAC7ivBk1uaj0KBERAEREAREQBERAFBzw0bzzZRWGnSH7VeYNEcsYbknIFS2mzJmISPdWWBdSUzeBe0HhvEkgXvyKz21rUva0aFPqx6TLnEcyZewdu/i+OUFCOd6moZHw/jELhnar6YDh8cXLP21g+EtAOO5vzXmmvlxHMWZcTxGqmcXPkqKp7yT6yuNMs4AAqJB/wDkP5V19PgzK8ur7jH20V0R9B/x2NMPojZZ+20HwlD46+l/0Rcs/bWD4S+fDran91Sf6w/lTran91Sf6w/lV/qZT+1f3Ijto+B9B/x2NMPoi5Z+2sHwk+Oxph9EXLP22g+Evnw62p/dUn+sP5UM04HGrk/1h/Kn1Mp/av7kO2j4H0KQaoabVEgig1By3I93ANbisBJ9W8uwUuI0NbEJ6OrinjPJ8Tw5p9YXzmMqZ28W1Ut/CR35V33TDXvVnR/HqfHck5yxGnEL2ukpJJnSU87QeLHxuNiDy4cfFYK/B04xbo1Mv0olVoM3+hzXcuKjYdy862f9WMP1t0owHUigiEJxWmvUQh1+pnaS2Rnqc0+oheirjp03Tm4S6oyYwLDs4Lh8azhlPLkkcOYMzYXhb5QTG2srI4S8DmRvkXXLk2WsLpcJJYs6ZEbHK9oNBVHzXEfNtW3ptitQuY2+cZz7h0TZsTOqumA56i5Zv9daf4al+Oxph9EbLX22g+Evnw8oqL/4RL9mVEzTjnUTf6w/lXXfUyH2r+5GLto+B9B3x2NMPoi5Z+20HwlH47GmH0Rss/bWD4S+e8zVBNxVSj/8h/KnW1P7ql+zP5U+plP7V/ch20fA+hD47GmH0RstfbaD4SfHY0w+iNlr7bQfCXz3mWptfyqX7M/lUPKJv3XKf45/Kj4Mp/av7kO2i+4+iXC835Txv/EuZMLr7/uWrjl/BJXLB7HcjdfOhh+YMfweoZV4PjlfQ1ETg5ktPUvY5pHaCCtm3RybXmbNT62s0g1OxQ4ji9DTeU4XiEotLUQtI3o324Oc0G4NrkXvyuvI1Phurp9J1oS3RXXkXjOM+SM+0RFzfIsQJA5q3qsSw+hiM9bWQ08Y5vleGN908F5ttKa0UmgukeOaiVELJ56OIR0ULuUtS8hkYPhvEE+AK0maj64aq6tY3UY7njOmJVr5nEspxO6OCJpPsWsbYAL2tJ0Orq2Zxe2K5ZIlJQWWb3ZdUdNYZHRy6g5bje3gWuxWAEekbypfHY0w+iNlr7bQfCXz4GomLreVy37byH8qddUfuuQn6s/lXQLgyHfV9yMfbR8D6DzqvpeeeouWftrB8JPjsaYfRFyz9toPhL57+uqf3VL/AKwqPW1P7qk/1h/Kn1Mp/av7kO2XgfQf8djTD6I2WvttB8JRGqumJFxqLln7bQfCXz39bU/uqT/WH8qh19SDY1E1u8SFPqZT+1f3IduvA+h3Ds/ZHxd/V4XnHBa1x4btPXxSH3riudZLE8+a4FfOXDX4hSvElNiVVC9puHRzOaQfSCsw9hfbKz9k3UnBNNM+5jqsXytj0zaGndWPMklFO82iLXnjulxDSDe17jktC/4TqWtJ1aU9yXdgvCpGXI26Lg8RznlLB6t1DiuZ8MpKhtiYp6uONwv3gm65mOTrBvCxHYViHt0aRPxbCYdUMFheavDh1GINZ81Tk+a/+KbD0O8F4ulWlG/vIW9aexS5Zx0Z52r3VaxtZV6Mdzj3egyZOo+QR/8AeuC/bCL4Sv8ACc1Zax2R8WDZgoK98Yu9tPUMkLR4hpNlpwZJMxx35HEj6Yr0bQPVir0n1Kw/MLp3/E6dwpsQjBJBgJ4ut3t5hfQNQ/w3drbTq0K26SWcY6+84y042lVrRhVppRb8Ta+0gi4UVZYXXQ4hQQVtNKx8MzA9jmm4IPKyvV8uSw2mfRIyUlldAiInIsQcQBcrga/PWTsMqX0eI5pwqlnj9nFNWRse30gm4XH6qZ7w/TnImK5txORgZQwOdGwm3WSWsxg9LiB61qczTmfEcz5grsxYrM+SpxGd1RK4uPNxvbwAXWcM8K1OIpTblthHvx1fgcvr/ES0dxpwjuk/cbazqRkDtzrgvqxCL4S57D8RoMUpGVuHVkNVTyewlhkD2O9BHArULkDJuLag5xwrKeFmQyYpO2MkEncjHF7j6GgrbRkzLGG5PyvhmWsKj6ulw+mZAwd+6LXPieapxNw9Q4fqQpQq75tZaxjBbh7WrjWN05wUYroc2oOsBcqKg7gL9y5Y6boUqiqgpYnTVErY2MBc5zjYNA7SewLgTqRkIHdOcsEBH/eEXwl4Jtt6xHJeTWZIwioLcVzA0tkLTxipb+efDe4tHrWvvrpT+2P4/TFdzw3wVPXLZ3VSeyOeXLOTitb4r/Rtx2FKKk119ZuDpM+5Lr6qOjos1YTUTymzI4q2N7nHwANyufDg7kVpmwnGsTwLFqPGcLqZIquhmbPE8ONw5putq2huqFBqzkDDs00j4xUPiEVXE03MU7eD2n1i48CFqcTcJVOHlCqpboPvx0Zt6BxItXnKlUjtkunpPRLDuRQBUVyKwzqk8hERSAiIgCIiAIiIAiIgC089J7is1ftLCjdKXR0GB00bWk+xJlmcT67j3FuGWmfpKv2UmKfWyl/9a6ThVf6hn0MpU81mLO80N3hdZfbOHR3YltC6YUOpsGp8eDR1008LaR2G9cWdXI5l97rBz3b8lh8R8rt4rcz0af7FXAB/22v/AKQ9dXxJeVrK2jUoPDyY6PPOTHf85/xf6NsH2m/vVH86Axf6NlP9pv71bL1AuA5kBcT9YNQX7z3IzbV4GtE9EBi/ZrbAP9Df3qgeiAxk8Pj3Qfab+9WzC471AnuKPiHUUuVT3IbY+Bo62sNlSs2W8WwDC6rOLMf+LcM8oe2j6jqurLBa28699/7i8G3gObgPSt1u1BsaZc2ocey/imZM3YnhEGBxTx9VQxsLpusLDxLgbW3PurhMj9Gxsu5SbHNiOVq7MFVH+3YnXyuB9MbXBnutXRWfFdOjbR+kZlU78IpKjFvkdV6KHFZ6zZ9xahkkc+OjzFO2K5uGtMMJIHhe59azXK67kjIGR9N8I+IOQ8r4ZgVAXdY6CgpmQte+wG84NA3nWAFzx4BdiXGXlwrq4nWSxueTJ6CDlrA6XP8AVrkT+AVX4bVs/ctYHS6fq1yJ/AKr8Nq9Thv9pQ9vwEvMZr/WTuylsP4ltRZVxbNVLqFHgDcLr/IepdQ9eX+Y129ffbb2VreCxiK2n9Ee4HSPNxJH6oD+JYu34gu6tnZupReHlGvSSbeToH50BjB562wX+s396ojogMX+jbB9pv71bL0XCLiHUMZ7T3IzbY+BrQPRAYwBw1ugv9Zv71eea49GnnTRzT7FdQcN1AoMfp8FgNVVUz6N1O/qh7IsO84EgcbGy25Lx3a6MR2b9Q99wv8AEGq7fpCti14hv3cQjKeU2u5eIUIt4aNEPZdZGdHxVy0m1lk0xEjrm1kbrHmDTSrHQ8uHeshdgHjtZZIv89Wf0WVd/q3Oxq/9WYKXno3chRRF8fXJG13mDPSy4pJT6L5cwyObdFZjzHPbf2TWRPNvdsVqmvZbR+lu4aZZNt24y/8AFOWrdfTeF1jT013tmCtzaPeNk/ZbrNqPMGN4HSZujwA4PTR1BldSdf1m861rbzbclkx+dAYx262wfab+9XFdEb+uDngf92U/4wraKvD1zWb21vZUaU8RWPDwLQjHC5Gs/wDOf8X+jbB9pv71R/OgMW+jZB9pv71bL0XkfWDUPtPcvkX2rwNZ/wCc/wCL/Rtg+0396pJuiDxuONz263QkNaTb4j2//atmapVX+DS/UO+8n1g1D7T3IbY+B85mJUL8Mr6rDny9Yaad8W/a29um11d5Wq5sPzTgWIU7t2amxOlmjPc5srSPuhRzT+qXFf4bN+GVQwT/AB5hf8Ng/GNX0tN1LbMu+P8AIwNYqcvE+iPAHvkwWhlkN3vp43OPpaEx7CaLG8Iq8JxGnZNS1kL4Zo3C4c1wsRZQy9/iPD/4LF+AFyBAIsvjKlJS3R6pmapFTTjLozUprbplW6S6gYjladrzS9Z11DKRwkgfxbx7bX3T4tK6G08SW8xwWxvbN0b/ADf5FdmbBaMPxrL8bp2WHnSwc5GePDzh4jxWuItd2gtLeBuv0PwfrS1nT1vflx5S+ftPinEGmPTLxwXmvmv79Bn7sO6wDM+U3ad4vUh2JYC1vk28eMlLyH2JsPQQsrL+BWofSjP+J6a57wrOGGyPHkswbURNNuthPBzT9/0gLbBlbMNDmnBKLHsNqGy0tdAyeJzTwLXC4++vlfG2iPStQdWmsQqc16H3o77hLVfplr2E35UPgcyOKle7d7FEEW5hdJ1k1DodMdP8WzXVvbv00JbTxk2MkzuDGj1n3AVyNGlO4nGnBZbeEdTcVo29KVWbwksmH+3ZqyMbx+n0ywmrvS4YRPX7vJ85F2tv3NBB9J8FiaGXNpTf0K+x7GK7MWNVuM4lO6arrZ31Ezyb3e9xJ++u0aP6bVuqWfsJyjSB3V1MofVyC/yqBpG+73OA8Sv0VplrR4X0lRm8bVmT9PefELyvV1m+clzcnyMsthDSE4VglVqdi1P+iMRBp8ODx7CnB85w+qIFvALMBgIaAVxmXsDoMuYRR4FhVM2no6KFsMMbG2a1rRYABcovgerajU1W8nc1Orfu7j7HpVhHTrWFCPcufrIE2XF5kx/DsuYHXY5ik4gpaGF08r3cg1ouVybiARc2WHO3XrE7D6CHSrBaz5bXWqMTcw+xhHsIyfpjY+hvip0fTamrXsLWn3vn6F3ldX1COm2sq0uvd6zFLWHUfEdUc+4lmysld1VTKY6WIm4ip2mzGj1AE+JK6tRYfXYjI6GgpJah7I3SubG0khjRdxPgALqz3QXb1+XIFZu7EWiscWAV2ouZqESPxZhpKGOSO/6H+bfY/PHh6G+K+86tqdvwrpkFBebhJeJ8gsLGvrl4455vLbMI3EixbwJ5rIjY01gZp9nv8yeK1Jbg+YntjG8eEdTazD/G4N9xdB2gNMZtKtS8Sy91Lm4fMTVYe43sYHHzRc8yOR9C82gfLFNHNFI5kkbg9j2mzmuBuCD2FbFzQt+J9Jwuk1leh/0MVCrV0e9Uu+LN0UB32NeDe4Buqy8W2W9XotVdO6SWrnb8V8LApK6Pe84uaPNfbucOPu9y9pX5zurWpZVpUKqxKLwfbrK6p3lCNan0fMIiLAbQREQBERAEREAREQBaaOko/ZR4r9bKT771uXWmjpKP2UeK/Wyk++9dNwp/v/YylTzWYr9h9C3MdGn+xTwD+G139IetM/YfQtzHRp/sVMB/htd/SHr3uL/9pH/sUodGZTlYjdJLqTnvTLR3CcayDmarwOumxuGmknpiA4xmKUlvEHhdo9xZcrFPpENHdRdaNJcLy5pvl92LYhT4zFVyQtkYy0bYpQTdxA5uHurh9NdNXlN1cbc889DOjWWdrvacBt8enMP2cfwFD5Lzad+jTmL7OP4C7L8gRtajlpPP/LIPhqHyBG1pc30mn/lsHw19I7bRvGHuMGKx1sbX203fjrTmL7OP4Cm+S/2muQ1ozF9nH8Bdb1U0R1R0TqaGg1Nyw/B58Sa+Sma6Zkm+1lg72JNrbw91dCJNua26VpY14KdOEWn4JFZTqQ5M3CdGvqTn3VDR/G8a1AzPV43XU+OSU8c9SQXNjEMRDeAHC7j7qy8Cwf6Jr9YzMP8AnHL+IhWcC+X6rCNO+qQisJM2O5EHLWB0un6tMifwCq/Datn7lrA6XP8AVrkT+AVX4bVu8N/tKHt+Al5jNf69r0H2uNXNnXAsQy9p2MH8kxKq8smNbTOldv7obwIe3hYBeKDmuZwPJucM0U8lRlzKeL4pBE/q5JKOkfK1rrXsS0Gxsvpd3RoVqey4xt9JqQcovyTKH89D2nBw3cqn/R0n/up+eh7TvzuVPtfJ/wC6sbZdLtTaOGSoqNO8yxxRtLnvfhkoDQOZJ3V1kuPeR4FaENJ0up5lOL9RZzqLqZcfnom052tyr9rpP/dXSdVtuzaB1iyxV5LzNjWGUeFV7OrqosPpOrMzPnXOc5xt6LLH0cea5fB8l5vzRA6qy7lTFsUhjduPko6R8rWu7iWg2KutK0+3aqdnFYJVSo+SOHA4LJTo68JqMV2scqvgY57KKCtqJSPmW+TSC59ZA9a82yps06/Z1rGUGXtJsxTPk4CSakMEY8S+SwC2cbC+xnV7O2HVWcM6zU9Tm/GYhFIyA70VDT3B6priAXOJF3Hl2Dlc6Gv6pb0rSdKMk5NYwi1Gm4vdIy8Q8kUDyXzH/iZkYE9Lb+tnk369P/EuWrcc1tI6W39bPJv16f8AiXLVuOa+n8L/ALPj638TDX7jProj/wBcPPH1rp/xhW0VauuiP/XCzz9a6f8AGFbRVxvEn7Tn7PgXh5qCIi8QuFSqv8Gl+oP3lVVKq/waX6g/eUeAR87Gaf1S4r/DZvwyqGC/4/wv+GwfjGqvmn9UuK/w2b8Mqhgv+P8AC/4bB+MavstP/aL/AK/yMEv1h9EWXv8AEeH/AMFi/AC5Fcdl7/EeH/wWL8ALkV8ZXVmcoVVOypifDKA5j2lpaRcG61hbUekEmlWo1S6jp3NwbGXOqqJ+7ZrDfz4v4pIPocFtEIB5ryLaZ0kh1X01rMMpKdhxWjvU4e8jiJWj2N+53L3F0/CutS0W/VRvyJcpfM5vibSlqVo3FeXHmvkas3ODRvBZtbCWsLpaKp0pxqr35qfeqsM3zxMZN3x+okkeB8FhVU0k9LUSUtTE+GaFxjljcLFjhwIIXJ5NzhimR80YfmbB5Syqw2dsrSDbeaD5zD4OFwfAr7VxJpFPXdNlCPOWN0WfLtI1Cel3kandnDRuLLz1Zkt2XWv/AG5dXXZpzbT6eYVPegwJ3WVe6eD6ki1j9SCR6XFZN5t1/wAv4ToR8dKgqmzOrKMCkh3hd1S4WDD6HXv6CtZmI4jX41iE+KYnO+aqq5HTTPebl73G5J8SvnHAegSq3cr24jyp8kv/AC/odnxbrMXQhbUJZ3rL9XcWcQc3ee4WHMrYNsQ6PDKeTn6gYtTFmJ5gs6BrxxipR7G3dvEk+jd7liJs/wCldXqxqTh+XCwvw+ncKvEH2uBC03LT9UbN9a2pYXh9NhtDBQ0sLY4qdgjY0Cwa0CwC9H/EXXHy02i/TL5GpwZpfaTd7UXJcl6y9RFTkdYGxN7L5Lk+ms65qPnTC9Pso4hm3GJgymoIHSG54udbzWjxJ4Bam885wxTPWa8UzXjcpfU4jO6Yi9wy54NHgBwCyZ25tY3Y1jNNpbg1YTTYdaoxLdd5r5j7Bn8Ucf4w7QsSJW9Y6znetfbP8P8AQ/ods7+svKn09C/qfJeLtVd7c/R6b8mHxO3aRadYhqjnzC8oUTHg1UwfUSgfpVO3jI7wO7cDxIW2XLeXqDLWC0WCYZG2KmooGQRsaLANAstZ+z5rthGiNRiFe7KJxfEq20bZzUdX1cQ+ZHmnt4n1L3H88RaP+jo/y7+wvL4z0zWdbvcUKTdOHTmufpNzhjUdN0ug5VpeXLryfJHpO2dpGM+6fvzJhlNv4tl7eqGbjbulp7fLGerg7+L4rXG8GNvI35hZmVXSDQVsEtNNprvxysLHg13Ag8D8wsRsx1+GYpjtdX4RQPo6SonfNFTufv8AVBxvu37QL29C9zga11PTaUrS+ptQXNPK9qPL4nubK9rq4tJc315Ho2zBq9NpPqZSVtdO9uD4oW0mIMvwAJ82T+KT7hK2j0dVFWU7KiFweyRoc0g8CCtMDjuecBxWw/Yu1iOd8i/mOxqt38YwFrY/OPnS0/JjvG1rFeF/iLoeGtUoR9Ev5M9fgzVtknY1XyfT1mTCIOSL5SfSwiIgCIiAIiIAiIgC00dJRw2o8V+tlJ9963LlahelKy9VYZtDUONyRFsGK4HEGOtwc9ksocPcc33V0nC0lG/SfemVn5rMOQeBPZZbmOjT/YqYD/Da7+kPWmizrABq9EybtDa36dYJHlrJOpWL4RhcTnPjpactDGOcSXEXaeZJXZa7ptTVLdUqbw088zBSko5yb+1AgHtWiMbYe039GXMP2UfwE+TD2m/oy5h+yj+AuU+p90/+cfeZu1gb3bBLc1oi+TD2m/oy5g+yj+AnyYe059GbMP2UfwFH1Ouv44+8drDxMoOl0Fs25B4//SVv4UK1+kXC7bn7VfUXVSoparUTN9fjs1C1zKZ9WW3ja628BYDnYe4upOPBdppNlLT7SNvN5a8DDVmpyyja90TR/wDkXmE//wCjl/EQrOELDnos8tVmCbN0mKVkJYMaxqpq4CRbeiDI47/ZRv8AcWYy+Y6tJTvqrXibPciDlrA6XP8AVrkT+AVX4bVs/ctYHS5m2dcifwCq/Datzht/6lD2/AS8xmv8Gy2ndEgxp0jzdvC//wAQHn+8RrVjftW1DokSPjR5u/zg/wD0sXX8VP8A09+tGCiubwZ1z0sNTE+CWNrmPBa4FoII9C1mbdmwOcEfiGsmi2HF1K5zqnF8Egb+lA8XTQMHzPMuaOV7gWWzhSTQxzxuimja9jwWuDhcEdy+f6ff1tPqKrSfrXiZ+qwz5wmngedxwI7l6loFtD6g7O2bY8x5NrXy0Urm+X4XM4mnq2DsLeQdx4OHEehZjbd2wXNE6v1k0TwQWdvVGM4LTM58y6eFo91zR4kdy10+fu2eC1w4OB5gr6fZ3dtrVthrOeq8DWlGVKW5G9/Z22kNO9o3KjMfyjWMhroWtFfhkzgKmkkI5Ob2jnZw4FevNFhZfPTptqVnbSPNVJnPIWMT4diVI692nzJm9rHt5Oae4rb/ALJu2jkraJwmLCK90OD5ypYr1eGOk4T25yQ34ub224lvbfmuG1rh+rYN1aXOHwM8JqouRkwh4qVkjJL7p5KZc4unMsYD9LfcaZZOIFwMad92Jy1b8ittPStZfqsT0Dw3GadhczB8bgmnIHsWPDox75zVqWvfivpvCslKwS8GzBW6oz66I8//ADBzzbmcMp/xhW0UX7V89uQdU9RNLaqprtPM11+BVFYwR1ElKWgyNBuAbgru42wNp7t1lzB9lH8BaOrcN3F/dyuKckk8dS0akUkmb3EWiP5MHad+jJmD7OP4CfJg7Tv0ZMwfZx/AXnfU+7/jXvLdrA3uKlVECml4/MO+8tFPyYG079GXMH2UfwFSl2v9p512fHkzAWuFiCY+PvVD4Pu1z3r3hVYHmWaDfM2LDurZvwiqOC/48wx3/bYPxjVbzTS1Mkk873PlkcXve7m5x5lc1kHBanMWe8s4DRtL5sQxejp2NbxN3zNH+9d24OjbbZPpH+RhzunlH0I5f4YHh4/7LD+AFyCssHgdTYbS07jxigjjPpDQFer4uliTNkKSZgkYWk2up1Ai45XViGso13bbWjrsl52bn3CKUtwvMDyagtHmxVXM/ZC59IKxntYbzmgeK25avab4dqfkXFcp4hE39FQkwSWuYpm8WOHoP3LrVDmXAsRyvjNblzGIHQ1tBO+nmYRYgtNr+g8x4FfceAdd+nWn0Kr58PfHuPkXFmlfQbrtoebP495PLmjHajLdLlSXEJX4VSVElVHTk+aJHgAn3o+73rimh0shsePYP9ym3d1oA43HYvXdmHSOTVHU2jjqor4ThDhWV5Iu14B8yP1u+4Cusv7q30a0qXOEksv1s561oVb+vCjHm28GYWxrpB8brT9mO4pTiPGMwNbVThw86OL9rZ4eaQSO8lZDW7lRpYI4GNiijDGMaGtA7AAq6/Nd9eT1C4nc1esnk+52FpCxt40IdEgvPNcNTqLSnT/FM01TmGaKMxUkRPGWdwswAdvE3PgCvQJJWRtLn3sBda59tLWF2fc/Nyhg05fhGXnFjyHebLU8nH+L7H0gr1eGtHlrV/CgvNXOXq/qebxDqkdNs3JPypckY/4vi9ZjuKVWMYlM6WqrZn1Ez3c3OcbkqwdutN97t7SpvPc42aAStg+yZoBl7CdNabGs5Zcoq7EsbIrLVdO15iiI8xo3hw4G58Svtuva9b8M20Go5zhKOe4+V6VpVbWq7pxfpbNepkbvh7SAR3FTb5+eb9xbefjSaaf9QsD/AJFH+RPjS6bf9QsD/kUf5FyC/wATofYP7/6HTfUSt9qvuNQZd51+tA8LqZj239m33Vt6+NHpqeP5hMDHh5FH+RSP0h02IsMh4GT/AANg/wByP/FCOP1D+8j6h1vtV9xqLJ3SAbG/Jd00Z1Lr9K9QMNzXRySGKKQRVkYJtJA628CBztYOHiAu57VekbtLNSqh1BSdXguLg1dDujzYzez4x6DY27nBeLMjIF2tN/Fd7b1rbiDTVN84VFz9H/4chVp1tLu3B8pQZuWwDHqDMWEUeNYZUMmpa2Bk8UjTcOa4Aj765IG44rDfYV1hNdh1RpXjlVvVFA0z4bvni6Em7mDv3Tc+g+CzHjdvNvay/O+rabU0q9na1O58vSu4+06TqEdStY149e/1kyIi849MIiIAiIgCIiALHrbB2UcJ2nMlQUUNczDMx4Q90uF1rmbzQXAb0b7cdx1hy5Wvx5LIVFko1qlvUVWk8NA0hZs2DNqrKVXLTnTKfGYYiQKrCqhksTx3gOLXe60LqnyJ+0wT5uiuZ79oNO34S30JYdy6aPFt4liUU/vMfZRNDHyJ2039BTM38nb8JPkTtpv6CmZv5O34S3z2Hclh3Kfrdd/wr3k9nE0MfInbTf0FMzfydvwlA7J2039BXM38nb8Jb6LDuSw7k+t11/CveR2UTQ1DsjbTlVK2GPRLMrnOPAOhY0e6XL1rSXo1df8APONU5z7hMWTsDa5pqX1UzZKl7b8WxxsuL/VOH+5bi7DuRYq3FV7Vg4RSQVKKeTrOm2R8D01yXhWRct03UYbg1M2mgaRxIHNx7ySST4ldmRFzLbk3KT5syd+SDlrx6TjR3VTU3NmTavT3IuKY/FR0dQyofRxhwjJc2wNyFsPRbVjdzsK6rw5tDqsGhg7J+0wAd7RTMx7/ANDt+EtjfRkaaZ90z0wzNhuoGVK/Aaqrxozww1bA1z4+qYN4WJ4XBWZlh3IvU1HX6+pUexqRSREYqHQDkiIvASwSSSxsmjMcjbtdzHetd23B0f8ALjVVVasaGYS5+JTOMuK4FCABUOJ4zQ9gf3t4A8+fPYoi3LG+rafV7Wi/Z3P1kNZWGaFvkUdpgf8AQrmj+Tt+Er3A9m7awyxjNHj+XtKM3YfiNDK2aCpgiDZI3g3BBDlvcsO5LBdBPi26qLbKEce0oqUYvKMatkvWTW3N+BnLOuul2N4Dj1DGN3E5acNpq5g7SAbsk7xyPYRyWSjTvNDu8KKLmK81Wm5pKOe5dDI+Z1HVLTzL2q2ScWyBmml67DcYpnQS8OLCfYvaexzTZw8QFqb1X6NvaFyJi1Q3JOCwZywYvJppqGZsdQGdgkjksAfQ4rcki39O1a50xvsXyfcyJRUlhmhqbZK2maaV0MuimZg5vAgQtI90OspDspbS/Zormgf+Hb8Jb6LDuSw7l7P1vu/4F7ynZQNC3yKO0x9BbNH8nb8JPkUdpj6C2aP5O34S302Hclh3J9b7v+Be8dlA0L/Io7S30Fc0fydvwlA7KG0t9BXNH8nb8Jb6bDuSw7lP1vu/4F7x2UDRNhOxntS41M2Cj0Xx5u8bF9QYomt8SXPWbGxd0e2OaYZvo9VNY5qKTFaAF2GYVTP61lPI4EdZK8gAuAPAC4Bsb92wFFpXvEd5e03SeEn4ExhGPNEGtDeSiiLny4REQEHC7SB3LDTbL2esczFilLqBkLAJMQrKgNpsRp6dvnuIFmS27eADT6lmYi9HS9Tr6RcxuqHVfczztU02lqlB0Kv/AOGpdugOswBI02xwuPL5SPyrPzZW0b+NRptTwYrS9XjmJEVWIX9kx5HCP+KDb3V7ZZvzoRexrfFl5rtFUKyUY5zy7zytJ4Zt9Kq9tGTk/T3ErRa1lMicexcqkdLyXI8811x7NWAacYnU5JwarxLGZo/J6WKmZvOa53AvPcALn02Hatbk+g+ttVO+pqNOMefLK4vkcYgd5xNyefets1h3JZvzoXS6DxLX4fjJW8E3Lq31Oc1jh+GsTU6s2ku41m6MbMWoOYdRcKpM45OxLDMGhlFRVy1MYDXMYb7nPmeS2VUdJDSwxQwtDWxtDWtHYFcWHci1dc12516sq1xhYWEl0NrR9Go6PBxpPLfeERF4p7QUCbKKIDxbaj0hfqxpvVUeG0okxnDSavD+HnOeB50Y+qHD02Wv92gOtgNvjcY2Lc7wj8q20qFh3BdTofFt5oVF0KKUo5zz7jmdW4Yt9VrdtKTi/QasclaYa/ZDzZhmbsF09xwVWGTtlDeqAD2385h48nC4PpWzrK2I1OLZfocSrKKWjnqIWySQSiz43EcWkd4K5S3cwKPHtWpruv1dfqRq14JSSxlG1o2iR0dSjCbafcERF4J7gREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAES4704d6DIROHYiEZQREUZDYRE9aZCfiLHuSx7lCx70se9SMkbHuSx7lCx70se9BkjY9yWPcoWPelj3oMkbHuSx7lCx70se9BkjY9yWPcoWPelj3oMojYonHvRCAiKBNkIIooX8CoqM5AQEEFFACxsFJZEUREJCIiAIiIBZERAEREAREQBERAEREAREQBERAEREAREQBERAEROHagCJw7E4dqFdwsUtZOHf91OHf91BuCIiYYyLnuREQZCX8ERCMD1J6kRAEREJCIinJARFAkDmmQRRS7xTeKgjciZFLvFN4oT1JkUu8U3igbwTIqbpCDawUokI7FZRyV3Iqm6lv6VL1ru4KG+U2sOSRUue9QJPYVJvnuUzSTzTaFLIMh71LvlC3meKlU7UM5KgkHaSo9YO9UiQOahe/ap2oNYKpfcjipmkE8CqQIsbkBTw8iquOBHqVE9CIqllyFz3JcnmERCchERMMbsCxROA7fupw7EG4IlwOaehCU8hERCSDSHC45KKkjbusDT2BToQ3gXI5IiIQufMIiIWCIiAIic+whCMhES1kIbCetEshGWERLjvTCJCJcd6XHegCJcd6XHehGAiXHelx3oAnrS471Dh3/dQZIooEgKG94ICZFLcpcoMkyhvN71C5ULDuQjJNvN71Au7lCw7kQZCIiBvwCIiFdwS4vZQ3uwuUpPG91bDI3k5NlTceJ4qO8FKVZIxSk31CKIt2p5vcpIIDmp1Lw7EsSoYJksTyKkPBRBsmATbpCKAdx5lQLvFR1BBFG/gFKd6/YrmRVEyKjD2qXeI5hTRA8Sqy6FlJPkircjko7wUqLGXTJt4JvBSohbPInRS3KXKEEyKW5UQe8oCKIiAInaiFo9Aig17HgOY4EEXBHcooSEHAHxREK5HDtKJz7FC4QZI8e/gilJ481Fp4ceaDJFOKXChcIQRRSk+KXPegJibKG94KCggIkk8lBEQZwEREIywiIgyEREI5hEQ8OaDAREuEI5BFAuHeob/YpwMomRSFx71HrD3BTtK7iZFJvuJ5cFNf6ZNpO5ESSOxAbqk54BuSFSdM72LTa6lQKSqYfIuC8AW5FUy8nkVRu7xUQT3lXUcGGUm+RUUQHKnc/PFS77u8qcEIr8e0KG6VRL3drlHrXW5ptZbGSqAQhNlTEjz2BOseO5QPQVN7wU1yqO+88d4BRErhzN0wFlFW1+N0IsqDpX8e5Tsk83iowyzXIm3lAm6g7jyUjpS02LCpwY22Vd7wUrpLEcOak3xz4qBe08DdSkRknMg53VWJ128uatLgnmriE+YLHjdQ0Xpvyitc9yiqdze1uKn4gLHtM+5tkUUAeHioB1uajBbKRMihvBRvdRgncEREJ69SINlMOKkUboM45ESbIocSiF1gtMGIdhdI8cjCy3uBXqscHs3CqRrTe0LAPcV5coVIk2IHeoqQnvUN8E2uVKTYJybKVQcbKI5XRrBVSyFEcBfvUELgGgFQEyJ4qCA3F0QnIREuEI6hFDfam8FKWSHJLkRQ8AoHjyUhvdTtKueCoDfmpd8jmFAclE27kxhkdoOsHcoh3gpbjkoq2EVcnkb47lEOv2KQ80amETvZNv8AGwTePepTdQue9MEOWSPnKPZxUt7dqiHCynmU6Am6gokg8gg5oCCKY2CtpqncuBxcpxkjKSyVi/d5mwCpSVN+DOKt+tdISXH1KW4V1Ao6hV378XKLXNLuAVG9wQo3tyVtpTeXG8VK43Kpb3pUQ8dqjbgORU3r80uO9SbzSoAgE3HBMBTZV9CiGlUhI3kLqYHj2pglVO4n5c0JuoX9KgCDyVS+SKmt6PcUhNlAO4XU9Q5k9t64so33eCgLWUbDuVcDdkdYRyCPO96VB1kupwVzzJHAhtwLhSB7SOKq3J4FSuZvDgFKZD9BIS24sFUpyS3n2q3dwNjwVxT8WWHO6mXQmlPyuZXaDfeLiqhcbKQjlZQs5YjYTSZOHWTeaVId6yixwtayNEuSaKgAuo8uSk3uKjvDvVcBLwI76b/HkocCo8O5GRuJuN7FFKohwHBVwZIy3E7UUgcLhFBePQs8I3WYXS27IWD3qui834Kzwmwwuk4ftLPvK7DSeKvhEbiNyVCxHFR4Dtum93KfUY3PmQuTzCn3iBZS73eoEpjJVyx0JrlQcSm94qBvZMFdzJgeCbxPIWUoIA4oXW7EwWjPHUmuTwugJUgee4KYG4UYJUt3QOIA4qHm+KgnqViz295MPAoRfipDvHlwQPI5oYceBNy5Jz5qBcD2IHAIThk26EAspC8XtYpvhqJEPkTEElLEKTrCTyKdb4FTgjcTm9uJUFIZeHIqHWelMEbicedwKXAJB5KkJCO9PZcSFOPEcypvDsN0Ly3iqd7dihcJgc8FUP3hxXH1D92U2V1dcfO8dc4WuslOPMxVeUeZHrQCo9Ye5Ud69wOBsotfcjvWZRwaymuhXa8/OlR6xUi89pRNpLqIqb5TfKp3PeVEOITaWVRFQSOHIBOtcfBUy4lRa7sUbSd6ZUvyN1EOdfmqbXBR3x4qNpO5FXftzKdaWjhZUg4FRB7lGxEqZOZC48e1TgkcOCo71jzUd53f91NpLkmVhI4cLBRMzr8lb71+1Ruo2Ebiv1hPYm+VRD7digXFxTaW5PmV2yA9oUd49/BW4FxbuS5B5qNqKqSK7gx3suKmhswcO0qgHXIF+arB4BtYqrXcTHk8lze/IpcEcCFbmTwKb4B5quGjMpZLkcRxUu6AQB2qlvnsdZREnebqME5KpHG4UeKkEgtyKdaO4qME733E483kVHeO9bsUFK7mowTHqT7wPJL3KlBIaATdRabn0Jgt38iINjwRBzRRhGSHNFrhJacMpf3ln3grolWtB5tFALcOrb95XBcFODHKXIiiAg9iEtAvZTgrlBQuO9SOkF+AUC8HnwRFW0ycuAKb6plw7OKF4cBwTDIKhIKcAOBuqW93JvHvU4GUio42Hmm5UokeFLdCR32TBGSqHjvCmuO9ULlA49vFME7slbeb3hS3Z3qnvBC9vJRhllnuKocwdoUDIO9U+fJQIPapwRnnzJi8E81G9+1SWHco+b3KcCTSF+PNCR3lQLgDy+6gc3tCYKqWCPuqCiHN7FBzhfigzkgL9qjvWUN4KUy2PJTzZOWVLg8ypfOupBO0fMo6YOFgLFTtZGWVL94XFTv+Xv49qvt53euHqZPlz7Hk5ZaUG2a1y/JKpfY8Cotfum9+atmneUxPm7t/Qs+w0t5db/aSoiXuKtRJwsexN/uTYyVJMuuuPgnWd91bMJJVXe8E2jcVt8d4TrAL2PFUd4KXeAcR3quCVLJdCRtuJsoOey3Am6ty8BN7wTBffjkXQezlvIX2+aVuDc3UQ64vZQ0O054K+9fjdL+KobxvdTdZ4JgneVd63anWWVAuv2+pN7vKYJUiuXk96qM3eZVt1h7bIZHdhUNDf6S5D7cSQoOceJHFW3WGwCqRylzuXJVcQmmy4YCG8ean3/D1qmJL34ck60DmqpNmbdtJ98lQJtxCkMjSm+3vHuphlozKrZONnKcObzBVsXjuRshHI8FG1st2/cy6EhB5hVQbjsVo2QP58LKs07w4c1SUTJGaZWDgfyqIPaDwVIXaOd1M08OSq0X5YJw4E8bhTXaBwKpE35KIcQFGApPJXZxNiikid5w4cgio1zM9PoW2Hk+SQ8f2tv3lcXAVpQkmigPtbfvKvvACxVsGqT74sbFSEk9qgCUJsLqyIckLkdl0LgeFlDe7lKTZSkVc/AiXW9ChvcQL81KXG3JU3SNuCTY9ytgpuLkOHLtQnj3Kg2a/EclF0vaTZRtL7srmVS9g+aUjpO61vFWxqAXbrGlzu4Ko2mrZuG6Ix4phR6kxe7zeZMXm3svuqHXgDi5TDChb5dUuJ7hwCj5FQxcZHDh889N0Szpz78Iomsjb23UwrYeZKnvgreHXwA+Lwgkwcmwlpj/H/rUJrwYUJfxIl8rjPFpsPSpm1DHDi5TinoJTw6v1OUThtKfYgj0FN0F1MnZzkuTRKJmct4IJGk8HKR+FsFyyd48DxVA0NXHcseH/AHFKdOXRmJwqx6ouC8EnwTi7krQvqIf06J4HfbgjKtjrcVkUfAxuaXUuiS3iXWCbzTx3iqHWNebA371Nfusm3xG/PQq74KgX24boVLfCgZAEwhvwVi4HmAFDeaFS6y/JQ3im1Fe0WStvBcFUO+Xv9JXL77u9ddqZSKh9j80VsUIeUaV7U8lF0H2PNRM1+StN8jtUzHk2481s7DRVRMr9bZ1yeBVQSNPEOVq83Nka42t2hHDJbtFFl51zB2p147CrQuKma42VdiLRq8y560DndR3wVbXN1Nv2PAptLdqi43h3qG+OwqgZC7+pA+3IG6r2ZKmi8bKLJ1rT2kK06x3cnW+CjswqiyXfWNJ4EqPWAq1bJ2qJkuo2F3NFxvi/Pim+DzIVtvFDIBzCbCFPBd7wt3qG/bsVsJATzUS6/HeUOmWUs8yuZL2ABueSumNDWgHmrKAlx60k8OAVwJru48VRrmZIvHMr3PeqYPE3Kh1xuPNUjng3HJVwW34Km+0cEMg7FR3m/Pe6oCS5sFKRDmyr1hTrCFSMljZSukuO5TtKufcVut8VUirSxwDjcFWO8L+Kg99vWp7NMlVdhzYk3hcOuPSp2vbbzibrhaepe1t2niDY3V9FVsk81xAcsEqWDbp3EZ9S+a5pPAqe43VbNcLAhTiQjzeCwtYM+UV43eeACikhN5Aiq+ps0X5JbUMgbSQhxtaNoPuKsZW9/BcdTNHk8Q3R7EdngqvVx8flYPqWVwPJVw33F4HtPIoXNHarHdib8yB6lA247oVlTLKtnuLxxBPNUzK0dqtC1x5PePQ4qG4QPZyfZFWUMGPtZRfQuHSDvUtxzuqAjkkO7H1jndlirkx0tDF5RidSG7vHdLkbUeXeXg5T5vkhF1sji2KM2+ePIKvJHSUreur6lrQPGwXUsf1EjpYnxYcI4WDh1r+A9QXk+YtVqXecDUPq5fpnHdXoWmj3V81tWEeLqPFNhpnkJ75HttdnnB8PaWUcZnI5lvAe6uqYrqnVRg2qqWkZ6d5wWNWbdbYaNj2VeJwwDjuxRG7vRYcV5vV6l5zzG8ty5lPFKxh9jLMwxxnxu6wsuvseCU1vrvHrOSuuNNQuf9vHav7/AL6mV+IarUjd7ynMU8ngw7osuvVOruXo3OMldM4nnvSLFasoNU68kYxmPA8Bj7WPq2ue37DeK4CvwPLTHEZh1Uq6t3zcdLSTPB77Eiy6OhwrplPk55foTf8ALB4k9W1Ku8yq/dzMs6nWvKEJvLWMHpmUkWueQC6z8YYw93Wj8qwzqhorh5+XV+Yqsdu5Sbv31YyZi0EaQ2SkzKLd7Wj/AHr0YcJWVReRTm/YI3d8/wB437DPLDdWMl4lJ+g8yNDh3TA29S7XQ56le0DDcyh1+QLr/wC9a5YMc2f5zwnzJSOv7IsBH3CV2XBMw6ZxvHxG1exjDHW80PjkaPRwC0rvgunjyIzXrj8jbp6re0Orf3M2K0GoWOwgdd1c4J5g3v8AeXYsP1Kopd2Osgcw8jun8qwZy1nTNdJ1ZwDVbBcWiFj1NZLuOd6DzuvVsA1Tx7cbHmPAS+PgDNTPbNH6rHe+4uRv+FI084XxTPYtOMK1FpTl/fxMsqPMOEVwAiq27x+ZdwKuZKKknG8WC57WlY/4bmPCsQAdQVroHkA7jrj7hXacOzZjWGFvy4yxgc2neb7i5mvolSi/8ttehnVWnFVC6X+asrxR6XJhUsdzTS730rlZyPqKd27LGW+rguOwrUKiq7R1YDD2ub+Rdmhq6WujDoJGStI42sV5041rd4qxPbpu3u1mhL2HHMqGubYOsp94u+bVefCIZATCerd9xcbNBWUjrPiJA+abxCU5wqLkzHUo1KXVci6uT80o3HerJtS53MH7in64nkwrI44MO9dxcl4HNdbnkaZXcfmiubLnEXII9K6tJMeueN13sj8yVsW0eZ5t/WUUsl+J2tPeo+UNBA4rj2yHtBt4gqdklyOz1La2I0I1EX4mClfMGv6wjgef5VbOfz4n3Cj3NdGQTzHcVVpZMnaJly6ccwOamElwOSsGOksA6NzvEEKrvuHAxuCOIjV8S86496iJwOdrq0EgbxId7iGUW7fsSquJZVlku+vFxwup2ycOParFsgvxafcI++qnXAc+XrUbS/a56F31oHJSlw7CrbrQfYk+4UMzeG6Tf0FNo7UuxK21iOPpQStBVp1ov7Ie6o9Y2489vuptHbovDKL+CCRhVqJWcfPHuqO+23C3uqu0dui43xdAd9wjBFzzPcFaGcAG7rK5pQQC9w4u4+pGsGWFTc8F/wCa1gaCOCiDwVDrAnWAd/urDtNvtUuhdF/AWVJz+JJPBUuu4XBVAzlymMCs6hcOlAPAqDpCTcK3MvgAoF7R2lXUMGPtF3lw2Qgm6GS/arYyjsUOt7ip2FHV55LnrLA3KkMgvZpKoOkNjxVAzEG91ZQKOsmXrZAyQG/PgVcdYAOPNcQ6YkcDzVanqjIyzj5zeBUSpMinWWTl4a50dmvNx95chHOx7Q5puF14TMIurimMpdvNcWjvK16lFNHoUrl9Op2Gne3rALorGmL31LHF1wQfvItCpHDPXt6uYZwRgPyiM/Sj7yn3ipIB8pZxvwH3lO7ha3athHjrpglsSSSVDtUyg4hWJb8CV4AF7qaGnkn43s0cyqkcDCzrqh+5E3iSe1dQzjn2lw6nfDBUdVE0W4c3ehTSpVLmfZ0kYrq6oabS7e6lhdy8TnMZzVh+BRPjpi18oHFx5N9JXjWbNUm78tpfKJG3N7+Y1dUzFm7EsbMop3CKmZcve9260Dvc5ecYjjFCZhT4bTSY7WONgwNIga70c3+ngF2emaHQoeVW8qX99WfNNW4nu9Vnso+RDwX98jlsZzljmYpZG4dHNXlvsjGd2GP6p580esroWM1FEXkZgza88f8AA8IAIPg6Ygj3Fy+IYVjNdG382GPxYdAziygpxvub4dWCAPWVxvW5bw+zMNwJtQ8X+XV7us94LAe6V1VGptW2Cx/1+fyPDhCMHnq/772cNTYpHv8AVZJyNTukvxqJInVc5Pfd1wD6AlfhmpOIgvxav8ijtxZUVTKdoH1NwrqrzLjdSw04rXRRAbojiAja0d1guGe6QgtdIXE87lb0N/gvbzfvMqznPL4ls/KFFvE1+bcPY7tMW9N90c1ayYBlOA/Ls1yH6mheVcubfzd0Ad1lRfE08xx7Fv06lSPWT9mC3N9/w+RZy4PkGQFr80vB+moXhWc+RNP65p6vNNCXH/K0zm/dK5CalifwfG0+kKxqcFopwQIQCe1q26d1KPNVJL7iVF+Pw+RxNToXhWJD/kTHMKmkcLgRVjQ73CV1jHNBM+4YzrqfCpKqLmCxhdw9LeC7PUZcqYDvUc7m/cVvTY3mzLshkpsRqou/ckIBXq0NRvv3VZS9DRmhXrUvNl9/9/yPKqqjxfL85ZWwVVFIw8Q5pt7q5rA9T83YIWnD8wVke6bjclJA9R4L1eDVWtrmCkzRhOH4vCRukVVOC631QsrCuyfovmtxfBDiGVqx/wC2U7hPTX7yw2IHoW5PVac/J1C39qWV8zap3FKtyrRS9K+RfZS2ps10BYzF6alxOIW33lvVy+62w+4vd8h7UmTsYkip6mtlwqdx3erqrbhP1fL3ViljWgOeKSF2J5UqaPM1EP2zD3/LgPpozxHqJXRHVddhtQ6hxOlmp6iN265kjDHI09vArRr8P6PrMXK0kk/D+jJVqovdbvK9HU2qYfmDDsWjbUNka0PALJonAtcO+44FdiosYxLDi2anqHSxix34zxt4ha29MdbswZKlZAK6aqogbOjcblo8PyLLzIOs+F5hooqsT7oeAN5nYfEdi+ea1wjc6fzxuiZaGsVbWeKuV6TJrA9QI57MrQO7eHMeldtp6ylro+sgkD2uC8Go8Qo8SYyaOQNcRwkjPA+kLmqDHcRweQSb5czhZ7Tw9a+f3ejxbfZcn4Hb6bxI9q7V7o+J6tVYTDNd8R6t/hyK4yeKqoyRLG63z1uBVtgeeKata2OsIDuHnD/euzskp6mPeYWvjd3HgvHmq1rLbVXI6SKtr+O+3fM4EVAcOfYutiUb7iRzcbFdxrcGDg59H5jvnTyK6HUMqqGV0NZE+NzTyPL1Fb9nKFTzep4WrUqlHG5cvEves4W4KLDwHLgrNk7HdvNVhIOFitxxaeDyozyXLZDcklThwc65VrvnsVVslgLWVXEzKWCE3yp++LkO5+CgZO8/dUZCZWFosT2elWRlI81xsRwKmEd3IxyqODyy+jfvHgVU37O5rj2TOaLA+tTtnJJu5HTYVbnkvjJcgn7qgX9wVp1zjbioiY9rkUMGSNfJyDH2bzUTJbtViKhx4D7injmdxLiFRwLKpzyXYkHOwQvZ2NAVr1pJ5qPWFV2mRVkXW+CDwCqDdIAsD6lZseG8XdqnbPu8ioaJVXvLgMY4hrY2Ag8fNBVwG973fcVKnG4wuf7J3EqqHAnnwWJm1D0oCMNPBzuPihYL8HuUbsPaEuO66jmZFjHIg2P2w81bmJ3+Vd9xXNxYnlZWbpCCbK0epWpJJIiYn9jx6wpHNmt7NvuH8qj1jjxuoGV5Fir4NWe1khjnJ9m33D+VQ3J2/Nt+6pjJbtUrpGniX8PQmWY3LBK41IHF0ZHoVMum3rb7VO54tyuFITvmwHNXjyXMx70SF0gaN1rHE9xspWms6wGNhZvcCeYVw2mdzkN/C6uHNHVFjeFx7imUkOxcnuyRpzJHYvcx58Wn8qufKpweBjsPpT+VcbHO6xbIRvNNiLW4qoJxa11jdPdzM8amOjOaw2tkdVMa4MNweQ8EVjhUoNdHY34O+8i8y5io1MI96wq5orLOwsaGxtaewIQOfcq1rdikeOB4KqeTX2sp8+FlHdjZGaiocGRs7+1Ra1jGmaZwbG0XJK821Bz8yGOWnppmxsZwLifNb+U+Cz29Cd1U7OmaOo6lR0ah29bm30XiVc96hNhHkdKN5/JsTeJPiV4ZmXNRmqy2cvxGucbMpoXEsZ4OI5nwHuhVqifFsySSihlNPSDjPVyutfvufvNbx71YSVVHgsZp8vwgSuFpa2QfLX/U9jB6OPiu10+xjbLs6a5/31f8kfJ9R1G41Wr290/UjiMWwyeoDajOeJOiYzzosMpbbw9IHms9PNcVUY9LT05o8CpWYVTng5sH6ZIPp5PZO91VamJ7pHTPkc9zjcuJ4kqwnj7bLoaFtGH6zn8DVjPlhHDTNDn7zhYntvzVrJGOJHNcrLDcnsVpLGGizRclegppdDJFnETQEP6w2AfwPpVu6PceSACuTliMrHMJsD99WT43HzSLEcCs0JZRlTOPcy54Ki+N28O5X/VgXBVCRljzWVSZdFlJEHE8bW4Ki9tm2B8Fdvab8lQkBcCBzReDLplsQ4cyVSdEyS7ZG7w7iq7mHtKpjgVkisZaLZOBxHAKeZ5MPyt3YOxddqqTEMOl3d07vf2Lv79y9zzUkkEE8ZbKwG45EL0be/lS/WLKIayuR1PCMw1uHziairZqadnJ0Ty0j1hdunzzhuaaYUOo+WKPMENg1tTuiOrjH0so4n1ldbxfLR3TNRjj86uAjmq6OTqpQeHAghb6tbW/XaQ5PxXJ/euZCqTpSyjna/QrBsxdZX6Q5sbLUNu44JizxDUjwjefNk91dWwHNGbdLsxeR45htbh1S11pYKhhaHAdo7D6QuYtHKBNHI6ORpu17HlrmnvBHEFdji1LmfQDL+qeX4c34HbdZLI0Nr6Ud7JhYm3cb371kjXvKEXTqf50PB8p+x9H7T0I1aV2tlfl6T2rT7U34r0rMVwau3mn2bb8j3Oavasq6gYdigFNVOFPKeB3vYOP+5YbZbyHJSPfnLQbMzsyYTF59ZgspAxCnb3GPgXdvED3V6dlnNlNmKnbUUZdTVLfMmge3dex44FrmniDcLk9R0uz1DLo8mu7o4+ho8+rSudJqboPyfczKfycN+XUThG48d35ly5jBM31uGy9VMXMN7Oa7kV4dlPUaswdzaPEHGeAcC1/sm+gr1igrMPzDQirw+RlREeYHs2H/cuE1DT523kXEcx8To9N1ftZZpPbPw8fUeuYTmagxNrR1jY5D8yTz9CvMQw6jxOIxVULXtPI24j1rxTra/DQZoHvkjZf6pvpXYMq6mPj3afEH9bHe29fzmrmrnR6kU6ts8pHYWvElCbVverr3nNYvlKqoCZ6BxniAuW/ND8q4RtQY3bsgII7DwIXptDiFHicAno5myMcOzsXG4zlmgxUF4b1U4HB7Rz9IWChqDg+zro2LrRYVo9taP2fI6bHUMcDYquJBZW2I4TX4Q/dqI7svwe0eaqEdUL7pK9RKNRboM56TlRlsqLDORa8cTY81Z1zurcJGtuHcHKs2pjISUMmjdGfmhzUR8l8ytT/ADI4RYie45AKZs1iuNdUdS4xSX3mGxRtWHO5rc7PPNHlfSGnhnLdeT2KLZeK4zyplxxdfxVQ1TQOZPrVHTZlhcnJCYjiosqOFrFce2qDrG/D0qc1DeQd29io6foMkbjJyDZ+PJT9dwuQuPbUsFzvH1qZtS08iFV0vQZFXL4zBXFGOtfc8mm/rXGNkL3hreJJsFzMEQgiawW4dvesFVKKwb1rJ1HkvGuBbZRBsqLTfkVOXC/ArVPTyTg2UQ6xvZUw4j5oFA43N06kqbJ3uO6SrIyOVy95DDfuXH9aFeCMNeeEVXS2FyoCYH0qi57TxJVIyt77LLsyabq8i4fICT4qTrOABCo9dv8AmsF/EqIbbiTcqVHBjVRy5IqsaXE71wO9VoyYzZvb3q3Y93Eb11Va+9kaM1NbeZdXuOSiX27FSEzORco7zSbrDtM8JPvLetHVETjk47rrKgJbg3V9JGJIjGbecOK6+6okglfDKbOYbFZ6Md2UaN1J0nu7jn8FlviEYueTvvFFYYFUb2JwgnmHfglF5t7DFU9rSqynbp+k9HPFS7veUReZuZ1f0Wj1x8SwxrC6jFYRTw15pmDnaPev90Lzyu0N+KlYKjEM1Okia64iZR7v3esPuoi2qF/cWvKlLHsXyPMvuHNM1KaqXVPc1/5SXwaRCv0NdWsbTx5pbBTRi0cDKDzWj/WcT4ripNm9sn/3jb/R/wDeoi26eu39JYhUx7I/I03wZoknl0PxT/MW8mzG14t+bW3+jf71W52WGm9s88+/DP71EWVcSamv3v4Y/IfU3RF+4/FP8xbybJwkN/zfW/0X/fKi/ZGDv+kC3+iv75EU/WXVPtfwx+Rb6n6Kv3P4p/mKL9j4OHm6h2P1o/vlRl2NRK7e+ONYnn/yRz/n0RWXE+qrpV/DH5E/VHRvsfxT/MUDsW3N/jlf7G/v1I7Yo3jf45f+xv79EVvrTq32v4Y/lJ+qWj/Y/in+YkdsSb3PU3/Yv9+qZ2H7n9c//Yv9+iJ9atW+1/DH8pP1T0dfufxS/MSO2G97/pQ/2J/xCpnYVJP66dv9B/8AEIilcV6uv334Y/lJ+qmkfY/il+YgNhQXudUr/wChP+IU42GB9FH/AGJ/xCIn1r1jp234Y/lH1U0j7H8UvzEHbCwdwOqPD6yf8QuOxHo/aXEG2OqIjd88MCuf6QiK9Pi/WqTzCtj/AOsfykfVTSH+5/FL8xxP53HIHbzdaCB3fmd/4pXX53e1zNyXV3fFrfqf/wCJRFsvjviB/wDyPwQ/KQuEtHX7n8U/zHHRdG9W4Xi8WP5W14qcFxGA3ZPT4Abj0jyoXXqx2TW4jhUb8yZ4gqsyRtDTjVFg3khmAFh1sXXPDj6HD1Ii1a/F2s3Mozq1syj0e2Cftaim/bkzvhvTJU+ydLyf+0vmItk+UU/V1WoXXSW/TBhO79zriuYy5s8Yvlqo8potRi5/C4OF+a4dxHXcURY63FGq3EHCpVyn/wCMPymrHg7RIvKo8/8AtP8AMd4m07ErQ5uLBktrFzabgeHHhvLhqjRdkkonpsxdRJe7i2juHerfRFo09WvKXmT9y+RtVuG9MrvNSln/AO0vmczgeQ8UwORslPmgutzBpLBw8RvruLd8MaHuDnAcSBa5RFp17ipcy31Xl+pL4HoWmn29jHZQjhetv4tkk9PDUxmKdge08CCF1uryJTzTdbS1xp2/OdVvAfdCIppXFWh+reCbiwt7pYrRz96+BI3Ihbyxb+Y/tKs3JpH/ADl/M/2kRZXf3D/5e5fI1VoliukPfL5llW6cirkEjcX6s9v6Hvf3yt2aYFpucdv/AOF/toiyLVLtLCn7l8jXnwzpc5bpUuf/AGl8yf42juB+LnL/ALN/bT42jr/48/8ALf20RP0pd/x+5fIfVnS1+6/FL5k/xt/++v8Ay/8AaURpy4cRjX/lv7SIo/Sd1/F7l8iy4c0xfuvxS+ZH43bjzxn/AMv/AGlM3T0t/wCeP/L/ANpET9JXT/5e5fIt9XtNX7v8UvmXFJkdtNIZH4l1htYfKbW98r0ZasLCtsP3r+tEWKV5Xk8uXuRnp6PZUliEPe/mRblst5Vv83/WpvzO9vln83/WiKv0mr4/Ayfoy1/h97+YGXiOVZ/N/wBam+IB/df83/WiJ9Jq+PwH6Ntf4fe/mQfl/eaW+V2v7X/WrX8yX/eH81/aRFKu6y6P3IrLSrSfnQ97+ZK7J5cQfij/ADP9pUjkkudd2KcO7qf7SIrK+uF/y9y+Rjei2L6w98vmTtyY5v8Azn/M/wBpT/mQd24l/M/2kRHfV3/y9y+QWi2K6Q97+ZFuUbf84fzP9pTDKlv/AK/+a/rRE+m13/y9yLLSLNdIe9/MfmU768f6r+tTjLFhYV381/WiKPplf+L3It+irT+D3v5kRlo/u7+a/rVhX5EbWzCZuJ9W61j8pvf3yIkb2vF5UvcilTRrKtHbOGV638xh2Rn0FWyqOLdYGAjd6i17i3PeREValzVqy3TfP2FqGk2dvDZShhet/M//2Q=='

                    } );
                }
            }
          ],
          paging:false,
          "scrollY": "300px",
          "scrollX": true,
          "scrollCollapse": true,
          "order":[],
          "ordering": true,
        });

    $('.dataTables_length').addClass('bs-select');

    $("#agregar_producto").submit(function(event){
        event.preventDefault();
        var selected_product =$("#select_productos option:selected");
        var id_clientescotizacion=<?=$id_cotizacion?>;

        var id_producto     = selected_product.data("id_producto");

        if(id_producto == undefined){
          $(".icon-remove").click();
        }else{
          var cantidad        =$('#cantidad').val();
          if(cantidad ==0){
            exito("<h5>Ingrese una cantidad mayor a "+cantidad+"<h5/>","danger auto_close");
            setTimeout(function(){$('input[name="cantidad"]').focus();},500);
          }else{
            var codigo_barras   =selected_product.data("cod_barras");
            var nombre_producto =selected_product.text();
            var unidad          =selected_product.data("unidad");
            var costo_unitario  =$('#precio').val();
            var iva             =selected_product.data("iva");
            var ieps            =selected_product.data("ieps");
            var costo_total     =$('#importe').val();

            var url ="<?php echo base_url()?>index.php/clientescotizacion/agregar_producto";
            ajaxJson(url,{
                'id_clientescotizacion'   : id_clientescotizacion,
                'id_producto'             : id_producto,
                'nombre_producto'         : nombre_producto,
                'descripcion_de_la_unidad': unidad,//id de la unidad
                'cantidad'                : cantidad,
                'costo_unitario'          : costo_unitario,
                'IVA'                     : iva,
                'IEPS'                    : ieps,
                'costo_total'             : costo_total
              },
              "POST","",function(result){

              console.log(result);
              json_response = JSON.parse(result);
              //alert(typeof(result)+"||"+result);
              aux = "";
              if(result == "false"){
                /* $.each( obj_output.errors, function( key, value ) {
                  aux +=value+"<br/>";
                }); */
                exito("<h5>ERROR intente de nuevo<h5/> <br/>"+aux,"danger auto_close");
                $("#enviar").show();
                $("#cargando").hide();
              }
              if(result == "true"){
                
                exito("<h5>Producto Agregado!<h5/> <br/>"+aux,"success auto_close");
                $("#enviar").show();
                $("#cargando").hide();
                ////.row( $("row_delete_"+id_producto).parents('tr') )
                redraw_table();
                get_fecha_modificacion(id_clientescotizacion);
                /* tabla_productos
                    .row( $("#product_id_"+id_producto) )
                    .remove()
                    .draw("full-reset");
                  //agregar row a datatables
                  let costo_iva=Math.floor10(costo_unitario*(iva/100),-3);
                  let costo_ieps=Math.floor10(costo_unitario*(ieps/100),-3);

                  var new_row=tabla_productos.row.add( [
                    codigo_barras,
                    nombre_producto,
                    unidad,
                    cantidad,
                    costo_unitario,
                    iva+"%("+costo_iva+")",
                    ieps+"%("+costo_ieps+")",
                    costo_total
                  ] ).draw("full-reset")
                  .node();

                  $(new_row)
                    .css('color','red')
                    .attr("id","product_id_"+id_producto)
                    .addClass("context-menu-one hover-row");
                */
                $("#select_productos").val("").trigger("change");
                $("#cantidad").val("");
                $("#precio").val("");
                $("#iva").val("");
                $("#ieps").val("");
                $("#importe").val("");
                $("#cantidad").blur();
                
              }
            });//...ajaxJson
            calcular_totales();
          }//...else cantidad >0
        }//...else id_producto != undefined
        
    });//...submit agregar_producto

    $(".main-sidebar").css("height", "120%");
    calcular_totales();

    var contextMenu_obj ="";
    $('#scrolltable tbody').on( 'contextmenu', 'td', function () {

      var row=$(this).parents('tr');
      var row_id=row.attr("id");
      var id_producto = row_id.replace("product_id_", "");
      var nombre_producto = $("#"+row_id+" td")[1].textContent;
      var cod_barr_producto = $("#"+row_id).data("all_codes");

        contextMenu_obj = function(key,options){

        var m = "clicked: " + key;
        //window.console && console.log(m+"|"+row.attr("id")) || alert(m+"|"+row.attr("id"));
        if(key=="delete"){// contextMenu->eliminar
              //nombre del producto:
              
              //eliminar de la bd
              ConfirmCustom("Desea eliminar el producto '"+nombre_producto+"' de la lista?", 
              function(){//opcion Sí
                //eliminar de datatables
                tabla_productos
                  .row( row )
                  .remove()
                  .draw();
                //eliminar de base de datos
                eliminar_producto(id_producto);
                calcular_totales()
              }, function(){//opcion No
                  id_producto="";
              }, "Sí", "No");
        } 
        if(key=="edit"){// contextMenu->editar
              //nombre del producto:
              //var nombre_producto = $("#"+row_id+" td")[1].textContent;
              var cantidad = $("#"+row_id+" td")[3].textContent;
              var precio = $("#"+row_id+" td")[4].textContent;
              var importe = $("#"+row_id+" td")[7].textContent;

              $("#select_productos").val(cod_barr_producto).trigger("change");
              $("#cantidad").val((cantidad==0)?1:cantidad);
              $("#precio").val(precio);
              $("#importe").val(importe);

        } 

      }//...contextMenu

      $.contextMenu({
            selector: '.context-menu-one', 
            callback: function(key,options){return contextMenu_obj(key,options);},
            items: {

                "edit": {name: "  Editar", icon: function(){
                    return 'context-menu-icon--fa fa fa-pen-square';
                }},
                "sep1": "---------",
                "delete": {name: "  Eliminar", icon: function(){
                    return 'context-menu-icon--fa fa fa-trash';
                }}
            }
        });//...$.contextMenu({})

      $('.context-menu-one').on('click', function(e){
          console.log('clicked', this);
        })//...$(".context-menu-one").click()

    });//...$("#scrolltable tbody).on("contextmenu")

    $('#enviar_a_pedidos').click(function(event){
      event.preventDefault();
      let status_cot=$("#status option:selected").val();
      if(status_cot == 5){//status cerrado
      
          var prefijo="CLIEPED";
          var arr_folio_documento=<?php echo json_encode(crear_foliodocumento("CLIEPED",$folio_unico_documento));?>;
          /*alert(JSON.stringify(arr_folio_documento,0,4));
            alert(arr_folio_documento.err); */

            if(arr_folio_documento.errorFUNI=="FALSE"){

                if(arr_folio_documento.errorPrefijo=="FALSE"){

                    ajaxJson(site_url+"/clientespedido/Clientespedido/alta", 
                      {
                      "nuevo_folio_documento":arr_folio_documento.folio_string,
                      "nuevo_FUNI_documento":arr_folio_documento.funi,
                      "id_foliosdocumentos":arr_folio_documento.id_foliosdocumentos,
                      "id_FUNI_foliosdocumentos":arr_folio_documento.id_FUNI_foliosdocumentos

                      }, "POST", "", function(id_nuevo_pedido){
                      //alert(typeof(result)+"|"+result[0]+"|"+JSON.stringify(result));

                          if(id_nuevo_pedido!="false"){
                              //copiar los productos de clientescotizacion_detalle a clientespedido_detalle
                              ajaxJson(site_url+"/clientespedido/Clientespedido/importar_productos_de_cCotizacion_detalle", 
                                  {
                                  "folio_unico_documento":arr_folio_documento.funi,
                                  "id_clientespedido":id_nuevo_pedido

                                  }, "POST", "",function(){
                                        ExitoCustom("Se ha creado con éxito el pedido", function(){
                                          window.location.href=site_url+("/clientespedido/Clientespedido/editar/"+id_nuevo_pedido);
                                        },"ok");
                                  });
                          }else{
                            ErrorCustom("No tienes permiso para realizar esta accion");
                          }

                        });

                }else{
                  ErrorCustom("Por favor verifique que exista el folio_documento '"+prefijo+"' y sus fechas de disponibilidad", "","");
                }
                
            }else{
              ErrorCustom("Por favor verifique que exista el folio_documento 'FUNI' y sus fechas de disponibilidad", "","");
            }
      }else{
        if(status_cot == 6){//status cerrado
          ErrorCustom("La cotizacion se encuentra cancelada","","");
        }else{//status abierto
          ErrorCustom("La cotizacion debe cerrarse antes de enviar a pedidos","","");
        }
        
      }


    });//enviar_a_pedidos
  var buttonPdf=$(".dt-button");
  buttonPdf.removeClass("buttons-pdf buttons-html5");
  buttonPdf.addClass("btn btn-export-pdf");
  buttonPdf.html('<i class="fa fa-file-pdf"></i> PDF');
});//...$(document).ready()

var id_cotizacion=<?=$id_cotizacion?>;

$("#comentarios").blur(function(){
  //alert($(this).val());
  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/agregar_comentario",
   {
    "id_cotizacion":id_cotizacion,
    "comentario":$(this).val()
   },
    "POST",
    "",
    function(){
      get_fecha_modificacion(id_cotizacion);
    });
});//...comentarios

$("#select_productos").change(function(){
    let select_cliente    =$("#id_clientes");
    let select_cliente_val=$("#id_clientes option:selected").val();
    let select_vendedor_val=$("#id_agentes_venta option:selected").val();
    if(select_cliente_val==-1){
      exito("<h5>Por favor seleccione un cliente!<h5/> <br/>","danger auto_close");
      //$("#select_productos").val("-1").trigger("change");
    }else{
    
      if(select_vendedor_val==-1){
        exito("<h5>Por favor seleccione un vendedor!<h5/> <br/>","danger auto_close");
      }else{
          let option =$("#select_productos option:selected");
          let porcentaje_condicion=$("#id_clientes_condicion option:selected").data("porcentaje");
              //alert(porcentaje_condicion+"|||"+typeof(porcentaje_condicion))
          if(porcentaje_condicion != undefined){

            //if(cantidad==""){cantidad=1;$("#cantidad").val("1");}
            var cantidad              = "";
            let existencia            = "";
            let id_producto           = option.data("id_producto");
            let unidad                = option.data("unidad");
            //let existencia            = option.data("existencia");
            let iva                   = option.data("iva")/100;
            let ieps                  = option.data("ieps")/100;
            let precio_sin_condicion  = option.data("precio");
            let precio_con_condicion  = parseFloat(precio_sin_condicion)+parseFloat((precio_sin_condicion*(porcentaje_condicion/100)));
            let costo_iva             = Math.floor10(precio_con_condicion*iva,-3);
            let costo_ieps            = Math.floor10(precio_con_condicion*ieps,-3);
            let precio_con_impuestos  = precio_con_condicion + costo_iva + costo_ieps;

            let importe               = Math.floor10(precio_con_impuestos*cantidad,-3);

            $("#unidad").val(unidad);
            $("#precio").val(precio_con_condicion);
            $("#iva").val(costo_iva);
            $("#ieps").val(costo_ieps);
            $("#importe").val(importe);

            ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_cant_producto/"+id_cotizacion+"/"+id_producto,
                            {}, 
                            "POST", 
                            "", 
                            function(result){
                              result=JSON.parse(result);

                              cantidad  =result.cantidad;
                              existencia=result.existencias;

                              $("#existencia").val(existencia);
                              $("#cantidad").val(cantidad);
              }); 

            setTimeout(function() { $('input[name="cantidad"]').focus() }, 500);

          }//... if porcentaje_condicion
        }//...else select_vendedor
    }//...else select_cliente
      
});//...select_productos

$("#cantidad").on("input",function(){

    let cantidad  = valida_solo_float($(this));
    let producto =$("#select_productos option:selected");

    if(producto.val() != -1){
      var precio   = $("#precio").val();
    }

    calcular_input_importe(precio,cantidad);
});//...cantidad

$("#precio").on("input",function(){
    let precio  = valida_solo_float($(this));
    let producto =$("#select_productos option:selected");

    if(producto.val() != -1){
      var cantidad   = $("#cantidad").val();
    }

    calcular_input_importe(precio,cantidad);
});//...precio

$(".form_select_rt_autoupdate").change(function(){
    let select    =$(this);                             // select [htmlobj] (select que se está editando)
    let db_field  =select.attr("id");                   // id del select
    let option    =$("#"+db_field+" option:selected");  // option [htmlobj] (opcion seleccionada)

    let field_value = option.val();                     // vaue de la opcion seleccionada
    let field_txt = option.data("text");                // data-text de la opcion seleccionada
    let left_input = select.data("left_input");         // id del input:text que se va a actualizar

    //actualizar en tiempo real la base de datos
                  
                  switch (db_field) {
                    
                    case "id_clientes_condicion":
                      let producto =$("#select_productos option:selected");
                      
                      controller_function="update_info_clientes_condicion";
                      /* objForm={"tabla":"clientescotizacion",
                        "campo_edit_1":db_field,
                        "valor_1":field_value,
                        "campo_id":"id",
                        "value_id":<?=""/* $id_cotizacion */?>
                      }; */
                      //var callback = function(result){/*alert(result);*/};
                      objForm={
                        "id_cotizacion":id_cotizacion,
                        "id_clientes_condicion":field_value,
                        "descripcion_clientes_condicion":field_txt
                      };
                      var callback = function(result){
                        get_fecha_modificacion(id_cotizacion);
                        //si hay un producto seleccionado cambia el valor del input costo e importe de acuerdo a la condicion de pago
                        if(producto.val() != -1 && producto.val()!= "" && producto.val()!= undefined){
                        
                        let precio_sin_condicion  = producto.data("precio");
                        let porcentaje_condicion  = option.data("porcentaje");
                        let precio_con_condicion  = parseFloat(precio_sin_condicion)+parseFloat((precio_sin_condicion*(porcentaje_condicion/100)));
                        
                        let iva                   = producto.data("iva")/100;
                        let ieps                  = producto.data("ieps")/100;
                        let costo_iva             = precio_con_condicion*iva;
                        let costo_ieps            = precio_con_condicion*ieps;
                        
                        let precio_con_impuesto=precio_con_condicion+costo_iva+costo_ieps;
                        //alert(precio_sin_condicion+"|"+precio_con_condicion+"|"+precio_con_impuesto+"|");
                        let cantidad              = $("#cantidad").val();
                        if(cantidad==""){cantidad=1;}
                        let nuevo_importe=parseFloat(cantidad)*parseFloat(precio_con_impuesto);
                        
                        nuevo_importe=isNaN(nuevo_importe) ? "0" : nuevo_importe;
                        let nuevo_precio=isNaN(precio_con_condicion) ? "0" : precio_con_condicion;

                        $("#importe").val(Math.floor10(nuevo_importe,-3));
                        $("#precio").val(Math.floor10(nuevo_precio,-3));

                        //$("#product_id_9 td")[4]; // <td> costo
                        //$("#product_id_9 td")[4].textContent="ffff4444";// cambiar costo
                            
                        }//...if hay un producto seleccionado


                        //si hay productos en la tabla cambia el costo unitario de acuerdo a la condicion de pago seleccionada
                        if($("tbody tr").length>0){
                          var id_condicionpago = option.val();
                          ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/update_costo_by_condicion_pago/"+id_cotizacion+"/"+id_condicionpago,
                                {},
                                "POST",
                                "",
                                function(update_costoXcondicion){//actualizar costo unitario segun la condicion de pago 
                                    redraw_table();//redibujar tabla
                                });
                                calcular_totales();
                                init_contextMenu();
                                return productos.length;
                              }//... if hay productos en la tabla

                      };//...callback
                      ajax_rt_autoupdate(controller_function,objForm,callback);
                      
                    break;

                    case "id_clientes":
                                      
                        controller_function="get_info_cliente";
                        objForm={
                          "cliente_id":field_value
                        };
                        var callback = function(datos_cliente){
                              datos_cliente=JSON.parse(datos_cliente);
                              /* alert(datos_cliente.cliente_nombre);
                                alert(datos_cliente['cliente_nombre']); */
                                var cliente_domicilio="Calle: " +datos_cliente.cliente_calle+"  int.("+datos_cliente.cliente_no_interior+"), ext.("+datos_cliente.cliente_no_exterior+"), colonia:"+datos_cliente.cliente_colonia+", c.p."+datos_cliente.cliente_cp+", "+datos_cliente.cliente_ciudad+", "+datos_cliente.cliente_municipio+", "+datos_cliente.cliente_estado+", "+datos_cliente.cliente_pais;
                                //$("#cliente_nombre").val(datos_cliente.cliente_nombre);
                                $("#domicilio_cliente").val(cliente_domicilio);

                                ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/update_cliente_nombre_domicilio",
                                  { "id_cotizacion":id_cotizacion,
                                    "id_clientes":datos_cliente.cliente_id,
                                    "nombre_cliente":datos_cliente.cliente_nombre,
                                    "domicilio_cliente":cliente_domicilio
                                  },
                                  "POST",
                                  "",
                                  function(result){
                                      ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_condicion_pago_by_cliente_id",
                                        { "cliente_id":field_value,
                                        },
                                        "POST",
                                        "",
                                        function(condiciones_pago){
                                            //alert(Object.keys(condiciones_pago).length);
                                            let select_condicion=$("#id_clientes_condicion");
                                            let input_condicion=$("#descripcion_clientes_condicion");

                                            //limpio select condicion
                                            select_condicion.empty();
                                            
                                          if(Object.keys(condiciones_pago).length>2){
                                            var val_clientes_condicion,descripcion_clientes_condicion;
                                            let input_condicion=$("#descripcion_clientes_condicion");

                                            select_condicion.append($("<option></option>")
                                            .attr("value",-1)
                                            .attr("disabled","")
                                            .text("-Seleccione condición-")
                                            );

                                                //actualizo valores de select clientes_condicion
                                                $.each(JSON.parse(condiciones_pago), function(key,value) {

                                                  let opt= $("<option></option>")
                                                        .attr("value",this.clientescondicion_id)
                                                        .attr("data-text",this.metodopago_descripcion)
                                                        .attr("data-porcentaje",this.tipodeprecio_porcentaje)
                                                        .text(this.metodopago_descripcion);

                                                        if(this.metodopago_descripcion == "EFECTIVO"){
                                                          //alert(input_condicion.val());
                                                          val_clientes_condicion=this.clientescondicion_id;
                                                          descripcion_clientes_condicion=this.metodopago_descripcion;

                                                        }
                                                        //alert(val_clientes_condicion+"||"+descripcion_clientes_condicion+"||"+typeof(descripcion_clientes_condicion));
                                                        select_condicion.append(opt);
                                                
                                                });
                                                select_condicion.val(val_clientes_condicion).trigger('change');

                                          }else{
                                            ErrorCustom("No hay condiciones de pago registradas para este cliente", function(){select_condicion.empty();});
                                            
                                          }
                                          
                                      });
                                });

                        };//...callback

                        ajax_rt_autoupdate(controller_function,objForm,callback);
                      
                    break;
                      
                    case "id_agentes_venta":

                      controller_function="update_info_agentesventa";
                      objForm={
                        "id_cotizacion":id_cotizacion,
                        "agenteventa_id":field_value,
                        "nombre_agentes_venta":field_txt
                      };
                      var callback = function(datos_vendedor){
                        // $("#nombre_agentes_venta").val(datos_vendedor.agenteventa.nombre+" "+datos_vendedor.agenteventa_a_paterno+" "+datos_vendedor.agenteventa_a_materno);
                        get_fecha_modificacion(id_cotizacion);
                      };//...callback

                      ajax_rt_autoupdate(controller_function,objForm,callback);
                    break;

                    case "status" :
                        
                        if(field_value==4){
                          exito("se ha abierto la cotizacion","success auto_close");
                          abrirDocumento();
                        }
                        if(field_value==5){
                          exito("se ha cerrado la cotizacion","success auto_close");
                          cerrarDocumento();
                        }
                        if(field_value==6){
                          exito("se ha cancelado la cotizacion","success auto_close");
                          cerrarDocumento();
                        }

                        controller_function="cambiar_status/"+id_cotizacion+"/"+field_value;
                        objForm={
                          /* "id_cotizacion":,
                          "status":field_value */
                        };
                        var callback = function(status_result){
                          get_fecha_modificacion(id_cotizacion);
                        };//...callback

                        ajax_rt_autoupdate(controller_function,objForm,callback);
                    break;

                    default:
                      /*  */
                    break;
                  }

    if(field_txt!=undefined){
        $("#"+left_input).val(field_txt);
    }
});//...form_select_rt_autoupdate


function eliminar_producto(id_producto){
  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/eliminar_producto/"+id_cotizacion+"/"+id_producto,
    {}, 
    "POST", 
    "", 
    function(result){
      if(result){
        exito("Producto eliminado correctamente","success auto_close");
        get_fecha_modificacion(id_cotizacion);
      }
    }
  );
}//...eliminar_procucto

function ajax_rt_autoupdate(controller_function,objForm,callback){
    ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/"+controller_function,
      
      objForm,
      "POST",
      "",
      callback);
}//...ajax_rt_autoupdate

function calcular_input_importe(precio,cantidad){
  if(cantidad==""){cantidad=1;}
  if(precio==""){precio=0;}
  let iva=$("#iva").val();
  let ieps=$("#ieps").val();
  let disponible = (isNaN($("#disponible").text()))? 0:parseFloat($("#disponible").text());
  let nuevo_importe = (parseFloat(precio)+parseFloat(iva)+parseFloat(ieps))*parseFloat(cantidad);
    nuevo_importe = Math.round10(nuevo_importe, -4);

    if (isNaN(nuevo_importe)){
      nuevo_importe=0;
    }
    if($("#disponible").text()!="-"){
      if((disponible-cantidad) <=0){
        $("#disponible").css("color","red");
      }else{
        $("#disponible").css("color","black");
      }
    }
    $("#importe").val(nuevo_importe);
}//...calcular_input_importe

function calcular_totales(){

  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/calcular_totales/"+id_cotizacion,
   {},
   "POST",
   "", 
   function(result){
      result=JSON.parse(result);
      var subtotal  =result.subtotal;
      var iva       =result.iva;
      var ieps      =result.ieps;
      var total     =result.total;

            $("#infocot-subtotal").text(Math.floor10(subtotal,-2));
            $("#infocot-iva").text(Math.floor10(iva,-2));
            $("#infocot-ieps").text(Math.floor10(ieps,-2));
            $("#infocot-total").text(Math.floor10(total,-2));


    }
  );
}//...calcular_totales

function get_fecha_modificacion(id_cotizacion){
  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_fecha_modificacion",
      {"id_cotizacion":id_cotizacion},
      "POST",
      "",
      function(result){
        //alert(result+"||"+typeof(result));
        fecha=JSON.parse(result);
        $("#fecha_modificacion").val(fecha);
      });
}//...get_fecha_modificacion

/* funcion para inicializar tabla productos*/
function init_tabla_productos(id_cotizacion){
  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_clientescotizacion_detalle/"+id_cotizacion,
      
      {},
      "POST",
      "",
      function(result){
        productos=JSON.parse(result);
        //agregar lista de productos
        var cont=0;
        var last_id_prod = [];
        var new_arr_prod = [];

        for (var prod_1 of productos) {
              
              if(last_id_prod.includes(prod_1.id_producto)){
                new_arr_prod[prod_1.id_producto].codigobarra_codigo += "," + prod_1.codigobarra_codigo;
              }else{
                new_arr_prod[prod_1.id_producto]  = prod_1;
                last_id_prod.push(prod_1.id_producto);

              }
        
        }
        new_arr_prod = clean_array(new_arr_prod);
        for (var producto of new_arr_prod) {
          let costo_iva   =Math.floor10(producto.costo_unitario*(producto.IVA/100),-3);
          let costo_ieps  =Math.floor10(producto.costo_unitario*(producto.IEPS/100),-3);
          let importe     =Math.floor10(producto.costo_total,-3);
          let all_codes=producto.codigobarra_codigo;
          let n = all_codes.split(",");
          producto.codigobarra_codigo=n[0];
          $("#tbody_productos").append($("<tr id='product_id_"+producto.id_producto+"' data-all_codes='"+all_codes+"'>"+
                          "<td>"+producto.codigobarra_codigo+"</td>"+
                          "<td>"+producto.nombre_producto+"</td>"+
                          "<td>"+producto.descripcion_de_la_unidad+"</td>"+
                          "<td>"+producto.cantidad+"</td>"+
                          "<td>"+producto.costo_unitario+"</td>"+
                          "<td>"+producto.IVA+"%("+costo_iva+")</td>"+
                          "<td>"+producto.IEPS+"%("+costo_ieps+")</td>"+
                          "<td>"+importe+"</td>"+
                          "<td>"+producto.familia_tipo+"</td>"+
                        "</tr>"
                      ));
            cont++;
        }
        
      }); 

    return productos.length;
}//...init_tabla_productos

function redraw_table(){
  ajaxJson("<?php echo base_url()?>index.php/clientescotizacion/get_clientescotizacion_detalle/"+id_cotizacion,
          
                                        {},
                                        "POST",
                                        "",
                                        function(get_cCotizacion_detalle){//obtener tabla clientescotizacion_detalle con costo_unitario e importe actualizados
                                          productos=JSON.parse(get_cCotizacion_detalle);
                                          //agregar lista de productos
                                          var cont=0;
                                          for (var producto of productos) {
                                            let tabla_productos=$("#scrolltable").DataTable()
                                              .row( $("#product_id_"+producto.id_producto) )
                                              .remove();//.draw("full-reset");
                                            //agregar row a datatableslet costo_iva=Math.floor10(producto.costo_unitario*(producto.IVA/100),-3);
                                            let costo_iva=Math.floor10(producto.costo_unitario*(producto.IVA/100),-3);
                                            let costo_ieps=Math.floor10(producto.costo_unitario*(producto.IEPS/100),-3);

                                            let all_codes=producto.codigobarra_codigo;
                                            let n = all_codes.split(",");
                                            producto.codigobarra_codigo=n[0];

                                            var new_table=tabla_productos.row.add( [
                                              producto.codigobarra_codigo,
                                              producto.nombre_producto,
                                              producto.descripcion_de_la_unidad,
                                              producto.cantidad,
                                              producto.costo_unitario,
                                              producto.IVA+"%("+costo_iva+")",
                                              producto.IEPS+"%("+costo_ieps+")",
                                              producto.costo_total,
                                              producto.familia_tipo
                                            ] ).draw("full-reset")
                                            .node();

                                            $(new_table)
                                              .css('color','red')
                                              .attr("id","product_id_"+producto.id_producto)
                                              .attr("data-all_codes",all_codes)
                                              .addClass("context-menu-one");
                                            cont++;
                                          }//...for
                                          //new_table.draw("full-reset");
                                          
                                      });
}//...redraw_table

function init_contextMenu(){
  $(".odd").addClass("context-menu-one hover-row");
  $(".even").addClass("context-menu-one hover-row");
}//...init_contextMenu

function delete_contextMenu(){
  $(".odd").removeClass("context-menu-one");
  $(".even").removeClass("context-menu-one");
}//...delete_contextMenu

function abrirDocumento(){
          $("#id_clientes").removeAttr("disabled");
          $("#id_clientes_condicion").removeAttr("disabled");
          $("#id_agentes_venta").removeAttr("disabled");
          $("#select_productos").removeAttr("disabled");
          $("#comentarios").removeAttr("disabled");
          $("#cantidad").removeAttr("disabled");
          $("#precio").removeAttr("disabled");
          init_contextMenu();
}//...abrirDocumento

function cerrarDocumento(){
          $("#id_clientes").attr("disabled","");
          $("#id_clientes_condicion").attr("disabled","");
          $("#id_agentes_venta").attr("disabled","");
          $("#select_productos").attr("disabled","");
          $("#comentarios").attr("disabled","");
          $("#cantidad").attr("disabled","");
          $("#precio").attr("disabled","");
          delete_contextMenu();
}//...cerrarDocumento
</script>
