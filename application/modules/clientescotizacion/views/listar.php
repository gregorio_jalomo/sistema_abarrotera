
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Cotizaciones de clientes</h3>
    </div>
    <div class="col-md-6">

      <button id="agregar_clientescotizacion" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Cotización</button>

    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="tabla_clientescotizacion" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio</th>
        <th>Cliente</th>
        <th>Condicion pago</th>
        <th>Status</th>
        <th>Usuario del sistema</th>
        <th>Vendedor</th>
        <th>Opciones</th>

      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_clientescotizacion);*/ 
      foreach ($con_clientescotizacion as $ClieCot) {

        $id_cotizacion              = $ClieCot->id;
        //$fecha_creacion             = explode(" ",$ClieCot->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $fecha_creacion             = $ClieCot->fecha_creacion;
        $folio_documento            = $ClieCot->folio_documento;
        $folio_unico_documento      = $ClieCot->folio_unico_documento;
        $comentario                 = $ClieCot->comentario;
        $status                     = $ClieCot->status;
        $subtotal                   = $ClieCot->subtotal;
        $IVA                        = $ClieCot->IVA;
        $IEPS                       = $ClieCot->IEPS;
        $TOTAL                      = $ClieCot->TOTAL;

        $id_agentes_venta           = $ClieCot->id_agentes_venta;
        $nombre_agentes_venta       = $ClieCot->nombre_agentes_venta;
        $descr_agentes_venta        = $ClieCot->descripcion_agentes_venta;
        
        $perfil_usuario             = $ClieCot->perfil_usuario;
        $descr_id_usuario           = $ClieCot->descripcion_id_usuario;

        $id_clientes                = $ClieCot->id_clientes;
        $nombre_cliente             = $ClieCot->nombre_cliente;
        $domicilio_cliente          = $ClieCot->domicilio_cliente;        
        $descr_clientes_condicion   = $ClieCot->descripcion_clientes_condicion;

        $id_clientes_condicion      = $ClieCot->id_clientes_condicion;
        $descr_clientes_condicion   = $ClieCot->descripcion_clientes_condicion;


        /* INNERS */
        $cat_estatus_nombre                = $ClieCot->cat_estatus_nombre;
        /* ...INNERS */


        echo
        '<tr id="borrar_'.$id_cotizacion.'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$nombre_cliente.'</td>
          <td>'.$descr_clientes_condicion.'</td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$descr_id_usuario.'</td>
          <td>'.$nombre_agentes_venta.'</td>

          <td>
            <a href="'.base_url().'index.php/clientescotizacion/clientescotizacion/ver_detalle/'.$id_cotizacion.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>
            <a href="'.base_url().'index.php/clientescotizacion/clientescotizacion/editar/'.$id_cotizacion.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
              <a href="'.base_url().'index.php/clientescotizacion/clientescotizacion/cambiar_status/'.$id_cotizacion.'/6" class="eliminar_relacion" flag="'.$id_cotizacion.'" id="delete'.$id_cotizacion.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Cancelar cotizacion"><i class="fas fa-trash-alt"></i> Borrar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>
  $(document).ready(function() {

    $('#menuclientescotizacion').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_clientescotizacion").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": false
    });

    $('#agregar_clientescotizacion').click(function(){

      var prefijo="CLIECOT";
      var arr_folio_documento=<?php echo json_encode(crear_foliodocumento("CLIECOT",""));?>;
      /*alert(JSON.stringify(arr_folio_documento,0,4));
        alert(arr_folio_documento.err); */

        if(arr_folio_documento.errorFUNI=="FALSE"){

            if(arr_folio_documento.errorPrefijo=="FALSE"){

                ajaxJson(site_url+"/clientescotizacion/Clientescotizacion/alta", 
                  {
                  "nuevo_folio_documento":arr_folio_documento.folio_string,
                  "nuevo_FUNI_documento":arr_folio_documento.funi,
                  "id_foliosdocumentos":arr_folio_documento.id_foliosdocumentos,
                  "id_FUNI_foliosdocumentos":arr_folio_documento.id_FUNI_foliosdocumentos

                  
                  }, "POST", "", function(id_nueva_cotizacion){
                  //alert(typeof(result)+"|"+result[0]+"|"+JSON.stringify(result));
                      if(id_nueva_cotizacion!="false"){
                        ExitoCustom("Se ha creado con éxito la cotización", function(){
                          window.location.href=site_url+("/clientescotizacion/Clientescotizacion/editar/"+id_nueva_cotizacion);
                        },"ok");
                      }else{
                        ErrorCustom("No tienes permiso para realizar esta accion");
                      }



                    });

            }else{
              ErrorCustom("Por favor verifique que exista el folio_documento '"+prefijo+"' y sus fechas de disponibilidad", "","");
            }
            
        }else{
          ErrorCustom("Por favor verifique que exista el folio_documento 'FUNI' y sus fechas de disponibilidad", "","");
        }

    });//agregar_clientescotizacion

    $(".eliminar_relacion").click(function(event){
          event.preventDefault();
          bootbox.dialog({
          message: "Desea eliminar el registro?",
          closeButton: true,
          buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
    });

  });//document ready

</script>
