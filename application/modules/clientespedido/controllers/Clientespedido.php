<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Clientespedido extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mclientespedido', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('clientespedido/Clientespedido_model');
      $this->load->model('catalogos/Clientelugar_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 34;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,111) || $this->is_admin ){

      //recibir post del ajax
      $dataClientespedido['folio_documento'] = $this->input->post('nuevo_folio_documento');
      $dataClientespedido['folio_unico_documento'] = $this->input->post('nuevo_FUNI_documento');
      $dataClientespedido['perfil_usuario'] = $this->session->userdata['infouser']['id'];
      $dataClientespedido['descripcion_id_usuario'] = $this->session->userdata['infouser']['nombre'];
      $dataClientespedido["fecha_creacion"]         = date("Y-m-d H:i:s");

      $foliosdocumentos_id= $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI= $this->input->post('id_FUNI_foliosdocumentos');

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que una cotizacion se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        //insert a clientespedido (solo foliodocumento, FUNI,id y nombre del usuario en sesion)
        $clientespedido_id=$this->guardar($dataClientespedido);
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }else{
        $row_clientescotizacion=$this->Clientespedido_model->get_clientescotizacion_by_FUNI($dataClientespedido['folio_unico_documento']);
        //insert a clientespedido (importar datos de clientes cotizacion)
                  $data_import["id_clientes"]                     =$row_clientescotizacion->id_clientes;
                  $data_import["nombre_cliente"]                  =$row_clientescotizacion->nombre_cliente;
                  $data_import["domicilio_cliente"]               =$row_clientescotizacion->domicilio_cliente;
                  $data_import["id_clientes_condicion"]           =$row_clientescotizacion->id_clientes_condicion;
                  $data_import["descripcion_clientes_condicion"]  =$row_clientescotizacion->descripcion_clientes_condicion;
                  $data_import["id_agentes_venta"]                =$row_clientescotizacion->id_agentes_venta;
                  $data_import["nombre_agentes_venta"]            =$row_clientescotizacion->nombre_agentes_venta;
                  $data_import["descripcion_agentes_venta"]       =$row_clientescotizacion->descripcion_agentes_venta;
                  $data_import["perfil_usuario"]                  =$row_clientescotizacion->perfil_usuario;
                  $data_import["descripcion_id_usuario"]          =$row_clientescotizacion->descripcion_id_usuario;
                  $data_import["comentario"]                      =$row_clientescotizacion->comentario;
                  $data_import["subtotal"]                        =$row_clientescotizacion->subtotal;
                  $data_import["IVA"]                             =$row_clientescotizacion->IVA;
                  $data_import["IEPS"]                            =$row_clientescotizacion->IEPS;
                  $data_import["TOTAL"]                           =$row_clientescotizacion->TOTAL;
                  $data_import["folio_documento"]                 =$dataClientespedido['folio_documento'];
                  $data_import["folio_unico_documento"]           =$row_clientescotizacion->folio_unico_documento;
                  $data_import["fecha_creacion"]         = date("Y-m-d H:i:s");

        $clientespedido_id=$this->guardar($data_import);
        //importar datos a clientespedido desde clientescotizacion
        //$this->importar_datos_de_cCotizacion($dataClientespedido['folio_unico_documento'],$clientespedido_id);
      }

      echo json_encode($clientespedido_id);
    
    }else{
      echo json_encode(false);
    }


  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('clientespedido', $data);
    //  echo $response;
    //echo json_encode(array('output' => $response));
  }//...guardar

  public function editar($id_pedido){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,134) || $this->is_admin ){

      $data['titulo_seccion'] = "Pedido de cliente";
      $data['flecha_ir_atras'] = "clientespedido/listar_clientespedido";
      $data['force_landscape']=true;

      $data['clientespedido'] = $this->Clientespedido_model->get_clientespedido_by_id($id_pedido);
      $data['clientescondicion'] =$this->Clientespedido_model->get_clientes_condicion($data['clientespedido']->id_clientes);
      $data['lugares'] = $this->Clientelugar_model->get_lugares($data['clientespedido']->id_clientes,null);

      //$data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['productos'] = $this->Clientespedido_model->get_productos('productos.cat_estatus_id',1);

      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      $data["fecha_creacion"]         = date("Y-m-d H:i:s");
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js'
      
      );

      $this->cargar_vista('clientespedido/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar pedidos de clientes","clientespedido/listar_clientespedido");
    }
    
  }//...editar

  public function ver_detalle($id_clientespedido){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,113) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del pedido";
      $data['flecha_ir_atras'] = "clientespedido/clientespedido/listar_clientespedido";

      $data['clientespedido'] = $this->Clientespedido_model->get_clientespedido_by_id($id_clientespedido);
      $data['clientespedido_detalle'] =$this->Clientespedido_model->get_clientespedido_detalle($id_clientespedido);

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('clientespedido/ver_detalle',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("ver detalle de pedidos de clientes","clientespedido/clientespedido/listar_clientespedido");
    }

  }//...ver_detalle

  public function listar_clientespedido(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,116) || $this->is_admin ){

      $data['titulo_seccion'] = "Pedidos de Clientes ";
      $data['con_clientespedido'] = $this->Clientespedido_model->listar_clientespedido();
      $data['force_landscape']=true;

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('clientespedido/listar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar usuarios","");
    }

  }//...listar_clientespedido

  public function get_clientespedido_detalle($id_clientespedido){
    echo json_encode($this->Clientespedido_model->get_clientespedido_detalle($id_clientespedido));

  }//...get_clientespedido_detalle

  public function get_info_cliente(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Clientespedido_model->get_info_cliente($cliente_id));
  }//...get_info_cliente

  public function get_condicion_pago_by_cliente_id(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Clientespedido_model->get_clientes_condicion($cliente_id));
  }//...get_condicion_pago_by_cliente_id

  public function get_set_ultimo_status($id_clientespedido,$nvo_stat){
    echo json_encode($this->Clientespedido_model->get_set_ultimo_status($id_clientespedido,$nvo_stat));
  }//...get_set_ultimo_status

  public function cambiar_status($id_pedido,$estatus_id) {
    //$id_pedido=$this->input->post('id_pedido');
    //$estatus_id=$this->input->post('status');

    $data['status'] = $estatus_id;
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row('clientespedido',$data,'id',$id_pedido);

  }//...cambiar_estatus

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
      $this->Clientespedido_model->actualizar_fecha_modificacion($id);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_cliente_nombre_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['id_clientes']=$this->input->post('id_clientes');
    $data['nombre_cliente']=$this->input->post('nombre_cliente');
    $data['domicilio_cliente']=$this->input->post('domicilio_cliente');
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("clientespedido",$data,"id",$id_pedido);
  }//...update_cliente_nombre_domicilio

  public function update_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['domicilio_cliente']=$this->input->post('lugar_cliente');
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("clientespedido",$data,"id",$id_pedido);
    echo json_encode($data['domicilio_cliente']);
  }//...update_domicilio

  public function update_info_agentesventa(){
    $data['id_agentes_venta']=$this->input->post('agenteventa_id');
    $data['nombre_agentes_venta']=$this->input->post('nombre_agentes_venta');
    $id_pedido=$this->input->post('id_pedido');
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("clientespedido",$data,"id",$id_pedido));

  }//...update_info_agentesventa
  
  public function update_fecha_entrega(){
    $id_pedido=$this->input->post('id_pedido');
    $fecha_entrega=$this->input->post('fecha_entrega');

    $time = strtotime($fecha_entrega);
    $newformat = date('Y-m-d H:i:s',$time);

    $data['fecha_entrega']=$newformat;
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("clientespedido",$data,"id",$id_pedido));
  }//...update_fecha_entrega

  public function update_info_clientes_condicion(){
    $data['id_clientes_condicion']=$this->input->post('id_clientes_condicion');
    $data['descripcion_clientes_condicion']=$this->input->post('descripcion_clientes_condicion');
    $id_pedido=$this->input->post('id_pedido');
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("clientespedido",$data,"id",$id_pedido));

  }//...update_info_clientes_condicion

  public function agregar_producto(){

    $this->form_validation->set_rules('id_clientespedido', ' ID del pedido', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    $this->form_validation->set_rules('costo_unitario', ' Precio del producto', 'required');
    $this->form_validation->set_rules('costo_total', ' Importe', 'required');
    
    $id_clientespedido=$this->input->post('id_clientespedido');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total');

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    $comprometidos=0;
    $producto=$this->Clientespedido_model->buscar_producto_cPedido_detalle($id_clientespedido,$id_producto);
    if($producto){

      $dataUpdate['id_clientespedido']  = $id_clientespedido;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      $dataUpdate['costo_unitario']         = $costo_unitario;
      $dataUpdate['costo_total']            = $costo_total;

      $comprometidos=$cantidad-$producto->cantidad;

      echo json_encode($this->Clientespedido_model->actualizar_producto_cPedido_detalle($dataUpdate));

    }else{

    $dataAdd['id_clientespedido']  = $id_clientespedido;
    $dataAdd['id_producto']            = $id_producto;
    $dataAdd['nombre_producto']        = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']   = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']               = $cantidad;
    $dataAdd['costo_unitario']         = $costo_unitario;
    $dataAdd['IVA']                    = $this->input->post('IVA');
    $dataAdd['IEPS']                   = $this->input->post('IEPS');
    $dataAdd['costo_total']            = $costo_total;

    $comprometidos=$cantidad;

    echo json_encode($this->Clientespedido_model->agregar_producto_cPedido_detalle($dataAdd));
    }
    if($comprometidos!=0){
      $this->Clientespedido_model->actualizar_comprometidos_producto($id_producto,$comprometidos);
    }
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_clientespedido);
  }//...agregar_producto

  public function eliminar_producto($id_pedido,$id_producto){
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Clientespedido_model->eliminar_producto_cPedido_detalle($id_pedido,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Clientespedido_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_pedido=$this->input->post('id_pedido');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Mgeneral->update_table_row('clientespedido',$data,'id',$id_pedido);
    $this->Clientespedido_model->actualizar_fecha_modificacion($id_pedido);
  }//...agregar_comentarios

  public function update_costo_by_condicion_pago($id_clientespedido,$id_clientescondicion){
      //$this->Clientespedido_model->actualizar_fecha_modificacion($id_clientespedido);
      echo json_encode($this->Clientespedido_model->update_costo_by_condicion_pago($id_clientespedido,$id_clientescondicion));
  }//...update_costo_by_condicion_pago

  public function calcular_totales($id_clientespedido){
    echo json_encode($this->Clientespedido_model->calcular_totales($id_clientespedido));
  }//...calcular_totales

  public function get_fecha_modificacion(){
    $id_pedido=$this->input->post('id_pedido');
    echo json_encode($this->Clientespedido_model->get_fecha_modificacion($id_pedido));
  }//...get_fecha_modificacion

  public function importar_datos_de_cCotizacion($folio_unico_documento,$clientespedido_id){

    echo json_encode($this->Clientespedido_model->importar_datos_de_cCotizacion($folio_unico_documento,$clientespedido_id));
  }//...importar_datos_de_cCotizacion

  public function importar_productos_de_cCotizacion_detalle(){
    $folio_unico_documento=$this->input->post('folio_unico_documento');
    $id_clientespedido=$this->input->post('id_clientespedido');
    $id_cotizacion=$this->Clientespedido_model->get_id_clientescotizacion_by_FUNI($folio_unico_documento);
    $result_clientescotizacion=$this->Clientespedido_model->get_clientescotizacion_detalle($id_cotizacion);
    //$data_import["folio_unico_documento"]           =$row_clientespedido->folio_unico_documento;
    
    $cont=0;
    foreach($result_clientescotizacion as $row_cot){
      $data_import[$cont]["id_clientespedido"]         =$id_clientespedido;
      $data_import[$cont]["id_producto"]               =$row_cot->id_producto;
      $data_import[$cont]["nombre_producto"]           =$row_cot->nombre_producto;
      $data_import[$cont]["codigo_barras_producto"]    =$row_cot->codigo_barras_producto;
      $data_import[$cont]["descripcion_de_la_unidad"]  =$row_cot->descripcion_de_la_unidad;
      $data_import[$cont]["cantidad"]                  =$row_cot->cantidad;
      $data_import[$cont]["costo_unitario"]            =$row_cot->costo_unitario;
      $data_import[$cont]["IVA"]                       =$row_cot->IVA;
      $data_import[$cont]["IEPS"]                      =$row_cot->IEPS;
      $data_import[$cont]["costo_total"]               =$row_cot->costo_total;
      $cont++;
    }

    echo json_encode($this->Clientespedido_model->importar_productos_de_cCotizacion_detalle($id_clientespedido,$data_import));
  }//...importar_productos_de_cCotizacion_detalle
}
