
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Pedidos de clientes</h3>
    </div>
    <div class="col-md-6">

      <button id="agregar_clientespedido" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Pedido</button>
      
      <a href="<?=base_url().'index.php/pedidosaduana/Pedidosaduana/listar_pedidosaduana'?>">
        <button id="listar_pedidosaduana" class="btn btn-primary float-right" style="margin-right: 5px;">
          Pedidos en Aduana
        </button>
      </a>
      
    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="tabla_clientespedido" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio</th>
        <th>Cliente</th>
        <th>Condicion pago</th>
        <th>Status</th>
        <th>Usuario del sistema</th>
        <th>Vendedor</th>
        <th>Opciones</th>

      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_clientespedido);*/ 
      foreach ($con_clientespedido as $ClieCot) {

        $id_pedido              = $ClieCot->id;
        //$fecha_creacion             = explode(" ",$ClieCot->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $fecha_creacion             = $ClieCot->fecha_creacion;
        $folio_documento            = $ClieCot->folio_documento;
        $folio_unico_documento      = $ClieCot->folio_unico_documento;
        $comentario                 = $ClieCot->comentario;
        $status                     = $ClieCot->status;
        $subtotal                   = $ClieCot->subtotal;
        $IVA                        = $ClieCot->IVA;
        $IEPS                       = $ClieCot->IEPS;
        $TOTAL                      = $ClieCot->TOTAL;

        $id_agentes_venta           = $ClieCot->id_agentes_venta;
        $nombre_agentes_venta       = $ClieCot->nombre_agentes_venta;
        $descr_agentes_venta        = $ClieCot->descripcion_agentes_venta;

        $perfil_usuario             = $ClieCot->perfil_usuario;
        $descr_id_usuario           = $ClieCot->descripcion_id_usuario;

        $id_clientes                = $ClieCot->id_clientes;
        $nombre_cliente             = $ClieCot->nombre_cliente;
        $domicilio_cliente          = $ClieCot->domicilio_cliente;        
        $descr_clientes_condicion   = $ClieCot->descripcion_clientes_condicion;

        $id_clientes_condicion      = $ClieCot->id_clientes_condicion;
        $descr_clientes_condicion   = $ClieCot->descripcion_clientes_condicion;
        /* INNERS */
        $cat_estatus_nombre                = $ClieCot->cat_estatus_nombre;
        /* ...INNERS */


        echo
        '<tr id="borrar_'.$id_pedido.'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$nombre_cliente.'</td>
          <td>'.$descr_clientes_condicion.'</td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$descr_id_usuario.'</td>
          <td>'.$nombre_agentes_venta.'</td>

          <td>
            <a href="'.base_url().'index.php/clientespedido/clientespedido/ver_detalle/'.$id_pedido.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>
            <a href="'.base_url().'index.php/clientespedido/clientespedido/editar/'.$id_pedido.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
              <a href="'.base_url().'index.php/clientespedido/clientespedido/cambiar_estatus/'.$id_pedido.'/3" class="eliminar_relacion" flag="'.$id_pedido.'" id="delete'.$id_pedido.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Borrar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>
  $(document).ready(function() {

    $('#menuclientespedido').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_clientespedido").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": false
    });

    $('#agregar_clientespedido').click(function(){

      var prefijo="CLIEPED";
        var arr_folio_documento=<?php echo json_encode(crear_foliodocumento("CLIEPED",""));?>;
        /*alert(JSON.stringify(arr_folio_documento,0,4));
         alert(arr_folio_documento.err); */

          if(arr_folio_documento.errorFUNI=="FALSE"){

              if(arr_folio_documento.errorPrefijo=="FALSE"){

                  ajaxJson(site_url+"/clientespedido/Clientespedido/alta", 
                    {
                    "nuevo_folio_documento":arr_folio_documento.folio_string,
                    "nuevo_FUNI_documento":arr_folio_documento.funi,
                    "id_foliosdocumentos":arr_folio_documento.id_foliosdocumentos,
                    "id_FUNI_foliosdocumentos":arr_folio_documento.id_FUNI_foliosdocumentos
                    
                    }, "POST", "", function(id_nuevo_pedido){
                    //alert(typeof(result)+"|"+result[0]+"|"+JSON.stringify(result));
                          if(id_nuevo_pedido!="false"){
                            ExitoCustom("Se ha creado con éxito el pedido", function(){
                              window.location.href=site_url+("/clientespedido/Clientespedido/editar/"+id_nuevo_pedido);
                            },"ok");
                          }else{
                            ErrorCustom("No tienes permiso para realizar esta accion");
                          }


                      });

              }else{
                ErrorCustom("Por favor verifique que exista el folio_documento '"+prefijo+"' y sus fechas de disponibilidad", "","");
              }
              
          }else{
            ErrorCustom("Por favor verifique que exista el folio_documento 'FUNI' y sus fechas de disponibilidad", "","");
          }

          });


        $(".eliminar_relacion").click(function(event){
        event.preventDefault();
        bootbox.dialog({
        message: "Desea eliminar el registro?",
        closeButton: true,
        buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $("#borrar_"+id).slideUp();
                                $.get(url);
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
      });

    });//document ready

</script>
