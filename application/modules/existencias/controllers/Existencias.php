<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Existencias extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('Existencias_model');
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');
    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 15;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista
  
  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function actualizar(){//vista

    if(validar_permiso($this->rol_usuario,$this->id_modulo,69) || $this->is_admin ){

      $data['titulo_seccion'] = "Entradas y salidas";
      $data['flecha_ir_atras'] = "existencias/listar";
      $data['productos'] = $this->Existencias_model->get_productos('productos.cat_estatus_id',1);
      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['motivos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'motivos');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('existencias/actualizar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("modificar existencias de productos","existencias/listar");
    }

  }

  public function listar(){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,68) || $this->is_admin ){

      $data['titulo_seccion'] = "Entradas y salidas";
      $data['flecha_ir_atras'] = "inicio/inicio";
      $data['existencias_log'] = $this->Existencias_model->get_existencias_log();

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('existencias/listar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("ver registro de entradas y salidas","");
    }

  }

  public function actualizar_existencias(){
    $this->form_validation->set_rules('id_producto', 'Producto', 'required');
    $this->form_validation->set_rules('accion', 'Accion', 'required');
    $this->form_validation->set_rules('motivo', 'Motivo', 'required');
    $this->form_validation->set_rules('cantidad', 'Cantidad', 'required');
    $msg_error="";

    $response = validate($this);
 
    if($response['status']){

      $dataExs['id_producto']     = $this->input->post('id_producto');
      $dataExs['accion']          = $this->input->post('accion');
      $dataExs['id_motivo']       = $this->input->post('motivo');
      $dataExs['cantidad']        = $this->input->post('cantidad');
      $dataExs['comentario']      = $this->input->post('comentario');

      $dataExs['id_usuario']      = $_SESSION['infouser']['id'];
      $dataExs['fecha_registro']  = date("Y-m-d H:i:s");      

      $cantidad=($dataExs['accion']==2)?$dataExs['cantidad']*-1:$dataExs['cantidad'];

      $row_producto                     = $this->Existencias_model->get_existencias_producto($dataExs['id_producto']);
      $existencias_producto             = $row_producto->producto_existencias;

      $dataProd['producto_existencias'] = $existencias_producto + $cantidad;

      if($dataProd['producto_existencias']>0){
        $this->Mgeneral->save_register("existencias_log", $dataExs);
        $this->Mgeneral->update_table_row("productos",$dataProd,"producto_id",$dataExs['id_producto']);
      }else{
        $msg_error="Solo hay ".$existencias_producto. " en inventario, verifique la cantidad que desea modificar";
        $response['status']=false;
      }

    }

    echo json_encode(array('output' => $response, "msg_error"=>$msg_error));
  }//... actualizar_existencias

}
