<?php
/**

 **/
class Existencias_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function get_productos($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->order_by("productos.producto_id")
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_existencias_producto($id_producto){
        return $this->db->select("producto_existencias,producto_existencias_comprometidas_pedido,producto_existencias_comprometidas_compras")
                        ->where("producto_id",$id_producto)
                        ->get("productos")->row();
    }//...get_existencias_producto

    public function get_existencias_log(){
        return $this->db
        ->select("existencias_log.*, productos.producto_nombre, motivos.motivo_descripcion, usuarios.usuario_nombre_compl")
        ->join("productos","productos.producto_id = existencias_log.id_producto")
        ->join("motivos","motivos.motivo_id = existencias_log.id_motivo")
        ->join("usuarios","usuarios.usuario_id = existencias_log.id_usuario")
        ->order_by("fecha_registro")
        ->get("existencias_log")
        ->result();
    }//...get_existengias_log
    
}//...class
