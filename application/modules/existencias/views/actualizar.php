<style>
  .select2-container {
      display: inline-block!important;
      min-width: 200px!important;
  }
.my-input{
  
    display: inline-block;
    height: calc(2.25rem + 2px);
     /*width: 100%;*/
    
    padding: .375rem .75rem; 
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;

  }
</style>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Actualizar existencias de producto</h3>
          </div>
          
          <form role="form" id="actualizar_existencias_form">
            <div class="card-body">
              <div class="form-row align-items-center">
                    <span class="col-4">
                      <label class="mr-sm-2" for="">Producto:</label>
                      <select name="select_productos" id="select_productos" 
                        class="select2">
                        <option value=""></option>
                        <?php
                          $last_id_prod=[];
                          $new_arr_prod=[];
                          
                          foreach ($productos as $a) {
                            foreach($unidades as $unidad){//obtener descripcion de la unidad
                              if($unidad->unidad_id==$a->unidad_id){
                                $descripcion_de_la_unidad=$unidad->unidad_tipo;
                                $a->descr_unidad=$descripcion_de_la_unidad;
                              }
                            }
                            if(in_array($a->producto_id,$last_id_prod)){
                              $new_arr_prod[$a->producto_id]->codigobarra_codigo .= ",".$a->codigobarra_codigo;
                            }else{
                              $new_arr_prod[$a->producto_id]=$a;
                              array_push($last_id_prod,$a->producto_id);
                            }
                          }
                          foreach ($new_arr_prod as $producto) {
                              echo 
                                "<option value='".$producto->codigobarra_codigo
                                ."' data-id_producto='".$producto->producto_id
                                ."' data-unidad='".$producto->descr_unidad
                                ."' data-existencia='".$producto->producto_existencias
                                ."' data-precio='".$producto->producto_costo_venta
                                ."' data-iva='".$producto->producto_iva
                                ."' data-ieps='".$producto->producto_ieps
                                ."' >".$producto->producto_nombre
                                ." </option>";
                            }

                        ?>
                      </select>
                    </span>

                    <span class="col-3">
                      <label class="mr-sm-2" for="accion">Accion:</label>
                      <select name="accion" id="accion" class="select2 ">
                          <option value="1" selected>Aumento</option>
                          <option value="2">Decremento</option>
                      </select>
                    </span>

                    <span class="col-3">
                      <label class="mr-sm-2" for="motivo">Motivo:</label>
                      <select name="motivo" id="motivo" class="select2 ">
                      
                          <?php
                            foreach ($motivos as $m) {
                              if($m->motivo_accion==1){
                                echo "<option value='".$m->motivo_id."'>".$m->motivo_descripcion."</option>";
                              }
                            }
                          
                          ?>
                      </select>
                    </span>
    
              </div>

              <br>
              
              <div class="form-row align-items-center">

                    <span class="col-3">
                      <label class="mr-sm-2" for="cantidad">Cantidad:</label>
                      <input class="my-input mr-sm-2 text-center" type="text" autocomplete="off" name="cantidad" id="cantidad" value="">
                    </span>

                    <span class="col-3">
                      <label class="mr-sm-2" for="comentario">Comentario:</label>
                      <textarea class="form-control" name="comentario" id="comentario" cols="50" rows="3"></textarea>
                     
                    </span>
              </div>


            </div>

            <div class="card-footer">
              <button type="submit" class="btn btn-primary float-right" id="enviar"><i class="fas fa-save"></i> Actualizar</button>
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Cargando</button>
            </div>

          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div><!-- /.row -->
   
  </div><!-- /.container-fluid -->
</section>

<script>
  $('document').ready(function() {
    $("#cargando").hide();
    $('#menuEntradasSalidas').addClass('active-link');

    $("#accion").on("change",function(){
      var url ="<?=base_url()?>index.php/catalogos/motivos/get_motivos_by_accion/"+$(this).val();

      ajaxJson(url,
        {},
        "POST","",function(result){
        
        var motivos = JSON.parse(result);

        $("#motivo").empty();//limpiar select

        $.each(motivos,function(key,value){
          let option="";

          option+="<option value='"+this.motivo_id+"'>"+this.motivo_descripcion+"</option>"
          $("#motivo").append(option);
        });//... $.each

      });//... ajaxJson
    });//... accion.on("change")

    $("#cantidad").on("input",function(){
      let cantidad  = valida_solo_float($(this));
    });//...cantidad

    $('#actualizar_existencias_form').submit(function(event){
      event.preventDefault();

      var obj_form=$('#actualizar_existencias_form').serialize();
      obj_form+="&id_producto="+$("#select_productos option:selected").data("id_producto");

        console.log(obj_form);

        ajaxJson("<?=base_url()?>index.php/existencias/actualizar_existencias",

          obj_form,
          "POST","",function(result){
            //console.log(id_producto+"||"+motivo_id+"||"+accion+"||"+cantidad);
            console.log(result);
            json_response = JSON.parse(result);
            
            obj_output = json_response.output;
            obj_status = obj_output.status;

            if(obj_status == false){
              aux = "";
              $.each( obj_output.errors, function( key, value ) {
                aux +=value+"<br/>";
              });
              exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux+"<br>"+json_response.msg_error,"danger");
              $("#enviar").show();
              $("#cargando").hide();
            }
            if(obj_status == true){
              exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/existencias/listar");
              $("#enviar").show();
              $("#cargando").hide();
            }
          }
        );//... ajaxJson





    });

  });//...document.ready
</script>
