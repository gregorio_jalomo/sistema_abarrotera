<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Registro de actualizaciones a existencias de productos</h3>
          </div>
          <div class="col-md-12">

            <button id="alta_baja_producto" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Alta/baja producto</button>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Producto</th>
                  <th>Accion</th>
                  <th>Motivo</th>
                  <th>Cantidad</th>
                  <th>Usuario</th>
                  <th>Fecha</th>

                </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($existencias_log as $row) {

                      $producto = $row->producto_nombre;
                      $accion   = ($row->accion==1)?"Aumento":"Decremento";
                      $motivo   = $row->motivo_descripcion;
                      $cantidad = $row->cantidad;
                      $usuario  = $row->usuario_nombre_compl;
                      $fecha    = $row->fecha_registro;

                      echo '
                        <tr>
                          <td>'.$producto.'</td>
                          <td>'.$accion.'</td>
                          <td>'.$motivo.'</td>
                          <td>'.$cantidad.'</td>
                          <td>'.$usuario.'</td>
                          <td>'.$fecha.'</td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<script>
  $('document').ready(function() {
    $('#menuEntradasSalidas').addClass('active-link');
    $("#alta_baja_producto").on("click",function(){
      window.location.href=site_url+("/existencias/actualizar/");
    });
  });
</script>
