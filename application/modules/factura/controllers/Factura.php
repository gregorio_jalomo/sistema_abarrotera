<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Factura extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

        //  if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function index($id_factura){      
      
       //$data['titulo_seccion'] = "Clientes";//clientes
       $factura = $this->Mgeneral->get_row('factura_id', $id_factura, 'factura');
       $pedido = $this->Mgeneral->get_row('id',$factura->id_pedido_aduana,'pedidosaduana');
       $receptor = $this->Mgeneral->get_row('cliente_id',$factura->receptor_id_cliente,'clientes');
        
      //  print_r($receptor);
      //  print_r($factura);
      // print_r($pedido);
      
      

       $data = [];       
       if(isset($factura) && isset($pedido) && isset($receptor)){
          $data['id_factura'] = $factura->factura_id;
          $data['lugar_expedicion'] =  $factura->factura_lugarExpedicion;
          $data['total'] = $pedido->TOTAL;
          $data['subtotal'] = $pedido->subtotal;
          $data['id_pedido'] = $pedido->id;
          $data['folio'] = $pedido->folio_unico_documento;
          $data['uso_cfdi'] = $this->Mgeneral->get_row('clientecfdi_id',$receptor->id_clientecfdi, 'clientescfdi')->clientecfdi_clave;
          $data['receptor_nombre'] = $receptor->cliente_nombre;
          $data['rfc'] = $receptor->cliente_rfc;
          $data['uso_cfdi'] = "G01";
       }      

      //  DATOS GENERALES
      
       $data['forma_pago'] = "01";
       $data['metodo_pago'] = "PUE";
       $data['codicion_pago'] = "CONTADO";
      //  

      // catalogos_sat
        // Usos CFDI
        $cat_cfdi = $this->Mgeneral->get_table('clientescfdi');
        // // cleaning data
        // $tmp = [];
        // foreach( $cat_cfdi as $c){
        //   $tmp[$c->clientes] = $c->descripcion;
        // }
        $data['cat_cfdi'] = $cat_cfdi;

        // Formas de Pago
        $cat_m_pago = $this->Mgeneral->get_table('metodo_pago');
        // cleaning data
        $tmp = [];
        foreach( $cat_m_pago as $c){
          $tmp[$c->id] = $c->descripcion;
        }
        $data['cat_metodo_pago'] = $tmp;
      
        // Formas de Pago
        $cat_f_pago = $this->Mgeneral->get_table('forma_pago');
        // cleaning data
        $tmp = [];
        foreach( $cat_f_pago as $c){
          $tmp[$c->id] = $c->descripcion;
        }
        $data['cat_forma_pago'] = $tmp;

       $data['menu_derecho'] = '';
       $contenido = "OK";
       $opciones = $this->load->view('main_template/menu', '', True);
       $contenido = $this->load->view('factura/form', $data, True);
       $header = $this->load->view('main_template/head', '', TRUE);
       $menu = $this->load->view('main_template/menu', '', TRUE);
       $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
       $footer = $this->load->view('main_template/footer', '', TRUE);
       $this->load->view('main_template/main', array('header'=>$header,
                                            'menu'=>$menu,
                                            'header_contenido'=>$header_contenido,
                                            'contenido'=>$contenido,
                                            'footer'=>$footer,
                                            'included_js'=>array('statics/js/bootbox.min.js', 'statics/js/general.js')));
                                             
    }
    public function crear(){
      $data['titulo_seccion'] = "Crear Factura";//clientes
      $data['menu_derecho'] = '';
      // $titulo['titulo'] = "Crear Factura" ;
      // $titulo['titulo_dos'] = "Crear Factura" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      // $datos= "tmp";

      


      $header = $this->load->view('main_template/head', '', TRUE);
      $menu = $this->load->view('main_template/menu', '', TRUE);
      // $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      // $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
      $contenido = $this->load->view('factura/crear', $datos, TRUE);
      $footer = $this->load->view('main_template/footer', '', TRUE);
      $this->load->view('main_template/main', array('header'=>$header,
                                           'menu'=>$menu,
                                          //  'header'=>$header,
                                          //  'titulo'=>$titulo['titulo'],
                                           'header_contenido' => $header_contenido,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array(/*'statics/js/bootbox.min.js',*/
                                           'statics/js/general.js',
                                           'statics/js/factura.js',
                                         /*'statics/bootstrap4/js/bootstrap.min.js'*/)));

    }

    public function buscarproductos(){
      $filtro    = $this->input->get("term");
		  $productos = $this->Mgeneral->buscar_producto($filtro);
		  echo json_encode($productos);
    }


    public function lista($filtro = 1){
      $titulo['titulo'] = "Lista de Facturas" ;
      $titulo['titulo_dos'] = "Lista de facturas" ;
      $datos['row'] = 'informacion';//$this->Mgeneral->get_row('id',1,'informacion');
      if($filtro == 1){
          $datos['facturas'] = $this->Mgeneral->get_table('factura');
      }else{
        $datos['facturas'] = $this->Mgeneral->get_result('prefactura',1,'factura');
      }
      $datos['filtro'] = $filtro;
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/lista', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           
                                           'statics/bootstrap4/js/bootstrap.min.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));

    }

    /*
    status = 1.-creada, 2.- en proceso,3.-facturada
    */
    public function crear_pre(){
      $id_cliente = $this->input->post('id_cliente');
      $id_pedido = $this->input->post('id_pedido');
      $emisor = $this->Mgeneral->get_row('perfil_id',$id_cliente,'perfil');    

      $pedido = $this->Mgeneral->get_row('id', $id_pedido, 'pedidosaduana');

      if($pedido && $pedido->status != 5){
        echo json_encode((Object)['status' => 'fail',                                  
                                  'message' => 'El pedido no ha sido cerrado.']);
        return;
      }

      $id_factura = $this->Mgeneral->get_row('id_pedido_aduana', $id_pedido, 'factura');
      // Si el se encontro la factura y su status = 2 (cerrado)
      if($id_factura){
        echo json_encode((Object)['status' => 'ok',
                                    'id_factura' => $id_factura->factura_id,
                                    'message' => 'Success']);
        return;
      }


      if(isset($emisor)){
        $data['status'] = 1;
        $data['emisor_RFC'] = $emisor->perfil_rfc;
        $data['emisor_regimenFiscal'] = $emisor->perfil_regimen_fiscal;
        $data['emisor_nombre'] = $emisor->perfil_razonsocial;
        $data['factura_lugarExpedicion'] = $emisor->perfil_codigo_postal;
        $data['receptor_id_cliente'] = $id_cliente;
        $data['factura_moneda'] = "MXN";
        $data['pagada'] = "SI";
        $data['id_pedido_aduana'] = $id_pedido;
        $id_factura = $this->Mgeneral->save_register('factura', $data);

        if($id_factura){
          echo json_encode((Object)['status' => 'ok',
                                    'id_factura' => $id_factura,
                                    'message' => 'Success']);
          return;
        }
        else{
          echo json_encode((Object) ['status' => 'fail',
                                     'id_factura' => NULL,
                                     'message' => 'Servidor no disponible, intenta de nuevo.']);
          return;
        }

      }
      else{
        echo json_encode((Object) ['status' => 'fail',
                                 'id_factura' => NULL,
                                 'message' => 'Datos del emisor incorrectos']);
        return;
      }
      // $id_factura = get_guid();
      // $data['facturaID'] = $id_factura;
      

    }

    public function gen_xml(){

      $id_pedido = $this->input->post('id_pedido');
      $id_factura = $this->input->post('id_factura');
      
      $cond_pago = $this->input->post('cond_pago');
      $data = [ 
                'receptor_nombre' => $this->input->post('recep_nombre'),
                'receptor_RFC' => $this->input->post('recep_rfc'),
                'receptor_uso_CFDI' => $this->input->post('uso_cfdi'),
                'factura_folio' => $this->input->post('folio'),
                'factura_formaPago' => $this->input->post('forma_pago'),
                'factura_metodoPago' => $this->input->post('metodo_pago'),
                'factura_fecha' => date("Y-m-d"),       
                'factura_subtotal' => $this->input->post('subtotal'),
                'factura_total' =>  $this->input->post('total'),
                
              ];
                         
      // echo json_encode(['status' => $this->Mgeneral->get_row('factura_id',$id_factura, 'factura')->status]);
      if($this->Mgeneral->get_row('factura_id',$id_factura, 'factura')->status != 5){
        $factura = $this->Mgeneral->update_table_row('factura',$data,'factura_id',$id_factura);
      }

      // Impotamos la libreria
      require $this->config->item( $_SERVER['DOCUMENT_ROOT']).'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';

      $cfdi = new Comprobante();
      // // Preparar valores
      
      $factura = $this->Mgeneral->get_row('factura_id',$id_factura, 'factura');
      // // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura->factura_lugarExpedicion;
      $cfdi->FormaPago         = $factura->factura_formaPago;
      $cfdi->MetodoPago        = $factura->factura_metodoPago;
      $cfdi->Folio             = $factura->factura_folio;
      $cfdi->Serie             = 'GROVA';
      $cfdi->TipoDeComprobante = 'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $factura->factura_moneda;
      $cfdi->CondicionesDePago = $cond_pago;
      $cfdi->setSubTotal($factura->factura_subtotal);
      $cfdi->setTotal($factura->factura_total);
      // $cfdi->setDescuento($descuento);
      $cfdi->setFecha(date("Y-m-d"));


      // // Agregar emisor
      $cfdi->Emisor = Emisor::init(
        $factura->emisor_RFC,
        $factura->emisor_regimenFiscal,
        $factura->emisor_nombre,
      );

      // // Agregar receptor
      $cfdi->Receptor = Receptor::init(
        $factura->receptor_RFC,
        $factura->receptor_uso_CFDI,
        $factura->receptor_nombre,
      );

      $detalle_factura  = $this->Mgeneral->obtenerProductosPorPedido($id_pedido);
      $data["detalle"] = $detalle_factura;
      // echo json_encode((Object) $data);
      foreach($detalle_factura as $concepto_fac):          
        // Datos del producto

      $total = $concepto_fac->costo_unitario * $concepto_fac->cantidad;
      $concepto = Concepto::init(
        $concepto_fac->producto_id_productosat,//'52141807',                        // clave producto SAT
        $concepto_fac->cantidad,///$concepto_fac->concepto_cantidad,//'2',                               // cantidad
        $concepto_fac->producto_id_unidad_sat, //$concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
        $concepto_fac->nombre_producto,//$concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
        $concepto_fac->costo_unitario,//$concepto_fac->concepto_precio,//95.00,                             // precio
        $total,//$concepto_fac->concepto_importe//190.00                             // importe
      );
      $concepto->NoIdentificacion = $concepto_fac->producto_id;//$concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
      $concepto->Unidad = $concepto_fac->producto_id_unidad_sat;//$concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
      //$concepto->Descuento = 0.0;

      //   // Agregar impuesto (traslado) al concepto 1
      //     // sumar iva
      if($concepto_fac->IVA != 0)
      {
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = $concepto_fac->IVA / 100;
        $traslado->Base = $total;
        $traslado->Importe = $total * ($concepto_fac->IVA / 100);
        $concepto->agregarImpuesto($traslado);
      }

      if($concepto_fac->IEPS != 0)
      {
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '003';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = $concepto_fac->IEPS / 100;
        $traslado->Base = $total;
        $traslado->Importe = $total * ($concepto_fac->IEPS / 100);
        $concepto->agregarImpuesto($traslado);
      }
    
      $cfdi->agregarConcepto($concepto);
      endforeach;
      $file_url = $_SERVER['DOCUMENT_ROOT']."/statics/facturas/".$factura->factura_folio.".xml";
      file_put_contents($file_url,$cfdi->obtenerXml());
      $status = $this->Mgeneral->update_table_row('factura',['status' => 1 , 
                                                  'url_prefactura' => 'statics/facturas/'.$factura->factura_folio.'.xml'],
                                                  'factura_id',$id_factura);

      if(file_exists($file_url) && isset($status)){
        echo json_encode((Object) ['status' => 'success', 
                                   'message' => 'Factura generada con  éxito.',
                                   'url' => $factura->factura_folio]);
        
      }else{        
          echo json_encode((Object) ['status' => 'fail', 'message' => 'Hubo un error al generar tu factura, intenta de nuevo.']);                  
      }
            
    }

    public function obtener_xml($filename){
      $file = $filename.".xml";
      $path = $_SERVER['DOCUMENT_ROOT'].'/statics/facturas/'.$filename.".xml";
      $type = '';

      // echo json_encode(["file" => $path]);
      if(is_file($path)){
        $size = file_get_contents($path);

        if(function_exists("mime_content_type")){
          $type = mime_content_type($path);
        }else if(function_exists("finfo_file")){
          $info = finfo_open(FILEINFO_MIME);
          $type = finfo_file($info, $path);
          finfo_close($info);
        }

        if ($type == '') {
          $type = "application/force-download";
        }
        // Definir headers
        $this->output->set_content_type($type);


          $this->output->set_header("Content-Disposition: attachment; filename=$file");
        // header("Content-Type: $type");
        // header();
          $this->output->set_header("Content-Transfer-Encoding: binary");
          $this->output->set_header("Content-Length: " . $size);
        // Descargar archivo
          readfile($path);
      } else {
        die("El archivo no existe.");        

      }
      
    }

    public function buscar_cliente(){
      $filtro    = $this->input->get("term");
  		$clientes = $this->Mgeneral->buscar_cliente($filtro);
  		echo json_encode($clientes);
    }

    public function nueva($id_factura){

      $datos['emisor'] =  'ositopolar';//$this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['clientes'] = $this->Mgeneral->get_table('clientes');
      $titulo['titulo'] = "Crear Factura" ;
      $titulo['titulo_dos'] = "Crear Factura" ;
      $datos['row'] = '';//$this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/nueva', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }

    public function relaciones_facturas(){
      $id_factura = $this->input->post('id_factura');
      $uuid = $this->input->post('uuid');
      $tipo_relacion = $this->input->post('tipo_relacion');
      $data['relacionados_tipoRelacion'] = $tipo_relacion;
      $data['relacion_UUID'] = $uuid;
      $data['relacion_idFactura'] = $id_factura;
      $this->Mgeneral->save_register('factura_cfdi_relacionados', $data);

      $rows = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->relacionados_id.'">
                      <th scope="row">'.$row->relacionados_tipoRelacion.'</th>
                      <td>'.$row->relacion_UUID.'</td>
                      <td><a href="'.base_url().'index.php/factura/eliminar_relacion_id/'.$row->relacionados_id.'" flag="'.$row->relacionados_id.'" id="delete'.$row->relacionados_id.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function ver_relaciones_facturas(){
      $id_factura = $this->input->post('id_factura');
      $rows = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->relacionados_id.'">
                      <th scope="row">'.$row->relacionados_tipoRelacion.'</th>
                      <td>'.$row->relacion_UUID.'</td>
                      <td><a href="'.base_url().'index.php/factura/eliminar_relacion_id/'.$row->relacionados_id.'" flag="'.$row->relacionados_id.'" id="delete'.$row->relacionados_id.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function eliminar_relacion_id($id){
      $this->Mgeneral->delete_row('factura_cfdi_relacionados','relacionados_id',$id);
    }

    public function guarda_datos($id_factura){
      $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'required');
      $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente', 'required');
      $this->form_validation->set_rules('factura_moneda', 'factura_moneda', 'required');
      $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion', 'required');
      $this->form_validation->set_rules('factura_fecha', 'factura_fecha', 'required');
      $this->form_validation->set_rules('receptor_email', 'receptor_email', 'required');
      $this->form_validation->set_rules('factura_folio', 'factura_folio', 'required');
      $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago', 'required');
      $this->form_validation->set_rules('factura_serie', 'factura_serie', 'required');
      $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion', 'required');
      $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago', 'required');
      $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante', 'required');
      $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC', 'required');
      $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI', 'required');
      $this->form_validation->set_rules('pagada', 'pagada', 'required');
      //$this->form_validation->set_rules('comentario', 'comentario', 'required');
      $response = validate($this);

      if($response['status']){

        $data['receptor_nombre'] = $this->input->post('receptor_nombre');
        $data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
        $data['factura_moneda'] = $this->input->post('factura_moneda');
        $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
        $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
        $data['receptor_email'] = $this->input->post('receptor_email');
        $data['factura_folio'] = $this->input->post('factura_folio');
        $data['factura_serie'] = $this->input->post('factura_serie');
        $data['receptor_direccion'] = $this->input->post('receptor_direccion');
        $data['factura_formaPago'] = $this->input->post('factura_formaPago');
        $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
        $data['factura_tipoComprobante'] = $this->input->post('factura_tipoComprobante');
        $data['receptor_RFC'] = $this->input->post('receptor_RFC');
        $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
        $data['pagada'] = $this->input->post('pagada');
        $data['comentario'] = $this->input->post('comentario');

        $this->Mgeneral->update_table_row('factura',$data,'facturaID',$id_factura);
      }
      //  echo $response;
      echo json_encode(array('output' => $response));

    }


    public function guarda_datos_fac($id_factura){
      $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
      $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente');
      $this->form_validation->set_rules('factura_moneda', 'factura_moneda');
      $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion');
      $this->form_validation->set_rules('factura_fecha', 'factura_fecha');
      $this->form_validation->set_rules('receptor_email', 'receptor_email');
      $this->form_validation->set_rules('factura_folio', 'factura_folio');
      $this->form_validation->set_rules('factura_serie', 'factura_serie');
      $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion');
      $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago');
      $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago');
      $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante');
      $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC');
      $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI');
      $this->form_validation->set_rules('pagada', 'pagada');
      $this->form_validation->set_rules('comentario', 'comentario');

      $response = validate($this);
      $response['status'] = true;

      if($response['status']){

        $data['receptor_nombre'] = $this->input->post('receptor_nombre');
        $data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
        $data['factura_moneda'] = $this->input->post('factura_moneda');
        $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
        $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
        $data['receptor_email'] = $this->input->post('receptor_email');
        $data['factura_folio'] = $this->input->post('factura_folio');
        $data['factura_serie'] = $this->input->post('factura_serie');
        $data['receptor_direccion'] = $this->input->post('receptor_direccion');
        $data['factura_formaPago'] = $this->input->post('factura_formaPago');
        $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
        $data['factura_tipoComprobante'] = $this->input->post('factura_tipoComprobante');
        $data['receptor_RFC'] = $this->input->post('receptor_RFC');
        $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
        $data['pagada'] = $this->input->post('pagada');
        $data['comentario'] = $this->input->post('comentario');

        $this->Mgeneral->update_table_row('factura',$data,'facturaID',$id_factura);
      }
      //  echo $response;
      echo json_encode(array('output' => $response));

    }


    public function conceptos($id_factura){
      $datos['emisor'] ='algo' ;//$this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['medidas'] = 'medidas';//$this->Mgeneral->get_table('medidas');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['productos'] = $this->Mgeneral->get_table('productos');
      $datos['facturaID'] = $id_factura;
      $titulo['titulo'] = "Factura" ;
      $titulo['titulo_dos'] = "prefactura" ;
      $datos['row'] = 'informacion';//$this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/conceptos', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }

    public function buscar_producto(){
      $filtro    = $this->input->get("term");
  		$clientes = $this->Mgeneral->buscar_producto($filtro);
  		echo json_encode($clientes);
    }


    public function get_dados_producto($id_producto){
      //  $id_cliente = $this->input->post('id_cliente');
      $res = $this->Mgeneral->get_row('productoId',$id_producto,'productos');
      echo json_encode($res);
    }


    public function guarda_datos_concepto($id_factura){
      //$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
      //$response = validate($this);
      $response['status'] = true;

      if($response['status']){

        $data['concepto_facturaId'] = $id_factura;
        $data['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');
        $data['concepto_unidad'] = $this->input->post('concepto_unidad');
        //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
        $data['clave_sat'] = $this->input->post('clave_sat');
        $data['unidad_sat'] = $this->input->post('unidad_sat');
        $data['concepto_nombre'] = $this->input->post('concepto_nombre');
        $data['concepto_precio'] = $this->input->post('concepto_precio');
        $data['concepto_importe'] = $this->input->post('concepto_importe');
        $data['impuesto_iva'] = $this->input->post('impuesto_iva');
        $data['impuesto_iva_tipoFactor'] = $this->input->post('impuesto_iva_tipoFactor');
        $data['impuesto_iva_tasaCuota'] = $this->input->post('impuesto_iva_tasaCuota');
        $data['impuesto_ISR'] = $this->input->post('impuesto_ISR');
        $data['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
        $data['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
        $data['tipo'] = $this->input->post('tipo');
        $data['nombre_interno'] = $this->input->post('nombre_interno');
        $data['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');
        $data['concepto_cantidad'] = $this->input->post('concepto_cantidad');
        $data['importe_iva'] = $this->input->post('importe_iva');
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $this->Mgeneral->save_register('factura_conceptos',$data);
      }
      //  echo $response;
      echo json_encode(array('output' => $response));

    }

    public function eliminar_concepto($id_concepto,$id_factura){
      $this->Mgeneral->delete_row('factura_conceptos','concepto_id',$id_concepto);
      redirect("factura/conceptos/".$id_factura);

    }

    public function ver_factura($id_factura){
      $datos['emisor'] = 'datos';//$this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['facturaID'] = $id_factura;
      $titulo['titulo'] = "Factura" ;
      $titulo['titulo_dos'] = "prefactura" ;
      $datos['row'] = 'info';//$this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/ver', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }

    public function generar_factura($id_factura){
      $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      //error_reporting(1);
      //ini_set('display_errors', 1);
      require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
      $cfdi = new Comprobante();
      // Preparar valores
      $moneda = 'MXN';
      $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
      $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
      //$descuento =   1.40;
      $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
      $fecha     = time();
      // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
      $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
      $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
      $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
      $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
      $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $moneda;
      $cfdi->setSubTotal($subtotal);
      $cfdi->setTotal($total);
      //$cfdi->setDescuento($descuento);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
          $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
          $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
          $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
          $factura_nueva->receptor_nombre//'Receptor Ejemplo'                 // Nombre (opcional)
      );

      $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      foreach($conceptos_factura as $concepto_fac):
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
            $concepto_fac->concepto_cantidad,//'2',                               // cantidad
            $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
            $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
            $concepto_fac->concepto_precio,//95.00,                             // precio
            $concepto_fac->concepto_importe//190.00                             // importe
        );
        $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
        $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
        //$concepto->Descuento = 0.0;

        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);

        // Agregar concepto 1 a CFDI
        $cfdi->agregarConcepto($concepto);

        // Agregar más conceptos al CFDI
        // $concepto = Concepto::init(...);
        // ...
        // $cfdi->agregarConcepto($concepto);
     endforeach;


      // Mostrar XML del CFDI generado hasta el momento
      // header('Content-type: application/xml; charset=UTF-8');
      // echo $cfdi->obtenerXml();
      // die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();

      // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
      // UtilCertificado::establecerRutaOpenSSL();

      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');

      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);
            $this->enviar_factura($id_factura);
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');
    }


    public function enviar_factura($id_factura){
      //echo "".base_url()."index.php/factura/generar_factura/".$id_factura;
      //echo  $datos_fac->url_prefactura;
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

      # Username and Password, assigned by FINKOK
      $username = 'liberiusg@gmail.com';
      $password = '*Libros7893811';

      # Read the xml file and encode it on base64
      $invoice_path = $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
      $xml_file = fopen($invoice_path, "rb");
      $xml_content = fread($xml_file, filesize($invoice_path));
      fclose($xml_file);

      # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
      #$xml_content = base64_encode($xml_content);

      # Consuming the stamp service
      $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
      $client = new SoapClient($url);

      $params = array(
        "xml" => $xml_content,
        "username" => $username,
        "password" => $password
      );
      $response = $client->__soapCall("stamp", array($params));
      //print_r($response);
      ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
      //print $response->stampResult->xml;

      ####mostrar el código de error en caso de presentar alguna incidencia
      #print $response->stampResult->Incidencias->Incidencia->CodigoError;
      ####mostrar el mensaje de incidencia en caso de presentar alguna
      if(isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)){
        $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
        //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }else{
        $data_sat['sat_uuid'] = $response->stampResult->UUID;
        $data_sat['sat_fecha'] = $response->stampResult->Fecha;
        $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
        $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
        $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
        $data_sat['sat_error'] = "0";
        $data_sat['sat_codigo_error'] = "0";
        $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
        file_put_contents($this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
        $data_respone['error'] = 0;
        $data_respone['error_mensaje'] = "Factura timbrada";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
    }

    public function genera_pdf($id_factura){
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

       $datos_cliente = $this->Mgeneral->get_row('id',$datos_fac->receptor_id_cliente,'clientes');

      $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'www.fumigacioneselastillero.com';
      $pdf->titulo = 'Factura';
      $pdf->asunto = 'CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = $datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Factura';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = $datos_cliente->comentario_extra;

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = $datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }

    public function cancelar_factura($id_factura){
      $factura_datos = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      # Generar el certificado y llave en formato .pem
        shell_exec("openssl x509 -inform DER -outform PEM -in ".$this->config->item('url_real')."cfdi/xml/ejemplos/LAN7008173R5.cer -pubkey -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.cer.pem");
        shell_exec("openssl pkcs8 -inform DER -in ".$this->config->item('url_real')."cfdi/xml/ejemplos/LAN7008173R5.key -passin pass:12345678a -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.key.pem");
        shell_exec("openssl rsa -in ".$this->config->item('url_real')."cfdi/LAN7008173R5.key.pem -des3 -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.enc -passout pass:*Libros7893811");

        # Username and Password, assigned by FINKOK
        $username = 'liberiusg@gmail.com';
        $password = '*Libros7893811';
        
        # Consuming the cancel service
        # Read the x509 certificate file on PEM format and encode it on base64
        $cer_path = $this->config->item('url_real').'cfdi/LAN7008173R5.cer.pem';
        $cer_file = fopen($cer_path, "r");
        $cer_content = fread($cer_file, filesize($cer_path));
        fclose($cer_file);
        
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$cer_content = base64_encode($cer_content);

        # Read the Encrypted Private Key (des3) file on PEM format and encode it on base64
        $key_path = $this->config->item('url_real')."cfdi/LAN7008173R5.enc";
        $key_file = fopen($key_path, "r");
        $key_content = fread($key_file,filesize($key_path));
        fclose($key_file);
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$key_content = base64_encode($key_content);

        $taxpayer_id = 'LAN7008173R5'; # The RFC of the Emisor
        $invoices = array($factura_datos->sat_uuid); # A list of UUIDs

        $url = "https://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl";
        $client = new SoapClient($url);
        $params = array(
          "UUIDS" => array('uuids' => $invoices),
          "username" => $username,
          "password" => $password,
          "taxpayer_id" => $taxpayer_id,
          "cer" => $cer_content,
          "key" => $key_content,
          "get_sat_status" => false
        );
        $response = $client->__soapCall("cancel", array($params));
        //print_r($response);
        //echo $response->cancelResult->CodEstatus ;
        //die;
        
      /*  echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Fecha;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->UUID;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->EstatusUUID;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->EstatusCancelacion;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->RfcEmisor;
        */

        if(is_object($response)){
          if($response->cancelResult->Folios->Folio->EstatusUUID == "202"){
            $data_cancelar['cancelar_Fecha'] = $response->cancelResult->Fecha;
            $data_cancelar['cancelar_UUID'] = $response->cancelResult->Folios->Folio->UUID;
            $data_cancelar['cancelar_EstatusUUID'] = $response->cancelResult->Folios->Folio->EstatusUUID;
            $data_cancelar['cancelar_EstatusCancelacion'] = $response->cancelResult->Folios->Folio->EstatusCancelacion;
            $data_cancelar['cancelar_RfcEmisor'] = $response->cancelResult->RfcEmisor;
            $this->Mgeneral->update_table_row('factura',$data_cancelar,'facturaID',$id_factura);
            //redirect("factura/lista");
            $data_cancelar['error'] = false;
            echo json_encode($data_cancelar);
          }else{
            $data_cancelar['error'] = true;
            echo json_encode($data_cancelar);
          }

        }else{
           $data_cancelar['error'] = true;
            echo json_encode($data_cancelar);
        }


    }



    public function generar_prefactura($id_factura){
      $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      //error_reporting(1);
      //ini_set('display_errors', 1);


      require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
      $cfdi = new Comprobante();
      // Preparar valores
      
      $moneda = 'MXN';
      $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
      $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
      //$descuento =   1.40;
      $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
      $fecha     = time();
      
      // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
      $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
      $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
      $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
      $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
      $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $moneda;
      $cfdi->setSubTotal($subtotal);
      $cfdi->setTotal($total);
      //$cfdi->setDescuento($descuento);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
          $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
          $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
          $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
          $factura_nueva->receptor_nombre//'Receptor Ejemplo'                 // Nombre (opcional)
      );

      $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      foreach($conceptos_factura as $concepto_fac):
        // Preparar datos del concepto 1
      // Datos del producto
        $concepto = Concepto::init(
            $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
            $concepto_fac->concepto_cantidad,//'2',                               // cantidad
            $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
            $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
            $concepto_fac->concepto_precio,//95.00,                             // precio
            $concepto_fac->concepto_importe//190.00                             // importe
        );
        $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
        $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
        //$concepto->Descuento = 0.0;

        // Agregar impuesto (traslado) al concepto 1
          // sumar iva
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);

        // Agregar concepto 1 a CFDI
        $cfdi->agregarConcepto($concepto);

        // Agregar más conceptos al CFDI
        // $concepto = Concepto::init(...);
        // ...
        // $cfdi->agregarConcepto($concepto);
     endforeach;


      // Mostrar XML del CFDI generado hasta el momento
      // header('Content-type: application/xml; charset=UTF-8');
      // echo $cfdi->obtenerXml();
      // die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();

      // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
      // UtilCertificado::establecerRutaOpenSSL();

      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');

      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

           $data_sat['prefactura'] = 1;
           $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
           $data_respone['error'] = 0;
          echo json_encode($data_respone);
          die;
            
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');
    }

    public function genera_pdf_prefactura($id_factura){
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'www.fumigacioneselastillero.com';
      $pdf->titulo = 'Factura';
      $pdf->asunto = 'CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = $datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Factura';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = '';

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = $datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/facturas/prefactura/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }

    public function leer_xml($factura_xml){
        $xml = simplexml_load_file(base_url()."statics/facturas/facturas_xml/".$factura_xml.".xml"); 
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);
         
         
        //EMPIEZO A LEER LA INFORMACION DEL CFDI E IMPRIMIRLA 
        foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){ 
              echo $cfdiComprobante['Version']; 
              echo "<br />"; 
              echo $cfdiComprobante['Fecha']; 
              echo "<br />"; 
              echo $cfdiComprobante['Sello']; 
              echo "<br />"; 
              echo $cfdiComprobante['Total']; 
              echo "<br />"; 
              echo $cfdiComprobante['SubTotal']; 
              echo "<br />"; 
              echo $cfdiComprobante['Certificado']; 
              echo "<br />"; 
              echo $cfdiComprobante['FormaDePago']; 
              echo "<br />"; 
              echo $cfdiComprobante['NoCertificado']; 
              echo "<br />"; 
              echo $cfdiComprobante['TipoDeComprobante']; 
              echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){ 
           echo $Emisor['rfc']; 
           echo "<br />"; 
           echo $Emisor['Nombre']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal') as $DomicilioFiscal){ 
           echo $DomicilioFiscal['Pais']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Calle']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Estado']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Colonia']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Municipio']; 
           echo "<br />"; 
           echo $DomicilioFiscal['NoExterior']; 
           echo "<br />"; 
           echo $DomicilioFiscal['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn') as $ExpedidoEn){ 
           echo $ExpedidoEn['Pais']; 
           echo "<br />"; 
           echo $ExpedidoEn['Calle']; 
           echo "<br />"; 
           echo $ExpedidoEn['Estado']; 
           echo "<br />"; 
           echo $ExpedidoEn['Colonia']; 
           echo "<br />"; 
           echo $ExpedidoEn['NoExterior']; 
           echo "<br />"; 
           echo $ExpedidoEn['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){ 
           echo $Receptor['rfc']; 
           echo "<br />"; 
           echo $Receptor['nombre']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio') as $ReceptorDomicilio){ 
           echo $ReceptorDomicilio['Pais']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Calle']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Estado']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Colonia']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Municipio']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['NoExterior']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['NoInterior']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){ 
           echo "<br />"; 
           echo $Concepto['Unidad']; 
           echo "<br />"; 
           echo $Concepto['Importe']; 
           echo "<br />"; 
           echo $Concepto['Cantidad']; 
           echo "<br />"; 
           echo $Concepto['Descripcion']; 
           echo "<br />"; 
           echo $Concepto['ValorUnitario']; 
           echo "<br />";   
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){ 
           echo $Traslado['Tasa']; 
           echo "<br />"; 
           echo $Traslado['Importe']; 
           echo "<br />"; 
           echo $Traslado['Impuesto']; 
           echo "<br />";   
           echo "<br />"; 
        } 
         
        //ESTA ULTIMA PARTE ES LA QUE GENERABA EL ERROR
        foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
           echo $tfd['SelloCFD']; 
           echo "<br />"; 
           echo $tfd['FechaTimbrado']; 
           echo "<br />"; 
           echo $tfd['UUID']; 
           echo "<br />"; 
           echo $tfd['NoCertificadoSAT']; 
           echo "<br />"; 
           echo $tfd['Version']; 
           echo "<br />"; 
           echo $tfd['SelloSAT']; 
        } 
    }
    
}