
<div id="app" class="container">    
    <form>
        <h2>Receptor:</h2>
        <div class="form-group">   
            <div class="row">
                <div class="col">
                    <label for=""> Nombre / Razón Social</label>
                    <input type="text" class="form-control"  id="recep_nombre" value="<?= $receptor_nombre?>">
                </div>                        
            </div>            
            <div class="row">
                <div class="col-5">
                    <label for="">R.F.C.</label>
                    <input type="text" class="form-control" id="recep_rfc" value="<?= $rfc?>">
                </div>
                <div class="col"></div>
            </div>
            <div class="row">
                <div class="col">                                                
                    <label for="">Uso de CFDI</label>                    
                    <select class="form-control" id="uso_cfdi">                        
                        <?php foreach($cat_cfdi as $cfdi): ?>                            
                            <?php $cfdi->clientecfdi_clave == $uso_cfdi ? $opt = "selected" : $opt = ""; ?>
                            <option value="<?= $cfdi->clientecfdi_clave?>" <?php echo $opt ?> >
                                <?= $cfdi->clientecfdi_clave ?> -- <?= $cfdi->clientecfdi_descripcion ?>
                            </option>      
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
                
            <!-- </select> -->
            <!-- o -->
        </div>
        <hr>
        <h2>Datos Generales:</h2>                        
        <div class="form-group">   
            <div class="row">
                <div class="col">
                    <label for="">Folio</label>
                    <input type="text" class="form-control" id="folio" disabled value="<?= $folio?>">
                </div>        
                <!-- <div class="col">
                    <label for="">Fecha</label>
                    <input type="text" class="form-control" id="" disabled>
                </div> -->
                <div class="col">
                    <label for="">Lugar de expedición</label>
                    <input type="text" class="form-control" id="lugar_exped" value="C.P. <?= $lugar_expedicion?>" disabled>
                </div>
            </div>            
            <div class="row">
                <div class="col">                                        
                    <label for="">Forma de pago</label>
                    <select class="form-control" id="forma_pago">                        
                        <?php foreach($cat_forma_pago as $key => $pago): ?>                            
                            <?php $key == $forma_pago ? $opt = "selected" : $opt = ""; ?>
                            <option value="<?= $key?>" <?php echo $opt ?> >
                                <?= $key ?> -- <?= $pago ?>
                            </option>      
                        <?php endforeach; ?>
                    </select>
                </div>     
                <div class="col">
                    <label for="">Método de pago</label>
                    <select class="form-control" id="metodo_pago" disabled>                        
                            <?php foreach($cat_metodo_pago as $key => $pago): ?>                            
                            <?php $key == $metodo_pago ? $opt = "selected" : $opt = ""; ?>
                                <option value="<?= $key?>" <?php echo $opt ?> >
                                    <?= $key ?> -- <?= $pago ?>
                                </option>      
                            <?php endforeach; ?>
                        </select>
                </div>
            </div>            
            <div class="row">
                <div class="col-9">
                    <label for="">Condición de pago</label>
                    <select class="form-control" id="cond_pago" disabled>                        
                        <option value="CONTADO" selected>CONTADO</option>      
                        <option value="CREDITO">CREDITO</option>      
                    </select>
                </div>
                <div class="col-3">
                    <label for="" hidden>Días de Crédito</label>
                    <input type="number" class="form-control" id="d_credito" hidden>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col">
                    <label for="">Moneda</label>
                    <select class="form-control" id="">
                        <option>Selecciona una opción...</option>      
                    </select>
                </div> -->
                <div class="col">
                    <label for="">Subtotal</label>
                    <input type="number" class="form-control" id="subtotal" value="<?= $subtotal?>" disabled>
                </div>        
                <div class="col">
                    <label for="">Total</label>
                    <input type="number" class="form-control" id="total" value="<?= $total?>"disabled>
                </div>                
                
                <!-- <input type="number" class="form-control" id="id_pedido" value="<?= $id_pedido?>"disabled hidden>                
                <input type="number" class="form-control" id="id_factura" value="<?= $id_factura?>"disabled hidden>                 -->
            </div>            
        </div>        
        <button class="btn btn-danger" id="back">Regresar</button>
        <button type="submit" class="btn btn-primary" id="submit">Generar</button>
    </form>                

    <script>
        $("#back").click(() => {
            window.history.back();
        });

        $("#submit").click((e) => {
            e.preventDefault();
                                    
            let data = { id_pedido: <?= $id_pedido ?>,
                         id_factura: <?= $id_factura ?>,
                         recep_nombre: $("#recep_nombre").val(),
                         recep_rfc: $("#recep_rfc").val(),
                         uso_cfdi:$("#uso_cfdi").val(),
                         folio: $("#folio").val(),
                         forma_pago: $("#forma_pago").val(),                        
                         metodo_pago: $("#metodo_pago").val(),
                         cond_pago: $("#cond_pago").val(),
                        subtotal: $("#subtotal").val(),
                        total: $("#total").val(),
                       };

            // alert(id_pedido + " - " + id_factura);
            $.post("<?php echo base_url() ?>index.php/factura/gen_xml", data)
                .done((response) => {                    
                    response = JSON.parse(response);                    
                    if(response.status){            
                        var base_url = window.location.protocol + "//" + window.location.hostname + "/index.html/";
                        // console.log(url);
                        var file = {"file": "statics/facturas/"+ response.url + ".xml"}
                        var message = `<h5>` + response.message + `</h5>` +
                                      `<input type="hidden" id="downloadXML" data-url="`+ response.url+ `" />`;


                        ConfirmCustom(message, obtener_xml, "", "Descargar", "Cerrar");
                    }
                    
                });
            });



            function obtener_xml(){                
                  el = $("#downloadXML");                  
                  window.location.href = "<?php echo base_url() ?>index.php/factura/obtener_xml/" + el.data("url");                  
            }

    </script>