<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inicio extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproductos', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');

  }

  /*
  * falta cargar permisos, un susuario puede tener opcion de ver productos pero no editar, o editar y ver, etc
  */
  public function index(){

      //$data['titulo_seccion'] = "Clientes";//clientes
      $data['menu_derecho'] = '';
      $contenido = $this->load->view('inicio/inicio', '', TRUE);
      $opciones = $this->load->view('main_template/menu', '', True);
      //$contenido = $this->load->view('main_template/opcproductos', '', True);
      $header = $this->load->view('main_template/head', '', TRUE);
      $menu = $this->load->view('main_template/menu', '', TRUE);
      $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
      $footer = $this->load->view('main_template/footer', '', TRUE);
      $this->load->view('main_template/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_contenido'=>$header_contenido,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/general.js')));


    }
}
