<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Pdf extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));

      date_default_timezone_set('America/Mexico_City');

       //if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function orden_venta($id_orden = null){
    

    $comentarios = $this->load->view('pdf/comentarios', '', TRUE);
    $contenido = $this->load->view('pdf/orden_venta', array('comentarios'=>$comentarios), TRUE);
    $this->load->view('pdf/main', array('contenido'=>$contenido));
  }
  public function clientescotizacion($id_orden = null){
    

    $comentarios = $this->load->view('pdf/comentarios', '', TRUE);
    $contenido = $this->load->view('pdf/clientescotizacion', array('comentarios'=>$comentarios), TRUE);
    $this->load->view('pdf/main', array('contenido'=>$contenido));
  }
  
}