<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Pedidosaduana extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mpedidosaduana', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('pedidosaduana/Pedidosaduana_model');
      $this->load->model('catalogos/Clientelugar_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 35;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

/* 
  if(validar_permiso($this->rol_usuario,$this->id_modulo,32) || $this->is_admin ){

	  //code

  }else{
    $this->p_denied_view("listar pedidos en aduana(clientes)","pedidosaduana/listar_pedidosaduana");
  }

*/

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,118) || $this->is_admin ){

      //recibir post del ajax
      $dataPedidosaduana['folio_documento'] = $this->input->post('nuevo_folio_documento');
      $dataPedidosaduana['folio_unico_documento'] = $this->input->post('nuevo_FUNI_documento');
      $dataPedidosaduana['perfil_usuario'] = $this->session->userdata['infouser']['id'];
      $dataPedidosaduana['descripcion_id_usuario'] = $this->session->userdata['infouser']['nombre'];
      $dataPedidosaduana["fecha_creacion"]         = date("Y-m-d H:i:s");

      $foliosdocumentos_id= $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI= $this->input->post('id_FUNI_foliosdocumentos');

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que un pedido se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        //insert a pedidosaduana (solo foliodocumento, FUNI,id y nombre del usuario en sesion)
        $pedidosaduana_id=$this->guardar($dataPedidosaduana);
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }else{
        $row_clientespedido=$this->Pedidosaduana_model->get_clientespedido_by_FUNI($dataPedidosaduana['folio_unico_documento']);
        //insert a pedidosaduana (importar datos de clientespedido)
                  $data_import["id_clientes"]                     = $row_clientespedido->id_clientes;
                  $data_import["nombre_cliente"]                  = $row_clientespedido->nombre_cliente;
                  $data_import["domicilio_cliente"]               = $row_clientespedido->domicilio_cliente;
                  $data_import["id_clientes_condicion"]           = $row_clientespedido->id_clientes_condicion;
                  $data_import["descripcion_clientes_condicion"]  = $row_clientespedido->descripcion_clientes_condicion;
                  $data_import["id_agentes_venta"]                = $row_clientespedido->id_agentes_venta;
                  $data_import["nombre_agentes_venta"]            = $row_clientespedido->nombre_agentes_venta;
                  $data_import["descripcion_agentes_venta"]       = $row_clientespedido->descripcion_agentes_venta;
                  $data_import["perfil_usuario"]                  = $row_clientespedido->perfil_usuario;
                  $data_import["descripcion_id_usuario"]          = $row_clientespedido->descripcion_id_usuario;
                  $data_import["comentario"]                      = $row_clientespedido->comentario;
                  $data_import["subtotal"]                        = $row_clientespedido->subtotal;
                  $data_import["IVA"]                             = $row_clientespedido->IVA;
                  $data_import["IEPS"]                            = $row_clientespedido->IEPS;
                  $data_import["TOTAL"]                           = $row_clientespedido->TOTAL;
                  $data_import["folio_documento"]                 = $dataPedidosaduana['folio_documento'];
                  $data_import["folio_unico_documento"]           = $row_clientespedido->folio_unico_documento;
                  $data_import["fecha_creacion"]                  = date("Y-m-d H:i:s");
                  $data_import["fecha_entrega"]                   = (isset($row_clientespedido->fecha_entrega))?$row_clientespedido->fecha_entrega:date('Y-m-d 12:00:00',strtotime("+1 days"));
                  $data_import["status"]                          = 4;


        $pedidosaduana_id=$this->guardar($data_import);
        //importar datos a pedidosaduana desde clientespedido
        //$this->importar_datos_de_cPedido($dataPedidosaduana['folio_unico_documento'],$pedidosaduana_id);
      }

      echo json_encode($pedidosaduana_id);
  
    }else{
      echo json_encode(false);
    }

  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('pedidosaduana', $data);
  }//...guardar

  public function editar($id_pedido){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,132) || $this->is_admin ){

      $data['titulo_seccion'] = "Pedido en aduana";
      $data['flecha_ir_atras'] = "pedidosaduana/listar_pedidosaduana";
      $data['force_landscape']=true;

      $data['pedidosaduana'] = $this->Pedidosaduana_model->get_pedidosaduana_by_id($id_pedido);
      $data['clientescondicion'] =$this->Pedidosaduana_model->get_clientes_condicion($data['pedidosaduana']->id_clientes);
      $data['lugares'] = $this->Clientelugar_model->get_lugares($data['pedidosaduana']->id_clientes,null);
      //$data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['productos'] = $this->Pedidosaduana_model->get_productos('productos.cat_estatus_id',1);
      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['clientes'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientes');
      
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6','7'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js'

      
      );

      $this->cargar_vista('pedidosaduana/editar',$data,$archivos_js);
  
    }else{
      $this->p_denied_view("listar pedidos en aduana(clientes)","pedidosaduana/listar_pedidosaduana");
    }

    
  }//...editar

  public function ver_detalle($id_pedidosaduana){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,119) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del pedido";
      $data['flecha_ir_atras'] = "pedidosaduana/pedidosaduana/listar_pedidosaduana";

      $data['pedidosaduana'] = $this->Pedidosaduana_model->get_pedidosaduana_by_id($id_pedidosaduana);
      $data['pedidosaduana_detalle'] =$this->Pedidosaduana_model->get_pedidosaduana_detalle($id_pedidosaduana);

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('pedidosaduana/ver_detalle',$data,$archivos_js);      //code
  
    }else{
      $this->p_denied_view("ver detalle de pedidos en aduana(clientes)","pedidosaduana/listar_pedidosaduana");
    }

  }//...ver_detalle

  public function listar_pedidosaduana(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,122) || $this->is_admin ){

      $data['titulo_seccion'] = "Pedidos en aduana ";
      $data['flecha_ir_atras'] = "clientespedido/listar_clientespedido";
      $data['force_landscape']=true;

      $data['con_pedidosaduana'] = $this->Pedidosaduana_model->listar_pedidosaduana();

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('pedidosaduana/listar',$data,$archivos_js);
  
    }else{
      $this->p_denied_view("listar pedidos en aduana(clientes)","");
    }

  }//...listar_pedidosaduana

  public function get_pedidosaduana_detalle($id_pedidosaduana){
    echo json_encode($this->Pedidosaduana_model->get_pedidosaduana_detalle($id_pedidosaduana));

  }//...get_pedidosaduana_detalle

  public function get_pedidosaduana_by_FUNI($folio_unico_documento){
    echo json_encode($this->Pedidosaduana_model->get_pedidosaduana_by_FUNI($folio_unico_documento));
  }//...get_pedidosaduana_by_FUNI

  public function get_info_cliente(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Pedidosaduana_model->get_info_cliente($cliente_id));
  }//...get_info_cliente

  public function get_condicion_pago_by_cliente_id(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Pedidosaduana_model->get_clientes_condicion($cliente_id));
  }//...get_condicion_pago_by_cliente_id

  public function get_set_ultimo_status($id_pedidosaduana,$nvo_stat){
    echo json_encode($this->Pedidosaduana_model->get_set_ultimo_status($id_pedidosaduana,$nvo_stat));
  }//...get_set_ultimo_status

  public function cambiar_estatus($id_pedido,$estatus_id) {
    $data['status'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,132) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,121) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){
      $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
      if($estatus_id=="5"){
        $data["chk_aduana"]=1;
      }
      $this->Mgeneral->update_table_row('pedidosaduana',$data,'id',$id_pedido);     
    }
    echo json_encode($go);

  }//...cambiar_estatus

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
      $this->Pedidosaduana_model->actualizar_fecha_modificacion($id);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_cliente_nombre_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['id_clientes']=$this->input->post('id_clientes');
    $data['nombre_cliente']=$this->input->post('nombre_cliente');
    $data['domicilio_cliente']=$this->input->post('domicilio_cliente');
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido);
  }//...update_cliente_nombre_domicilio

  public function update_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['domicilio_cliente']=$this->input->post('lugar_cliente');
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido);
    echo json_encode($data['domicilio_cliente']);
  }//...update_domicilio

  public function update_info_agentesventa(){
    $data['id_agentes_venta']=$this->input->post('agenteventa_id');
    $data['nombre_agentes_venta']=$this->input->post('nombre_agentes_venta');
    $id_pedido=$this->input->post('id_pedido');
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));

  }//...update_info_agentesventa
  
  public function update_fecha_entrega(){
    $id_pedido=$this->input->post('id_pedido');
    $fecha_entrega=$this->input->post('fecha_entrega');

    $time = strtotime($fecha_entrega);
    $newformat = date('Y-m-d H:i:s',$time);

    $data['fecha_entrega']=$newformat;
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));
  }//...update_fecha_entrega

  public function update_info_clientes_condicion(){
    $data['id_clientes_condicion']=$this->input->post('id_clientes_condicion');
    $data['descripcion_clientes_condicion']=$this->input->post('descripcion_clientes_condicion');
    $id_pedido=$this->input->post('id_pedido');
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));

  }//...update_info_clientes_condicion

  public function agregar_producto(){

    $this->form_validation->set_rules('id_pedidosaduana', ' ID del pedido', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    $this->form_validation->set_rules('costo_unitario', ' Precio del producto', 'required');
    $this->form_validation->set_rules('costo_total', ' Importe', 'required');
    
    $id_pedidosaduana=$this->input->post('id_pedidosaduana');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    $cantidad_original=$this->input->post('cantidad_original');
    $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total');

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    $comprometidos=0;
    $producto=$this->Pedidosaduana_model->buscar_producto_pAduana_detalle($id_pedidosaduana,$id_producto);
    if(isset($producto)){
      //$cant=$cantidad+$producto->cantidad;

      /* if($producto->validado==0){
        
        //$valid=($cant>=$producto->cantidad_original)?1:0;
        $valid=1;
      } */
      $dataUpdate['id_pedidosaduana']  = $id_pedidosaduana;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      $dataUpdate['costo_unitario']         = $costo_unitario;
      //$dataUpdate['costo_total']            = $costo_total+$producto->costo_total;
      $dataUpdate['costo_total']            = $costo_total;
      $dataUpdate['validado']               = 1;

      $comprometidos=$cantidad-$producto->cantidad;

      echo json_encode($this->Pedidosaduana_model->actualizar_producto_pAduana_detalle($dataUpdate));
      //echo json_encode(array("status"=>true,"producto"=>$dataUpdate));

    }else{

    $dataAdd['id_pedidosaduana']          = $id_pedidosaduana;
    $dataAdd['id_producto']               = $id_producto;
    $dataAdd['nombre_producto']           = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']  = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']                  = $cantidad;
    $dataAdd['cantidad_original']         = 0;
    $dataAdd['costo_unitario']            = $costo_unitario;
    $dataAdd['IVA']                       = $this->input->post('IVA');
    $dataAdd['IEPS']                      = $this->input->post('IEPS');
    $dataAdd['costo_total']               = $costo_total;
    $dataAdd['validado']                  = 1;

    $comprometidos=$cantidad;

    echo json_encode($this->Pedidosaduana_model->agregar_producto_pAduana_detalle($dataAdd));
    }
    if($comprometidos!=0){
      $this->Pedidosaduana_model->actualizar_comprometidos_producto($id_producto,$comprometidos);
    }
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedidosaduana);
  }//...agregar_producto

  public function get_productos_no_validados($id_pedidosaduana){

    echo json_encode($this->Pedidosaduana_model->get_productos_no_validados($id_pedidosaduana));
  }//...get_productos_no_validados

  public function eliminar_producto($id_pedido,$id_producto){
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Pedidosaduana_model->eliminar_producto_pAduana_detalle($id_pedido,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Pedidosaduana_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_pedido=$this->input->post('id_pedido');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Mgeneral->update_table_row('pedidosaduana',$data,'id',$id_pedido);
    $this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedido);
  }//...agregar_comentarios

  public function update_costo_by_condicion_pago($id_pedidosaduana,$id_clientescondicion){
      //$this->Pedidosaduana_model->actualizar_fecha_modificacion($id_pedidosaduana);
      echo json_encode($this->Pedidosaduana_model->update_costo_by_condicion_pago($id_pedidosaduana,$id_clientescondicion));
  }//...update_costo_by_condicion_pago

  public function calcular_totales($id_pedidosaduana){
    echo json_encode($this->Pedidosaduana_model->calcular_totales($id_pedidosaduana));
  }//...calcular_totales

  public function get_fecha_modificacion(){
    $id_pedido=$this->input->post('id_pedido');
    echo json_encode($this->Pedidosaduana_model->get_fecha_modificacion($id_pedido));
  }//...get_fecha_modificacion

  public function importar_datos_de_cPedido($folio_unico_documento,$pedidosaduana_id){

    echo json_encode($this->Pedidosaduana_model->importar_datos_de_cPedido($folio_unico_documento,$pedidosaduana_id));
  }//...importar_datos_de_cPedido

  public function importar_productos_de_cPedido_detalle(){
    $folio_unico_documento=$this->input->post('folio_unico_documento');
    $id_pedidosaduana=$this->input->post('id_pedidosaduana');
    $id_pedido=$this->Pedidosaduana_model->get_id_clientespedido_by_FUNI($folio_unico_documento);
    $result_clientespedido=$this->Pedidosaduana_model->get_clientespedido_detalle($id_pedido);
    //$data_import["folio_unico_documento"]           =$row_pedidosaduana->folio_unico_documento;
    
    $cont=0;
    foreach($result_clientespedido as $row_ped){
      $data_import[$cont]["id_pedidosaduana"]          =$id_pedidosaduana;
      $data_import[$cont]["id_producto"]               =$row_ped->id_producto;
      $data_import[$cont]["nombre_producto"]           =$row_ped->nombre_producto;
      $data_import[$cont]["codigo_barras_producto"]    =$row_ped->codigo_barras_producto;
      $data_import[$cont]["descripcion_de_la_unidad"]  =$row_ped->descripcion_de_la_unidad;
      $data_import[$cont]["cantidad"]                  =$row_ped->cantidad;
      $data_import[$cont]["cantidad_original"]         =$row_ped->cantidad;
      $data_import[$cont]["IVA"]                       =$row_ped->IVA;
      $data_import[$cont]["IEPS"]                      =$row_ped->IEPS;
      $data_import[$cont]["costo_total"]               =$row_ped->costo_total;
      $data_import[$cont]["costo_unitario"]            =$row_ped->costo_unitario;
      $data_import[$cont]["IVA"]                       =$row_ped->IVA;
      $data_import[$cont]["IEPS"]                      =$row_ped->IEPS;
      $data_import[$cont]["costo_total"]               =$row_ped->costo_total;
      $data_import[$cont]["validado"]                  =0;
      $cont++;
    }

    echo json_encode($this->Pedidosaduana_model->importar_productos_de_cPedido_detalle($id_pedidosaduana,$data_import));
  }//...importar_productos_de_cPedido_detalle

  public function check_pressed(){
    $id_pAduana=str_replace("pAduana_","",$this->input->post('id_pAduana'));
    $input=$this->input->post('input');
    $value=$this->input->post('value');
    $data[$input]=$value;
    
    $check=$this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pAduana);
    return $check;

  }//...check_pressed

}//...class
