<style>
  .tr-info{
    border-radius:4px;
    width:5px;
    height:5px;
  }
  .tr-info-in-process{
    color:transparent;
    border:1px solid black;
  }
  .tr-info-ok{
    color:rgba(0,255,0,.7);
  }
  .tr-info-alert{

    color:rgba(255,255,0,.7);
  }
  .tr-info-danger{

    color:rgba(255,0,0,.7);
  }
  .tr-in-process{
    background-color:transparent;
    border:1px solid black;
  }
  .tr-ok{
    background-color: rgba(0,255,0,.7)!important;
  }
  .tr-alert{
    background-color: rgba(255,255,0,.7)!important;
  }
  .tr-danger{
    background-color: rgba(255,0,0,.7)!important;
  }
  .span-invisible{
    position:absolute;
    display:none;
  }
  .icofont-checked{
      font-size:2em;
      padding-left: 2px;
  }
  .icofont-close-squared{
      font-size:2em;
      padding-left: 2px;
  }
  .td-check{
      text-align:-webkit-center;
      padding: .40rem 0rem 0rem 0rem!important;
  }
  .check-container{
      width: 35px;
      height: 35px;
      border-radius: 10%;
      background: white;
      border: 1px solid black;
      cursor: pointer;
      box-shadow: inset -4px -4px 8px rgba(255, 255, 255, 1), inset 4px 4px 8px rgba(0, 123, 255, 0.3);
  }
  .check-container:hover{
      /* background: #FAFFA8; */
      background: rgba(0, 123, 255, 0.3);
      box-shadow:none;
  }
</style>

<div class="card">
  <!--<div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Pedidos en Aduana</h3>
    </div>
    <div class="col-md-6">

       <button id="agregar_pedidosaduana" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Pedido</button> 

    </div>

  </div>-->
  <!-- /.card-header -->
  <div class="card-body">
  <?php
  echo "Tu rol: ".$rol_name=strtolower($_SESSION["infouser"]["rol_name"])
  ?>
<span id="timer"></span>
    <table id="tabla_pedidosaduana" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio</th>
        <th>Cliente</th>
        <th>Condicion pago</th>

        <th>Almacén</th>
        <th>Aduana</th>
        <th>Entregado</th>
        <th>Opciones</th>
        <th>Status</th>
        <th>Usuario del sistema</th>
        <th>Vendedor</th>
      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_pedidosaduana);*/ 



      function color_tr($chk_almacen,$chk_aduana,$chk_entregado){
          $arr=array();

          if($chk_entregado){
            $arr["class"]  ="tr-ok";
          }else{
            $arr["class"]  ="tr-in-process";

          }
          return $arr;

          /*if($hoy_bd==$dia_producto){
              $transcurrido_ms        = timeToMS($hora_bd)-timeToMS($hora_producto); //la hora actual de bd - la hora en que se pidió el producto
              $arr["transcurrido_ms"] = $transcurrido_ms;
              $arr["transcurrido"]    = msToTime($transcurrido_ms);
              
              if($chk_entregado){
                  $arr["class"]           ="tr-ok";
              }else{
                  if($transcurrido_ms<$limite_elab_ms){
                  
                      $arr["class"]           =(!$chk_aduana)?"tr-in-process":"tr-danger";
                  }
                  if($transcurrido_ms>=$limite_elab_ms && $transcurrido_ms<=$limite_entrega_ms){
                      $arr["class"]           =(!$chk_entregado)?"tr-alert":"tr-danger";
                  }
                  if($transcurrido_ms>=$limite_elab_ms && $transcurrido_ms>=$limite_entrega_ms){
                      $arr["class"]           =(!$chk_aduana)?"tr-alert":"tr-danger";
                  }
              }
          }else{
              $arr["transcurrido"]    ="ok";
              $arr["class"]           ="tr-in-process";
          } 
          $arr["class"].=" ".$chk_aduana." ".$chk_entregado;
          return $arr;*/

      }//...color_tr
      
      function make_check($input,$checked_icon,$pAduDet_id,$status_ped){
          $data_input="data-input='".$input."'";
          if($checked_icon!=""){
              $string         ="<td class='td-check'>
                              <span class='span-invisible'>1</span>
                                  <div class='check-container' data-check='true' ".$data_input." id='pAduana_".$pAduDet_id."' data-status_ped='".$status_ped."'>".$checked_icon."</div>
                              </td>";
          }else{
              $string         ="<td class='td-check'>
                                  <span class='span-invisible'>0</span>
                                  <div class='check-container' data-check='false' ".$data_input." id='pAduana_".$pAduDet_id."' data-status_ped='".$status_ped."'></div>
                              </td>";
          }
          
          return $string;
      }//...make_check
      //=======================================================================================================
      $perm_chk_almacen=["almacenista","aduana","administrador","Administrador"];//roles que pueden checkear chk_almacen
      $perm_chk_aduana=["almacenista","aduana","administrador","Administrador"];//roles que pueden checkear chk_aduana
      $perm_chk_entregado=["almacenista","aduana","administrador","Administrador"];//roles que pueden checkear chk_entregado
      //=======================================================================================================

      $checked_icon="<i class='icofont-checked'></i>";
      //$td_empty="<td><i class='icofont-close-squared'></i></td>";
      $td_empty="<td></td>";
      $td_checked_disabled    ="<td class='td-check'>
                                  <span class='span-invisible'>1</span>".$checked_icon.
                              "</td>";

      foreach ($con_pedidosaduana as $PedAduana) {

        $pAduDet_id              = $PedAduana->id;
        //$fecha_creacion             = explode(" ",$PedAduana->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $td_checked     ="<td class='td-check'>
        <div class='check-container' data-check='true' id='pAduana_".$pAduDet_id."'>".$checked_icon."</div>
            </td>";
        $td_no_checked  ="<td class='td-check'>
                <span class='span-invisible'>0</span>
                <div class='check-container' data-check='false' id='pAduana_".$pAduDet_id."'></div>
            </td>";
        
        $fecha_creacion             = $PedAduana->fecha_creacion;
        $folio_documento            = $PedAduana->folio_documento;
        $folio_unico_documento      = $PedAduana->folio_unico_documento;
        $comentario                 = $PedAduana->comentario;
        $status                     = $PedAduana->status;
        $subtotal                   = $PedAduana->subtotal;
        $IVA                        = $PedAduana->IVA;
        $IEPS                       = $PedAduana->IEPS;
        $TOTAL                      = $PedAduana->TOTAL;

        $id_agentes_venta           = $PedAduana->id_agentes_venta;
        $nombre_agentes_venta       = $PedAduana->nombre_agentes_venta;
        $descr_agentes_venta        = $PedAduana->descripcion_agentes_venta;

        $perfil_usuario             = $PedAduana->perfil_usuario;
        $descr_id_usuario           = $PedAduana->descripcion_id_usuario;

        $id_clientes                = $PedAduana->id_clientes;
        $nombre_cliente             = $PedAduana->nombre_cliente;
        $domicilio_cliente          = $PedAduana->domicilio_cliente;        
        $descr_clientes_condicion   = $PedAduana->descripcion_clientes_condicion;

        $id_clientes_condicion      = $PedAduana->id_clientes_condicion;
        $descr_clientes_condicion   = $PedAduana->descripcion_clientes_condicion;
        /* INNERS */
        $cat_estatus_nombre                = $PedAduana->cat_estatus_nombre;
        /* ...INNERS */

        /*$almacen             = $PedAduana->chk_almacen ? $td_checked_disabled : $td_no_checked ;
        $aduana              = $PedAduana->chk_aduana ? $td_checked_disabled : $td_no_checked ;
        
        $timestamp_producto = $PedAduana->fecha_creacion;             //"AAAA/MM/DD HH:MM:SS"
        $fecha_creado       = explode(" ",$timestamp_producto); //array("AAAA/MM/DD","HH:MM:SS")
        $dia_producto       = $fecha_creado[0];
        $hora_producto      = $fecha_creado[1];
        $limite_elab_ms     = timeToMS("00:".$PedAduana->producto_tiempo1);
        $limite_entrega_ms  = timeToMS("00:".$PedAduana->producto_tiempo2);

        $timestamp_bd       = $PedAduana->timestamp;              //"AAAA/MM/DD HH:MM:SS"
        $fecha_actual_bd    = explode(" ",$timestamp_bd);   //array("AAAA/MM/DD","HH:MM:SS")
        $hoy_bd             = $fecha_actual_bd[0];
        $hora_bd            = $fecha_actual_bd[1];

        $verif_producto=color_tr($hoy_bd,$hora_bd,$dia_producto,$hora_producto,$limite_elab_ms,$limite_entrega_ms,$PedAduana->chk_entregado,$PedAduana->chk_aduana);
        */
        $verif_producto =color_tr($PedAduana->chk_almacen,$PedAduana->chk_aduana,$PedAduana->chk_entregado);
        
        if($PedAduana->chk_almacen==1){

            $almacen      =(in_array($rol_name,$perm_chk_almacen))?make_check("chk_almacen",$checked_icon,$pAduDet_id,$status):$td_checked_disabled;
            $aduana       =(in_array($rol_name,$perm_chk_aduana))?make_check("chk_aduana",$checked_icon,$pAduDet_id,$status):$td_empty;
            $entregado    =(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado",$checked_icon,$pAduDet_id,$status):$td_empty; 

        }
        else{
            $almacen       =(in_array($rol_name,$perm_chk_aduana))?make_check("chk_almacen","",$pAduDet_id,$status):$td_empty;
        }//...if almacen

        if($PedAduana->chk_aduana==1){
            
            $almacen    =$td_checked_disabled;
            $aduana     =(in_array($rol_name,$perm_chk_aduana))?make_check("chk_aduana",$checked_icon,$pAduDet_id,$status):$td_checked_disabled;
            $entregado  =(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado",$checked_icon,$pAduDet_id,$status):$td_empty; 

        }
        else{
            $aduana     =(in_array($rol_name,$perm_chk_aduana))?make_check("chk_aduana","",$pAduDet_id,$status):$td_empty;
        }//...if aduana

        if($PedAduana->chk_entregado==1){
            
            $almacen=$td_checked_disabled;
            $aduana=$td_checked_disabled;
            $entregado=(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado",$checked_icon,$pAduDet_id,$status):$td_empty;
        }
        else{
            $entregado  =(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado","",$pAduDet_id,$status):$td_empty;
        }//...if entregado

        echo
        '<tr id="borrar_'.$pAduDet_id.'" class="'.$verif_producto["class"].'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$nombre_cliente.'</td>
          <td>'.$descr_clientes_condicion.'</td>

          '.$almacen.'
          '.$aduana.'
          '.$entregado.'

          <td>
            <a href="'.base_url().'index.php/pedidosaduana/pedidosaduana/ver_detalle/'.$pAduDet_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>
            <a href="'.base_url().'index.php/pedidosaduana/pedidosaduana/editar/'.$pAduDet_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>';
            if ($status==4){ 
              echo '<a href="'.base_url().'index.php/pedidosaduana/pedidosaduana/cambiar_estatus/'.$pAduDet_id.'/3" class="eliminar_relacion" flag="'.$pAduDet_id.'" id="delete'.$pAduDet_id.'">
                <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Borrar</button>';
            }
          echo '
          </td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$descr_id_usuario.'</td>
          <td>'.$nombre_agentes_venta.'</td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>

  $(document).ready(function() {


/*     setInterval(function() {
        //window.($(location).attr("href"));<span>No elaborado<span><i class="icofont-square "></i></span></span>
        location.reload();
        $("body").addClass("modal-open");
    }, 15000); */
    $('#menupedidosaduana').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_pedidosaduana").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": true
    });

    add_check_events();

    $("th").click(function(){
      $(".check-container").off();//evitar redundancia de eventlisteners
      add_check_events();
    });
    function add_check_events(){
      $(".check-container").on("click",function(){
            
        var check           =$(this);
        var id_pAduana      =check.attr("id");
        var checked         =check.data("check");
        var input           =check.data("input");
        var go              = false;
        var span_invisible  = check.parent().find( "span" );
        if(!checked){
          var input_val=1;
          var status_ped=check.data("status_ped");

          var callback1=function(){
                let iconCheck=$.parseHTML("<i class='icofont-checked'></i>");
                check.append(iconCheck);
                check.data("check",true);
                span_invisible.text("1");
            };
          go=true;
          if(input == "chk_entregado"){
            if(!enviar_a_ruta(id_pAduana.replace("pAduana_",""),status_ped)){
              callback1="";
              go=false;
            }

          }

        }

        if(checked){
          var input_val=0;
          go=true;
          var callback1=function(){
              check.empty();
              check.data("check",false);
              span_invisible.text("0");

          };

        }
        if(go){
          ajaxJson("<?php echo base_url()?>index.php/pedidosaduana/Pedidosaduana/check_pressed",
                    {
                    "id_pAduana":id_pAduana,
                    "input":input,
                    "value":input_val
                    }, 
                    "POST", 
                    "", 
                    callback1
            );
        }

      });//...$(".check-container").click
    }//...add_check_events

    $(".eliminar_relacion").click(function(event){
        event.preventDefault();
        bootbox.dialog({
        message: "Desea eliminar el registro?",
        closeButton: true,
        buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
      });

    });//document ready

    function enviar_a_ruta(id_pedido,status_ped){     
      if(val_status_bef_ruta(status_ped)){
        ajaxJson(site_url+"/rutas/Rutas/alta", 
          {
          "id_pedidosaduana":id_pedido
          }, "POST", "", function(id_nvo_pedidoaduana){
                $.get(url,{},function(result){
                  if(id_nvo_pedidoaduana!="false"){
                    ExitoCustom("Enviado");
                  }else{
                    ErrorCustom("No tienes permiso enviar pedidos a ruta");
                  }
                });
            });
      }else{
        return false;
      }

    }//...enviar_a_ruta

    function val_status_bef_ruta(id_pedido,s){
      let msg="";
      if(s == 7){//status pagado
        return true;
      }else{
          if(s == 6){//status cancelado
            msg="Este pedido se encuentra cancelado";

          }else{//status abierto
            msg="Este pedido debe pagarse antes de enviar a ruta";
          }
          ConfirmCustom(
            msg, 
            function(){window.location.assign(site_url+"/pedidosaduana/pedidosaduana/editar/"+id_pedido);},//btn_ok
            "",//btn_cancel
            "Ir a editar",
            "Cancelar"
          )
          return false;
      }
    }//...val_status_bef_ruta
</script>
