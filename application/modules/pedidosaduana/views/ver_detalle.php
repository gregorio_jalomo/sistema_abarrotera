<?php
//...datos_pedido
$id_pedido                  = $pedidosaduana->id;
$fecha_creacion             = explode(" ",$pedidosaduana->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
$fecha_entrega              = explode(" ",$pedidosaduana->fecha_entrega);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
$folio_documento            = $pedidosaduana->folio_documento;
$folio_unico_documento      = $pedidosaduana->folio_unico_documento;
$comentario                 = $pedidosaduana->comentario;
$status                     = $pedidosaduana->status;
$subtotal                   = $pedidosaduana->subtotal;
$IVA                        = $pedidosaduana->IVA;
$IEPS                       = $pedidosaduana->IEPS;
$TOTAL                      = $pedidosaduana->TOTAL;

$id_agentes_venta           = $pedidosaduana->id_agentes_venta;
$nombre_agentes_venta       = $pedidosaduana->nombre_agentes_venta;
$descr_agentes_venta        = $pedidosaduana->descripcion_agentes_venta;

$perfil_usuario             = $pedidosaduana->perfil_usuario;
$descr_id_usuario           = $pedidosaduana->descripcion_id_usuario;

$id_clientes                = $pedidosaduana->id_clientes;
$nombre_cliente             = $pedidosaduana->nombre_cliente;
$id_clientes_condicion      = $pedidosaduana->id_clientes_condicion;
$domicilio_cliente          = $pedidosaduana->domicilio_cliente;
$descr_clientes_condicion   = $pedidosaduana->descripcion_clientes_condicion;

/* INNERS */

  $cat_estatus_nombre                = $pedidosaduana->cat_estatus_nombre;
/* ...INNERS */
switch ($status) {
  case 4:
    $status_text="Abierto";
    $badge="success";
    break;
  case 5:
    $status_text="Cerrado";
    $badge="secondary";
    break;
  case 6:
    $status_text="Cancelado";
    $badge="danger";
    break;
}
//...datos_pedido
?>
<style>
.ul-costo{
  /* text-align:center; */
  padding-inline-start: 0px!important;
}
.badge{
  float:right;
}
.height-extended{
  height: 100%;
}
.div-info-cot-total{
    background-color: rgba(23,162,184,.2);
    border-radius: 5px;
}
</style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> <b>pedido:</b> "<?=$folio_documento?>" <span class="badge bg-<?=$badge?>"><?=$status_text?></span></h5>
          <div>
            <b>FUNI:</b> "<?=$folio_unico_documento?>",  
            <b>Fecha de creacion:</b> <?=$fecha_creacion[0]." ".$fecha_creacion[1]?>,  
            <b>Creado por:</b> <?=$descr_id_usuario?>
          </div>
          <span><b>Comentarios:</b> <?=$comentario?></span>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-3 mb-3">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-7 border-right">
                  <!-- title row -->
                  <div class="row height-extended">

                    <div class="col-6 border-right ">
                      <h4>
                        <small><b>Cliente:</b></small>
                      </h4>
                      <span><?=$nombre_cliente?></span>
                    </div>
                    <div class="col-6">
                      <h4>
                        <small><b>Vendedor:</b></small>
                      </h4>
                      <span><?=$nombre_agentes_venta?></span>
                    </div><!-- /.col -->
                  </div>


                </div>
                <!-- /.col-6 -->
                <!-- title row -->
                <div class="col-5">
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small><b>Método de pago:</b> <?=$descr_clientes_condicion?></small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row div-info-cot-total">

                    <div class="col-sm-3 invoice-col">
                      <ul class="ul-costo">
                        <li class="font-weight-bolder">Subtotal</li>
                        <li>$<?=$subtotal?></li>
                      </ul>
                    </div>

                    <div class="col-sm-3 invoice-col">
                      <ul class="ul-costo">
                        <li class="font-weight-bolder">IVA</li>
                        <li>$<?=$IVA?></li>
                      </ul>
                    </div>

                    <div class="col-sm-3 invoice-col">
                      <ul class="ul-costo">
                        <li class="font-weight-bolder">IEPS</li>
                        <li>$<?=$IEPS?></li>
                      </ul>
                    </div>

                    <div class="col-sm-3 invoice-col">
                      <ul class="ul-costo">
                        <li class="font-weight-bolder">TOTAL</li>
                        <li>$<?=$TOTAL?></li>
                      </ul>
                    </div>

                  </div>

                </div><!-- /.col5 -->
              </div>
            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Unidad</th>
                    <th>Cant.</th>
                    <th>Precio</th>
                    <th>IVA</th>
                    <th>IEPS</th>
                    <th>Importe</th>
                </tr>
                </thead>
                <tbody id="productos_pedido">
                <?php
                  foreach ($pedidosaduana_detalle as $producto) {
                    echo "
                    <tr>
                      <td>$producto->codigo_barras_producto</td>
                      <td>$producto->nombre_producto</td>
                      <td>$producto->descripcion_de_la_unidad</td>
                      <td>$producto->cantidad</td>
                      <td>$producto->costo_unitario</td>
                      <td>$producto->IVA%</td>
                      <td>$producto->IEPS%</td>
                      <td>$producto->costo_total</td>
                    </tr>
                    ";
                  }
                ?>
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');


  });
</script>
