<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 04elarodriguez@gmail.com
 * Daniela Rodríguez Vaca
 */
class Productos extends MX_Controller {

  /**

   **/
  public function __construct(){
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    $this->load->model('productos/Productos_m');
    date_default_timezone_set('America/Mexico_City');

    $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
    $this->id_modulo    = 1;//id_modulo en bd
    $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
    $this->opc_edt	    = array(1,2,4,5,7);
    $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

    /* 
      if(validar_permiso($this->rol_usuario,$this->id_modulo,1) || $this->is_admin ){

	      //code

      }else{
        $this->p_denied_view("agregar productos","");
      }
    */

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,1) || $this->is_admin ){

      $data['titulo_seccion'] = "Productos";
      $data['flecha_ir_atras'] = "productos/Productos/listar_productos";
      $data['pasillos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'pasillo');
      $data['areas'] = $this->Mgeneral->get_result('cat_estatus_id',1,'area');
      $data['familias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['categorias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'categoria');
      $data['subcategorias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'subcategoria');
      $data['unidad'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('productos/alta',$data,$archivos_js);

    }else{
      $this->p_denied_view("agregar productos","productos/Productos/listar_productos");
    }


  }//...alta

  public function guardar_productos(){
    $this->form_validation->set_rules('producto_nombre', 'producto_nombre', 'required');
    $this->form_validation->set_rules('pasillo_id', 'pasillo_id', 'required');
    $this->form_validation->set_rules('area_id', 'area_id', 'required');
    $this->form_validation->set_rules('familia_id', 'familia_id', 'required');
    $this->form_validation->set_rules('categoria_id', 'categoria_id', 'required');
    $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'required');
    $this->form_validation->set_rules('producto_costo_real', 'producto_costo_real', 'required');
    $this->form_validation->set_rules('producto_costo_factura', 'producto_costo_factura', 'required');
    $this->form_validation->set_rules('producto_costo_venta', 'producto_costo_venta', 'required');
    $this->form_validation->set_rules('unidad_id', 'unidad_id', 'required');
    $this->form_validation->set_rules('producto_iva', 'producto_iva', 'required');
    $this->form_validation->set_rules('producto_ieps', 'producto_ieps', 'required');
    $this->form_validation->set_rules('producto_punto_reorden', 'producto_punto_reorden', 'required');
    $this->form_validation->set_rules('producto_stock_minimo', 'producto_stock_minimo', 'required');
    $this->form_validation->set_rules('producto_stock_maximo', 'producto_stock_maximo', 'required');
    $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'required');
    $this->form_validation->set_rules('producto_id_productosat', 'producto_id_productosat', 'required');
    $this->form_validation->set_rules('producto_id_unidad_sat', 'producto_id_unidad_sat', 'required');
    $this->form_validation->set_rules('codigodebarras', 'codigodebarras', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
      $data['producto_nombre'] = $this->input->post('producto_nombre');
      $data['pasillo_id'] = $this->input->post('pasillo_id');
      $data['area_id'] = $this->input->post('area_id');
      $data['familia_id'] = $this->input->post('familia_id');
      $data['categoria_id'] = $this->input->post('categoria_id');
      $data['subcategoria_id'] = $this->input->post('subcategoria_id');
      $data['producto_costo_real'] = $this->input->post('producto_costo_real');
      $data['producto_costo_factura'] = $this->input->post('producto_costo_factura');
      $data['producto_costo_venta'] = $this->input->post('producto_costo_venta');
      $data['unidad_id'] = $this->input->post('unidad_id');
      $data['producto_iva'] = $this->input->post('producto_iva');
      $data['producto_ieps'] = $this->input->post('producto_ieps');
      $data['producto_punto_reorden'] = $this->input->post('producto_punto_reorden');
      $data['producto_stock_minimo'] = $this->input->post('producto_stock_minimo');
      $data['producto_stock_maximo'] = $this->input->post('producto_stock_maximo');
      $data['proveedor_id'] = $this->input->post('proveedor_id');
      $data['producto_id_productosat'] = $this->input->post('producto_id_productosat');
      $data['producto_id_unidad_sat'] = $this->input->post('producto_id_unidad_sat');
      //$data['codigodebarras_id'] = $this->input->post('codigodebarras_id');
      $data['check_paquete'] = $this->input->post('check_paquete');
      $data['check_habilitado'] = $this->input->post('check_habilitado');

      //...
      $data['cat_estatus_id'] = 1;
      $prod_id=$this->Mgeneral->save_register('productos', $data);

      
      $cod_barras=$this->input->post('codigodebarras');
      if(isset($cod_barras) && $cod_barras!=""){
        $data_codBarras['producto_id'] = $prod_id;
        $data_codBarras['codigobarra_codigo'] = $this->input->post('codigodebarras');
        $data_codBarras['cat_estatus_id'] = 1;
        $data_codBarras['fecha_registro'] = date("Y-m-d H:i:s");
        $this->Mgeneral->save_register('codigosbarras', $data_codBarras);
      }
 
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar_productos

  public function ver_producto($id_producto = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,4) || $this->is_admin ){

      $data['titulo_seccion'] = "Productos";
      $data['flecha_ir_atras'] = "productos/Productos/listar_productos";
      $data['con_productos'] = $this->Productos_m->get_producto($id_producto);
      $data['pasillos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'pasillo');
      $data['areas'] = $this->Mgeneral->get_result('cat_estatus_id',1,'area');
      $data['familias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['categorias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'categoria');
      $data['subcategorias'] = $this->Mgeneral->get_result('cat_estatus_id',1,'subcategoria');
      $data['unidad'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');
  
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );
  
      $this->cargar_vista('productos/editar',$data,$archivos_js);

    }else{
      $this->p_denied_view("editar productos","productos/Productos/listar_productos");
    }

  }//..ver_producto

  public function ver_detalle($producto_id=false){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,3) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de Producto";
      $data['flecha_ir_atras'] = "productos/Productos/listar_productos";
      $data['productos'] = $this->Mgeneral->get_row("producto_id",$producto_id,'productos');
      $data['pasillos'] = $this->Mgeneral->get_row(false,false,'pasillo');
      $data['areas'] = $this->Mgeneral->get_row(false,false,'area');
      $data['familias'] = $this->Mgeneral->get_row(false,false,'familia');
      $data['categorias'] = $this->Mgeneral->get_row(false,false,'categoria');
      $data['subcategorias'] = $this->Mgeneral->get_row(false,false,'subcategoria');
      $data['unidad'] = $this->Mgeneral->get_row(false,false,'unidad');
      $data['proveedores'] = $this->Mgeneral->get_result(false,false,'proveedores');
  
      $data['estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');
      $data['tiposdeprecio'] = $this->Mgeneral->get_result('cat_estatus_id',1,'tipodeprecio');
  
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );
  
      $this->cargar_vista('productos/ver_detalle',$data,$archivos_js);

    }else{
      $this->p_denied_view("ver detalle de productos","productos/Productos/listar_productos");
    }

  }//...ver_detalle

  public function editar_producto(){
    $this->form_validation->set_rules('producto_nombre', 'producto_nombre', 'required');
    $this->form_validation->set_rules('pasillo_id', 'pasillo_id', 'required');
    $this->form_validation->set_rules('area_id', 'area_id', 'required');
    $this->form_validation->set_rules('familia_id', 'familia_id', 'required');
    $this->form_validation->set_rules('categoria_id', 'categoria_id', 'required');
    $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'required');
    $this->form_validation->set_rules('producto_costo_real', 'producto_costo_real', 'required');
    $this->form_validation->set_rules('producto_costo_factura', 'producto_costo_factura', 'required');
    $this->form_validation->set_rules('producto_costo_venta', 'producto_costo_venta', 'required');
    $this->form_validation->set_rules('unidad_id', 'unidad_id', 'required');
    $this->form_validation->set_rules('producto_iva', 'producto_iva', 'required');
    $this->form_validation->set_rules('producto_ieps', 'producto_ieps', 'required');
    $this->form_validation->set_rules('producto_punto_reorden', 'producto_punto_reorden', 'required');
    $this->form_validation->set_rules('producto_stock_minimo', 'producto_stock_minimo', 'required');
    $this->form_validation->set_rules('producto_stock_maximo', 'producto_stock_maximo', 'required');
    $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'required');
    $this->form_validation->set_rules('producto_id_productosat', 'producto_id_productosat', 'required');
    $this->form_validation->set_rules('producto_id_unidad_sat', 'producto_id_unidad_sat', 'required');
    $this->form_validation->set_rules('codigodebarras', 'codigodebarras', 'required');
    //.... los demás campos
    $response = validate($this);
    if($response['status']){
    // var_dump($this->input->get());
      $data['producto_id'] = $this->input->post('producto_id');
      $data['producto_nombre'] = $this->input->post('producto_nombre');
      $data['pasillo_id'] = $this->input->post('pasillo_id');
      $data['area_id'] = $this->input->post('area_id');
      $data['familia_id'] = $this->input->post('familia_id');
      $data['categoria_id'] = $this->input->post('categoria_id');
      $data['subcategoria_id'] = $this->input->post('subcategoria_id');
      $data['producto_costo_real'] = $this->input->post('producto_costo_real');
      $data['producto_costo_factura'] = $this->input->post('producto_costo_factura');
      $data['producto_costo_venta'] = $this->input->post('producto_costo_venta');
      $data['unidad_id'] = $this->input->post('unidad_id');
      $data['producto_iva'] = $this->input->post('producto_iva');
      $data['producto_ieps'] = $this->input->post('producto_ieps');
      $data['producto_punto_reorden'] = $this->input->post('producto_punto_reorden');
      $data['producto_stock_minimo'] = $this->input->post('producto_stock_minimo');
      $data['producto_stock_maximo'] = $this->input->post('producto_stock_maximo');
      $data['proveedor_id'] = $this->input->post('proveedor_id');
      $data['producto_id_productosat'] = $this->input->post('producto_id_productosat');
      $data['producto_id_unidad_sat'] = $this->input->post('producto_id_unidad_sat');
      //$data['codigodebarras_id'] = $this->input->post('codigodebarras_id');
      $data['check_paquete'] = $this->input->post('check_paquete');
      $data['check_habilitado'] = $this->input->post('check_habilitado');

      //...
      $data['cat_estatus_id'] = 1;
      $this->Mgeneral->update_table_row('productos',$data,'producto_id',$data['producto_id']);

      $data_codBarra["codigobarra_codigo"]=$this->input->post('codigodebarras');
      $this->Mgeneral->update_table_row('codigosbarras',$data_codBarra,'producto_id',$data['producto_id']);

    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...editar_producto

  public function listar_productos($pagina_inicio = 1, $pagina_fin = 10){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,2) || $this->is_admin ){

      $data['titulo_seccion'] = "Productos";
      $data['con_productos'] = $this->Productos_m->get_productos_by_status(1);

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('productos/listar',$data,$archivos_js);

    }else{
      $this->p_denied_view("listar productos","");
    }

  }

  /*
  * funcion para cambiar el estatus del producto
  */
  public function cambiar_estatus_producto($producto_id, $estatus_id) {

		$data['cat_estatus_id'] = $estatus_id;

    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,4) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,5) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['producto_id'] = $producto_id;
      $this->Mgeneral->update_table_row('productos',$data,'producto_id',$data['producto_id']); 

    }
    echo json_encode($go);

  }//...cambiar_estatus_producto

}
