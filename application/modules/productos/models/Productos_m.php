<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_m extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get_producto($id_producto){
    return $this->db->select("productos.*,codigosbarras.codigobarra_codigo")
                    ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
                    ->where("productos.producto_id",$id_producto)
                    ->get("productos")->row();
  }//...get_producto
  
  public function get_productos_by_status($status){
    $result= $this->db->select("productos.*,codigosbarras.codigobarra_codigo")
                    ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
                    ->where("productos.cat_estatus_id",$status)
                    ->get("productos")->result();
   
    return $result;
  }//...get_productos_by_status
}