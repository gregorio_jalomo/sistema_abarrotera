<?php
  $producto_id                = $con_productos->producto_id;
  $producto_nombre            = $con_productos->producto_nombre;
  $pasillo_id                 = $con_productos->pasillo_id;
  $area_id                    = $con_productos->area_id;
  $familia_id                 = $con_productos->familia_id;
  $categoria_id               = $con_productos->categoria_id;
  $subcategoria_id            = $con_productos->subcategoria_id;
  $producto_costo_real        = $con_productos->producto_costo_real;
  $producto_costo_factura     = $con_productos->producto_costo_factura;
  $producto_costo_venta       = $con_productos->producto_costo_venta;
  $unidad_id                  = $con_productos->unidad_id;
  $producto_iva               = $con_productos->producto_iva;
  $producto_ieps              = $con_productos->producto_ieps;
  $producto_punto_reorden     = $con_productos->producto_punto_reorden;
  $producto_stock_minimo      = $con_productos->producto_stock_minimo;
  $producto_stock_maximo      = $con_productos->producto_stock_maximo;
  $proveedor_id               = $con_productos->proveedor_id;
  $producto_id_productosat    = $con_productos->producto_id_productosat;
  $producto_id_unidad_sat     = $con_productos->producto_id_unidad_sat;
  $codigodebarras             = $con_productos->codigobarra_codigo;

  $producto_existencias       = $con_productos->producto_existencias;
  $comprometidos_pedido       = $con_productos->producto_existencias_comprometidas_pedido;
  $comprometidos_compras      = $con_productos->producto_existencias_comprometidas_compras;

  // $date = str_replace('/', '-', $producto_fecha_pedido);
  // $productoFechaPedido = date('Y-m-d', strtotime($date));

?>
<script>
 //menu_activo = "proveedores";
//$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
  $('#ver_producto').submit(function(event){
    event.preventDefault();
    $("#enviar").hide();
    $("#cargando").show();
    console.log($('#check_paquete').is(':checked'));
    var url ="<?php echo base_url()?>index.php/productos/Productos/editar_producto";
    ajaxJson(url,{"producto_id" : '<?php echo $producto_id; ?>',
                  "producto_nombre" : $('#producto_nombre').val(),
                  "pasillo_id" : $('#pasillo_id').val(),
                  "area_id" : $('#area_id').val(),
                  "familia_id" : $('#familia_id').val(),
                  "categoria_id" : $('#categoria_id').val(),
                  "subcategoria_id" : $('#subcategoria_id').val(),
                  "producto_costo_real" : $('#producto_costo_real').val(),
                  "producto_costo_factura" : $('#producto_costo_factura').val(),
                  "producto_costo_venta" : $('#producto_costo_venta').val(),
                  "unidad_id" : $('#unidad_id').val(),
                  "producto_iva" : $('#producto_iva').val(),
                  "producto_ieps" : $('#producto_ieps').val(),
                  "producto_punto_reorden" : $('#producto_punto_reorden').val(),
                  "producto_stock_minimo" : $('#producto_stock_minimo').val(),
                  "producto_stock_maximo" : $('#producto_stock_maximo').val(),
                  "proveedor_id" : $('#proveedor_id').val(),
                  "producto_id_productosat" : $('#producto_id_productosat').val(),
                  "producto_id_unidad_sat" : $('#producto_id_unidad_sat').val(),
                  "codigodebarras" : $('#codigodebarras').val(),
                  "check_paquete" : $('#check_paquete').is(':checked') == true ? 1 : 0,
                  "check_habilitado" : $('#check_habilitado').is(':checked') == true ? 1 : 0

                  },
              "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/productos/Productos/listar_productos");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Producto</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="ver_producto">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-6">
                  <label for="">Nombre</label>
                  <input type="text" class="form-control" name="producto_nombre" id="producto_nombre" value="<?php echo $producto_nombre; ?>">
                </div>
                <div class="col-md-3">
                  <label for="">Pasillo</label>
                  <select class="form-control select2" name="pasillo_id" id="pasillo_id">
                    <option selected disabled>- Pasillo -</option>
                    <?php
                      foreach ($pasillos as $row) {
                        echo 
                          '<option value="'.$row->pasillo_id.'">'.$row->pasillo_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <label for="">Área</label>
                  <select class="form-control select2" name="area_id" id="area_id">
                    <option selected disabled>- Área -</option>
                    <?php
                      foreach ($areas as $row) {
                        echo 
                          '<option value="'.$row->area_id.'">'.$row->area_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Familia</label>
                  <select class="form-control select2" name="familia_id" id="familia_id">
                    <option selected disabled>- Familia -</option>
                    <?php
                      foreach ($familias as $row) {
                        echo 
                          '<option value="'.$row->familia_id.'">'.$row->familia_tipo.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="">Categoría</label>
                  <select class="form-control select2" name="categoria_id" id="categoria_id">
                    <option selected disabled>- Categoría -</option>
                    <?php
                      foreach ($categorias as $row) {
                        echo 
                          '<option value="'.$row->categoria_id.'">'.$row->categoria_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="">Subcategoría</label>
                  <select class="form-control select2" name="subcategoria_id" id="subcategoria_id">
                    <option selected disabled>- Subcategoría -</option>
                    <?php
                      foreach ($subcategorias as $row) {
                        echo 
                          '<option value="'.$row->subcategoria_id.'">'.$row->subcategoria_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Costo real</label>
                  <input type="text" class="form-control" name="producto_costo_real" id="producto_costo_real" value="<?php echo $producto_costo_real; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Costo de factura</label>
                  <input type="text" class="form-control" name="producto_costo_factura" id="producto_costo_factura" value="<?php echo $producto_costo_factura; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Precio de venta</label>
                  <input type="text" class="form-control" name="producto_costo_venta" id="producto_costo_venta" value="<?php echo $producto_costo_venta; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Existencias reales</label>
                  <input type="text" class="form-control" readonly value="<?php echo $producto_existencias; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Existencias comprometidas en pedidos</label>
                  <input type="text" class="form-control" readonly value="<?php echo $comprometidos_pedido; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Existencias comprometidas en compras</label>
                  <input type="text" class="form-control" readonly value="<?php echo $comprometidos_compras; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-2">
                  <label for="">Unidad</label>
                  <select class="form-control select2" name="unidad_id" id="unidad_id">
                    <option selected disabled>- Unidad -</option>
                    <?php
                      foreach ($unidad as $unidad) {
                        echo 
                          '<option value="'.$unidad->unidad_id.'">'.$unidad->unidad_tipo.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <label for="">IVA</label>
                  <input type="number" class="form-control" name="producto_iva" id="producto_iva" value="<?php echo $producto_iva; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">IEPS</label>
                  <input type="number" class="form-control" name="producto_ieps" id="producto_ieps" value="<?php echo $producto_ieps; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Punto reorden</label>
                  <input type="number" class="form-control" name="producto_punto_reorden" id="producto_punto_reorden" value="<?php echo $producto_punto_reorden; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Stock mínimo</label>
                  <input type="number" class="form-control" name="producto_stock_minimo" id="producto_stock_minimo" value="<?php echo $producto_stock_minimo; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Stock máximo</label>
                  <input type="number" class="form-control" name="producto_stock_maximo" id="producto_stock_maximo" value="<?php echo $producto_stock_maximo; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-6">
                  <label for="">Último Proveedor</label>
                  <select class="form-control select2" name="proveedor_id" id="proveedor_id">
                    <option selected disabled>- Proveedor -</option>
                    <?php 
                      foreach ($proveedores as $proveedor) {
                        /* $selected="";
                        if($proveedor_id==$proveedor->proveedor_id){
                          $selected="selected='selected'";
                        } */
                        echo '<option value="'.$proveedor->proveedor_id.'">'.$proveedor->proveedor_nombre.'</option>';
                      }
                    
                    ?>
                    
                  </select>
                </div>
                <div class="col-md-3">
                  <label for="">Producto SAT</label>
                  <input type="text" class="form-control" name="producto_id_productosat" id="producto_id_productosat" value="<?php echo $producto_id_productosat; ?>">
                </div>
                <div class="col-md-3">
                  <label for="">Unidad SAT</label>
                  <input type="text" class="form-control" name="producto_id_unidad_sat" id="producto_id_unidad_sat" value="<?php echo $producto_id_unidad_sat; ?>">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-6">
                  <label for="">Código de barras</label>
                  <input type="text" class="form-control" name="codigodebarras" id="codigodebarras" value="<?php echo $codigodebarras; ?>">
                </div>
                <div class="col-md-2 form-group clearfix mt-3">
                  <div class="icheck-info d-inline">
                    <input type="checkbox" class="mt-4" id="check_paquete">
                    <label for="check_paquete" class="mt-4 font-weight-bold">¿Es paquete?</label>
                  </div>
                  <div class="icheck-info d-inline">
                    <input type="checkbox" class="mt-4" id="check_habilitado">
                    <label for="check_habilitado" class="mt-4 font-weight-bold">¿Habilitado?</label>
                  </div>
                </div>
              </div>
              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Actualizar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/productos/Productos/listar_productos'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>  
              <hr />
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Tipos de precio</h3>

                      <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control float-right" placeholder="Buscar...">

                          <div class="input-group-append">
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0 table-productos-precios">
                      <table class="table table-head-fixed text-nowrap">
                        <thead>
                          <tr>
                            <th>Descripción</th>
                            <th>Porcentaje %</th>
                            <th>Total $</th>
                          </tr>
                        </thead>
                        <tbody id="tipos-de-precio">
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
              </div>
              
            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

<script>

  $(document).ready(function() {
    $('#menuProductos').addClass('active-link');
    
    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
      aTiposDePrecio.forEach(element => {
        nTipoDePrecio++;
        tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
        tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
        tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
      });
    $('#tipos-de-precio').html(tbody);
    //...

    let precioNormal = $('#producto_costo_venta').val();

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(precioNormal) + (precioNormal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });

    $('#pasillo_id').val('<?= $pasillo_id; ?>');
    $('#area_id').val('<?= $area_id; ?>');
    $('#familia_id').val('<?= $familia_id; ?>');
    $('#categoria_id').val('<?= $categoria_id; ?>');
    $('#subcategoria_id').val('<?= $subcategoria_id; ?>');
    $('#unidad_id').val('<?= $unidad_id; ?>');
    $('#proveedor_id').val('<?= $proveedor_id; ?>');
    let check_paquete = '<?php echo $con_productos->check_paquete;?>';
    let check_habilitado = '<?php echo $con_productos->check_habilitado;?>';

    console.log(check_paquete);
    
    if (check_paquete === '1') {
      $('#check_paquete').prop('checked', true);
    } 
    if (check_habilitado === '1') {
      $('#check_habilitado').prop('checked', true);
    } 
  });

  $('#producto_costo_venta').on('input',function(){
    let valuePrecioNormal = $(this).val();

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valuePrecioNormal) + (valuePrecioNormal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });

  });
  
</script>
