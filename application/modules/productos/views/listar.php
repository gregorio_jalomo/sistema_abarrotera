<script>
$(document).ready(function(){
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para eliminar esto");
                                }
                              });
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Productos registrados</h3>
    </div>
    <div class="col-md-6">
      <a href="<?php echo base_url().'index.php/productos/Productos/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Producto</button></a>
    </div>
  
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Nombre</th>
        <!-- <th>Pasillo</th>
        <th>Área</th>
        <th>Familia</th>
        <th>Categoría</th>
        <th>Subcategoría</th>
        <th>Unidad</th>
        <th>IVA</th>
        <th>IEPS</th>
        <th>Punto de reorden</th>
        <th>Stock mín.</th>
        <th>Stock máx.</th>
        <th>Tipo de precio</th>
        <th>Proveedor</th>
        <th>Producto SAT</th>
        <th>Unidad SAT</th> -->
        <th>Precio normal</th>
        <th>C.B.</th>
        <th>Existencias</th>
        <th>Comprom. pedidos</th>
        <th>Comprom. compras</th>
        <th>Opciones</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($con_productos as $productos) {
        $producto_id              = $productos->producto_id;
        $producto_nombre          = $productos->producto_nombre;
        $pasillo_id               = $productos->pasillo_id;
        $area_id                  = $productos->area_id;
        $familia_id               = $productos->familia_id;
        $categoria_id             = $productos->categoria_id;
        $subcategoria_id          = $productos->subcategoria_id;
        $producto_costo_real      = $productos->producto_costo_real;
        $producto_costo_factura   = $productos->producto_costo_factura;
        $producto_costo_venta     = $productos->producto_costo_venta;
        $unidad_id                = $productos->unidad_id;
        $producto_iva             = $productos->producto_iva;
        $producto_ieps            = $productos->producto_ieps;
        $producto_punto_reorden   = $productos->producto_punto_reorden;
        $producto_stock_minimo    = $productos->producto_stock_minimo;
        $producto_stock_maximo    = $productos->producto_stock_maximo;
        $proveedor_id             = $productos->proveedor_id;
        $producto_id_productosat  = $productos->producto_id_productosat;
        $producto_id_unidad_sat   = $productos->producto_id_unidad_sat;
        $codigodebarras           = $productos->codigobarra_codigo;
        $cat_estatus_id           = $productos->cat_estatus_id;

        $producto_existencias     = $productos->producto_existencias;
        $comprometidos_pedido     = $productos->producto_existencias_comprometidas_pedido;
        $comprometidos_compras    = $productos->producto_existencias_comprometidas_compras;
        echo
        '<tr id="borrar_'.$producto_id.'">
          <td>'.$producto_nombre.'</td>
          <td>'.$producto_costo_real.'</td>
          <td>'.$codigodebarras.'</td>
          <td>'.$producto_existencias.'</td>
          <td>'.$comprometidos_pedido.'</td>
          <td>'.$comprometidos_compras.'</td>

          <td>
            <a href="'.base_url().'index.php/productos/Productos/ver_detalle/'.$producto_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
            <a href="'.base_url().'index.php/productos/Productos/ver_producto/'.$producto_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
            <a href="'.base_url().'index.php/productos/Productos/cambiar_estatus_producto/'.$producto_id.'/3" class="btn btn-danger btn-sm eliminar_relacion" flag="'.$producto_id.'" id="delete'.$producto_id.'"><i class="fas fa-trash-alt"></i> Eliminar</a>
          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>
<script>
  $(document).ready(function() {

    $('#menuProductos').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  });
  
  function estatusProducto(producto_id, cat_estatus_id) {
    $.post('<?php echo base_url()."index.php/productos/cambiar_estatus_producto"; ?>',{
      producto_id : producto_id,
      cat_estatus_id : cat_estatus_id
      }, function(data){}
    );
  }
</script>