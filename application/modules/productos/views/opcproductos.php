<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Opciones Productos</h1>
        </div><!-- /.col -->

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content" >
    <div class="container-fluid" >
      <!-- Info boxes -->
      <div class="row" >
        <div class="col-12 col-sm-6 col-md-3" >
          <a href="<?php echo base_url()?>index.php/productos/alta">
            <div class="info-box" >
              <span class="info-box-icon bg-info elevation-1">
              <img  src="<?php echo base_url()?>statics/tema/dist/img/productos.png"></span>
              <div class="info-box-content">
                <span class="info-box-text">Nuevo Producto</span>
                  <!--span class="info-box-number">
                    10
                    <small></small>
                  </span-->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </a>
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
          <a href="<?php echo base_url()?>index.php/productos/listar_productos">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1">
                <img src="<?php echo base_url()?>statics/tema/dist/img/lista2.jpg">
              </span>
              <div class="info-box-content">
                <span class="info-box-tet">Lista de productos</span>
                <!--span class="info-box-number">41,410</span-->
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->

        




        </div>
        <!-- /.col -->


        <!-- /.col -->





      </div>
      <!-- /.row -->
