<?php
  $producto_id = $productos->producto_id;
  $producto_nombre = $productos->producto_nombre;
  $pasillo_id= $pasillos->pasillo_id;
  $pasillo_descripcion = $this->Mgeneral->get_row('pasillo_id',$productos->pasillo_id,'pasillo')->pasillo_descripcion;
  $area_descripcion = $this->Mgeneral->get_row('area_id',$productos->area_id,'area')->area_descripcion;
  $unidad_tipo = $this->Mgeneral->get_row('unidad_id',$productos->unidad_id,'unidad')->unidad_tipo;
  $categoria_descripcion = $this->Mgeneral->get_row('categoria_id',$productos->categoria_id,'categoria')->categoria_descripcion;
  $subcategoria_descripcion = $this->Mgeneral->get_row('subcategoria_id',$productos->subcategoria_id,'subcategoria')->subcategoria_descripcion;
  $familia_tipo = $this->Mgeneral->get_row('familia_id',$productos->familia_id,'familia')->familia_tipo;
  $producto_costo_real = $productos->producto_costo_real;
  $producto_costo_factura = $productos->producto_costo_factura;
  $producto_costo_venta = $productos->producto_costo_venta;
  $producto_iva = $productos->producto_iva;
  $producto_ieps = $productos->producto_ieps;
  $producto_stock_maximo = $productos->producto_stock_maximo;
  $producto_stock_minimo = $productos->producto_stock_minimo;
  $producto_punto_reorden = $productos->producto_punto_reorden;

  foreach($proveedores AS $proveedor){
    if($productos->proveedor_id === $proveedor->proveedor_id){
      $nombre_proveedor=$proveedor->proveedor_nombre;
    }
  }
  $proveedor->proveedor_nombre = $nombre_proveedor;
  $producto_id_productosat = $productos->producto_id_productosat;
  $producto_id_unidad_sat = $productos->producto_id_unidad_sat;
  $producto_nombre = $productos->producto_nombre;
  $cat_estatus_nombre = ($productos->cat_estatus_id === $estatus->cat_estatus_id) ? $estatus->cat_estatus_nombre : '';
  $codigodebarras_id = $productos->codigodebarras_id;
  // var_dump($productos);

?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> Producto: <?php echo $producto_nombre; ?></h5>
          ID # <?php echo $producto_id; ?>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-3 mb-3">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-7 border-right">
                  <!-- title row -->
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Producto</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">
                    <div class="col-sm-6 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Nombre</li>
                        <li><?php echo $producto_nombre; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Pasillo</li>
                        <li><?php echo $pasillo_descripcion; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Área</li>
                        <li><?php echo $area_descripcion; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Familia</li>
                        <li><?php echo $familia_tipo; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Categoría</li>
                        <li><?php echo $categoria_descripcion; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4">
                      <ul>
                        <li class="font-weight-bolder">Subcategoría</li>
                        <li><?php echo $subcategoria_descripcion; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Costo Real</li>
                        <li><?php echo $producto_costo_real; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Costo de Factura</li>
                        <li><?php echo $producto_costo_factura; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Precio de Venta</li>
                        <li><?php echo $producto_costo_venta; ?></li>
                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Unidad</li>
                        <li><?php echo $unidad_tipo; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4">
                      <ul>
                        <li class="font-weight-bolder">Último proveedor</li>
                        <li><?php echo $proveedor->proveedor_nombre; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">
                    <div class="col-sm-9 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Código de barras</li>
                        <li><?php echo $codigodebarras_id; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Estatus</li>
                        <li><span class="badge bg-success"><?php echo $cat_estatus_nombre; ?></span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- /.col-6 -->
                <!-- title row -->
                <div class="col-5">
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Impuestos</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">
                    <div class="col-sm-6 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">IVA</li>
                        <li><?php echo $producto_iva; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">IEPS</li>
                        <li><?php echo $producto_ieps; ?></li>
                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Producto SAT</li>
                        <li><?php echo $producto_id_productosat; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Unidad SAT</li>
                        <li><?php echo $producto_id_unidad_sat; ?></li>
                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Punto de reorden</li>
                        <li><?php echo $producto_punto_reorden; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Stock mínimo</li>
                        <li><?php echo $producto_stock_minimo; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4">
                      <ul>
                        <li class="font-weight-bolder">Stock máximo</li>
                        <li><?php echo $producto_stock_maximo; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                </div>
                <!-- /.col-6 -->
              </div>
            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Descripción</th>
                  <th>Porcentaje %</th>
                  <th>Total $</th>
                </tr>
                </thead>
                <tbody id="tipos-de-precio">
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {
    $('#menuProductos').addClass('active-link');

    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
    $('#tipos-de-precio').html(tbody);

    const valueCostoReal = '<?php echo $producto_costo_real; ?>';

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });

  });
</script>
