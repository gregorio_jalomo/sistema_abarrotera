<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Promociones extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproductos', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 3;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,11) || $this->is_admin ){

      $data['titulo_seccion'] = "Promociones";
      $data['flecha_ir_atras'] = "promociones/listar_promociones";
      $data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['metodopago'] = $this->Mgeneral->get_result('cat_estatus_id',1,'metodopago');
      $data['promociones'] = $this->Mgeneral->get_result('cat_estatus_id',1,'promociones');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('promociones/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar promociones","");
    }

  }//...alta

  public function guardar(){
    $this->form_validation->set_rules('promocion_nombre', 'promocion_nombre', 'required');
    $this->form_validation->set_rules('promocion_fechainicio', 'fecha inicio', 'required');
    $this->form_validation->set_rules('promocion_fechafinal', 'promocion_fechafinal', 'required');
    $this->form_validation->set_rules('promocion_porcentaje', 'promocion_porcentaje', 'required');
    $this->form_validation->set_rules('promocion_cantidad', 'promocion_cantidad', 'required');
    $this->form_validation->set_rules('metodopago_id', 'metodopago_id', 'required');
    $this->form_validation->set_rules('producto_id', 'producto_id', 'required');

    $response = validate($this);
    if($response['status']){
      $data['promocion_nombre'] = $this->input->post('promocion_nombre');
      $data['promocion_fechainicio'] = $this->input->post('promocion_fechainicio');
      $data['promocion_fechafinal'] = $this->input->post('promocion_fechafinal');
      $data['promocion_porcentaje'] = $this->input->post('promocion_porcentaje');
      $data['promocion_cantidad'] = $this->input->post('promocion_cantidad');
      $data['metodopago_id'] = $this->input->post('metodopago_id');
      $data['producto_id'] = $this->input->post('producto_id');
      $data["fecha_registro"]         = date("Y-m-d H:i:s");

      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->save_register('promociones', $data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }//...guardar

  public function ver_detalle($promocion_id=false){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,13) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de las Promociones";
      $data['flecha_ir_atras'] = "promociones/promociones/listar_promociones";
      $data['promociones'] = $this->Mgeneral->get_row("promocion_id",$promocion_id,'promociones');
      $data['productos'] = $this->Mgeneral->get_row(false,false,'productos');
      $data['metodopago'] = $this->Mgeneral->get_row(false,false,'metodopago');
      $data['metodopago'] = $this->Mgeneral->get_row(false,false,'metodopago');
      $data['estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('promociones/ver_detalle',$data,$archivos_js);      
    
    }else{
      $this->p_denied_view("ver detalle de promociones","promociones/promociones/listar_promociones");
    }

  }//...ver_detalle


  public function listar_promociones($pagina_inicio = 1, $pagina_fin = 10){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,12) || $this->is_admin ){

      $data['titulo_seccion'] = "Promociones";
      $data['con_promociones'] = $this->Mgeneral->get_result('cat_estatus_id',1, 'promociones');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('promociones/listar',$data,$archivos_js);      
    
    }else{
      $this->p_denied_view("listar promociones","");
    }

  }

  public function editar($id_promocion = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,14) || $this->is_admin ){

      $data['titulo_seccion'] = "Promociones";
      $data['flecha_ir_atras'] = "promociones/listar_promociones";
      $data['metodopago'] = $this->Mgeneral->get_result('cat_estatus_id',1,'metodopago');
      $data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $get_row = $this->Mgeneral->get_row('promocion_id',$id_promocion,'promociones');
      $data['promociones'] = $get_row;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('promociones/editar',$data,$archivos_js);      
    
    }else{
      $this->p_denied_view("editar promociones","promociones/listar_promociones");
    }

  }//...editar

  public function actualizar(){
    $this->form_validation->set_rules('promocion_nombre', 'promocion_nombre', 'required');
    $this->form_validation->set_rules('promocion_fechainicio', 'promocion_fechainicio', 'required');
    $this->form_validation->set_rules('promocion_fechafinal', 'promocion_fechafinal', 'required');
    $this->form_validation->set_rules('promocion_porcentaje', 'promocion_porcentaje', 'required');
    $this->form_validation->set_rules('promocion_cantidad', 'promocion_cantidad', 'required');
    $this->form_validation->set_rules('producto_id', 'producto_id', 'required');
    $this->form_validation->set_rules('metodopago_id', 'metodopago_id', 'required');

    $response = validate($this);
    if($response['status']){
      $data['promocion_id'] = $this->input->post('promocion_id');
      $data['promocion_nombre'] = $this->input->post('promocion_nombre');
      $data['promocion_fechainicio'] = $this->input->post('promocion_fechainicio');
      $data['promocion_fechafinal'] = $this->input->post('promocion_fechafinal');
      $data['promocion_porcentaje'] = $this->input->post('promocion_porcentaje');
      $data['promocion_cantidad'] = $this->input->post('promocion_cantidad');
      $data['metodopago_id'] = $this->input->post('metodopago_id');
      $data['producto_id'] = $this->input->post('producto_id');

      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->update_table_row('promociones',$data,'promocion_id',$data['promocion_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar
  public function cambiar_estatus($promocion_id, $estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,14) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,15) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['promocion_id'] = $promocion_id;
      $this->Mgeneral->update_table_row('promociones',$data,'promocion_id',$data['promocion_id']);

    }
    echo json_encode($go);

  }//...cambiar_estatus
}
