<?php
  $promocion_id = $promociones->promocion_id;
  $promocion_nombre = $promociones->promocion_nombre;
  $promocion_fechainicio = $promociones->promocion_fechainicio;
  $promocion_fechafinal = $promociones->promocion_fechafinal;
  $promocion_porcentaje = $promociones->promocion_porcentaje;
  $promocion_cantidad = $promociones->promocion_cantidad;
  $producto_id = $promociones->producto_id;
  $metodopago_id = $promociones->metodopago_id;

?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_promocion').submit(function(event){
    event.preventDefault();
    console.log($('#promocion_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/promociones/actualizar";
    ajaxJson(url,{
        'promocion_id' : '<?php echo $promocion_id; ?>',
        'promocion_nombre' : $('#promocion_nombre').val(),
        'promocion_fechainicio' : $('#promocion_fechainicio').val(),
        'promocion_fechafinal' : $('#promocion_fechafinal').val(),
        'promocion_porcentaje' : $('#promocion_porcentaje').val(),
        'promocion_cantidad' : $('#promocion_cantidad').val(),

        'producto_id' : $('#producto_id').val(),
        'metodopago_id' : $('#metodopago_id').val(),

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/promociones/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Promoción</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_promocion">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Nombre</label>
                  <input type="text" class="form-control" name="promocion_nombre" id="promocion_nombre" value="<?php echo $promocion_nombre; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Fecha Inicio</label>
                  <input type="datetime-local" class="form-control" name="promocion_fechainicio" id="promocion_fechainicio" value="<?php echo $promocion_fechainicio; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Fecha Final</label>
                  <input type="datetime-local" class="form-control" name="promocion_fechafinal" id="promocion_fechafinal" value="<?php echo $promocion_fechafinal; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-3">
                  <label for="">Producto</label>
                  <select class="form-control select2" name="producto_id" id="producto_id">
                    <option selected disabled>- Galletas -</option>

                    <?php
                      foreach ($productos as $row) {
                        echo
                          '<option value="'.$row->producto_id.'">'.$row->producto_nombre.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>


                <div class="col-md-3">
                  <label for="">Cantidad</label>
                  <input type="number" class="form-control" name="promocion_cantidad" id="promocion_cantidad" value="<?php echo $promocion_cantidad; ?>">
                </div>

                <div class="col-md-3">
                  <label for="">Porcentaje</label>
                  <input type="number" class="form-control" name="promocion_porcentaje" id="promocion_porcentaje" value="<?php echo $promocion_porcentaje; ?>">
                </div>

                <div class="col-md-3">
                  <label for="">Método Pago</label>
                  <select class="form-control select2" name="metodopago_id" id="metodopago_id">
                    <option selected disabled>- Contado -</option>

                    <?php
                      foreach ($metodopago as $row) {
                        echo
                          '<option value="'.$row->metodopago_id.'">'.$row->metodopago_descripcion.'</option>
                        ';
                      }
                    ?>
                  </select>
                </div>

              </div>



              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/promociones/alta'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>


            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuPromociones').addClass('active-link');
    $('#metodopago_id').val('<?= $metodopago_id; ?>');
    $('#metodopago_id>option:selected').text();
  });
</script>
<script>
  $('document').ready(function() {
    $('#menupromociones').addClass('active-link');
    $('#producto_id').val('<?= $producto_id; ?>');
    $('#producto_id>option:selected').text();
  });
</script>
