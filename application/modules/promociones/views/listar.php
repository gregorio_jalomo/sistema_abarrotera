<script>
$(document).ready(function(){
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');
                            $.get(url,{},function(result){
                              if(result=="true"){
                                $("#borrar_"+id).slideUp();
                                ExitoCustom("Eliminado");
                              }else{
                                ErrorCustom("No tienes permiso para eliminar esto");
                              }
                            });
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">promociones registrados</h3>
    </div>
    <div class="col-md-6">
      <a href="<?php echo base_url().'index.php/promociones/promociones/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar promocion</button></a>
    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Nombre Comecial</th>
        <!-- <th>Pasillo</th>
        <th>Área</th>
        <th>Familia</th>
        <th>Categoría</th>
        <th>Subcategoría</th>
        <th>Unidad</th>
        <th>IVA</th>
        <th>IEPS</th>
        <th>Punto de reorden</th>
        <th>Stock mín.</th>
        <th>Stock máx.</th>
        <th>Tipo de precio</th>
        <th>promocion</th>
        <th>promocion SAT</th>
        <th>Unidad SAT</th> -->
        <th>Producto</th>
        <th>Cantidad</th>
        <th>Porcentaje</th>
        <th>Método de Pago</th>
        <th>Opciones</th>
      </tr>
      </thead>
      <tbody>
      <?php 
      $CI =&get_instance();
      $CI->load->model('Mgeneral');
      //echo serialize($con_promociones);
      foreach ($con_promociones as $promociones) {
        $promocion_id = $promociones->promocion_id;
        $promocion_nombre = $promociones->promocion_nombre;
        $promocion_fechainicio = $promociones->promocion_fechainicio;
        $promocion_fechafinal = $promociones->promocion_fechafinal;
        $promocion_porcentaje = $promociones->promocion_porcentaje;
        $promocion_cantidad = $promociones->promocion_cantidad;
        $producto_id = $promociones->producto_id;
        $metodopago_id = $promociones->metodopago_id;

        $metodopago = $CI->Mgeneral->get_row('metodopago_id',$metodopago_id,'metodopago');
        //echo $CI->db->last_query();
        $metodopago_descripcion = $metodopago->metodopago_descripcion;

        $productos = $CI->Mgeneral->get_row('producto_id',$producto_id,'productos');

        $producto_nombre = $productos->producto_nombre;

        echo '
          <tr id="borrar_'.$promocion_id.'">
            <td>'.$promocion_nombre.'</td>
            <td>'.$producto_nombre.'</td>
            <td>'.$promocion_cantidad.'</td>
            <td>'.$promocion_porcentaje.'</td>
            <td>'.$metodopago_descripcion.'</td>
          <td>
            <a href="'.base_url().'index.php/promociones/promociones/ver_detalle/'.$promocion_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
            <a href="'.base_url().'index.php/promociones/promociones/editar/'.$promocion_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
              <a href="'.base_url().'index.php/promociones/promociones/cambiar_estatus/'.$promocion_id.'/3" class="eliminar_relacion" flag="'.$promocion_id.'" id="delete'.$promocion_id.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>
<script>
  $(document).ready(function() {

    $('#menupromociones').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  });

  function estatuspromocion(promocion_id, cat_estatus_id) {
    $.post('<?php echo base_url()."index.php/promociones/cambiar_estatus_promocion"; ?>',{
      promocion_id : promocion_id,
      cat_estatus_id : cat_estatus_id
      }, function(data){}
    );
  }
</script>

<script>
  $(document).ready(function() {

    $('#menuPromociones').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  });

  </script>
