<?php
$promocion_id = $promociones->promocion_id;
$promocion_nombre = $promociones->promocion_nombre;
$promocion_fechainicio = $promociones->promocion_fechainicio;
$promocion_fechafinal = $promociones->promocion_fechafinal;
$promocion_porcentaje = $promociones->promocion_porcentaje;
$promocion_cantidad = $promociones->promocion_cantidad;



$producto_nombre = $this->Mgeneral->get_row('producto_id',$promociones->producto_id,'productos')->producto_nombre;
$metodopago_descripcion = $this->Mgeneral->get_row('metodopago_id',$promociones->metodopago_id,'metodopago')->metodopago_descripcion;






  // var_dump($promociones);
?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> Promoción: <?php echo $promocion_nombre; ?></h5>
          ID # <?php echo $promocion_id; ?>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-6 mb-6">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-12 border-right">
                  <!-- title row -->
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Promoción</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">

                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Nombre Promoción</li>
                        <li><?php echo $promocion_nombre; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Fecha de inicio</li>
                        <li><?php echo $promocion_fechainicio; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Fecha Final</li>
                        <li><?php echo $promocion_fechafinal; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->


                    <div class="col-sm-2">
                      <ul>
                        <li class="font-weight-bolder">Porcentaje</li>
                        <li><?php echo $promocion_porcentaje; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 ">
                      <ul>
                        <li class="font-weight-bolder">Cantidad</li>
                        <li><?php echo $promocion_cantidad; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Método Pago</li>
                        <li><?php echo $metodopago_descripcion; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->

                  <div class="row">
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Producto</li>
                        <li><?php echo $producto_nombre; ?></li>
                      </ul>
                    </div>

                    <!-- /.col -->



            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->

          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {


      $('#menupromociones').addClass('active-link');



    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
    $('#tipos-de-precio').html(tbody);

    const valueCostoReal = '<?php echo $promocion_costo_real; ?>';

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });
  });
</script>

<script>
  $('document').ready(function() {
    $('#menuPromociones').addClass('active-link');
  });
</script>
