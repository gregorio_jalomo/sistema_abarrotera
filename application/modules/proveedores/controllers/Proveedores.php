<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class proveedores extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproveedores', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 8;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);
  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function catalogos(){
    if(validar_permiso($this->rol_usuario,11,2) || $this->is_admin ){

      $data['titulo_seccion'] = "";
      $data['flecha_ir_atras'] = 'proveedores/listar_proveedores';
  
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js'
      
      );
      $this->cargar_vista('proveedores/catalogos',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("listar catalogos proveedores","listar_proveedores");
    }

  }//...catalogos

  public function alta(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,1) || $this->is_admin ){

      $data['titulo_seccion'] = "proveedores";
      $data['flecha_ir_atras'] = 'proveedores/listar_proveedores';

      $data['clientescredito'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescredito');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
      );
      $this->cargar_vista('proveedores/alta',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar proveedores","proveedores/listar_proveedores");
    }


  }//...alta
  public function guardar(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,1) || $this->is_admin ){

      $this->form_validation->set_rules('proveedor_nombre', 'Nombre Comercial', 'required');
      $this->form_validation->set_rules('proveedor_calle', 'Calle', 'required');
      $this->form_validation->set_rules('proveedor_no_exterior', 'Número Exterior', 'required');
      $this->form_validation->set_rules('proveedor_colonia', 'Colonia', 'required');
      $this->form_validation->set_rules('proveedor_cp', 'Código Postal', 'required');
      $this->form_validation->set_rules('proveedor_ciudad', 'Ciudad', 'required');
      $this->form_validation->set_rules('proveedor_municipio', 'Municipio', 'required');
      $this->form_validation->set_rules('proveedor_estado', 'Estado', 'required');
      $this->form_validation->set_rules('proveedor_pais', 'País', 'required');

      $this->form_validation->set_rules('proveedor_latitud', 'proveedor_latitud', 'required');
      $this->form_validation->set_rules('proveedor_longitud', 'proveedor_longitud', 'required');

      $response = validate($this);
      if($response['status']){
        $data['proveedor_nombre']       = $this->input->post('proveedor_nombre');
        $data['proveedor_calle']        = $this->input->post('proveedor_calle');
        $data['proveedor_no_interior']  = $this->input->post('proveedor_no_interior');
        $data['proveedor_no_exterior']  = $this->input->post('proveedor_no_exterior');
        $data['proveedor_colonia']      = $this->input->post('proveedor_colonia');
        $data['proveedor_cp']           = $this->input->post('proveedor_cp');
        $data['proveedor_ciudad']       = $this->input->post('proveedor_ciudad');
        $data['proveedor_municipio']    = $this->input->post('proveedor_municipio');
        $data['proveedor_estado']       = $this->input->post('proveedor_estado');
        $data['proveedor_pais']         = $this->input->post('proveedor_pais');
        $data['proveedor_latitud']      = $this->input->post('proveedor_latitud');
        $data['proveedor_longitud']     = $this->input->post('proveedor_longitud');
        $data["fecha_registro"]         = date("Y-m-d H:i:s");
        $data['cat_estatus_id']         = 1;
        
        $this->Mgeneral->save_register('proveedores', $data);
      }
      echo json_encode(array('output' => $response));
          
    }else{
      echo json_encode(false);
    }
  }//...guardar

  public function editar($id_proveedor = null){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,39) || $this->is_admin ){

      $data['titulo_seccion'] = "proveedores";
      $data['flecha_ir_atras'] = "proveedores/listar_proveedores";

      $data['clientescredito'] = $this->Mgeneral->get_result('cat_estatus_id',1,'clientescredito');
      $data['proveedores'] = $this->Mgeneral->get_row('proveedor_id',$id_proveedor,'proveedores');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedores/editar',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("editar proveedores","proveedores/listar_proveedores");
    }
  
  }//...editar

  public function ver_detalle($proveedor_id=false){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,38) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del Proveedor";
      $data['flecha_ir_atras'] = "proveedores/proveedores/listar_proveedores";
      $data['proveedores'] = $this->Mgeneral->get_row("proveedor_id",$proveedor_id,'proveedores');

      $data['estatus'] = $this->Mgeneral->get_row(false,false,'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );
      $this->cargar_vista('proveedores/ver_detalle',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("ver detalle de proveedores","proveedores/listar_proveedores");
    }

  }//...ver_detalle

  public function listar_proveedores($pagina_inicio = 1, $pagina_fin = 10){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,37) || $this->is_admin ){

      $data['titulo_seccion'] = "proveedores";
      $data['con_proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1, 'proveedores');
      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js'
      );
      $this->cargar_vista('proveedores/listar',$data,$archivos_js);      //code
    
    }else{
      $this->p_denied_view("listar catalogos proveedores","");
    }

  }

  public function actualizar(){
    $this->form_validation->set_rules('proveedor_nombre', 'Nombre Comercial', 'required');
    $this->form_validation->set_rules('proveedor_calle', 'Calle', 'required');
    $this->form_validation->set_rules('proveedor_no_exterior', 'Número Exterior', 'required');
    $this->form_validation->set_rules('proveedor_colonia', 'Colonia', 'required');
    $this->form_validation->set_rules('proveedor_cp', 'Código Postal', 'required');
    $this->form_validation->set_rules('proveedor_ciudad', 'Ciudad', 'required');
    $this->form_validation->set_rules('proveedor_municipio', 'Municipio', 'required');
    $this->form_validation->set_rules('proveedor_estado', 'Estado', 'required');
    $this->form_validation->set_rules('proveedor_pais', 'Pais', 'required');
    $this->form_validation->set_rules('proveedor_latitud', 'proveedor_latitud', 'required');
    $this->form_validation->set_rules('proveedor_longitud', 'proveedor_longitud', 'required');

    $response = validate($this);
    if($response['status']){
      $data['proveedor_id'] = $this->input->post('proveedor_id');
      $data['proveedor_nombre'] = $this->input->post('proveedor_nombre');
      $data['proveedor_calle'] = $this->input->post('proveedor_calle');
      $data['proveedor_no_interior'] = $this->input->post('proveedor_no_interior');
      $data['proveedor_no_exterior'] = $this->input->post('proveedor_no_exterior');
      $data['proveedor_colonia'] = $this->input->post('proveedor_colonia');
      $data['proveedor_cp'] = $this->input->post('proveedor_cp');
      $data['proveedor_ciudad'] = $this->input->post('proveedor_ciudad');
      $data['proveedor_municipio'] = $this->input->post('proveedor_municipio');
      $data['proveedor_estado'] = $this->input->post('proveedor_estado');
      $data['proveedor_pais'] = $this->input->post('proveedor_pais');

      $data['proveedor_latitud'] = $this->input->post('proveedor_latitud');
      $data['proveedor_longitud'] = $this->input->post('proveedor_longitud');
      //...
      $data['cat_estatus_id'] = 1;
      //...
      $this->Mgeneral->update_table_row('proveedores',$data,'proveedor_id',$data['proveedor_id']);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

  }//...actualizar

  public function cambiar_estatus($proveedor_id, $estatus_id) {
    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,39) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,40) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['proveedor_id'] = $proveedor_id;
      $this->Mgeneral->update_table_row('proveedores',$data,'proveedor_id',$data['proveedor_id']); 

    }
    echo json_encode($go);

  }//...cambiar_estatus
}
