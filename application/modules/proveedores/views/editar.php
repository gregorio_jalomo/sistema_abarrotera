<?php
  $proveedor_id = $proveedores->proveedor_id;
  $proveedor_nombre = $proveedores->proveedor_nombre;

  $proveedor_calle = $proveedores->proveedor_calle;
  $proveedor_no_interior = $proveedores->proveedor_no_interior;
  $proveedor_no_exterior = $proveedores->proveedor_no_exterior;
  $proveedor_colonia = $proveedores->proveedor_colonia;
  $proveedor_cp = $proveedores->proveedor_cp;
  $proveedor_ciudad = $proveedores->proveedor_ciudad;
  $proveedor_municipio = $proveedores->proveedor_municipio;
  $proveedor_estado = $proveedores->proveedor_estado;
  $proveedor_pais = $proveedores->proveedor_pais;

  $proveedor_latitud = $proveedores->proveedor_latitud;
  $proveedor_longitud = $proveedores->proveedor_longitud;
?>
<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#editar_proveedor').submit(function(event){
    event.preventDefault();
    console.log($('#proveedor_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/proveedores/actualizar";
    ajaxJson(url,{
        'proveedor_id' : '<?php echo $proveedor_id; ?>',
        'proveedor_nombre' : $('#proveedor_nombre').val(),

        'proveedor_calle' : $('#proveedor_calle').val(),
        'proveedor_no_interior' : $('#proveedor_no_interior').val(),
        'proveedor_no_exterior' : $('#proveedor_no_exterior').val(),
        'proveedor_colonia' : $('#proveedor_colonia').val(),
        'proveedor_cp' : $('#proveedor_cp').val(),
        'proveedor_ciudad' : $('#proveedor_ciudad').val(),
        'proveedor_municipio' : $('#proveedor_municipio').val(),
        'proveedor_estado' : $('#proveedor_estado').val(),
        'proveedor_pais' : $('#proveedor_pais').val(),

        'proveedor_latitud' : $('#proveedor_latitud').val(),
        'proveedor_longitud' : $('#proveedor_longitud').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/proveedores/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Proveedor</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_proveedor">
            <div class="card-body">
              <div class="row form-group">
                <div class="col-md-8">
                  <label for="">Nombre Comercial</label>
                  <input type="text" class="form-control" name="proveedor_nombre" id="proveedor_nombre" value="<?php echo $proveedor_nombre; ?>">
                </div>

              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Calle</label>
                  <input type="text" class="form-control" name="proveedor_calle" id="proveedor_calle" value="<?php echo $proveedor_calle; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. Interior</label>
                  <input type="number" class="form-control" name="proveedor_no_interior" id="proveedor_no_interior" value="<?php echo $proveedor_no_interior; ?>">
                </div>
                <div class="col-md-2">
                  <label for="">Núm. exterior</label>
                  <input type="number" class="form-control" name="proveedor_no_exterior" id="proveedor_no_exterior" value="<?php echo $proveedor_no_exterior; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">Colonia</label>
                  <input type="text" class="form-control" name="proveedor_colonia" id="proveedor_colonia" value="<?php echo $proveedor_colonia; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-2">
                  <label for="">Código Postal</label>
                  <input type="text" class="form-control" name="proveedor_cp" id="proveedor_cp" value="<?php echo $proveedor_cp; ?>">
                </div>
                <div class="col-md-5">
                  <label for="">Ciudad</label>
                  <input type="text" class="form-control" name="proveedor_ciudad" id="proveedor_ciudad" value="<?php echo $proveedor_ciudad; ?>">
                </div>
                <div class="col-md-5">
                  <label for="">Municipio</label>
                  <input type="text" class="form-control" name="proveedor_municipio" id="proveedor_municipio" value="<?php echo $proveedor_municipio; ?>">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="">Estado</label>
                  <input type="text" class="form-control" name="proveedor_estado" id="proveedor_estado" value="<?php echo $proveedor_estado; ?>">
                </div>
                <div class="col-md-4">
                  <label for="">País</label>
                  <input type="text" class="form-control" name="proveedor_pais" id="proveedor_pais" value="<?php echo $proveedor_pais; ?>">
                </div>

              </div>

              <div class="row">
                  <input id="proveedor_latitud" name="proveedor_latitud" type="hidden" class="form-control cc-exp form-control-sm" value="<?php echo $proveedor_latitud; ?>">
                  <input id="proveedor_longitud" name="proveedor_longitud" type="hidden" class="form-control cc-exp form-control-sm" value="<?php echo $proveedor_longitud; ?>">
                <div class="col-12">
                  <div class="form-group">
                    <label for="">Ubicación</label>
                    <div id="mapa-direccion" style="height: 350px; "></div>
                    <img class="hidden" id="imagen_mapa" src="<?php echo base_url();?>ubicacion.png" style="visibility: hidden;" />
                  </div>
                </div>
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/proveedores/listar_proveedores'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');

  });
</script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAHHWSkUcqyeERY0E2KCxx_Fggbo1BZN5I"></script>
<script type="text/javascript">
  var geoCode = new google.maps.Geocoder();
  function initialize() {

    <?php //if($negocios->negocioproveedor_Latitud == '' && $negocios->negocioproveedor_Longitud == ''): ?>
            var latLng = new google.maps.LatLng(19.264146,-103.718579);
    <?php //else: ?>
            //var latLng = new google.maps.LatLng('','');
    <?php //endif; ?>
    var markerImage = new google.maps.MarkerImage($("#imagen_mapa").attr('src'),
                                                          new google.maps.Size(34, 32),
                                                          new google.maps.Point(0, 0),
                                                          new google.maps.Point(0, 32));
    var map = new google.maps.Map(document.getElementById('mapa-direccion'),{
                zoom: 15,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
    var marker = new google.maps.Marker({
                                        position: latLng,
                                        title: 'Seleccione su ubicacion',
                                        map: map,
                                        icon: markerImage,
                                        draggable: true
                                      });

      //ACTUALIZAR LA INFORMACION DE LA POSICION ACTUAL
      updateMarkerPosition(latLng);
      geocodePosition(latLng);

      //AGREGAR EL EVENTO DE ARRASTRAR LOS DATOS
      google.maps.event.addListener(marker, 'dragstart', function(){
          updateMarkerAddress('...');
      });

      google.maps.event.addListener(marker, 'drag', function(){
          updateMarkerStatus('.....');
          updateMarkerPosition(marker.getPosition());
      });

      google.maps.event.addListener(marker, 'dragend', function(){
          updateMarkerStatus('....');
          geocodePosition(marker.getPosition());
      });
  }
    /**
     * METODO QUE SE USA PARA EL CODIGO
     * DE LA POSICION
     **/
    function geocodePosition(pos){
      geoCode.geocode({
        latLng: pos
        }, function(responses){
            if(responses && responses.lenght > 0){
              updateMarkerAddress(responses[0].formatted_address);
            } else {
              updateMarkerAddress('');
            }
          }
      );
    }
    /**
     * ACTUALIZAR EL DATO DE LOS MARKERS
     * EN EL GOOGLE MAPS
     **/
    function updateMarkerStatus(str){
        //document.getElementById('markerStatus').innerHTML = str;
    }

    /**
     * ACTUALIZA LA POSICION DEL MARCADOR PARA
     * QUE CAPTURAR SU proveedor_LATITUD Y proveedor_LONGITUD
     **/
    function updateMarkerPosition(latLng){
        var uno = latLng.lng();
        var dos = latLng.lat();
        document.getElementById('proveedor_longitud').value = uno;
        document.getElementById('proveedor_latitud').value = dos;
       // alert('hola: ' + latLng.lng())
        /*document.getElementById('info').innerHTML = [
            latLng.lat(),
            latLng.lng()
            ].join(', ');*/
    }

    /**
     * ACTUALIZA LA DIRECCION DEL MARCADOR PARA
     * MOSTRARLA LA USUARIO
     **/
    function updateMarkerAddress(str){
        //document.getElementById('address').innerHTML = str;
    }

    //EVENTO ONLOAD PARA ACTIVAR LA APLICACION
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
