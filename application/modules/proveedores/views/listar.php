<script>
$(document).ready(function(){
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
              {
                "danger":
                          {
                            "label": "<i class='icon-remove'></i>Eliminar ",
                            "className": "btn-danger",
                            "callback": function () {
                            id = $(event.currentTarget).attr('flag');
                            url = $("#delete"+id).attr('href');zz
                            $("#borrar_"+id).slideUp();
                              $.get(url);
                            }
                            },
                              "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Cancelar",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                          }
                      });
    });
});
</script>
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">proveedores registrados</h3>
    </div>
    <div class="col-md-6">
      <a href="<?php echo base_url().'index.php/proveedores/proveedores/catalogos'?>"><button class="btn btn-dark float-right"><i class="fas fa-archive"></i> Catálogos Proveedor</button></a>
      <a href="<?php echo base_url().'index.php/proveedores/proveedores/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar proveedor</button></a>

    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Nombre Comecial</th>
        <!-- <th>Pasillo</th>
        <th>Área</th>
        <th>Familia</th>
        <th>Categoría</th>
        <th>Subcategoría</th>
        <th>Unidad</th>
        <th>IVA</th>
        <th>IEPS</th>
        <th>Punto de reorden</th>
        <th>Stock mín.</th>
        <th>Stock máx.</th>
        <th>Tipo de precio</th>
        <th>Proveedor</th>
        <th>proveedor SAT</th>
        <th>Unidad SAT</th> -->
        <th>Domicilio</th>
        <th>Fecha de Registro</th>
        <th>Opciones</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($con_proveedores as $proveedores) {
        $proveedor_id = $proveedores->proveedor_id;
        $proveedor_nombre = $proveedores->proveedor_nombre;

        $proveedor_calle = $proveedores->proveedor_calle;
        $proveedor_no_interior = $proveedores->proveedor_no_interior;
        $proveedor_no_exterior = $proveedores->proveedor_no_exterior;
        $proveedor_colonia = $proveedores->proveedor_colonia;
        $proveedor_cp = $proveedores->proveedor_cp;
        $proveedor_ciudad = $proveedores->proveedor_ciudad;
        $proveedor_municipio = $proveedores->proveedor_municipio;
        $proveedor_estado = $proveedores->proveedor_estado;
        $proveedor_pais = $proveedores->proveedor_pais;

        $proveedor_latitud = $proveedores->proveedor_latitud;
        $proveedor_longitud = $proveedores->proveedor_longitud;
        $fecha_registro = $proveedores->fecha_registro;
        $cat_estatus_id = $proveedores->cat_estatus_id;


        echo
        '<tr id="borrar_'.$proveedor_id.'">
          <td>'.$proveedor_nombre.'</td>
            <td>'.$proveedor_calle.' '.$proveedor_no_interior.' '.$proveedor_no_exterior.'
                '.$proveedor_colonia.' '.$proveedor_municipio.' '.$proveedor_estado.'</td>
          <td>'.$fecha_registro.'</td>
          <td>
            <a href="'.base_url().'index.php/proveedores/proveedores/ver_detalle/'.$proveedor_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver detalle</a>
            <a href="'.base_url().'index.php/proveedores/proveedores/editar/'.$proveedor_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
              <a href="'.base_url().'index.php/proveedores/proveedores/cambiar_estatus/'.$proveedor_id.'/3" class="eliminar_relacion" flag="'.$proveedor_id.'" id="delete'.$proveedor_id.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>
<script>
  $(document).ready(function() {

    $('#menuProveedores').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  });

  function estatusproveedor(proveedor_id, cat_estatus_id) {
    $.post('<?php echo base_url()."index.php/proveedores/cambiar_estatus_proveedor"; ?>',{
      proveedor_id : proveedor_id,
      cat_estatus_id : cat_estatus_id
      }, function(result){

        if(result=="true"){
          $("#borrar_"+proveedor_id).slideUp();
          ExitoCustom("Eliminado");
        }else{
          ErrorCustom("No tienes permiso para eliminar esto");
        }
      }
    );
  }
</script>
