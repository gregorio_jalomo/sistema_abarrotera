<?php
$proveedor_id = $proveedores->proveedor_id;
$proveedor_nombre = $proveedores->proveedor_nombre;

$proveedor_calle = $proveedores->proveedor_calle;
$proveedor_no_interior = $proveedores->proveedor_no_interior;
$proveedor_no_exterior = $proveedores->proveedor_no_exterior;
$proveedor_colonia = $proveedores->proveedor_colonia;
$proveedor_cp = $proveedores->proveedor_cp;
$proveedor_ciudad = $proveedores->proveedor_ciudad;
$proveedor_municipio = $proveedores->proveedor_municipio;
$proveedor_estado = $proveedores->proveedor_estado;
$proveedor_pais = $proveedores->proveedor_pais;

$proveedor_latitud = $proveedores->proveedor_latitud;
$proveedor_longitud = $proveedores->proveedor_longitud;
$fecha_registro = $proveedores->fecha_registro;
$cat_estatus_id = $proveedores->cat_estatus_id;

  // var_dump($proveedores);
?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> proveedor: <?php echo $proveedor_nombre; ?></h5>
          ID # <?php echo $proveedor_id; ?>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-6 mb-6">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-12 border-right">
                  <!-- title row -->
                  <div class="row">
                    <div class="col-12">
                      <h4>
                        <small class="text-uppercase">Proveedor</small>
                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row">

                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Nombre Comercial</li>
                        <li><?php echo $proveedor_nombre; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Calle</li>
                        <li><?php echo $proveedor_calle; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Número Exterior</li>
                        <li><?php echo $proveedor_no_exterior; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->


                    <div class="col-sm-2">
                      <ul>
                        <li class="font-weight-bolder">Número Interior</li>
                        <li><?php echo $proveedor_no_interior; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 ">
                      <ul>
                        <li class="font-weight-bolder">Colonia</li>
                        <li><?php echo $proveedor_colonia; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li class="font-weight-bolder">Municipio</li>
                        <li><?php echo $proveedor_municipio; ?></li>
                      </ul>
                    </div>
                    <!-- /.col -->

                  <div class="row">
                    <div class="col-sm-3 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">Estado</li>
                        <li><?php echo $proveedor_estado; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-4 invoice-col">
                      <ul>
                        <li class="font-weight-bolder">País</li>
                        <li><?php echo $proveedor_pais; ?></li>
                      </ul>
                    </div>
                    <div class="col-sm-5">
                      <ul>
                        <li class="font-weight-bolder">Fecha de Registro</li>
                        <li><?php echo $fecha_registro; ?></li>
                      </ul>
                    </div>

                    <!-- /.col -->



            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->

          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {


      $('#menuProveedores').addClass('active-link');



    let aTiposDePrecio = <?php echo json_encode($tiposdeprecio); ?>;
    let tbody = '';
    let nTipoDePrecio = 1;
    aTiposDePrecio.forEach(element => {
      nTipoDePrecio++;
      tbody+=`<tr id="tipoDePrecio-${nTipoDePrecio}"><td>${element["tipodeprecio_descripcion"]}</td>`;
      tbody+=`<td id="porcentaje-${nTipoDePrecio}">${element["tipodeprecio_porcentaje"]}</td>`;
      tbody+=`<td id="totalPrecio-${nTipoDePrecio}">0</td></tr>`;
    });
    $('#tipos-de-precio').html(tbody);

    const valueCostoReal = '<?php echo $proveedor_costo_real; ?>';

    $('[id^="tipoDePrecio-"]').each(function() {
      let id = this.id;
      let split = id.split('-');
      let nTipoDePrecio = split[1];
      //...
      let tipoDePrecio = $('#porcentaje-'+nTipoDePrecio).html();
      //...
      let totalPrecio = parseFloat(valueCostoReal) + (valueCostoReal * tipoDePrecio) / 100;
      $('#totalPrecio-'+nTipoDePrecio).html(totalPrecio);
    });
  });
</script>

<script>
  $('document').ready(function() {
    $('#menuProveedores').addClass('active-link');
  });
</script>
