<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Proveedoresaduana extends MX_Controller {

  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproveedoresaduana', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('proveedoresaduana/Proveedoresaduana_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 13;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function crear_folio_prov_aduana($FUNI=""){
    echo json_encode(crear_foliodocumento("PROVADU",$FUNI));
  }//...crear_folio_compra

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,133) || $this->is_admin ){

      $alta_response=array(
        "id"=>"",
        "bool_error"=>false,
        "error_msg"=>"",
        "prod_error"=>array()
      );
      //recibir post del ajax
      $dataProveedoresaduana['folio_documento']         = $this->input->post('nuevo_folio_documento');
      $dataProveedoresaduana['folio_unico_documento']   = $this->input->post('nuevo_FUNI_documento');
      $dataProveedoresaduana['perfil_usuario']          = $this->session->userdata['infouser']['id'];
      $dataProveedoresaduana['descripcion_id_usuario']  = $this->session->userdata['infouser']['nombre'];

      $foliosdocumentos_id      = $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI = $this->input->post('id_FUNI_foliosdocumentos');
      $obj_productos            = $this->input->post('obj_productos');
      $obj_comprobante          = $this->input->post('obj_comprobante');
      $obj_emisor               = $this->input->post('obj_emisor');

      $rfc_proveedor            = isset($obj_emisor["Rfc"]) ? $obj_emisor["Rfc"] : "###";
      $info_proveedor           = $this->get_info_proveedor_by_rfc($rfc_proveedor);
      //$info_proveedor           = (isset($info_proveedor)) ? $info_proveedor : $this->get_proveedorescompra_by_funi($dataProveedoresaduana['folio_unico_documento']);
      //print_r($info_proveedor);
      $id_proveedor             = (isset($info_proveedor) && $info_proveedor!="")?$info_proveedor->id_proveedores:"";
      $dataProveedoresaduana['id_proveedores']  = $id_proveedor;

      $dataProveedoresaduana['total_xml']  = $obj_comprobante['Total'];
      
      //insert a proveedoresaduana
      $PROVADU_to_find=$this->get_proveedoresaduana_by_FUNI($dataProveedoresaduana['folio_unico_documento']);

      if(isset($PROVADU_to_find)){
        $this->delete_proveedoresaduana_by_id($PROVADU_to_find->id);
      }

      $proveedoresaduana_id=$this->guardar($dataProveedoresaduana);
      $alta_response["id"]=$proveedoresaduana_id;

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que una cotizacion se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }

      if(isset($obj_productos)){
        
        $err_productos_not_found=array();
        $resa="";
        foreach ($obj_productos as $key => $producto) {
          $product_w_error=array();
          $error_clave_sat=false;
          $info_producto = $this->get_info_producto_by_id_productosat($producto[0]["ClaveProdServ"]);
          
          if(count($info_producto)!=0){
            if(count($info_producto)==1){
              //se encontró solo un producto con la clave SAT proporcionada en el xml
              $data_prod["id_proveedoresaduana"]      =  $proveedoresaduana_id;
              $data_prod["id_producto"]               =  $info_producto[0]->producto_id;
              $data_prod["nombre_producto"]           =  $info_producto[0]->producto_nombre;
              $data_prod["descripcion_de_la_unidad"]  =  $info_producto[0]->unidad_tipo;
              $data_prod["cantidad"]                  =  $producto[0]["Cantidad"];

              $this->Mgeneral->save_register("proveedoresaduana_detalle", $data_prod);

            }else{
              //hay 2 o mas productos con la misma clave SAT 
              $error_clave_sat=true;
              $error_clave_sat_msg="clave sat duplicada"; 
            }

          }else{
              //no se encontró un producto con la clave SAT proporcionada en el xml
            $error_clave_sat=true;
            $error_clave_sat_msg="clave sat erronea o inexistente"; 

          }

          if($error_clave_sat){
            $data_prod_error["id_proveedoresaduana"]      =  $proveedoresaduana_id;
            $data_prod_error["descripcion"]               =  $producto[0]["Descripcion"];
            $data_prod_error["clave_prod_serv"]           =  $producto[0]["ClaveProdServ"];
            $data_prod_error["cantidad"]                  =  $producto[0]["Cantidad"];
            $data_prod_error["error_descripcion"]         =  $error_clave_sat_msg;

            $this->Mgeneral->save_register("proveedoresaduana_detalle_error", $data_prod_error);

            $alta_response["bool_error"]=true;
            $alta_response["error_msg"]="La clave SAT de los siguientes poductos está duplicada, no se encontró o no coincide con la base de datos local, por favor verifique.";
            $product_w_error=array(
                                    "nombre"=>$producto[0]["Descripcion"],
                                    "clave_sat"=>$producto[0]["ClaveProdServ"],
                                    "cantidad"=>$producto[0]["Cantidad"]);
            array_push($alta_response["prod_error"],$product_w_error);
          }

  
        }//...foreach $obj_productos

      }//...if isset($obj_productos)
      echo json_encode($alta_response);

    }else{
      echo json_encode(false);
    }

  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('proveedoresaduana', $data);
  }//...guardar

  public function editar($id_proveedoresaduana){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,63) || $this->is_admin ){

      $data['titulo_seccion'] = "Compra de proveedores en aduana";
      $data['flecha_ir_atras'] = "proveedoresaduana/listar_proveedoresaduana";
      $data['force_landscape']=true;

      $data['proveedoresaduana']  = $this->Proveedoresaduana_model->get_proveedoresaduana_by_id($id_proveedoresaduana);
      $provcompra                 = $this->Proveedoresaduana_model->get_proveedorescompra_by_FUNI($data['proveedoresaduana']->folio_unico_documento);
      $data['provcompra_detalle'] = $this->Proveedoresaduana_model->get_proveedorescompra_detalle_by_id_provcompra($provcompra->id);
      $data['provadu_error']      = $this->Proveedoresaduana_model->get_proveedoresaduana_error_by_id($id_proveedoresaduana);
      
      $data['productos']          = $this->Proveedoresaduana_model->get_productos('productos.cat_estatus_id',1);

      //$['costo_original']= $this->Proveedores_aduana_model->get_costo_original_proveedorescotizacion_by_funi();

      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data["fecha_creacion"]         = date("Y-m-d H:i:s");
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js',
        //'statics/js/libraries/filesaverjs/fileSaver.js',
        'statics/js/libraries/sheetjs/xlsx.js'
      );

      $this->cargar_vista('proveedoresaduana/editar',$data,$archivos_js);

    }else{
      $this->p_denied_view("editar pedidos en aduana(proveedores)","proveedoresaduana/listar_proveedoresaduana");
    }

    
  }//...editar

  public function read_xml(){
    $archivos_js=array(
      'statics/js/bootbox.min.js',
        'statics/js/general.js',
        'statics/bootstrap4/js/bootstrap.min.js'
    );

    $this->cargar_vista('proveedoresaduana/read_xml',$data,$archivos_js);
  }//...read_xml

  public function ver_detalle($id_proveedoresaduana){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,62) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de la compra";
      $data['flecha_ir_atras'] = "proveedoresaduana/proveedoresaduana/listar_proveedoresaduana";

      $data['proveedoresaduana'] = $this->Proveedoresaduana_model->get_proveedoresaduana_by_id($id_proveedoresaduana);
      $data['proveedoresaduana_detalle'] =$this->Proveedoresaduana_model->get_proveedoresaduana_detalle($id_proveedoresaduana);

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedoresaduana/ver_detalle',$data,$archivos_js);

    }else{
      $this->p_denied_view("ver detalle de pedidos en aduana(proveedores)","proveedoresaduana/proveedoresaduana/listar_proveedoresaduana");
    }

  }//...ver_detalle

  public function listar_proveedoresaduana(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,61) || $this->is_admin ){

      $data['titulo_seccion'] = "Aduana de Proveedores ";
      $data['con_proveedoresaduana']  = $this->Proveedoresaduana_model->listar_proveedoresaduana();
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data['force_landscape']=true;

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('proveedoresaduana/listar',$data,$archivos_js);

    }else{
      $this->p_denied_view("listar pedidos en aduana(proveedores)","");
    }

  }//...listar_proveedoresaduana

  public function get_proveedoresaduana_detalle($id_proveedoresaduana,$request_type="",$order_by="id",$order_type="DESC"){
    $do=$this->Proveedoresaduana_model->get_proveedoresaduana_detalle($id_proveedoresaduana,$order_by,$order_type);
    if($request_type=="return"){
      return $do;
    }else{
      echo json_encode($do);
    }

  }//...get_proveedoresaduana_detalle

  public function get_proveedoresaduana_by_FUNI($folio_unico_documento){
    return $this->Proveedoresaduana_model->get_proveedoresaduana_by_FUNI($folio_unico_documento);
  }//...get_proveedoresaduana_by_FUNI

  public function delete_proveedoresaduana_by_id($id_proveedoresaduana){
    return $this->Proveedoresaduana_model->delete_proveedoresaduana_by_id($id_proveedoresaduana);
  }//...delete_proveedoresaduana_by_id

  public function get_proveedorescompra_by_FUNI($folio_unico_documento){
    return $this->Proveedoresaduana_model->get_proveedorescompra_by_FUNI($folio_unico_documento);
  }//...get_proveedorescompra_by_FUNI

  public function get_proveedorescompra_detalle_by_id_provcompra($id_proveedorescompra){
    return $this->Proveedoresaduana_model->get_proveedorescompra_detalle_by_id_provcompra($id_proveedorescompra);
  }//...get_proveedorescompra_detalle_by_id_provcompra

  public function get_info_proveedor_by_rfc($proveedor_rfc){
    return $this->Proveedoresaduana_model->get_info_proveedor_by_rfc($proveedor_rfc);
  }//...get_info_proveedor

  public function get_info_proveedor(){
    $proveedor_id=$this->input->post('proveedor_id');

    echo json_encode($this->Proveedoresaduana_model->get_info_proveedor($proveedor_id));
  }//...get_info_proveedor

  public function get_set_ultimo_status($id_proveedoresaduana,$nvo_stat){
    echo json_encode($this->Proveedoresaduana_model->get_set_ultimo_status($id_proveedoresaduana,$nvo_stat));
  }//...get_set_ultimo_status

  public function cambiar_status($id_proveedoresaduana,$estatus_id) {
    $data['status'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,63) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,64) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){
      $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
      $this->Mgeneral->update_table_row('proveedoresaduana',$data,'id',$id_proveedoresaduana);     
    }
    echo json_encode($go);
  }//...cambiar_status

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
    $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
    $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_proveedores_destinatario(){
    $id_proveedoresaduana=$this->input->post('id_proveedoresaduana');
    $data['id_proveedores']=$this->input->post('id_proveedor');

    $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
    $this->Mgeneral->update_table_row("proveedoresaduana",$data,"id",$id_proveedoresaduana);
  }//...update_proveedores_destinatario

  public function get_info_producto_by_id_productosat($id_prod_sat){
    return $this->Proveedoresaduana_model->get_info_producto_by_id_productosat($id_prod_sat);
  }//...get_info_producto_by_id_productosat

  public function agregar_producto(){

    $this->form_validation->set_rules('id_proveedoresaduana', ' ID de la cotización', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    
    $id_proveedoresaduana=$this->input->post('id_proveedoresaduana');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    /* $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total'); */

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    $producto=$this->Proveedoresaduana_model->buscar_producto_pAduana_detalle($id_proveedoresaduana,$id_producto);
    if(isset($producto)){

      $dataUpdate['id_proveedoresaduana']  = $id_proveedoresaduana;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      $dataUpdate['validado']               = 1;
      /* $dataUpdate['costo_unitario']         = $costo_unitario;
      $dataUpdate['costo_total']            = $costo_total; */
      $comprometidos=$cantidad-$producto->cantidad;
      echo json_encode($this->Proveedoresaduana_model->actualizar_producto_pAduana_detalle($dataUpdate));

    }else{

    $dataAdd['id_proveedoresaduana']  = $id_proveedoresaduana;
    $dataAdd['id_producto']            = $id_producto;
    $dataAdd['nombre_producto']        = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']   = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']               = $cantidad;
    $dataUpdate['validado']               = 1;

    $comprometidos=$cantidad;

    echo json_encode($this->Proveedoresaduana_model->agregar_producto_pAduana_detalle($dataAdd));
    }

    if($comprometidos!=0){
      $this->Proveedoresaduana_model->actualizar_comprometidos_producto($id_producto,$comprometidos);
    }

    $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
  }//...agregar_producto

  public function get_productos_no_validados($id_proveedoresaduana){

    echo json_encode($this->Proveedoresaduana_model->get_productos_no_validados($id_proveedoresaduana));
  }//...get_productos_no_validados
  
  public function eliminar_producto($id_proveedoresaduana,$id_producto){
    $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
    echo json_encode($this->Proveedoresaduana_model->eliminar_producto_pAduana_detalle($id_proveedoresaduana,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Proveedoresaduana_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_proveedoresaduana=$this->input->post('id_proveedoresaduana');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Proveedoresaduana_model->actualizar_fecha_modificacion($id_proveedoresaduana);
    $this->Mgeneral->update_table_row('proveedoresaduana',$data,'id',$id_proveedoresaduana);

  }//...agregar_comentarios

  public function get_fecha_modificacion(){
    $id_proveedoresaduana=$this->input->post('id_proveedoresaduana');
    echo json_encode($this->Proveedoresaduana_model->get_fecha_modificacion($id_proveedoresaduana));
  }//...get_fecha_modificacion

  public function importar_datos_de_pCompra($folio_unico_documento,$proveedoresaduana_id){
    echo json_encode($this->Proveedoresaduana_model->importar_datos_de_pCompra($folio_unico_documento,$proveedoresaduana_id));
  }//...importar_datos_de_pCompra

  public function importar_productos_de_pCompra_detalle(){
    $folio_unico_documento      = $this->input->post('folio_unico_documento');
    $id_proveedoresaduana       = $this->input->post('id_proveedoresaduana');
    $id_proveedoresaduana       = $this->Proveedoresaduana_model->get_id_clientescotizacion_by_FUNI($folio_unico_documento);
    $result_clientescotizacion  = $this->Proveedoresaduana_model->get_clientescotizacion_detalle($id_proveedoresaduana);
    //$data_import["folio_unico_documento"]           =$row_clientespedido->folio_unico_documento;
    
    $cont=0;
    foreach($result_clientescotizacion as $row_cot){
      $data_import[$cont]["id_proveedoresaduana"]      =  $id_proveedoresaduana;
      $data_import[$cont]["id_producto"]               =  $row_cot->id_producto;
      $data_import[$cont]["nombre_producto"]           =  $row_cot->nombre_producto;
      $data_import[$cont]["codigo_barras_producto"]    =  $row_cot->codigo_barras_producto;
      $data_import[$cont]["descripcion_de_la_unidad"]  =  $row_cot->descripcion_de_la_unidad;
      $data_import[$cont]["cantidad"]                  =  $row_cot->cantidad;
      $data_import[$cont]["costo_unitario"]            =  $row_cot->costo_unitario;
      $cont++;
    }

    echo json_encode($this->Proveedoresaduana_model->importar_productos_de_pCompra_detalle($id_clientespedido,$data_import));
  }//...importar_productos_de_pCompra_detalle

}
