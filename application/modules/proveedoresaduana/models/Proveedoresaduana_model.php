<?php
/**

 **/
class Proveedoresaduana_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        
    }

    public function listar_proveedoresaduana(){
        $db=$this->db;
        $db->select("proveedoresaduana.*,cat_estatus.cat_estatus_nombre");
        $db->from("proveedoresaduana");
        $db->join("cat_estatus","proveedoresaduana.status=cat_estatus.cat_estatus_id");
        $db->where_in("status",[4,5,6]);
        $db->order_by("fecha_creacion","DESC");
        return $db->get()->result();

    }//...listar_proveedoresaduana

    public function get_proveedoresaduana_by_id($id_proveedoresaduana){
        $db=$this->db
        ->select("proveedoresaduana.*,cat_estatus.cat_estatus_nombre")
        ->from("proveedoresaduana")
        ->join("cat_estatus","proveedoresaduana.status=cat_estatus.cat_estatus_id");
        $db->where("id",$id_proveedoresaduana);
        
        return $db->get()->row();
    }//...get_proveedoresaduana_by_id
    
    public function count_proveedoresaduana_detalle($id_proveedoresaduana){
        return $this->db->where("id_proveedoresaduana",$id_proveedoresaduana)->get("proveedoresaduana_detalle")->num_rows();
    }

    public function get_proveedoresaduana_detalle($id_proveedoresaduana,$order_by="id",$order_type="DESC"){
        $result=$this->db
                ->select("proveedoresaduana_detalle.*, codigosbarras.codigobarra_codigo, productos.familia_id, familia.familia_tipo")
                ->join("productos","proveedoresaduana_detalle.id_producto = productos.producto_id")
                ->join("familia","productos.familia_id = familia.familia_id")
                ->join("codigosbarras","proveedoresaduana_detalle.id_producto = codigosbarras.producto_id")
                ->where("id_proveedoresaduana",$id_proveedoresaduana)
                ->order_by($order_by,$order_type)
                ->get("proveedoresaduana_detalle")->result();
        return $result;
    }//...get_proveedoresaduana_detalle
    
    public function get_proveedoresaduana_detalle_no_join($id_proveedoresaduana){

        $result=$this->db
                ->where("id_proveedoresaduana",$id_proveedoresaduana)
                ->order_by("id","DESC")
                ->get("proveedoresaduana_detalle")->result();
        return $result;
    }//...get_proveedoresaduana_detalle_no_join

    public function get_proveedoresaduana_error_by_id($id_proveedoresaduana){

        $result=$this->db
                ->where("id_proveedoresaduana",$id_proveedoresaduana)
                ->get("proveedoresaduana_detalle_error")->result();
        return $result;
    }//...get_proveedoresaduana_error_by_id

    public function get_proveedoresaduana_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get("proveedoresaduana")->row();
    }//...get_proveedoresaduana_by_FUNI

    public function delete_proveedoresaduana_by_id($id_proveedoresaduana){
        $this->db->delete("proveedoresaduana", array("id"=>$id_proveedoresaduana));
        $this->db->delete("proveedoresaduana_detalle", array("id_proveedoresaduana"=>$id_proveedoresaduana));
    }//...delete_proveedoresaduana_by_id

    public function get_proveedorescompra_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->select("proveedorescompra.*,cat_estatus.cat_estatus_nombre");
        $db->from("proveedorescompra");
        $db->join("cat_estatus","proveedorescompra.status=cat_estatus.cat_estatus_id");
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get()->row();
    }//...get_proveedorescompra_by_FUNI

    public function get_proveedorescompra_detalle_by_id_provcompra($id_proveedorescompra){
        $db=$this->db;
        $db->where("id_proveedorescompra",$id_proveedorescompra);
        return $db->get("proveedorescompra_detalle")->result();
      }//...get_proveedorescompra_detalle_by_id_provcompra

    public function get_info_producto_by_id_productosat($id_productosat){

        $db=$this->db
        ->select("productos.*,unidad.unidad_tipo")
        ->where("producto_id_productosat",$id_productosat)
        ->join("unidad","unidad.unidad_id = productos.unidad_id");
        return $db->get("productos")->result();

    }//...get_info_producto_by_id_productosat

    public function get_info_proveedor($proveedor_id){
        /* SELECT *
            FROM proveedores
            WHERE proveedor_id = $proveedor_id*/
        $db=$this->db;
        $db->where("proveedor_id",$proveedor_id);
        
        return $db->get("proveedores")->row();

    }//...get_info_proveedor

    public function get_info_proveedor_by_rfc($proveedor_rfc){

        $row_proveedor=$this->db
        ->where("proveedorrfc_rfc",$proveedor_rfc)
        ->get("proveedoresrfc")->row();
        if(isset($row_proveedor->proveedor_id)){
            return $this->get_info_proveedor($row_proveedor->proveedor_id);
        }else{

        }
        return "";

    }//...get_info_proveedor_by_rfc

    public function get_info_producto_by_name($prod_name){
        /* SELECT *
            FROM productos
            WHERE producto_nombre = $prod_name*/
        $db=$this->db
            ->select("productos.*,unidad.unidad_tipo")
            ->join("unidad","productos.unidad_id = unidad.unidad_id")
            ->where("producto_nombre",$prod_name);
        
        return $db->get("productos")->row();

    }//...get_info_producto_by_name

    public function get_productos($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_productos_no_validados($id_proveedoresaduana){
        $result= $this->db
        ->where(array("id_proveedoresaduana"=>$id_proveedoresaduana,"validado"=>0))
        ->get("proveedoresaduana_detalle")
        ->result();
        $ultimo_stat=$this->get_status($id_proveedoresaduana);
        return ($result)?$result:false;
    }//...get_productos_no_validados

    public function get_cant_producto($id_pedido,$id_producto){

        $product_row=$this->get_existencias_producto($id_producto);

        $detalle_row = $this->db->select("cantidad")
                         ->where(array("id_proveedoresaduana"=>$id_pedido,"id_producto"=>$id_producto))
                         ->get("proveedoresaduana_detalle")->row();
        $arr=array(
            "existencias"=>round($product_row->producto_existencias, 3, PHP_ROUND_HALF_EVEN),
            "comprometidos"=>round($product_row->producto_existencias_comprometidas_pedido, 3, PHP_ROUND_HALF_EVEN),
            "por_comprar"=>round($product_row->producto_existencias_comprometidas_compras, 3, PHP_ROUND_HALF_EVEN),
        );
        if(isset($detalle_row)){
            $arr["cantidad"]=round($detalle_row->cantidad, 3, PHP_ROUND_HALF_EVEN);
            $arr["status"]=true;
        }else{
            $arr["status"]=false;
        }
        return $arr;

    }//...get_cant_producto

    public function buscar_producto_pAduana_detalle($id_proveedoresaduana,$id_producto){
        $db=$this->db;
        $db->where("id_producto",$id_producto);
        $db->where("id_proveedoresaduana",$id_proveedoresaduana);

        return $db->get("proveedoresaduana_detalle")->row();
    }//...buscar_producto_pAduana_detalle
      
    public function actualizar_producto_pAduana_detalle($dataUpdate){
        /* 
        $dataUpdate['id_proveedoresaduana']  = $id_proveedoresaduana;
        $dataUpdate['id_producto']            = $id_producto;
        $dataUpdate['cantidad']               = $cantidad;
        $dataUpdate['costo_unitario']         = $costo_unitario;
        $dataUpdate['costo_total']            = $costo_total; 
        */
        $db=$this->db;
        $id_cot=$dataUpdate['id_proveedoresaduana'];
        $id_prod=$dataUpdate['id_producto'];
        return $db->update("proveedoresaduana_detalle", $dataUpdate, 
                            array("id_proveedoresaduana"=>$id_cot,"id_producto"=>$id_prod)
                          );
    }//...actualizar_producto_pAduana_detalle

    public function agregar_producto_pAduana_detalle($data){
        $agregado= $this->Mgeneral->save_register("proveedoresaduana_detalle",$data);
        if($agregado){
            return true;
        }else{
            return false;
        }
    }//...agregar_producto_pAduana_detalle

    public function get_existencias_producto($id_producto){
        return $this->db->select("producto_existencias,producto_existencias_comprometidas_pedido,producto_existencias_comprometidas_compras")
                        ->where("producto_id",$id_producto)
                        ->get("productos")->row();
    }//...get_existencias_producto

    public function actualizar_comprometidos_producto($id_producto,$comprometidos){
        $campo="producto_existencias_comprometidas_compras";
        $query="UPDATE `productos` 
            SET `$campo` = `$campo` + $comprometidos 
            WHERE `producto_id` = '$id_producto'";
        $this->db->query($query);
    }//...actualizar_comprometidos_producto

    private function update_inventario($id_proveedoresaduana,$campo,$multiplicador){
        $provAd_detalle=$this->db->select("id_producto,cantidad")
                            ->where("id_proveedoresaduana",$id_proveedoresaduana)
                            ->get("proveedoresaduana_detalle")
                            ->result();
        foreach ($provAd_detalle as $producto) {

            $query="UPDATE `productos` 
                    SET `$campo` = `$campo` + $producto->cantidad*$multiplicador 
                    WHERE `producto_id` = '$producto->id_producto'";
            $this->db->query($query);
        }
    }//...update_inventario

    public function get_status($id_proveedoresaduana){
        $row = $this->db->select("status")
                    ->where("id",$id_proveedoresaduana)
                    ->get("proveedoresaduana")->row();
                    return $row->status;
    }//...get_status

    public function get_set_ultimo_status($id_proveedoresaduana,$nvo_stat){

        $ultimo_stat=$this->get_status($id_proveedoresaduana);

            //    abierto->cerrado
            //        restar comprometidos, aumentar existencias
            //    abierto->cancelado
            //        restar comprometidos
            //    cerrado->abierto
            //        sumar comprometidos, restar existencias
            //    cerrado->cancelado
            //        restar existencias
            //    cancelado->abierto
            //        sumar comprometidos
            //    cancelado->cerrado
            //        sumar existencias
        if($ultimo_stat==4 && $nvo_stat==5){
            //    abierto->cerrado
            //        restar comprometidos, aumentar existencias
            
            $multipl_exist=1;
            $multipl_compr=-1;
        }

        if($ultimo_stat==4 && $nvo_stat==6){
            //    abierto->cancelado
            //        restar comprometidos
            
            $multipl_exist=0;
            $multipl_compr=-1;
        }

        if($ultimo_stat==5 && $nvo_stat==4){
            //    cerrado->abierto
            //        sumar comprometidos, restar existencias
            
            $multipl_exist=-1;
            $multipl_compr=1;
        }

        if($ultimo_stat==5 && $nvo_stat==6){
            //    cerrado->cancelado
            //        restar existencias
            
            $multipl_exist=-1;
            $multipl_compr=0;
        }

        if($ultimo_stat==6 && $nvo_stat==4){
            //    cancelado->abierto
            //        sumar comprometidos
            
            $multipl_exist=0;
            $multipl_compr=1;
        }

        if($ultimo_stat==6 && $nvo_stat==5){
            //    cancelado->cerrado
            //        sumar existencias
            
            $multipl_exist=1;
            $multipl_compr=0;
        }

        if($multipl_exist!=0){
            $this->update_inventario($id_proveedoresaduana,"producto_existencias",$multipl_exist);
        }
        if($multipl_compr!=0){
            $this->update_inventario($id_proveedoresaduana,"producto_existencias_comprometidas_compras",$multipl_compr);
        } 

            $this->actualizar_fecha_modificacion($id_proveedoresaduana);

            $data["status"]=$nvo_stat;
            return $this->db->update("proveedoresaduana", $data, array("id"=>$id_proveedoresaduana));
                    //return ($result)?$result:false;

    }//...get_set_ultimo_status

    public function eliminar_producto_pAduana_detalle($id_proveedoresaduana,$id_producto){
        
        $eliminado=$this->db->delete("proveedoresaduana_detalle",
                         array("id_proveedoresaduana"=>$id_proveedoresaduana,"id_producto"=>$id_producto
                                ));
        if($eliminado){
            return true;
        }else{
            return false;
        }
    }//...eliminar_producto_pAduana_detalle

    public function actualizar_fecha_modificacion($id_proveedoresaduana){
        $query="UPDATE `proveedoresaduana` SET `fecha_modificacion` = CURRENT_TIMESTAMP WHERE `id` = '$id_proveedoresaduana'";
           $this->db->query($query);
    }//...actualizar_fecha_modificado

    public function get_fecha_modificacion($id_proveedoresaduana){
        $row_compra=$this->db->select("fecha_modificacion")
                            ->where("id",$id_proveedoresaduana)
                            ->get("proveedoresaduana")->row();
        $timestamp=$row_compra->fecha_modificacion;
        $fecha=date('d/m/Y,H:i',strtotime($timestamp));
        return $fecha;
    }//...get_fecha_modificacion
    
    public function importar_productos_de_pCompra_detalle($id_clientespedido,$data_import){
        $cont=0;
        $result=array();
        foreach($data_import as $data){
            $insert=$this->Mgeneral->save_register("proveedoresaduana_detalle", $data);
            $result[$cont]=$insert;
            $cont++;

        }
        
        return $result;
    }//...importar_productos_de_pCompra_detalle
}
