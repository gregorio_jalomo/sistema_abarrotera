
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Compras a proveedores</h3>
    </div>
    <div class="col-md-6">

      

    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="tabla_proveedoresaduana" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio</th>
        <th>Proveedor</th>
        <th>Status</th>
        <th>Usuario del sistema</th>
        <th>Opciones</th>

      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_proveedoresaduana);*/ 
      foreach ($con_proveedoresaduana as $ProvComp) {

        $id_cotizacion              = $ProvComp->id;
        //$fecha_creacion             = explode(" ",$ProvComp->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $fecha_creacion             = $ProvComp->fecha_creacion;
        $folio_documento            = $ProvComp->folio_documento;
        $folio_unico_documento      = $ProvComp->folio_unico_documento;
        $comentario                 = $ProvComp->comentario;
        $status                     = $ProvComp->status;

        $perfil_usuario             = $ProvComp->perfil_usuario;
        $descr_id_usuario           = $ProvComp->descripcion_id_usuario;

        $id_proveedores             = $ProvComp->id_proveedores;

        /* INNERS */
        $cat_estatus_nombre         = $ProvComp->cat_estatus_nombre;
        /* ...INNERS */
        $nombre_proveedor="";
        foreach($proveedores as $prov){
          if($prov->proveedor_id==$id_proveedores){
            $nombre_proveedor=$prov->proveedor_nombre;
          }
        }

        echo
        '<tr id="borrar_'.$id_cotizacion.'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$nombre_proveedor.'</td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$descr_id_usuario.'</td>

          <td>
            <a href="'.base_url().'index.php/proveedoresaduana/proveedoresaduana/ver_detalle/'.$id_cotizacion.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>
            <a href="'.base_url().'index.php/proveedoresaduana/proveedoresaduana/editar/'.$id_cotizacion.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>';
            if ($status==4){  
            echo '<a href="'.base_url().'index.php/proveedoresaduana/proveedoresaduana/cambiar_status/'.$id_cotizacion.'/6" class="eliminar_relacion" flag="'.$id_cotizacion.'" id="delete'.$id_cotizacion.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Cancelar cotizacion"><i class="fas fa-trash-alt"></i> Borrar</button>';
            
            }
            echo '
          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>
  $(document).ready(function() {

    $('#menuproveedoresaduana').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_proveedoresaduana").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": false
    });


    $(".eliminar_relacion").click(function(event){
          event.preventDefault();
          bootbox.dialog({
          message: "Desea eliminar el registro?",
          closeButton: true,
          buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $("#borrar_"+id).slideUp();
                                $.get(url);
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
    });

  });//document ready

</script>
