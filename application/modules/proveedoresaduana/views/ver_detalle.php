<?php
//...datos_cotizacion
$id_cotizacion              = $proveedoresaduana->id;
$fecha_creacion             = explode(" ",$proveedoresaduana->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
$folio_documento            = $proveedoresaduana->folio_documento;
$folio_unico_documento      = $proveedoresaduana->folio_unico_documento;
$comentario                 = $proveedoresaduana->comentario;
$status                     = $proveedoresaduana->status;

$perfil_usuario             = $proveedoresaduana->perfil_usuario;
$descr_id_usuario           = $proveedoresaduana->descripcion_id_usuario;

/* INNERS */

  $cat_estatus_nombre         = $proveedoresaduana->cat_estatus_nombre;
/* ...INNERS */
switch ($status) {
  case 4:
    $status_text="Abierta";
    $badge="success";
    break;
  case 5:
    $status_text="Cerrada";
    $badge="secondary";
    break;
  case 6:
    $status_text="Cancelada";
    $badge="danger";
    break;
}
//...datos_cotizacion
?>
<style>
.ul-costo{
  /* text-align:center; */
  padding-inline-start: 0px!important;
}
.badge{
  float:right;
}
.height-extended{
  height: 100%;
}
.div-info-cot-total{
    background-color: rgba(23,162,184,.2);
    border-radius: 5px;
}
</style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> <b>Cotizacion:</b> "<?=$folio_documento?>" <span class="badge bg-<?=$badge?>"><?=$status_text?></span></h5>
          <div>
            <b>FUNI:</b> "<?=$folio_unico_documento?>",  
            <b>Fecha de creacion:</b> <?=$fecha_creacion[0]." ".$fecha_creacion[1]?>,  
            <b>Creada por:</b> <?=$descr_id_usuario?>
          </div>
          <span><b>Comentarios:</b> <?=$comentario?></span>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-3 mb-3">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-7 border-right">
                  <!-- title row -->
                  <div class="row height-extended">


                </div>
                <!-- /.col-6 -->
                <!-- title row -->
                <div class="col-5">
                  <div class="row">
                    <div class="col-12">
                      <h4>

                      </h4>
                    </div>
                    <!-- /.col -->
                  </div>
                  <div class="row div-info-cot-total">

                  </div>

                </div><!-- /.col5 -->
              </div>
            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>

                    <th>Nombre</th>
                    <th>Unidad</th>
                    <th>Cant.</th>

                </tr>
                </thead>
                <tbody id="productos_cotizacion">
                <?php
                  foreach ($proveedoresaduana_detalle as $producto) {
                    echo "
                    <tr>
                      <td>$producto->nombre_producto</td>
                      <td>$producto->descripcion_de_la_unidad</td>
                      <td>$producto->cantidad</td>
                    </tr>
                    ";
                  }
                ?>
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');


  });
</script>
