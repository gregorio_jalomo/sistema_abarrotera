<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\IOFactory;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

  class MyReadFilter implements IReadFilter
  {
      private $startRow = 0;
  
      private $endRow = 0;
  
      private $columns = [];
  
      public function __construct($startRow, $endRow, $columns)
      {
          $this->startRow = $startRow;
          $this->endRow = $endRow;
          $this->columns = $columns;
      }
  
      public function readCell($column, $row, $worksheetName = '')
      {
          if ($row >= $this->startRow && $row <= $this->endRow) {
              if (in_array($column, $this->columns)) {
                  return true;
              }
          }
  
          return false;
      }
  }
class Proveedorescompra extends MX_Controller {

  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproveedorescompra', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('proveedorescompra/Proveedorescompra_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 12;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);
  }

  public function cargar_vista($url,$data,$archivos_js){
    
    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function add_comp_xlsx($id_proveedorescompra){

    $cotizacion       = $this->Proveedorescompra_model->get_proveedorescompra_by_id($id_proveedorescompra);
    $folio_documento  = $cotizacion->folio_documento;

    if(strpos($cotizacion->id_proveedores,",")){
      $arr_id_prov=preg_split("/[\s,]+/", $cotizacion->id_proveedores);
    }else{
      $arr_id_prov=array($cotizacion->id_proveedores);
    }

    $cot_detalle      = $this->get_proveedorescompra_detalle($id_proveedorescompra,"return","nombre_producto","ASC");
    $spreadsheet      = new Spreadsheet();
    
    //$workSheet = new Spreadsheet();
    $workSheet=$spreadsheet;
    $workSheet->getActiveSheet()->getTabColor()->setRGB('17a2b8');// tab color

    //head
      $HeadTableStyle = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS,
        ],
        'borders' => [
            'top' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation' => 90,
            'startColor' => [
                'argb' => '17a2b8',
            ],
            'endColor' => [
                'argb' => 'FFFFFFFF',
            ],
        ],
      ];
      $workSheet->getActiveSheet()->getStyle('A3:D3')->applyFromArray($HeadTableStyle);
      $workSheet
      ->getActiveSheet()
      ->fromArray(
        ["Nombre del producto", "Unidad","Cantidad","Comentarios"],
        null,
        'A3'
      );
    //...head

    //productos
      //width columnas
      //$workSheet->getActiveSheet()->getDefaultColumnDimension("D")->setWidth(30); //setea todas las columnas
      $workSheet->getActiveSheet()->getColumnDimension("A")->setWidth(50); //col nombre producto
      $workSheet->getActiveSheet()->getColumnDimension("B")->setWidth(20); //col unidad
      $workSheet->getActiveSheet()->getColumnDimension("C")->setWidth(20); //col cantidad
      $workSheet->getActiveSheet()->getColumnDimension("D")->setWidth(40); //col comentarios


      //nuevo array de productos solo con las columnas requeridas
      $arr_prod=array();
      foreach ($cot_detalle as $p) {//extraer columnas del array proveniente de la bd
        array_push($arr_prod,[
                                $p->nombre_producto,
                                $p->descripcion_de_la_unidad,
                                $p->cantidad,
                                ""//columna comentariosseditable por el proveedor
                             ]);
      }

      //insertar array de productos
      $cont=4;
      foreach ($arr_prod as $prod) {
        $workSheet
          ->getActiveSheet()
          ->fromArray(
            $prod,
            null,
            'A'.$cont
          );
          $cont++;
      }//...for productos2
      $cont-=1;

      /* $precioStyles=[
        'borders' => [
          'all' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
          ]
        ]
      ];
      $workSheet->getActiveSheet()->getStyle('D4:D'.$cont)->applyFromArray($precioStyles); */
      //agregar color distintivo a la columna precio
      /* $spreadsheet->getActiveSheet()->getStyle('D4:D'.$cont)->getFill()
      ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
      ->getStartColor()->setARGB('d9f2ff'); */
    //...productos


    //crear archivos

      $successFiles=array();
      $date=date("Y-m-d");
      $o=array();
      $cont_prov=0;
      //prin_r($arr_id_prov);
      foreach ($arr_id_prov as $proveedor_id) {//for proveedores
        
        $prov=$this->Proveedorescompra_model->get_info_proveedor($proveedor_id);

        $workSheet
          ->getActiveSheet()
          ->fromArray(
            ["Proveedor","Num. productos","Folio"],
            null,
            'A1'
          );
        $workSheet
          ->getActiveSheet()
          ->fromArray(
            [$prov->proveedor_nombre,$cont-3,$folio_documento],
            null,
            'A2'
          );
        //propiedades de archivo
        $spreadsheet
          ->getProperties()
          ->setCreator("Abarrotera grova")
          ->setLastModifiedBy($cotizacion->descripcion_id_usuario)
          ->setTitle($folio_documento)
          ->setSubject($prov->proveedor_nombre)
          ->setDescription('Compra a proveedor')
          ->setKeywords('')
          ->setCategory('PROVCOMP');

        //nombre de archivo
        $nom=str_replace (".","","$folio_documento"."_".$date."_"."$prov->proveedor_nombre");

        $saveRoute="documentos/excel/downloads/$nom.xlsx";

        //guardar
        $writer = new Xlsx($workSheet);
        $writer->save($saveRoute);//void function in Xlsx Class
        $o["download"]="";
        $o["download"]=base_url($saveRoute);
        $o["filename"]=$nom;

        $successFiles[$cont_prov]=array();
        $successFiles[$cont_prov]=$o;
        //$successFiles = (object) $successFiles;
        $cont_prov++;
      }//...for_proveedores
    //...crear archivos
    echo json_encode($successFiles);
  }//...add_comp_xlsx

  public function upload_comp_xlsx(){
    
    try {
      //limit 41943040
      $dir_subida = 'documentos/excel/uploads/';
      $fichero_subido = $dir_subida . basename($_FILES['file']['name']);
      if(file_exists($fichero_subido)){
          unlink($fichero_subido);
      }
      move_uploaded_file($_FILES['file']['tmp_name'], $fichero_subido);
      
    } catch (\Throwable $th) {
      throw $th;
    }
  }//...upload_comp_xlsx

  public function read_file_xml(){

    $data['arr_files'] = $this->input->post("arr_files");

    /* $arr_filenames=array(
      "PROVCOMP201_2020-10-28_ABARROTERA DEL DUERO SA DE CV.xlsx",
      "PROVCOMP201_2020-10-28_ABARROTERA ESPINOZA.xlsx"
    ); */
    $arr_filenames=$data['arr_files'];

    $_SESSION["to_xml_view"]=$data;
    $this->comparar_xml();

  }//... read_file_xml

  public function upload_file_xml(){
    
    try {
      //limit 41943040
      $dir_subida = 'documentos/xml/uploads/';
      $fichero_subido = $dir_subida . basename($_FILES['file']['name']);
      if(file_exists($fichero_subido)){
          unlink($fichero_subido);
      }
      move_uploaded_file($_FILES['file']['tmp_name'], $fichero_subido);
      
    } catch (\Throwable $th) {
      throw $th;
    }
  }//...upload_file_xml

  public function comparar_xml(){
    //echo json_encode($_SESSION["to_xml_view"]);
    $data['arr_files']=$this->input->post("arr_files");
    $arr_filenames=$data['arr_files'];

    foreach ($arr_filenames as $filename) {

      if(isset($_SESSION["to_xml_view"])){
        $data=$_SESSION["to_xml_view"];
        
        $inputFileName ='documentos/xml/uploads/'.$filename;
        if (file_exists($inputFileName)) {
          $xml_obj  = simplexml_load_file($inputFileName);
          $xml_obj  -> registerXPathNamespace('prefix', 'http://www.sat.gob.mx/cfd/3');
          $cfdi_conceptos  = $xml_obj->xpath("//prefix:Concepto");
          $cfdi_emisor  = $xml_obj->xpath("//prefix:Emisor");
          $cfdi_comprobante  = $xml_obj->xpath("//prefix:Comprobante");

          $arr_result=array(
            "conceptos"=>array(),
            "emisor"=>array()
          );

          $arr_result["emisor"]=$cfdi_emisor;
          $arr_result["comprobante"]=$cfdi_comprobante;

          foreach ($cfdi_conceptos as $key => $concepto) {
            $attr=$concepto->attributes();

            array_push($arr_result["conceptos"],$attr);

          }

          echo json_encode($arr_result);

          } else {
              exit($inputFileName);
          }
      
      }
    }


  }//...comparar_xml

  public function up_files_xml($id_compra=""){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,56) || $this->is_admin ){

      $data['flecha_ir_atras'] = "proveedorescompra/editar/".$id_compra;
      $data['folio_unico_documento']="";
      if($id_compra!=""){
        $info_compra = $this->Proveedorescompra_model->get_proveedorescompra_by_id($id_compra);
        
        $data['folio_unico_documento']=(isset($info_compra->folio_unico_documento))?$info_compra->folio_unico_documento:"";
      }
      $_SESSION["to_comp_view"]=null;
      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );
      $this->cargar_vista('proveedorescompra/up_files_xml',$data,$archivos_js);
    
    }else{
      $this->p_denied_view("agregar compras de proveedores","");
    }


  }//...up_files

  public function crear_folio_prov_compra($FUNI){
    echo json_encode(crear_foliodocumento("PROVCOMP",$FUNI));
  }//...crear_folio_compra

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,56) || $this->is_admin ){

      //recibir post del ajax
      $dataProveedorescompra['folio_documento']         = $this->input->post('nuevo_folio_documento');
      $dataProveedorescompra['folio_unico_documento']   = $this->input->post('nuevo_FUNI_documento');
      $dataProveedorescompra['perfil_usuario']          = $this->session->userdata['infouser']['id'];
      $dataProveedorescompra['descripcion_id_usuario']  = $this->session->userdata['infouser']['nombre'];

      $foliosdocumentos_id      = $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI = $this->input->post('id_FUNI_foliosdocumentos');

      $nombre_proveedor         = $this->input->post('proveedor_nombre');
      $info_proveedor           = $this->get_info_proveedor_by_name(isset($nombre_proveedor) ? $nombre_proveedor : "###");
      $id_proveedor             = $info_proveedor->proveedor_id;
      $dataProveedorescompra['id_proveedores']  = (isset($id_proveedor)) ? $id_proveedor : "";

      //insert a proveedorescompra
      $proveedorescompra_id=$this->guardar($dataProveedorescompra);

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que una cotizacion se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }
      
      $obj_productos            = $this->input->post('obj_productos');

      if(isset($obj_productos)){
        foreach ($obj_productos as $producto) {
          $info_producto = $this->get_info_producto_by_name($producto["nombre_producto"]);

          $data_prod["id_proveedorescompra"]      =  $proveedorescompra_id;
          $data_prod["id_producto"]               =  $info_producto->producto_id;
          $data_prod["nombre_producto"]           =  $info_producto->producto_nombre;
          $data_prod["descripcion_de_la_unidad"]  =  $info_producto->unidad_tipo;
          $data_prod["cantidad"]                  =  $producto["cantidad"];
          $data_prod["costo_unitario"]            =  $producto["costo_unitario"];

          $this->Mgeneral->save_register("proveedorescompra_detalle", $data_prod);
        }//...foreach $obj_productos

      }//...if isset($obj_productos)

      echo json_encode($proveedorescompra_id);    

    }else{
      echo json_encode(false);
    }

  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('proveedorescompra', $data);
  }//...guardar

  public function editar($id_proveedorescompra){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,59) || $this->is_admin ){

      $data['titulo_seccion'] = "Compra de proveedor";
      $data['flecha_ir_atras'] = "proveedorescompra/listar_proveedorescompra";
      $data['force_landscape']=true;

      $data['proveedorescompra'] = $this->Proveedorescompra_model->get_proveedorescompra_by_id($id_proveedorescompra);
      //$data['proveedorescondicion'] =$this->Proveedorescompra_model->get_proveedores_condicion($data['proveedorescompra']->id_proveedores);
      //$data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['productos'] = $this->Proveedorescompra_model->get_productos('productos.cat_estatus_id',1);

      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data["fecha_creacion"]         = date("Y-m-d H:i:s");
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js',
        //'statics/js/libraries/filesaverjs/fileSaver.js',
        'statics/js/libraries/sheetjs/xlsx.js'
      );

      $this->cargar_vista('proveedorescompra/editar',$data,$archivos_js);

    }else{
      $this->p_denied_view("editar compra(proveedores)","");
    }
    
  }//...editar

  public function read_xml(){
    $archivos_js=array(
      'statics/js/bootbox.min.js',
        'statics/js/general.js',
        'statics/bootstrap4/js/bootstrap.min.js'
    );

    $this->cargar_vista('proveedorescompra/read_xml',$data,$archivos_js);
  }//...read_xml

  public function ver_detalle($id_proveedorescompra){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,57) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de la compra";
      $data['flecha_ir_atras'] = "proveedorescompra/proveedorescompra/listar_proveedorescompra";

      $data['proveedorescompra'] = $this->Proveedorescompra_model->get_proveedorescompra_by_id($id_proveedorescompra);
      $data['proveedorescompra_detalle'] =$this->Proveedorescompra_model->get_proveedorescompra_detalle($id_proveedorescompra);

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedorescompra/ver_detalle',$data,$archivos_js);
    }else{
      $this->p_denied_view("ver detalle de compra(proveedores)","");
    }

  }//...ver_detalle

  public function listar_proveedorescompra(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,58) || $this->is_admin ){

      $data['titulo_seccion'] = "Compras de Proveedores ";
      $data['con_proveedorescompra'] = $this->Proveedorescompra_model->listar_proveedorescompra();
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data['force_landscape']=true;

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('proveedorescompra/listar',$data,$archivos_js);

    }else{
      $this->p_denied_view("listar compras(proveedores)","");
    }

  }//...listar_proveedorescompra

  public function get_proveedorescompra_detalle($id_proveedorescompra,$request_type="",$order_by="id",$order_type="DESC"){
    $do=$this->Proveedorescompra_model->get_proveedorescompra_detalle($id_proveedorescompra,$order_by,$order_type);
    if($request_type=="return"){
      return $do;
    }else{
      echo json_encode($do);
    }

  }//...get_proveedorescompra_detalle

  public function get_info_proveedor_by_name($prov_name){
    return $this->Proveedorescompra_model->get_info_proveedor_by_name($prov_name);
  }//...get_info_proveedor

  public function get_info_proveedor(){
    $proveedor_id=$this->input->post('proveedor_id');

    echo json_encode($this->Proveedorescompra_model->get_info_proveedor($proveedor_id));
  }//...get_info_proveedor

  public function cambiar_status($id_proveedorescompra,$estatus_id) {
    $data['status'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,59) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,60) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){
      $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
      $this->Mgeneral->update_table_row('proveedorescompra',$data,'id',$id_proveedorescompra);  
    }
    echo json_encode($go);
  }//...cambiar_status

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
    $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_proveedores_destinatario(){
    $id_proveedorescompra=$this->input->post('id_proveedorescompra');
    $data['id_proveedores']=$this->input->post('id_proveedores');

    $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
    $this->Mgeneral->update_table_row("proveedorescompra",$data,"id",$id_proveedorescompra);
  }//...update_proveedores_destinatari

  public function get_info_producto_by_name($prov_name){
    return $this->Proveedorescompra_model->get_info_producto_by_name($prov_name);
  }//...get_info_producto

  public function agregar_producto(){

    $this->form_validation->set_rules('id_proveedorescompra', ' ID de la cotización', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    
    $id_proveedorescompra=$this->input->post('id_proveedorescompra');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    if($this->Proveedorescompra_model->buscar_producto_pCompra_detalle($id_proveedorescompra,$id_producto)){

      $dataUpdate['id_proveedorescompra']  = $id_proveedorescompra;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      /* $dataUpdate['costo_unitario']         = $costo_unitario;
      $dataUpdate['costo_total']            = $costo_total; */

      echo json_encode($this->Proveedorescompra_model->actualizar_producto_pCompra_detalle($dataUpdate));

    }else{

    $dataAdd['id_proveedorescompra']  = $id_proveedorescompra;
    $dataAdd['id_producto']            = $id_producto;
    $dataAdd['nombre_producto']        = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']   = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']               = $cantidad;

    echo json_encode($this->Proveedorescompra_model->agregar_producto_pCompra_detalle($dataAdd));
    }
    $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
  }//...agregar_producto

  public function eliminar_producto($id_proveedorescompra,$id_producto){
    $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
    echo json_encode($this->Proveedorescompra_model->eliminar_producto_pCompra_detalle($id_proveedorescompra,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Proveedorescompra_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_proveedorescompra=$this->input->post('id_proveedorescompra');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Proveedorescompra_model->actualizar_fecha_modificacion($id_proveedorescompra);
    $this->Mgeneral->update_table_row('proveedorescompra',$data,'id',$id_proveedorescompra);

  }//...agregar_comentarios

  public function get_fecha_modificacion(){
    $id_proveedorescompra=$this->input->post('id_compra');
    echo json_encode($this->Proveedorescompra_model->get_fecha_modificacion($id_proveedorescompra));
  }//...get_fecha_modificacion

  public function importar_datos_de_pCotizacion($folio_unico_documento,$proveedorescompra_id){

    echo json_encode($this->Proveedorescompra_model->importar_datos_de_pCotizacion($folio_unico_documento,$proveedorescompra_id));
  }//...importar_datos_de_pCotizacion

  public function importar_productos_de_pCotizacion_detalle(){
    $folio_unico_documento      = $this->input->post('folio_unico_documento');
    $id_proveedorescompra       = $this->input->post('id_proveedorescompra');
    $id_proveedorescompra       = $this->Proveedorescompra_model->get_id_clientescotizacion_by_FUNI($folio_unico_documento);
    $result_clientescotizacion  = $this->Proveedorescompra_model->get_clientescotizacion_detalle($id_proveedorescompra);
    //$data_import["folio_unico_documento"]           =$row_clientespedido->folio_unico_documento;
    
    $cont=0;
    foreach($result_clientescotizacion as $row_cot){
      $data_import[$cont]["id_proveedorescompra"]      =  $id_proveedorescompra;
      $data_import[$cont]["id_producto"]               =  $row_cot->id_producto;
      $data_import[$cont]["nombre_producto"]           =  $row_cot->nombre_producto;
      $data_import[$cont]["codigo_barras_producto"]    =  $row_cot->codigo_barras_producto;
      $data_import[$cont]["descripcion_de_la_unidad"]  =  $row_cot->descripcion_de_la_unidad;
      $data_import[$cont]["cantidad"]                  =  $row_cot->cantidad;
      $data_import[$cont]["costo_unitario"]            =  $row_cot->costo_unitario;
      $cont++;
    }

    echo json_encode($this->Proveedorescompra_model->importar_productos_de_pCotizacion_detalle($id_clientespedido,$data_import));
  }//...importar_productos_de_pCotizacion_detalle

}
