<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\IOFactory;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

  class MyReadFilter implements IReadFilter
  {
      private $startRow = 0;
  
      private $endRow = 0;
  
      private $columns = [];
  
      public function __construct($startRow, $endRow, $columns)
      {
          $this->startRow = $startRow;
          $this->endRow = $endRow;
          $this->columns = $columns;
      }
  
      public function readCell($column, $row, $worksheetName = '')
      {
          if ($row >= $this->startRow && $row <= $this->endRow) {
              if (in_array($column, $this->columns)) {
                  return true;
              }
          }
  
          return false;
      }
  }
class Proveedorescotizacion extends MX_Controller {

  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproveedorescotizacion', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('proveedorescotizacion/Proveedorescotizacion_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 10;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view


  public function add_cot_xlsx($id_cotizacion){

    $cotizacion       = $this->Proveedorescotizacion_model->get_proveedorescotizacion_by_id($id_cotizacion);
    $folio_documento  = $cotizacion->folio_documento;

    if(strpos($cotizacion->id_proveedores,",")){
      $arr_id_prov=preg_split("/[\s,]+/", $cotizacion->id_proveedores);
    }else{
      $arr_id_prov=array($cotizacion->id_proveedores);
    }

    $cot_detalle      = $this->get_proveedorescotizacion_detalle($id_cotizacion,"return","nombre_producto","ASC");
    $spreadsheet      = new Spreadsheet();

    /* $spreadsheet
              ->getActiveSheet()
              ->setCellValueByColumnAndRow(1, 1, "Valor en la posición 1, 1")
              ->setCellValue("B2", "Valor en celda B2"); */
    
    //$workSheet = new Spreadsheet();
    $workSheet=$spreadsheet;
    $workSheet->getActiveSheet()->getTabColor()->setRGB('17a2b8');// tab color

    //encabezado

    //head
      $HeadTableStyle = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS,
        ],
        'borders' => [
            'top' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation' => 90,
            'startColor' => [
                'argb' => '17a2b8',
            ],
            'endColor' => [
                'argb' => 'FFFFFFFF',
            ],
        ],
      ];
      $workSheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($HeadTableStyle);
      $workSheet
      ->getActiveSheet()
      ->fromArray(
        ["Nombre del producto", "Unidad","Cantidad","Precio unitario","Comentarios"],
        null,
        'A3'
      );
    //...head

    //productos
      //width columnas
      //$workSheet->getActiveSheet()->getDefaultColumnDimension("D")->setWidth(30); //setea todas las columnas
      $workSheet->getActiveSheet()->getColumnDimension("A")->setWidth(50); //col nombre producto
      $workSheet->getActiveSheet()->getColumnDimension("B")->setWidth(12); //col unidad
      $workSheet->getActiveSheet()->getColumnDimension("D")->setWidth(20); //col precio unitario
      $workSheet->getActiveSheet()->getColumnDimension("E")->setWidth(40); //col comentarios


      //nuevo array de productos solo con las columnas requeridas
      $arr_prod=array();
      foreach ($cot_detalle as $p) {//extraer columnas del array proveniente de la bd
        array_push($arr_prod,[
                                $p->nombre_producto,
                                $p->descripcion_de_la_unidad,
                                $p->cantidad,
                                "",//columna precio editable por el proveedor
                                ""//columna comentariosseditable por el proveedor
                             ]);
      }

      //insertar array de productos
      $cont=4;
      foreach ($arr_prod as $prod) {
        $workSheet
          ->getActiveSheet()
          ->fromArray(
            $prod,
            null,
            'A'.$cont
          );
          $cont++;
      }//...for productos2
      $cont-=1;

      $precioStyles=[
        'borders' => [
          'all' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
          ]
        ]
      ];
      $workSheet->getActiveSheet()->getStyle('D4:D'.$cont)->applyFromArray($precioStyles);
      //agregar color distintivo a la columna precio
      $spreadsheet->getActiveSheet()->getStyle('D4:D'.$cont)->getFill()
      ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
      ->getStartColor()->setARGB('d9f2ff');
    //...productos


    //crear archivos

      $successFiles=array();
      $date=date("Y-m-d");
      $o=array();
      $cont_prov=0;
      foreach ($arr_id_prov as $proveedor_id) {//for proveedores
        
        $prov=$this->Proveedorescotizacion_model->get_info_proveedor($proveedor_id);

        $workSheet
          ->getActiveSheet()
          ->fromArray(
            ["Proveedor","Num. productos","Folio"],
            null,
            'A1'
          );
        $workSheet
          ->getActiveSheet()
          ->fromArray(
            [$prov->proveedor_nombre,$cont-3,$folio_documento],
            null,
            'A2'
          );
        //propiedades de archivo
        $spreadsheet
          ->getProperties()
          ->setCreator("Abarrotera grova")
          ->setLastModifiedBy($cotizacion->descripcion_id_usuario)
          ->setTitle($folio_documento)
          ->setSubject($prov->proveedor_nombre)
          ->setDescription('Cotizacion de proveedor')
          ->setKeywords('')
          ->setCategory('COTPROV');

        //nombre de archivo
        $nom=str_replace (".","","$folio_documento"."_".$date."_"."$prov->proveedor_nombre");
        $saveRoute="documentos/excel/downloads/".$nom.".xlsx";

        //guardar
        $writer = new Xlsx($workSheet);
        $writer->save($saveRoute);//void function in Xlsx Class
        $o["download"]="";
        $o["download"]=base_url($saveRoute);
        $o["filename"]=$nom;

        $successFiles[$cont_prov]=array();
        $successFiles[$cont_prov]=$o;
        //$successFiles = (object) $successFiles;
        $cont_prov++;
      }//...for_proveedores
    //...crear archivos
    echo json_encode($successFiles);
  }//...add_cot_xlsx

  public function upload_cot_xlsx(){
    
    try {
      //limit 41943040
      $dir_subida = 'documentos/excel/uploads/';
      $fichero_subido = $dir_subida . basename($_FILES['file']['name']);
      if(file_exists($fichero_subido)){
          unlink($fichero_subido);
      }
      move_uploaded_file($_FILES['file']['tmp_name'], $fichero_subido);
      
    } catch (\Throwable $th) {
      throw $th;
    }
  }

  public function read_cot_xlsx($id_cotizacion){
    $data['files_notfound']=array();
    $data['files_ok']=array();
    $data['err_count']=0;
    $data['arr_files']=$this->input->post("arr_files");

    /* $arr_filenames=array(
      "PROVCOT201_2020-10-28_ABARROTERA DEL DUERO SA DE CV.xlsx",
      "PROVCOT201_2020-10-28_ABARROTERA ESPINOZA.xlsx"
    ); */
    $arr_filenames=$data['arr_files'];
    $inputFileType = 'Xlsx';
    $sheetname = 'Worksheet';

    $CotInfo=$this->Proveedorescotizacion_model->get_proveedorescotizacion_by_id($id_cotizacion);
    $data['foliodocumento_db']=$CotInfo->folio_documento;

    $data['cont_productos']=$this->Proveedorescotizacion_model->count_proveedorescotizacion_detalle($id_cotizacion);

    foreach ($arr_filenames as $filename) {
      $inputFileName ='documentos/excel/uploads/'.$filename;
      if(file_exists($inputFileName)){

        $reader = IOFactory::createReader($inputFileType);        //crear el reader para archivos xlsx
        $reader->setLoadSheetsOnly($sheetname);                   //solo leer hoja de trabajo "Worksheet"

        $filterSubset = new MyReadFilter(1, $data['cont_productos']+3, range('A', 'E')); //configurar el filtro de lectura (productos)
        $reader->setReadFilter($filterSubset);

        $spreadsheet = $reader->load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        //var_dump($sheetData);
        array_push($data['files_ok'],$sheetData);
        
      }else{
        $data['err_count']++;
        array_push($data['files_notfound'],$filename);
      }
      //return $data; 
    }//... foreach $arr_filenames
   
    $_SESSION["to_comp_view"]=$data;

  }

  public function comparar($id_cotizacion){
    if(isset($_SESSION["to_comp_view"])){
      $data=$_SESSION["to_comp_view"];
      
      $data['flecha_ir_atras'] = "proveedorescotizacion/up_files/".$id_cotizacion;
      $data["id_cotizacion"]=$id_cotizacion;
      $data['ProvCot']=$this->Proveedorescotizacion_model->get_proveedorescotizacion_by_id($id_cotizacion);
      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );
      $this->cargar_vista('proveedorescotizacion/comparar',$data,$archivos_js);
    }else{
      redirect("proveedorescotizacion/up_files");
    }

  }//...comparar

  public function up_files($id_cotizacion){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,46) || $this->is_admin ){

        //$data=$this->read_cot_xlsx($id_cotizacion,$arr_filenames);
        $_SESSION["to_comp_view"]=null;
        $data["id_cotizacion"]=$id_cotizacion;
        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );
        $this->cargar_vista('proveedorescotizacion/up_files',$data,$archivos_js);

    }else{
      $this->p_denied_view("agregar cotizacion","");
    }


  }//...up_files

  public function incrementar_foliosdocumentos($foliosdocumentos_id){
      
    //consulta la tabla foliosdocumentos con el prefijo_id del ajax
    $foliosdocumentos_row=$this->Mgeneral->get_row("foliodocumento_id",$foliosdocumentos_id,"foliosdocumentos");

    //update a foliodocumento_folio de la tabla foliosdocumentos
    $dataprefijo["foliodocumento_folio"]=$foliosdocumentos_row->foliodocumento_folio + 1;
    $this->Mgeneral->update_table_row("foliosdocumentos",$dataprefijo,"foliodocumento_id",$foliosdocumentos_row->foliodocumento_id);
  }

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,46) || $this->is_admin ){

      //recibir post del ajax
      $dataProveedorescotizacion['folio_documento'] = $this->input->post('nuevo_folio_documento');
      $dataProveedorescotizacion['folio_unico_documento'] = $this->input->post('nuevo_FUNI_documento');
      $dataProveedorescotizacion['perfil_usuario'] = $this->session->userdata['infouser']['id'];
      $dataProveedorescotizacion['descripcion_id_usuario'] = $this->session->userdata['infouser']['nombre'];

      $foliosdocumentos_id= $this->input->post('id_foliosdocumentos');
      $foliosdocumentos_id_FUNI= $this->input->post('id_FUNI_foliosdocumentos');


      //insert a proveedorescotizacion
      $proveedorescotizacion_id=$this->guardar($dataProveedorescotizacion);

      $this->incrementar_foliosdocumentos($foliosdocumentos_id);//incrementar prefijo del documento

      if($foliosdocumentos_id_FUNI != "false"){
        /* Si $foliosdocumentos_id_FUNI es false quiere decir que una cotizacion se está enviando
            a pedidos, por lo cual NO DEBE INCREMENTARSE el contador del prefijo FUNI
            */
        $this->incrementar_foliosdocumentos($foliosdocumentos_id_FUNI);//incrementar prefijo FUNI
      }
      
      echo json_encode($proveedorescotizacion_id);

    }else{
      echo json_encode(false);
    }


  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('proveedorescotizacion', $data);
  }//...guardar

  public function editar($id_cotizacion){//vista
    if(validar_permiso($this->rol_usuario,$this->id_modulo,49) || $this->is_admin ){

      $data['titulo_seccion'] = "Cotizacion de proveedores";
      $data['flecha_ir_atras'] = "proveedorescotizacion/listar_proveedorescotizacion";
      $data['force_landscape']=true;

      $data['proveedorescotizacion'] = $this->Proveedorescotizacion_model->get_proveedorescotizacion_by_id($id_cotizacion);
      //$data['proveedorescondicion'] =$this->Proveedorescotizacion_model->get_proveedores_condicion($data['proveedorescotizacion']->id_proveedores);
      //$data['productos'] = $this->Mgeneral->get_result('cat_estatus_id',1,'productos');
      $data['productos'] = $this->Proveedorescotizacion_model->get_productos_filt_pReorden('productos.cat_estatus_id',1);

      $data['unidades'] = $this->Mgeneral->get_result('cat_estatus_id',1,'unidad');
      $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');
      $data['familia'] = $this->Mgeneral->get_result('cat_estatus_id',1,'familia');
      $data['agentesventa'] = $this->Mgeneral->get_result('cat_estatus_id',1,'agentesventa');
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data["fecha_creacion"]         = date("Y-m-d H:i:s");
      
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js?v='.time(),
        'statics/bootstrap4/js/bootstrap.min.js',
        'statics/js/libraries/contextMenu/jquery_ui_position.min.js',
        'statics/js/libraries/contextMenu/jquery_contextMenu.min.js',
        //'statics/js/libraries/filesaverjs/fileSaver.js',
        'statics/js/libraries/sheetjs/xlsx.js'
      );

      $this->cargar_vista('proveedorescotizacion/editar',$data,$archivos_js);

    }else{
      $this->p_denied_view("editar cotizacion(proveedores)","");
    }

    
  }//...editar

  public function ver_detalle($id_proveedorescotizacion){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,48) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle de la cotizacion";
      $data['flecha_ir_atras'] = "proveedorescotizacion/proveedorescotizacion/listar_proveedorescotizacion";

      $data['proveedorescotizacion'] = $this->Proveedorescotizacion_model->get_proveedorescotizacion_by_id($id_proveedorescotizacion);
      $data['proveedorescotizacion_detalle'] =$this->Proveedorescotizacion_model->get_proveedorescotizacion_detalle($id_proveedorescotizacion);

      $string_proveedores         = $data['proveedorescotizacion']->id_proveedores;

      if(strpos($string_proveedores,",")){
        $arr_id_prov=preg_split("/[\s,]+/", $string_proveedores);
      }else{
        $arr_id_prov=array($string_proveedores);
      }
      $arr_nombres_proveedores=array();
      foreach ($arr_id_prov as $id_proveedor) {
        $info_proveedor=$this->get_info_proveedor($id_proveedor);
        array_push($arr_nombres_proveedores,$info_proveedor->proveedor_nombre);
      }
      $data["nombres_proveedores"]=$arr_nombres_proveedores;
      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('proveedorescotizacion/ver_detalle',$data,$archivos_js);      

    }else{
      $this->p_denied_view("ver detalle de cotizaciones(proveedores)","");
    }

  }//...ver_detalle

  public function listar_proveedorescotizacion(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,47) || $this->is_admin ){

      $data['titulo_seccion'] = "Cotizaciónes de Proveedores ";
      $data['con_proveedorescotizacion'] = $this->Proveedorescotizacion_model->listar_proveedorescotizacion();
      $data['proveedores'] = $this->Mgeneral->get_result('cat_estatus_id',1,'proveedores');
      $data['force_landscape']=true;

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('proveedorescotizacion/listar',$data,$archivos_js);      

    }else{
      $this->p_denied_view("listar cotizaciones(proveedores)","");
    }

  }//...listar_proveedorescotizacion

  public function get_proveedorescotizacion_detalle($id_proveedorescotizacion,$request_type="",$order_by="id",$order_type="DESC"){
    $do=$this->Proveedorescotizacion_model->get_proveedorescotizacion_detalle($id_proveedorescotizacion,$order_by,$order_type);
    if($request_type=="return"){
      return $do;
    }else{
      echo json_encode($do);
    }

  }//...get_proveedorescotizacion_detalle

  public function get_info_proveedor($proveedor_id=""){
    if($proveedor_id!=""){
     return $this->Proveedorescotizacion_model->get_info_proveedor($proveedor_id);
    }else{
      $proveedor_id=$this->input->post('proveedor_id');
      echo json_encode($this->Proveedorescotizacion_model->get_info_proveedor($proveedor_id));
    }

  }//...get_info_proveedor

  public function cambiar_status($id_cotizacion,$estatus_id) {
    $data['status'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,49) || $this->is_admin) && in_array($data['status'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,50) || $this->is_admin) && in_array($data['status'],$this->opc_del)){
      $go=true;
    }
    if($go){
      $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
      $this->Mgeneral->update_table_row('proveedorescotizacion',$data,'id',$id_cotizacion);      
    }
    echo json_encode($go);
  }//...cambiar_status

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
    $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_proveedores_destinatario(){
    $id_cotizacion=$this->input->post('id_cotizacion');
    $data['id_proveedores']=$this->input->post('id_proveedores');

    $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    $this->Mgeneral->update_table_row("proveedorescotizacion",$data,"id",$id_cotizacion);
  }//...update_proveedores_destinatario

  public function agregar_producto(){

    $this->form_validation->set_rules('id_proveedorescotizacion', ' ID de la cotización', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    /* $this->form_validation->set_rules('costo_unitario', ' Precio del producto', 'required');
    $this->form_validation->set_rules('costo_total', ' Importe', 'required'); */
    
    $id_proveedorescotizacion=$this->input->post('id_proveedorescotizacion');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    /* $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total'); */

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    if($this->Proveedorescotizacion_model->buscar_producto_pCotizacion_detalle($id_proveedorescotizacion,$id_producto)){

      $dataUpdate['id_proveedorescotizacion']  = $id_proveedorescotizacion;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      /* $dataUpdate['costo_unitario']         = $costo_unitario;
      $dataUpdate['costo_total']            = $costo_total; */

      echo json_encode($this->Proveedorescotizacion_model->actualizar_producto_pCotizacion_detalle($dataUpdate));

    }else{

    $dataAdd['id_proveedorescotizacion']  = $id_proveedorescotizacion;
    $dataAdd['id_producto']            = $id_producto;
    $dataAdd['nombre_producto']        = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']   = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']               = $cantidad;
    /* $dataAdd['costo_unitario']         = $costo_unitario;
    $dataAdd['IVA']                    = $this->input->post('IVA');
    $dataAdd['IEPS']                   = $this->input->post('IEPS');
    $dataAdd['costo_total']            = $costo_total; */

    echo json_encode($this->Proveedorescotizacion_model->agregar_producto_pCotizacion_detalle($dataAdd));
    }
    $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_proveedorescotizacion);
  }//...agregar_producto

  public function eliminar_producto($id_cotizacion,$id_producto){
    $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    echo json_encode($this->Proveedorescotizacion_model->eliminar_producto_pCotizacion_detalle($id_cotizacion,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Proveedorescotizacion_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_cotizacion=$this->input->post('id_cotizacion');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Proveedorescotizacion_model->actualizar_fecha_modificacion($id_cotizacion);
    $this->Mgeneral->update_table_row('proveedorescotizacion',$data,'id',$id_cotizacion);

  }//...agregar_comentarios

  /* public function calcular_totales($id_proveedorescotizacion){
    echo json_encode($this->Proveedorescotizacion_model->calcular_totales($id_proveedorescotizacion));
  }//...calcular_totales */

  public function get_fecha_modificacion(){
    $id_cotizacion=$this->input->post('id_cotizacion');
    echo json_encode($this->Proveedorescotizacion_model->get_fecha_modificacion($id_cotizacion));
  }//...get_fecha_modificacion

}
