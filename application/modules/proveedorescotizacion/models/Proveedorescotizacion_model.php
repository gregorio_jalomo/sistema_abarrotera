<?php
/**

 **/
class Proveedorescotizacion_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        
    }

    public function listar_proveedorescotizacion(){
        $db=$this->db;
        $db->select("proveedorescotizacion.*,cat_estatus.cat_estatus_nombre");
        $db->from("proveedorescotizacion");
        $db->join("cat_estatus","proveedorescotizacion.status=cat_estatus.cat_estatus_id");
        $db->where_in("status",[4,5,6]);
        $db->order_by("fecha_creacion","DESC");
        return $db->get()->result();

    }//...listar_proveedorescotizacion

    public function get_proveedorescotizacion_by_id($id_proveedorescotizacion){
        $db=$this->db;
        $db->select("proveedorescotizacion.*,cat_estatus.cat_estatus_nombre");
        $db->from("proveedorescotizacion");
        $db->join("cat_estatus","proveedorescotizacion.status=cat_estatus.cat_estatus_id");
        $db->where("id",$id_proveedorescotizacion);
        return $db->get()->row();
    }//...get_proveedorescotizacion_by_id
    public function count_proveedorescotizacion_detalle($id_proveedorescotizacion){
        return $this->db->where("id_proveedorescotizacion",$id_proveedorescotizacion)->get("proveedorescotizacion_detalle")->num_rows();
    }
    public function get_proveedorescotizacion_detalle($id_proveedorescotizacion,$order_by="id",$order_type="DESC"){
        $result=$this->db
                ->select("proveedorescotizacion_detalle.*, codigosbarras.codigobarra_codigo, productos.familia_id, familia.familia_tipo")
                ->join("productos","proveedorescotizacion_detalle.id_producto = productos.producto_id")
                ->join("familia","productos.familia_id = familia.familia_id")
                ->join("codigosbarras","proveedorescotizacion_detalle.id_producto = codigosbarras.producto_id")
                ->where("id_proveedorescotizacion",$id_proveedorescotizacion)
                ->order_by($order_by,$order_type)
                ->get("proveedorescotizacion_detalle")->result();
        return $result;
    }//...get_proveedorescotizacion_detalle
    
    public function get_proveedorescotizacion_detalle_no_join($id_proveedorescotizacion){

        $result=$this->db
                ->where("id_proveedorescotizacion",$id_proveedorescotizacion)
                ->order_by("id","DESC")
                ->get("proveedorescotizacion_detalle")->result();
        return $result;
    }//...get_proveedorescotizacion_detalle_no_join

    /* public function get_clientespedido_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->select("clientespedido.*,cat_estatus.cat_estatus_nombre");
        $db->from("clientespedido");
        $db->join("cat_estatus","clientespedido.status=cat_estatus.cat_estatus_id");
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get()->row();
    }//...get_clientespedido_by_FUNI */

    /* public function get_clientes_condicion($cliente_id){
        //SELECT clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion 
            //FROM `clientescondicion`
            //join tipodeprecio ON clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id
            //join metodopago ON clientescondicion.metodopago_id= metodopago.metodopago_id
            //WHERE clientescondicion.cat_estatus_id='1'
        $db=$this->db;
        $db->select("clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion");
        $db->from("clientescondicion");
        $db->join("tipodeprecio","clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id");
        $db->join("metodopago","clientescondicion.metodopago_id= metodopago.metodopago_id");
        $db->where("clientescondicion.cat_estatus_id",1);
        $db->where("clientescondicion.cliente_id",$cliente_id);

        return $db->get()->result();
    }//...get_clientes_condicion */

    public function get_info_proveedor($proveedor_id){
        /* SELECT *
            FROM proveedores
            WHERE proveedor_id = $proveedor_id*/
        $db=$this->db;
        $db->where("proveedor_id",$proveedor_id);
        
        return $db->get("proveedores")->row();

    }//...get_info_proveedor

    /* public function get_porcentaje_tipo_precio($id_clientescondicion){
         //SELECT tipodeprecio_porcentaje 
            //FROM `tipodeprecio`
            //WHERE tipodeprecio_id=
            //    (
            //        SELECT clientescondicion.tipodeprecio_id FROM clientescondicion WHERE clientescondicion.clientescondicion_id=12
            //    ) 
        
        $db=$this->db;
        $db->select("tipodeprecio_porcentaje");
        $db->from("tipodeprecio");
        $db->where("tipodeprecio_id= 
                    (SELECT clientescondicion.tipodeprecio_id 
                        FROM clientescondicion 
                        WHERE clientescondicion.clientescondicion_id=$id_clientescondicion)"
                    );
        $porcentaje=$db->get()->row();
        $porcentaje=$porcentaje->tipodeprecio_porcentaje/100;

        return $porcentaje;
    }//...get_porcentaje_tipo_precio */

    /* public function get_precio_producto($producto_id){
        $precio=$this->db->get_where("productos",array("producto_id"=>$producto_id))->row()->producto_costo_venta;
        return $precio;
    }//...get_precio_producto */

    public function get_productos($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_productos_filt_pReorden($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->where("(producto_existencias + producto_existencias_comprometidas_compras - producto_existencias_comprometidas_pedido) <=","producto_punto_reorden")
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_cant_producto($id_pedido,$id_producto){

        $product_row=$this->get_existencias_producto($id_producto);

        $detalle_row = $this->db->select("cantidad")
                         ->where(array("id_proveedorescotizacion"=>$id_pedido,"id_producto"=>$id_producto))
                         ->get("proveedorescotizacion_detalle")->row();
        $arr=array(
            "existencias"=>round($product_row->producto_existencias, 3, PHP_ROUND_HALF_EVEN),
            "comprometidos"=>round($product_row->producto_existencias_comprometidas_pedido, 3, PHP_ROUND_HALF_EVEN),
            "por_comprar"=>round($product_row->producto_existencias_comprometidas_compras, 3, PHP_ROUND_HALF_EVEN),
        );
        if(isset($detalle_row)){
            $arr["cantidad"]=round($detalle_row->cantidad, 3, PHP_ROUND_HALF_EVEN);
            $arr["status"]=true;
        }else{
            $arr["status"]=false;
        }
        return $arr;

    }//...get_cant_producto

    public function buscar_producto_pCotizacion_detalle($id_proveedorescotizacion,$id_producto){
        $db=$this->db;
        $db->where("id_producto",$id_producto);
        $db->where("id_proveedorescotizacion",$id_proveedorescotizacion);

        return $db->get("proveedorescotizacion_detalle")->row();
    }//...buscar_producto_pCotizacion_detalle
      
    public function actualizar_producto_pCotizacion_detalle($dataUpdate){
        /* 
        $dataUpdate['id_proveedorescotizacion']  = $id_proveedorescotizacion;
        $dataUpdate['id_producto']            = $id_producto;
        $dataUpdate['cantidad']               = $cantidad;
        $dataUpdate['costo_unitario']         = $costo_unitario;
        $dataUpdate['costo_total']            = $costo_total; 
        */
        $db=$this->db;
        $id_cot=$dataUpdate['id_proveedorescotizacion'];
        $id_prod=$dataUpdate['id_producto'];
        return $db->update("proveedorescotizacion_detalle", $dataUpdate, 
                            array("id_proveedorescotizacion"=>$id_cot,"id_producto"=>$id_prod)
                          );
    }//...actualizar_producto_pCotizacion_detalle

    public function agregar_producto_pCotizacion_detalle($data){
        $agregado= $this->Mgeneral->save_register("proveedorescotizacion_detalle",$data);
        if($agregado){
            return true;
        }else{
            return false;
        }
    }//...agregar_producto_pCotizacion_detalle

    public function get_existencias_producto($id_producto){
        return $this->db->select("producto_existencias,producto_existencias_comprometidas_pedido,producto_existencias_comprometidas_compras")
                        ->where("producto_id",$id_producto)
                        ->get("productos")->row();
    }//...get_existencias_producto

    public function eliminar_producto_pCotizacion_detalle($id_proveedorescotizacion,$id_producto){
        
        $eliminado=$this->db->delete("proveedorescotizacion_detalle",
                         array("id_proveedorescotizacion"=>$id_proveedorescotizacion,"id_producto"=>$id_producto
                                ));
        if($eliminado){
            return true;
        }else{
            return false;
        }
    }//...eliminar_producto_pCotizacion_detalle

    /* public function update_costo_by_condicion_pago($id_proveedorescotizacion,$id_clientescondicion){

        $db=$this->db;
        //obtener porcentaje de tipodeprecio
        $porcentaje=$this->get_porcentaje_tipo_precio($id_clientescondicion);
        //obtener los productos de la cotizacion
        $productos=$this->get_proveedorescotizacion_detalle($id_proveedorescotizacion);//viene como result

        $return_productos=array();
        //a cada producto hacer update a costo_unitario e importe en base al nuevo porcentaje
        foreach ($productos as $producto) {
            $id_producto            = $producto->id_producto;
            $costo_venta_producto   = $this->get_precio_producto($producto->id_producto);
            $iva                    = ($producto->IVA)/100;
            $ieps                   = ($producto->IEPS)/100;
            $cantidad               = $producto->cantidad;

            $costo_con_condicion    = $costo_venta_producto + ($costo_venta_producto*$porcentaje);
            $costo_iva              = $costo_con_condicion * ($iva);
            $costo_ieps             = $costo_con_condicion * ($ieps);
            $costo_con_impuestos    = $costo_con_condicion + $costo_iva + $costo_ieps;
            $nuevo_importe          = ($costo_con_impuestos) * $cantidad;

            $data['costo_unitario']=$costo_con_condicion;
            $data['costo_total']=$nuevo_importe;
            
            $exec=$db->update("proveedorescotizacion_detalle", $data, array("id_proveedorescotizacion"=>$id_proveedorescotizacion,"id_producto"=>$id_producto));
            
            $return_productos[$id_producto]=array();
            $return_productos[$id_producto]['exec']=($exec==true) ? true : false;
            $return_productos[$id_producto]['costo_venta_producto']=$costo_venta_producto;
            $return_productos[$id_producto]['costo_con_condicion']=$costo_con_condicion;
            $return_productos[$id_producto]['costo_iva']=$costo_iva;
            $return_productos[$id_producto]['costo_ieps']=$costo_ieps;
            $return_productos[$id_producto]['costo_con_impuestos']=$costo_con_impuestos;
            $return_productos[$id_producto]['nuevo_importe']=$nuevo_importe;

        }
        return $return_productos;

    }//...update_costo_by_condicion_pago */

    /* public function calcular_totales($id_proveedorescotizacion){
        $productos=$this->get_proveedorescotizacion_detalle_no_join($id_proveedorescotizacion);//viene como result
        
        $sum_subtotal=0;
        $sum_iva=0;
        $sum_ieps=0;
        $sum_impuestos=0;
        $sum_total=0;
        $rastreo=array();
        $cont=0;
        foreach ($productos as $producto) {
            $id_producto=$producto->id_producto;
            $rastreo[$id_producto]=array();
            $cantidad           = $producto->cantidad;
            $costo_unitario     = $producto->costo_unitario;
            $iva                = ($producto->IVA)/100;
            $ieps               = ($producto->IEPS)/100;

            $subtotal_producto  = round($cantidad*$costo_unitario, 4, PHP_ROUND_HALF_EVEN);
            $sum_subtotal       += $subtotal_producto;
            $costo_iva          = $subtotal_producto * ($iva);
            $costo_ieps         = $subtotal_producto * ($ieps);

            $impuestos_producto = round($costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $iva_producto       = round($costo_iva, 4, PHP_ROUND_HALF_EVEN);
            $ieps_producto      = round($costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $total_producto     = round($subtotal_producto + $costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);

            $sum_impuestos      += $impuestos_producto;
            $sum_iva            += $iva_producto;
            $sum_ieps           += $ieps_producto;
            $sum_total          += $total_producto;

            $rastreo[$id_producto]["costo_unitario"]=$costo_unitario;
            $rastreo[$id_producto]["cantidad"]=$cantidad;
            $rastreo[$id_producto]["subtotal_producto"]=$subtotal_producto;
            $rastreo[$id_producto]["impuestos_producto"]=$impuestos_producto;
            $rastreo[$id_producto]["iva_producto"]=$iva_producto;
            $rastreo[$id_producto]["ieps_producto"]=$ieps_producto;
            $rastreo[$id_producto]["total_producto"]=$total_producto;

            $cont++;
        }
        $totales=array(
            "rastreo"=>$rastreo,
            "subtotal"=>$sum_subtotal,
            "impuestos"=>$sum_impuestos,
            "iva"=>$sum_iva,
            "ieps"=>$sum_ieps,
            "total"=>$sum_total,
        );
        $this->actualizar_totales($id_proveedorescotizacion,$totales["subtotal"],$totales["iva"],$totales["ieps"],$totales["total"]);
        return $totales;

    }//...calcular_totales */

    /* public function actualizar_totales($id_proveedorescotizacion,$subtotal,$iva,$ieps,$total){
        $data["subtotal"]=$subtotal;
        $data["IVA"]=$iva;
        $data["IEPS"]=$ieps;
        $data["TOTAL"]=$total;

        return $this->Mgeneral->update_table_row("proveedorescotizacion",$data,"id",$id_proveedorescotizacion);
    }//...actualizar_totales */

    public function actualizar_fecha_modificacion($id_cotizacion){
        $query="UPDATE `proveedorescotizacion` SET `fecha_modificacion` = CURRENT_TIMESTAMP WHERE `id` = '$id_cotizacion'";
           $this->db->query($query);
    }//...actualizar_fecha_modificado

    public function get_fecha_modificacion($id_cotizacion){
        $this->db->select("fecha_modificacion");
        $this->db->where("id",$id_cotizacion);
        $row_cotizacion=$this->db->get("proveedorescotizacion")->row();
        $timestamp=$row_cotizacion->fecha_modificacion;
        $fecha=date('d/m/Y,H:i',strtotime($timestamp));
        return $fecha;
    }//...get_fecha_modificacion
}
