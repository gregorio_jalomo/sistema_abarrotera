<?php

$folio_unico_documento      = $ProvCot->folio_unico_documento;

$string_proveedores         = $ProvCot->id_proveedores;

if(strpos($string_proveedores,",")){
  $arr_id_prov=preg_split("/[\s,]+/", $string_proveedores);
}else{
  $arr_id_prov=array($string_proveedores);
}
?>

<style>
  .txt-center{
    text-align:center;
    
  }
  .th-short{
    max-width:100px;
  }
  .mark-cheap{
    background-color:#3c8dbc87;
  }
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
  }
  .comment-btn{
    position: absolute;
    padding: unset;
  }
  .even{
    background-color:#80808026;
  }
  .btn-comprar{
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
    font-size: 13px;
  }
</style>
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Cotizaciones de proveedores</h3>
    </div>
    <!-- <div class="col-md-6">

      <button id="agregar_proveedorescotizacion" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Cotización</button>

    </div> -->

  </div>
  <!-- /.card-header -->
  <div class="card-body">

  <?php 
    $data=$_SESSION["to_comp_view"];
    if($data["err_count"]>0){
        foreach ($data["files_notfound"] as $file_err) {
          echo "El archivo ".$file_err." contiene errores o no se subió correctamente<br>"; 
        } 
    }

    if(count($data["files_ok"])>0){
      $fill_productos       =true;
      $count_productos      =0;
      $arr_productos        =array();
      $arr_proveedores      =array();
      $arr_prod_unicos_prov =array();
      $selects_ids          =array(); //aqui se guardan los id de los selects de proveedores que se generarán
      $nombre_prov          ="";

        foreach ($data["files_ok"] as $file_ok) {
          $foliodocumento_xlsx=$file_ok[2]["C"];
          
          if($foliodocumento_db==$foliodocumento_xlsx){
              $count_productos = ( (count($file_ok)) > $count_productos) ? (count($file_ok)) : $count_productos;
              $nombre_prov=$file_ok[2]["A"];
              $precios_proveedor=array();

              for ($i=4; $i <= $count_productos; $i++) { 

                $nombre_producto=$file_ok[$i]["A"];

                $tmp1=array(
                              "cantidad"    =>  $file_ok[$i]["C"],
                              "precio"      =>  $file_ok[$i]["D"],
                              "proveedor"   =>  $nombre_prov,
                              "comentario"  =>  $file_ok[$i]["E"]
                            );
                array_push($precios_proveedor,$tmp1);

                if($fill_productos){
                  
                  array_push($arr_productos,$nombre_producto);
                }
              }//for productos

              $fill_productos=false;
              array_push($arr_prod_unicos_prov,$precios_proveedor);
              array_push($arr_proveedores,$nombre_prov);
              
          }//...if folio
        }//foreach archivo
    }
        //echo print_r($arr_proveedores);
          $Table="";
          $Table.= "<table id='products_table'>
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th style='max-width: 70px;'>Cant. cotizada</th>";

          foreach ($arr_proveedores as $nomb_prov) {
            $Table.= "    <th class='th-short' >".$nomb_prov."</th>";
          }

          $Table.= "      <th style='max-width: 70px;'>Cant. a comprar</th>
                          <th>Comprar a:</th>";

          $Table.= "    </tr>
                      </thead>
                      <tbody>
          ";

          $id_count_row=0;//variable para diferenciar los rows para acceder a ellos posteriormente

          foreach ($arr_productos as $key => $prod) {//ciclo productos únicos
            $cant = $arr_prod_unicos_prov[0][$key]["cantidad"];
              $Table.= "<tr>
                          <td id='row_prod_name_".$id_count_row."'>".$prod."</td>
                          <td class='txt-center'>".$cant."</td>";

            $prov_disp_producto=array();//proveedores que agregaron un precio al producto
            $all_precios=array();
            $cheapest=0;
            $proveedor_sugerido="";
            $mark_cheap="";
            foreach ($arr_prod_unicos_prov as $A) {

              $prc1       = $A[$key]["precio"];
              $prv        = $A[$key]["proveedor"];

              if(isset($prc1) && $prc1 != ""){
                array_push($all_precios,$prc1);
                array_push($prov_disp_producto,$prv);
              }
              
            }

            $cheapest=(!empty($all_precios))?min($all_precios):FALSE;//precio mas barato

            //La key del precio mas barato($all_precios) coincide con la key del proveedor que la ofrece ($arr_prod_unicos_prov)
            $key_to_find=array_search($cheapest,$all_precios);

            $cheapest_proveedor = (!$cheapest)?FALSE:$prov_disp_producto[$key_to_find];//proveedor que ofrece el precio mas barato

            foreach ($arr_prod_unicos_prov as $p) {//ciclo para imprimir los td con precio
              
              $prc = $p[$key]["precio"];
              $cmt = $p[$key]["comentario"];

              $comment_btn="";
              if(isset($prc) && $prc != ""){
                $mark_cheap = ($cheapest==$prc )?"mark-cheap":"";//remarcar los td con el precio mas barato
                $prc="$ ".$prc;
                //$circle_btn="<div class='btn btn-success btn-circle'><i class='icofont-comment'></i></i></div>";
                $comment_btn=(isset($cmt) && $cmt != "")?"<span class='btn comment-btn' data-toggle='tooltip' data-placement='top' title='".$cmt."'><i class='icofont-comment'></i></span>":"";

              }
              
              $Table.= "  <td class='txt-center ".$mark_cheap."' id='row_prod_prc_".$id_count_row."'>".$prc.$comment_btn."</td>";
            }

            if($cheapest_proveedor!=FALSE){
              array_push($selects_ids,"row_select_".$id_count_row);
              $Table.= "  <td class='txt-center'><input type='text' value='".$cant."' id='row_input_".$id_count_row."'></td>
                          <td>
                            <select name='' id='row_select_".$id_count_row."' style='width:200px'>";

                              foreach ($prov_disp_producto as $prov_opt) {

                                $selected = ($cheapest_proveedor == $prov_opt) ? "selected='selected'":"";

                                $Table.= "<option value='".$prov_opt."' ".$selected.">".$prov_opt."</option>";
                              }

              $Table.= "    </select>";
            }else{
              $Table.= "<td></td><td>";
            }
            $Table.= "    </td>
                        </tr>";
            $id_count_row++;
          }//...ciclo productos únicos

          $Table.= "</tbody></table>";

          echo $Table;

          echo "<div class='btn btn-comprar' id='enviar_a_compras'>Comprar</div>";
  ?>
 


  </div><!-- /.card-body -->
</div><!-- /.card -->

<script>
  $(document).ready(function() {

    $('#menuproveedorescotizacion').addClass('active-link');

    $('[data-toggle="popover"]').popover();
  
    $("#products_table").DataTable({
      "responsive": true,
      "autoWidth": false,
      "pageLength": 100
    });

    $('#enviar_a_compras').click(function(event){
      event.preventDefault();

          var selects_ids = JSON.parse('<?=json_encode($selects_ids)?>'); // extraer los ids de los selects que se generaron
          var detalle_compra_form=[];//contendrá los productos seleccionados separados por proveedor seleccionado
          var ik=0;
          $.each(selects_ids,function(key,value){//ciclo para leer rows de la tabla y separar los productos por proveedor
            let regex=/select/gi;
            let proveedor     = $("#"+value).val();
            let cant_prod     = $("#"+value.replace(regex,"input")).val();
            let txt_nomb_prod = $("#"+value.replace(regex,"prod_name")).text();
            let txt_precio_prod = $("#"+value.replace(regex,"prod_prc")).text().replace("$ ","");

            let obj_producto={
                                "nombre_producto":txt_nomb_prod,
                                "cantidad":cant_prod,
                                "costo_unitario":txt_precio_prod
                              }

            if(detalle_compra_form[proveedor]==undefined){
              detalle_compra_form[proveedor]=[];
             ik++; 
            }
            //console.log(detalle_compra_form[proveedor]);
            detalle_compra_form[proveedor].push(obj_producto);            
            
          });

          //console.log(detalle_compra_form);

          /* detalle_compra_form.forEach(function(proveedor,value){
            console.log(proveedor);
          }); */
          $.each(Object.keys(detalle_compra_form),function(key,value){

              ajaxJson(site_url+"/proveedorescompra/proveedorescompra/crear_folio_prov_compra/"+"<?=$folio_unico_documento?>", 
                {},
                "POST", "", function(result){
                  var arr_folio_documento=JSON.parse(result);
                  //console.log(arr_folio_documento);
                  var prefijo="PROVCOMP";
                if(arr_folio_documento.errorFUNI=="FALSE"){

                    if(arr_folio_documento.errorPrefijo=="FALSE"){
                        
                        ajaxJson(site_url+"/proveedorescompra/proveedorescompra/alta", 
                          {
                          "nuevo_folio_documento":arr_folio_documento.folio_string,
                          "nuevo_FUNI_documento":arr_folio_documento.funi,
                          "id_foliosdocumentos":arr_folio_documento.id_foliosdocumentos,
                          "id_FUNI_foliosdocumentos":arr_folio_documento.id_FUNI_foliosdocumentos,
                          "proveedor_nombre":value,
                          "obj_productos":detalle_compra_form[value]

                          }, "POST", "", function(id_nuevo_compra){
                          //alert(typeof(result)+"|"+result[0]+"|"+JSON.stringify(result));
                          //copiar los productos de proveedorescotizacion_detalle a proveedorescompra_detalle

                            ExitoCustom("Se han creado  las compras con éxito", function(){
                              window.location.href=site_url+("/proveedorescompra/proveedorescompra/listar_proveedorescompra/");
                            },"ok");
                          });
                    }else{
                      ErrorCustom("Por favor verifique que exista el folio_documento '"+prefijo+"' y sus fechas de disponibilidad", "","");
                    }
                    
                }else{
                  ErrorCustom("Por favor verifique que exista el folio_documento 'FUNI' y sus fechas de disponibilidad", "","");
                }

              });//... ajax crear_folio_prov_compra

          });
      
    });//enviar_a_compras


    function delete_interactions(){
      $(".comment-btn").off("mouseover");
    }
    function add_interactions(){
      $(".comment-btn").tooltip();
    }
    function reload_interactions(){
      delete_interactions()
      add_interactions();
    }

    $("th").on("click",function(){
      reload_interactions();
    });
    $('input[type=file]').on("change",function(){
      reload_interactions();
    });
    //$(".comment-btn").tooltip();
    add_interactions();

  });//document ready

</script>
