
<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Cotizaciones de proveedores</h3>
    </div>
    <div class="col-md-6">

      <button id="agregar_proveedorescotizacion" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Cotización</button>

    </div>

  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="tabla_proveedorescotizacion" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio</th>
        <th>Proveedor</th>
        <th>Status</th>
        <th>Usuario del sistema</th>
        <th>Opciones</th>

      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_proveedorescotizacion);*/ 
      foreach ($con_proveedorescotizacion as $ProvCot) {

        $id_cotizacion              = $ProvCot->id;
        //$fecha_creacion             = explode(" ",$ProvCot->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $fecha_creacion             = $ProvCot->fecha_creacion;
        $folio_documento            = $ProvCot->folio_documento;
        $folio_unico_documento      = $ProvCot->folio_unico_documento;
        $comentario                 = $ProvCot->comentario;
        $status                     = $ProvCot->status;

        $perfil_usuario             = $ProvCot->perfil_usuario;
        $descr_id_usuario           = $ProvCot->descripcion_id_usuario;

        //$id_proveedores             = $ProvCot->id_proveedores;
        $string_proveedores         = $ProvCot->id_proveedores;

        if(strpos($string_proveedores,",")){
          $arr_id_prov=preg_split("/[\s,]+/", $string_proveedores);
        }else{
          $arr_id_prov=array($string_proveedores);
        }
      
        /* INNERS */
        $cat_estatus_nombre         = $ProvCot->cat_estatus_nombre;
        /* ...INNERS */
        $nombre_proveedor="";

        foreach($proveedores as $prov){
          if(in_array($prov->proveedor_id,$arr_id_prov)){
            $nombre_proveedor.=$prov->proveedor_nombre.", ";
          }
        }

        echo
        '<tr id="borrar_'.$id_cotizacion.'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$nombre_proveedor.'</td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$descr_id_usuario.'</td>

          <td>
            <a href="'.base_url().'index.php/proveedorescotizacion/proveedorescotizacion/ver_detalle/'.$id_cotizacion.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>
            <a href="'.base_url().'index.php/proveedorescotizacion/proveedorescotizacion/editar/'.$id_cotizacion.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
              <a href="'.base_url().'index.php/proveedorescotizacion/proveedorescotizacion/cambiar_status/'.$id_cotizacion.'/6" class="eliminar_relacion" flag="'.$id_cotizacion.'" id="delete'.$id_cotizacion.'">
              <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Cancelar cotizacion"><i class="fas fa-trash-alt"></i> Borrar</button>

          </td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>
  $(document).ready(function() {

    $('#menuproveedorescotizacion').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_proveedorescotizacion").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": false
    });

    $('#agregar_proveedorescotizacion').click(function(){

      var prefijo="PROVCOT";
      var arr_folio_documento=<?php echo json_encode(crear_foliodocumento("PROVCOT",""));?>;
      /*alert(JSON.stringify(arr_folio_documento,0,4));
        alert(arr_folio_documento.err); */

        if(arr_folio_documento.errorFUNI=="FALSE"){

            if(arr_folio_documento.errorPrefijo=="FALSE"){

                ajaxJson(site_url+"/proveedorescotizacion/proveedorescotizacion/alta", 
                  {
                  "nuevo_folio_documento":arr_folio_documento.folio_string,
                  "nuevo_FUNI_documento":arr_folio_documento.funi,
                  "id_foliosdocumentos":arr_folio_documento.id_foliosdocumentos,
                  "id_FUNI_foliosdocumentos":arr_folio_documento.id_FUNI_foliosdocumentos

                  
                  }, "POST", "", function(id_nueva_cotizacion){
                  //alert(typeof(result)+"|"+result[0]+"|"+JSON.stringify(result));
                      if(id_nueva_cotizacion!="false"){
                        ExitoCustom("Se ha creado con éxito la cotización", function(){
                          window.location.href=site_url+("/proveedorescotizacion/proveedorescotizacion/editar/"+id_nueva_cotizacion);
                        },"ok");
                      }else{
                        ErrorCustom("No tienes permiso para agregar cotizaciones");
                      }
                      

                    });

            }else{
              ErrorCustom("Por favor verifique que exista el folio_documento '"+prefijo+"' y sus fechas de disponibilidad", "","");
            }
            
        }else{
          ErrorCustom("Por favor verifique que exista el folio_documento 'FUNI' y sus fechas de disponibilidad", "","");
        }

    });//agregar_proveedorescotizacion

    $(".eliminar_relacion").click(function(event){
          event.preventDefault();
          bootbox.dialog({
          message: "Desea eliminar el registro?",
          closeButton: true,
          buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                              id = $(event.currentTarget).attr('flag');
                              url = $("#delete"+id).attr('href');
                              $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para borrar cotizaciones");
                                }
                              });
                              
                              
                              
                              }
                              },
                                "cancel":
                                {
                                    "label": "<i class='icon-remove'></i> Cancelar",
                                    "className": "btn-sm btn-info",
                                    "callback": function () {

                                    }
                                }

                            }
                        });
    });

  });//document ready

</script>
