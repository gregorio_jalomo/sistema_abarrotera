
<style>
  .btn-comparar{
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
    font-size: 13px;
  }
</style>

<div class="card">
  <div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Cotizaciones de proveedores</h3>
    </div>
    <!-- <div class="col-md-6">

      <button id="agregar_proveedorescotizacion" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Cotización</button>

    </div> -->

  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <input type="hidden" id="arr_filenames" value="">
  <form id="upload_form" enctype="multipart/form-data" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <input type="file" name="fileUploader" id="fileUploader" class="btn btn-default" multiple accept=".xlsx">
                </div>
                <div class="col-md-4" style="text-align:right;">
                    <!-- <input type="button" id="btnUpload" style="display:none" class="btn btn-default" value="Upload File" onclick​="uploadFiles()"> -->
                    <input type="button" id="btnUpload" style="display:none" class="btn btn-default" value="Subir archivos">

                </div>
            </div>
            <div class="row">
                <div id="divFiles" class="files">
                </div>
            </div>
        </div>
    </form>
  <div id="btn_container">
  
  </div>
  </div><!-- /.card-body -->
</div><!-- /.card -->

<script>
  $(document).ready(function() {

    $('#menuproveedorescotizacion').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $('input[type=file]').val("");
    
    $('input[type=file]').change(function () {
     $('#btnUpload').show();
     $('#divFiles').html('');
     for (var i = 0; i < this.files.length; i++) { //Progress bar and status label's for each file genarate dynamically
          var fileId = i;
          $("#divFiles").append('<div class="col-md-12">' +
                  '<div class="progress-bar progress-bar-striped active" id="progressbar_' + fileId + '" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>' +
                  '</div>' +
                  '<div class="col-md-12">' +
                       '<div class="col-md-6">' +
                          '<input type="button" class="btn btn-danger" style="display:none;line-height:6px;height:25px" id="cancel_' + fileId + '" value="cancel">' +
                       '</div>' +
                       '<div class="col-md-6">' +
                          '<p class="progress-status" style="text-align: right;margin-right:-15px;font-weight:bold;color:saddlebrown" id="status_' + fileId + '"></p>' +
                       '</div>' +
                  '</div>' +
                  '<div class="col-md-12">' +
                       '<p id="notify_' + fileId + '" style="text-align-last: start;"></p>' +
                  '</div>');
                }
          $('#btnUpload').on("click",function(){
            var file = $("input#fileUploader")//All files
            var arr_filenames=[];
            for (var i = 0; i < file[0].files.length; i++) {
              if(file[0].files[i].type=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
                var filename=uploadSingleFile(file[0].files[i], i)
                if(filename!=false){
                  //$("#arr_filenames").val($("#arr_filenames").val()+filename+",");
                  arr_filenames.push(filename);
                }
              }else{
                ErrorCustom("El archivo "+file[0].files[i].type+" no coincide con un archivo de excel (.xlsx), por favor verifique. "+file[0].files[i].type,"","");
              }
                    
            }//...for files.length
            console.log(arr_filenames);
            $("#btn_container").html('');
            $("#btn_container").append("<div class='btn btn-comparar' id='btn_comparar'>Comparar</div>");
            $("#btn_comparar").off();
            $("#btn_comparar").on("click",function(){
            ajaxJson("<?php echo base_url()?>index.php/proveedorescotizacion/Proveedorescotizacion/read_cot_xlsx/"+<?=$id_cotizacion?>,
                {
                  "arr_files":arr_filenames
                },
                  "POST",
                  "",
                  function(result){
                    window.location.href=site_url+("/proveedorescotizacion/Proveedorescotizacion/comparar/"+<?=$id_cotizacion?>);
                    /* $("html").empty();
                    $("html").append(result); */
                  });
            });
          })
      })

      function uploadSingleFile(file, i) {
        var fileId = i;
        var ajax = new XMLHttpRequest();
        //Progress Listener
        ajax.upload.addEventListener("progress", function (e) {
            var percent = (e.loaded / e.total) * 100;
            $("#status_" + fileId).text(Math.round(percent) + "% cargado, por favor espere...");
            $('#progressbar_' + fileId).css("width", percent + "%")
            //$("#notify_" + fileId).text(file.name+" - " + (e.loaded / 1048576).toFixed(2) + " MB de " + (e.total / 1048576).toFixed(2) + " MB cargados correctamente.");
            $("#notify_" + fileId).text(file.name+" - " + (e.loaded / 1024).toFixed(2) + " KB de " + (e.total / 1024).toFixed(2) + " KB cargados correctamente.");
        }, false);
        //Load Listener
        ajax.addEventListener("load", function (e) {

            $("#status_" + fileId).text(event.target.responseText);
            $('#progressbar_' + fileId).css("width", "100%")
            //Hide cancel button
            var _cancel = $('#cancel_' + fileId);
            _cancel.hide();
        }, false);
        //Error Listener
        ajax.addEventListener("error", function (e) {
            $("#status_" + fileId).text("Carga Fallida");
        }, false);
        //Abort Listener
        ajax.addEventListener("abort", function (e) {
            $("#status_" + fileId).text("Carga cancelada");
        }, false);
        ajax.open("POST", "<?=base_url("index.php/proveedorescotizacion/Proveedorescotizacion/upload_cot_xlsx")?>");

        var uploaderForm = new FormData(); // Create new FormData
        uploaderForm.append("file", file); // append the next file for upload
        ajax.send(uploaderForm);

        //Cancel button
        var _cancel = $('#cancel_' + fileId);
        _cancel.show();

        _cancel.on('click', function () {
            ajax.abort();
        })
        if(file.name!="" && file.name!=undefined){
          return file.name;
        }else{
          return false;
        }
    }//...UploadSingleFile


  });//document ready

</script>
