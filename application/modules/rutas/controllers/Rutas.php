<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Rutas extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mpedidosaduana', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('rutas/Rutas_model');
      $this->load->model('catalogos/Clientelugar_model');
      $this->rol_usuario  = $this->session->userdata['infouser']['rol'];
      $this->id_modulo    = 42;//id_modulo en bd
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//

  }

  public function cargar_vista($url,$data,$archivos_js){

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', '', TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  public function alta(){//insert para db
    if(validar_permiso($this->rol_usuario,$this->id_modulo,155) || $this->is_admin ){

      //recibir post del ajax
      $dataPedidosaduana['id_pedidosaduana'] = $this->input->post('id_pedidosaduana'); 

        $row_pedidosaduana=$this->Rutas_model->get_pedidosaduana_by_id($dataPedidosaduana['id_pedidosaduana']);
        //insert a pedidosaduana (importar datos de pedidosaduana)

                  $cliente = $this->Rutas_model->get_location_cliente_by_id($row_pedidosaduana->id_clientes);

                  $data_import["id_pedidosaduana"]  = $dataPedidosaduana['id_pedidosaduana'];
                  $data_import["id_clientes"]       = $row_pedidosaduana->id_clientes;
                  $data_import["cliente_latitud"]   = $cliente->cliente_latitud;
                  $data_import["cliente_longitud"]  = $cliente->cliente_longitud;
                  $data_import["id_vehiculos"]      = "";
                  $data_import["id_choferes"]       = "";
                  $data_import["chk_ruta"]          = "1";
                  $data_import["chk_entregado"]     = "0";
                  $data_import["cat_estatus_id"]    = "4";
                  $data_import["fecha_creacion"]    = date("Y-m-d H:i:s");
                  //$data_import["status"]                          = 4;

        $pedidosaduana_id=$this->guardar($data_import);

      echo json_encode($pedidosaduana_id);
    
    }else{
      echo json_encode(false);
    }

  }//...alta

  public function guardar($data){
      return $this->Mgeneral->save_register('rutas', $data);
  }//...guardar

  public function ver_detalle($id_rutas){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,157) || $this->is_admin ){

      $data['titulo_seccion'] = "Detalle del pedido";
      $data['flecha_ir_atras'] = "rutas/rutas/listar_rutas";

      $data['rutas'] = $this->Rutas_model->get_rutas_by_id($id_rutas);
      $data['rutas_detalle'] =$this->Rutas_model->get_pedidosaduana_detalle($data['rutas']->id_pedidosaduana);
      $data['vehiculos']=$this->Mgeneral->get_table('vehiculos');
      $data['choferes']=$this->Mgeneral->get_table('choferes');

      $data['cat_estatus'] = $this->Mgeneral->get_where_in_result('cat_estatus_id',['4','5','6','7'],'cat_estatus');

      $archivos_js=array(
        'statics/js/bootbox.min.js',
          'statics/js/general.js',
          'statics/bootstrap4/js/bootstrap.min.js'
      );

      $this->cargar_vista('rutas/ver_detalle',$data,$archivos_js);      
    
    }else{
      $this->p_denied_view("ver detalle de rutas","rutas/rutas/listar_rutas");
    }

  }//...ver_detalle

  public function listar_rutas(){
    if(validar_permiso($this->rol_usuario,$this->id_modulo,159) || $this->is_admin ){

      $data['titulo_seccion'] = "Pedidos en ruta ";
      $data['flecha_ir_atras'] = "pedidosaduana/listar_pedidosaduana";
      $data['con_rutas'] = $this->Rutas_model->listar_rutas();

      $data['choferes'] = $this->Mgeneral->get_table("choferes");
      $data['vehiculos'] = $this->Mgeneral->get_table("vehiculos");

        $archivos_js=array(
          'statics/js/bootbox.min.js',
            'statics/js/general.js',
            'statics/bootstrap4/js/bootstrap.min.js'
        );

        $this->cargar_vista('rutas/listar',$data,$archivos_js);      
    
    }else{
      $this->p_denied_view("vlistar rutas","rutas/rutas/listar_rutas");
    }

  }//...listar_rutas

  public function get_pedidosaduana_detalle($id_pedidosaduana){
    echo json_encode($this->Rutas_model->get_pedidosaduana_detalle($id_pedidosaduana));

  }//...get_pedidosaduana_detalle

  public function get_pedidosaduana_by_FUNI($folio_unico_documento){
    echo json_encode($this->Rutas_model->get_pedidosaduana_by_FUNI($folio_unico_documento));
  }//...get_pedidosaduana_by_FUNI

  public function get_info_cliente(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Rutas_model->get_info_cliente($cliente_id));
  }//...get_info_cliente

  public function get_condicion_pago_by_cliente_id(){
    $cliente_id=$this->input->post('cliente_id');

    echo json_encode($this->Rutas_model->get_clientes_condicion($cliente_id));
  }//...get_condicion_pago_by_cliente_id

  public function get_set_ultimo_status($id_pedidosaduana,$nvo_stat){
    echo json_encode($this->Rutas_model->get_set_ultimo_status($id_pedidosaduana,$nvo_stat));
  }//...get_set_ultimo_status

  public function cambiar_status($id_ruta,$estatus_id) {

    $data['cat_estatus_id'] = $estatus_id;
    $go=false;
    if((validar_permiso($this->rol_usuario,$this->id_modulo,156) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_edt)){
      $go=true;
    }
    if((validar_permiso($this->rol_usuario,$this->id_modulo,158) || $this->is_admin) && in_array($data['cat_estatus_id'],$this->opc_del)){
      $go=true;
    }
    if($go){

      $data['cat_estatus_id'] = $estatus_id;
      $this->Mgeneral->update_table_row('rutas',$data,'rutas_id',$id_ruta);

    }
    echo json_encode($go);

  }//...cambiar_estatus

  public function form_select_rt_autoupdate(){
    $tabla=$this->input->post('tabla');
    $updt[$this->input->post('campo')]=$this->input->post('valor');
    $id_table=$this->input->post('campo_id');
    $id=$this->input->post('value_id');
      $this->Rutas_model->actualizar_fecha_modificacion($id);
      $this->Mgeneral->update_table_row($tabla,$updt,$id_table,$id);
  }//...form_select_rt_autoupdate

  public function update_cliente_nombre_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['id_clientes']=$this->input->post('id_clientes');
    $data['nombre_cliente']=$this->input->post('nombre_cliente');
    $data['domicilio_cliente']=$this->input->post('domicilio_cliente');
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido);
  }//...update_cliente_nombre_domicilio

  public function update_domicilio(){
    $id_pedido=$this->input->post('id_pedido');
    $data['domicilio_cliente']=$this->input->post('lugar_cliente');
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    $this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido);
    echo json_encode($data['domicilio_cliente']);
  }//...update_domicilio

  public function update_info_agentesventa(){
    $data['id_agentes_venta']=$this->input->post('agenteventa_id');
    $data['nombre_agentes_venta']=$this->input->post('nombre_agentes_venta');
    $id_pedido=$this->input->post('id_pedido');
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));

  }//...update_info_agentesventa
  
  public function update_fecha_entrega(){
    $id_pedido=$this->input->post('id_pedido');
    $fecha_entrega=$this->input->post('fecha_entrega');

    $time = strtotime($fecha_entrega);
    $newformat = date('Y-m-d H:i:s',$time);

    $data['fecha_entrega']=$newformat;
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));
  }//...update_fecha_entrega

  public function update_info_clientes_condicion(){
    $data['id_clientes_condicion']=$this->input->post('id_clientes_condicion');
    $data['descripcion_clientes_condicion']=$this->input->post('descripcion_clientes_condicion');
    $id_pedido=$this->input->post('id_pedido');
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedido));

  }//...update_info_clientes_condicion

  public function agregar_producto(){

    $this->form_validation->set_rules('id_pedidosaduana', ' ID del pedido', 'required');
    $this->form_validation->set_rules('id_producto', ' ID del producto', 'required');
    $this->form_validation->set_rules('cantidad', ' Cantidad del producto', 'required');
    $this->form_validation->set_rules('costo_unitario', ' Precio del producto', 'required');
    $this->form_validation->set_rules('costo_total', ' Importe', 'required');
    
    $id_pedidosaduana=$this->input->post('id_pedidosaduana');
    $id_producto=$this->input->post('id_producto');
    $cantidad=$this->input->post('cantidad');
    $cantidad_original=$this->input->post('cantidad_original');
    $costo_unitario=$this->input->post('costo_unitario');
    $costo_total=$this->input->post('costo_total');

    /*
      primero buscar id_producto
      si ya existe solo actualizar:
      cantidad, costo unitario, costo_total
    */
    $comprometidos=0;
    $producto=$this->Rutas_model->buscar_producto_pAduana_detalle($id_pedidosaduana,$id_producto);
    if(isset($producto)){
      //$cant=$cantidad+$producto->cantidad;

      /* if($producto->validado==0){
        
        //$valid=($cant>=$producto->cantidad_original)?1:0;
        $valid=1;
      } */
      $dataUpdate['id_pedidosaduana']  = $id_pedidosaduana;
      $dataUpdate['id_producto']            = $id_producto;
      $dataUpdate['cantidad']               = $cantidad;
      $dataUpdate['costo_unitario']         = $costo_unitario;
      //$dataUpdate['costo_total']            = $costo_total+$producto->costo_total;
      $dataUpdate['costo_total']            = $costo_total;
      $dataUpdate['validado']               = 1;

      $comprometidos=$cantidad-$producto->cantidad;

      echo json_encode($this->Rutas_model->actualizar_producto_pAduana_detalle($dataUpdate));
      //echo json_encode(array("status"=>true,"producto"=>$dataUpdate));

    }else{

    $dataAdd['id_pedidosaduana']          = $id_pedidosaduana;
    $dataAdd['id_producto']               = $id_producto;
    $dataAdd['nombre_producto']           = $this->input->post('nombre_producto');
    $dataAdd['descripcion_de_la_unidad']  = $this->input->post('descripcion_de_la_unidad');
    $dataAdd['cantidad']                  = $cantidad;
    $dataAdd['cantidad_original']         = 0;
    $dataAdd['costo_unitario']            = $costo_unitario;
    $dataAdd['IVA']                       = $this->input->post('IVA');
    $dataAdd['IEPS']                      = $this->input->post('IEPS');
    $dataAdd['costo_total']               = $costo_total;
    $dataAdd['validado']                  = 1;

    $comprometidos=$cantidad;

    echo json_encode($this->Rutas_model->agregar_producto_pAduana_detalle($dataAdd));
    }
    if($comprometidos!=0){
      $this->Rutas_model->actualizar_comprometidos_producto($id_producto,$comprometidos);
    }
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedidosaduana);
  }//...agregar_producto

  public function get_productos_no_validados($id_pedidosaduana){

    echo json_encode($this->Rutas_model->get_productos_no_validados($id_pedidosaduana));
  }//...get_productos_no_validados

  public function eliminar_producto($id_pedido,$id_producto){
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
    echo json_encode($this->Rutas_model->eliminar_producto_pAduana_detalle($id_pedido,$id_producto));
  }//...eliminar_producto

  public function get_cant_producto($id_pedido,$id_producto){
    $row=$this->Rutas_model->get_cant_producto($id_pedido,$id_producto);
    echo json_encode($row);
  }//...get_cant_producto

  public function agregar_comentario() {
    $id_pedido=$this->input->post('id_pedido');
    $comentario=$this->input->post('comentario');

    $data['comentario'] = $comentario;
    $this->Mgeneral->update_table_row('pedidosaduana',$data,'id',$id_pedido);
    $this->Rutas_model->actualizar_fecha_modificacion($id_pedido);
  }//...agregar_comentarios

  public function update_costo_by_condicion_pago($id_pedidosaduana,$id_clientescondicion){
      //$this->Rutas_model->actualizar_fecha_modificacion($id_pedidosaduana);
      echo json_encode($this->Rutas_model->update_costo_by_condicion_pago($id_pedidosaduana,$id_clientescondicion));
  }//...update_costo_by_condicion_pago

  public function calcular_totales($id_pedidosaduana){
    echo json_encode($this->Rutas_model->calcular_totales($id_pedidosaduana));
  }//...calcular_totales

  public function get_fecha_modificacion(){
    $id_pedido=$this->input->post('id_pedido');
    echo json_encode($this->Rutas_model->get_fecha_modificacion($id_pedido));
  }//...get_fecha_modificacion

  public function importar_datos_de_cPedido($folio_unico_documento,$pedidosaduana_id){

    echo json_encode($this->Rutas_model->importar_datos_de_cPedido($folio_unico_documento,$pedidosaduana_id));
  }//...importar_datos_de_cPedido

  public function importar_productos_de_pedidosAduana(){
    $folio_unico_documento=$this->input->post('folio_unico_documento');
    $id_pedidosaduana=$this->input->post('id_pedidosaduana');
    $id_pedido=$this->Rutas_model->get_id_clientespedido_by_FUNI($folio_unico_documento);
    $result_clientespedido=$this->Rutas_model->get_clientespedido_detalle($id_pedido);
    //$data_import["folio_unico_documento"]           =$row_pedidosaduana->folio_unico_documento;
    
    $cont=0;
    foreach($result_clientespedido as $row_ped){
      $data_import[$cont]["id_pedidosaduana"]          =$id_pedidosaduana;
      $data_import[$cont]["id_producto"]               =$row_ped->id_producto;
      $data_import[$cont]["nombre_producto"]           =$row_ped->nombre_producto;
      $data_import[$cont]["codigo_barras_producto"]    =$row_ped->codigo_barras_producto;
      $data_import[$cont]["descripcion_de_la_unidad"]  =$row_ped->descripcion_de_la_unidad;
      $data_import[$cont]["cantidad"]                  =$row_ped->cantidad;
      $data_import[$cont]["cantidad_original"]         =$row_ped->cantidad;
      $data_import[$cont]["IVA"]                       =$row_ped->IVA;
      $data_import[$cont]["IEPS"]                      =$row_ped->IEPS;
      $data_import[$cont]["costo_total"]               =$row_ped->costo_total;
      $data_import[$cont]["costo_unitario"]            =$row_ped->costo_unitario;
      $data_import[$cont]["IVA"]                       =$row_ped->IVA;
      $data_import[$cont]["IEPS"]                      =$row_ped->IEPS;
      $data_import[$cont]["costo_total"]               =$row_ped->costo_total;
      $data_import[$cont]["validado"]                  =0;
      $cont++;
    }

    echo json_encode($this->Rutas_model->importar_productos_de_pedidosAduana($id_pedidosaduana,$data_import));
  }//...importar_productos_de_pedidosAduana

  public function check_pressed(){
    $id_pAduana=str_replace("pRutas_","",$this->input->post('id_pRutas'));
    $input=$this->input->post('input');
    $value=$this->input->post('value');
    $data[$input]=$value;
    
    $check=$this->Mgeneral->update_table_row("rutas",$data,"rutas_id",$id_pAduana);

    return $check;

  }//...check_pressed
}
