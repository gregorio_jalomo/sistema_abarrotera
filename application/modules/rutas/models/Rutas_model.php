<?php
/**

 **/
class Rutas_model extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        
    }

    public function listar_rutas(){
        $db=$this->db;
        //$db->select("rutas.*,cat_estatus.cat_estatus_nombre");
        $db->select("rutas.*,clientes.cliente_latitud,clientes.cliente_longitud,pedidosaduana.folio_documento,pedidosaduana.nombre_cliente,cat_estatus.cat_estatus_nombre");
        $db->from("rutas");
        $db->join("cat_estatus","rutas.cat_estatus_id=cat_estatus.cat_estatus_id");
        $db->join("clientes","rutas.id_clientes=clientes.cliente_id");
        $db->join("pedidosaduana","pedidosaduana.id=rutas.id_pedidosaduana");
        //$db->where_in("status",[4,5,6,7]);
        $db->order_by("rutas.fecha_creacion","DESC");
        return $db->get()->result();

    }//...listar_rutas

    public function get_location_cliente_by_id($cliente_id){
        return $this->db->where("cliente_id",$cliente_id)->get("clientes")->row();
    }//...get_location_cliente_by_id

    public function get_rutas_by_id($id_rutas){
        $db=$this->db;
        $db->select("rutas.*,pedidosaduana.folio_documento,pedidosaduana.nombre_cliente,cat_estatus.cat_estatus_nombre");
        $db->join("pedidosaduana","pedidosaduana.id=rutas.id_pedidosaduana");
        $db->join("cat_estatus","rutas.cat_estatus_id=cat_estatus.cat_estatus_id");
        $db->where("rutas_id",$id_rutas);
        return $db->get("rutas")->row();
    }//...get_rutas_by_id

    public function get_pedidosaduana_by_id($id_pedidosaduana){
        $db=$this->db;
        $db->select("pedidosaduana.*,cat_estatus.cat_estatus_nombre");
        $db->from("pedidosaduana");
        $db->join("cat_estatus","pedidosaduana.status=cat_estatus.cat_estatus_id");
        $db->where("id",$id_pedidosaduana);
        return $db->get()->row();
    }//...get_pedidosaduana_by_id

    public function get_pedidosaduana_detalle($id_pedidosaduana){
        $result=$this->db
                ->select("pedidosaduana_detalle.*,codigosbarras.codigobarra_codigo, productos.familia_id, familia.familia_tipo")
                ->join("productos","pedidosaduana_detalle.id_producto = productos.producto_id")
                ->join("familia","productos.familia_id = familia.familia_id")
                ->join("codigosbarras","pedidosaduana_detalle.id_producto = codigosbarras.producto_id")
                ->where("id_pedidosaduana",$id_pedidosaduana)
                ->order_by("id","DESC")
                ->get("pedidosaduana_detalle")->result();

        return $result;
    }//...get_pedidosaduana_detalle

    public function get_pedidosaduana_detalle_no_join($id_pedidosaduana){

        $result=$this->db
                ->where("id_pedidosaduana",$id_pedidosaduana)
                ->order_by("id","DESC")
                ->get("pedidosaduana_detalle")->result();
        return $result;
    }//...get_pedidosaduana_detalle_no_join

    public function get_pedidosaduana_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->select("pedidosaduana.*,cat_estatus.cat_estatus_nombre");
        $db->from("pedidosaduana");
        $db->join("cat_estatus","pedidosaduana.status=cat_estatus.cat_estatus_id");
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get()->row();
    }//...get_pedidosaduana_by_FUNI

    public function get_id_pedidosaduana_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->where("folio_unico_documento",$folio_unico_documento);
        $row=$db->get("pedidosaduana")->row();
        $id_pedido=$row->id;

        return $id_pedido;
    }//...get_pedidosaduana_by_FUNI

    public function get_clientespedido_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->select("clientespedido.*,cat_estatus.cat_estatus_nombre");
        $db->from("clientespedido");
        $db->join("cat_estatus","clientespedido.status=cat_estatus.cat_estatus_id");
        $db->where("folio_unico_documento",$folio_unico_documento);
        return $db->get()->row();
    }//...get_clientespedido_by_FUNI

    public function get_id_clientespedido_by_FUNI($folio_unico_documento){
        $db=$this->db;
        $db->where("folio_unico_documento",$folio_unico_documento);
        $row=$db->get("clientespedido")->row();
        $id_pedido=$row->id;

        return $id_pedido;
    }//...get_id_clientespedido_by_FUNI

    public function get_existencias_producto($id_producto){
        return $this->db->select("producto_existencias,producto_existencias_comprometidas_pedido,producto_existencias_comprometidas_compras")
                        ->where("producto_id",$id_producto)
                        ->get("productos")->row();
    }//...get_comprometidos_producto

    public function actualizar_comprometidos_producto($id_producto,$comprometidos){
        $campo="producto_existencias_comprometidas_pedido";
        $query="UPDATE `productos` 
            SET `$campo` = `$campo` + $comprometidos 
            WHERE `producto_id` = '$id_producto'";
        $this->db->query($query);
    }//...actualizar_comprometidos_producto

    private function update_almacen($id_pedidosaduana,$campo,$multiplicador){
        $pedAd_detalle=$this->db->select("id_producto,cantidad")
                            ->where("id_pedidosaduana",$id_pedidosaduana)
                            ->get("pedidosaduana_detalle")
                            ->result();
        foreach ($pedAd_detalle as $producto) {

            $query="UPDATE `productos` 
                    SET `$campo` = `$campo` + $producto->cantidad*$multiplicador 
                    WHERE `producto_id` = '$producto->id_producto'";
            $this->db->query($query);
        }
    }//...update_almacen

    public function get_set_ultimo_status($id_pedidosaduana,$nvo_stat){

        $row=$this->db->select("status")
                    ->where("id",$id_pedidosaduana)
                    ->get("pedidosaduana")->row();
        $ultimo_stat=$row->status;

            if($ultimo_stat==7){
                /*
                    pagado->abierto
                        sumar existencias
                        sumar comprometidos  
                    pagado->cerrado
                        sumar existencias
                        sumar comprometidos    
                    pagado->cancelado
                        sumar existencias
                        sumar comprometidos  
                */
                $multipl_exist=1;
                $multipl_compr=1;
            }
            if($nvo_stat==7){
                /*  
                    abierto->pagado
                        restar existencias
                        restar comprometidos
                    cerrado->pagado
                        restar existencias
                        restar comprometidos
                    cancelado->pagado
                        restar existencias
                        restar comprometidos
                */
                $multipl_exist=-1;
                $multipl_compr=-1;
            }
            if($ultimo_stat==6 && ($nvo_stat==4 || $nvo_stat==5)){
                /* 
                    cancelado->abierto
                        sumar comprometidos
                    cancelado->cerrado
                        sumar comprometidos
                */
                $multipl_exist=0;
                $multipl_compr=1;
            }
            if($ultimo_stat==5 && $nvo_stat!=6){
                /* 
                    cerrado->cancelado
                        restar comprometidos
                */
                $multipl_exist=0;
                $multipl_compr=-1;
            }
            if($ultimo_stat==4 && $nvo_stat==6 ){
                /* 
                    abierto->cancelado
                        restar comprometidos
                */
                $multipl_exist=0;
                $multipl_compr=-1;
            }
            if(($ultimo_stat==5 && $nvo_stat==4) || ($ultimo_stat==4 && $nvo_stat==5) || ($ultimo_stat==$nvo_stat)){
                /* 
                    cerrado->abierto
                        nada (comprometidos se agregaron antes de enviar a aduana)
                    abierto->cerrado
                        nada (comprometidos se agregaron antes de enviar a aduana)
                */
                $multipl_exist=0;
                $multipl_compr=0;
            }

            if($multipl_exist!=0){
                $this->update_almacen($id_pedidosaduana,"producto_existencias",$multipl_exist);
            }
            if($multipl_compr!=0){
                $this->update_almacen($id_pedidosaduana,"producto_existencias_comprometidas_pedido",$multipl_compr);
            }

            
            $this->actualizar_fecha_modificacion($id_pedidosaduana);

            if($nvo_stat=="5"){
              $data["chk_aduana"]=1;
            }

            $data["status"]=$nvo_stat;
            return $this->db->update("pedidosaduana", $data, array("id"=>$id_pedidosaduana));
                    //return ($result)?$result:false;

    }//...get_set_ultimo_status

    public function get_clientespedido_detalle($id_clientespedido){
        $db=$this->db;
        $db->where("id_clientespedido",$id_clientespedido);
        $db->order_by("id","DESC");
        $result=$db->get("clientespedido_detalle")->result();
        return $result;
    }//...get_clientespedido_detalle
    
    public function get_clientes_condicion($cliente_id){
        /* SELECT clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion 
            FROM `clientescondicion`
            join tipodeprecio ON clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id
            join metodopago ON clientescondicion.metodopago_id= metodopago.metodopago_id
            WHERE clientescondicion.cat_estatus_id='1' */
        $db=$this->db;
        $db->select("clientescondicion.*,tipodeprecio.tipodeprecio_descripcion,tipodeprecio.tipodeprecio_porcentaje,metodopago.metodopago_descripcion");
        $db->from("clientescondicion");
        $db->join("tipodeprecio","clientescondicion.tipodeprecio_id=tipodeprecio.tipodeprecio_id");
        $db->join("metodopago","clientescondicion.metodopago_id= metodopago.metodopago_id");
        $db->where("clientescondicion.cat_estatus_id",1);
        $db->where("clientescondicion.cliente_id",$cliente_id);

        return $db->get()->result();
    }//...get_clientes_condicion

    public function get_info_cliente($cliente_id){
        /* SELECT *
            FROM clientes
            WHERE cliente_id = $cliente_id*/
        $db=$this->db;
        $db->where("cliente_id",$cliente_id);
        
        return $db->get("clientes")->row();

    }//...get_info_cliente

    public function get_porcentaje_tipo_precio($id_clientescondicion){
        /* SELECT tipodeprecio_porcentaje 
            FROM `tipodeprecio`
            WHERE tipodeprecio_id=
                (
                    SELECT clientescondicion.tipodeprecio_id FROM clientescondicion WHERE clientescondicion.clientescondicion_id=12
                ) 
        */
        $db=$this->db;
        $db->select("tipodeprecio_porcentaje");
        $db->from("tipodeprecio");
        $db->where("tipodeprecio_id= 
                    (SELECT clientescondicion.tipodeprecio_id 
                        FROM clientescondicion 
                        WHERE clientescondicion.clientescondicion_id=$id_clientescondicion)"
                    );
        $porcentaje=$db->get()->row();
        $porcentaje=$porcentaje->tipodeprecio_porcentaje/100;

        return $porcentaje;
    }//...get_porcentaje_tipo_precio

    public function get_precio_producto($producto_id){
        $precio=$this->db->get_where("productos",array("producto_id"=>$producto_id))->row()->producto_costo_venta;
        return $precio;
    }//...get_precio_producto

    public function get_productos($campo,$value){
        return $this->db
        ->join("codigosbarras","productos.producto_id = codigosbarras.producto_id")
        ->where($campo,$value)
        ->order_by("productos.producto_id")
        ->get("productos")
        ->result();
    }//...get_productos

    public function get_productos_no_validados($id_pedidosaduana){
        $result= $this->db
        ->where(array("id_pedidosaduana"=>$id_pedidosaduana,"validado"=>0))
        ->get("pedidosaduana_detalle")
        ->result();
        return ($result)?$result:false;
    }//...get_productos_no_validados

    public function get_cant_producto($id_pedido,$id_producto){

        $product_row=$this->get_existencias_producto($id_producto);

        $detalle_row = $this->db->select("cantidad,cantidad_original,validado")
                         ->where(array("id_pedidosaduana"=>$id_pedido,"id_producto"=>$id_producto))
                         ->get("pedidosaduana_detalle")->row();
        $arr=array(
            "existencias"=>round($product_row->producto_existencias, 3, PHP_ROUND_HALF_EVEN),
            "comprometidos"=>round($product_row->producto_existencias_comprometidas_pedido, 3, PHP_ROUND_HALF_EVEN),
            "por_comprar"=>round($product_row->producto_existencias_comprometidas_compras, 3, PHP_ROUND_HALF_EVEN),
        );
        if(isset($detalle_row)){
            $arr["cantidad_original"]=round($detalle_row->cantidad_original, 3, PHP_ROUND_HALF_EVEN);
            $arr["cantidad"]=round($detalle_row->cantidad, 3, PHP_ROUND_HALF_EVEN);
            $arr["validado"]=$detalle_row->validado;
            $arr["status"]=true;
        }else{
            $arr["status"]=false;
        }
        return $arr;

    }//...get_cant_producto

    public function buscar_producto_pAduana_detalle($id_pedidosaduana,$id_producto){
        $db=$this->db;
        $db->where("id_producto",$id_producto);
        $db->where("id_pedidosaduana",$id_pedidosaduana);

        return $db->get("pedidosaduana_detalle")->row();
    }//...buscar_producto_pAduana_detalle
      
    public function actualizar_producto_pAduana_detalle($dataUpdate){
        /* 
        $dataUpdate['id_pedidosaduana']  = $id_pedidosaduana;
        $dataUpdate['id_producto']            = $id_producto;
        $dataUpdate['cantidad']               = $cantidad;
        $dataUpdate['costo_unitario']         = $costo_unitario;
        $dataUpdate['costo_total']            = $costo_total; 
        */
        $db=$this->db;
        $id_cot=$dataUpdate['id_pedidosaduana'];
        $id_prod=$dataUpdate['id_producto'];
        return $db->update("pedidosaduana_detalle", $dataUpdate, 
                            array("id_pedidosaduana"=>$id_cot,"id_producto"=>$id_prod)
                          );
    }//...actualizar_producto_pAduana_detalle

    public function agregar_producto_pAduana_detalle($data){
        $agregado= $this->Mgeneral->save_register("pedidosaduana_detalle",$data);
        if($agregado){
            return true;
        }else{
            return false;
        }
    }//...agregar_producto_pAduana_detalle

    public function eliminar_producto_pAduana_detalle($id_pedidosaduana,$id_producto){
        $data['cantidad']=0;
        $data['costo_total']=0;

        $eliminado=$this->db->update("pedidosaduana_detalle", $data, array("id_pedidosaduana"=>$id_pedidosaduana,"id_producto"=>$id_producto));

        //$eliminado=$this->db->delete("pedidosaduana_detalle",
        //                 array("id_pedidosaduana"=>$id_pedidosaduana,"id_producto"=>$id_producto
        //                        ));
        if($eliminado){
            $exist_producto=$this->get_existencias_producto($id_producto);
            $this->actualizar_comprometidos_producto($id_producto, -1 * $exist_producto->producto_existencias_comprometidas_pedido);
            return true;
        }else{
            return false;
        }
    }//...eliminar_producto_pAduana_detalle

    public function update_costo_by_condicion_pago($id_pedidosaduana,$id_clientescondicion){

        $db=$this->db;
        //obtener porcentaje de tipodeprecio
        $porcentaje=$this->get_porcentaje_tipo_precio($id_clientescondicion);
        //obtener los productos del la pedido
        $productos=$this->get_pedidosaduana_detalle($id_pedidosaduana);//viene como result

        $return_productos=array();
        //a cada producto hacer update a costo_unitario e importe en base al nuevo porcentaje
        foreach ($productos as $producto) {
            $id_producto            = $producto->id_producto;
            $costo_venta_producto   = $this->get_precio_producto($producto->id_producto);
            $iva                    = ($producto->IVA)/100;
            $ieps                   = ($producto->IEPS)/100;
            $cantidad               = $producto->cantidad;

            $costo_con_condicion    = $costo_venta_producto + ($costo_venta_producto*$porcentaje);
            $costo_iva              = $costo_con_condicion * ($iva);
            $costo_ieps             = $costo_con_condicion * ($ieps);
            $costo_con_impuestos    = $costo_con_condicion + $costo_iva + $costo_ieps;
            $nuevo_importe          = ($costo_con_impuestos) * $cantidad;

            $data['costo_unitario']=$costo_con_condicion;
            $data['costo_total']=$nuevo_importe;
            
            $exec=$db->update("pedidosaduana_detalle", $data, array("id_pedidosaduana"=>$id_pedidosaduana,"id_producto"=>$id_producto));
            
            $return_productos[$id_producto]=array();
            $return_productos[$id_producto]['exec']=($exec==true) ? true : false;
            $return_productos[$id_producto]['costo_venta_producto']=$costo_venta_producto;
            $return_productos[$id_producto]['costo_con_condicion']=$costo_con_condicion;
            $return_productos[$id_producto]['costo_iva']=$costo_iva;
            $return_productos[$id_producto]['costo_ieps']=$costo_ieps;
            $return_productos[$id_producto]['costo_con_impuestos']=$costo_con_impuestos;
            $return_productos[$id_producto]['nuevo_importe']=$nuevo_importe;

        }
        return $return_productos;

    }//...update_costo_by_condicion_pago

    public function calcular_totales($id_pedidosaduana){
        $productos=$this->get_pedidosaduana_detalle_no_join($id_pedidosaduana);//viene como result
        
        $sum_subtotal=0;
        $sum_iva=0;
        $sum_ieps=0;
        $sum_impuestos=0;
        $sum_total=0;
        $rastreo=array();
        $cont=0;
        foreach ($productos as $producto) {
            $id_producto=$producto->id_producto;
            $rastreo[$id_producto]=array();
            $cantidad           = $producto->cantidad;
            $costo_unitario     = $producto->costo_unitario;
            $iva                = ($producto->IVA)/100;
            $ieps               = ($producto->IEPS)/100;

            $subtotal_producto  = round($cantidad*$costo_unitario, 4, PHP_ROUND_HALF_EVEN);
            $sum_subtotal       += $subtotal_producto;
            $costo_iva          = $subtotal_producto * ($iva);
            $costo_ieps         = $subtotal_producto * ($ieps);

            $impuestos_producto = round($costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $iva_producto       = round($costo_iva, 4, PHP_ROUND_HALF_EVEN);
            $ieps_producto      = round($costo_ieps, 4, PHP_ROUND_HALF_EVEN);
            $total_producto     = round($subtotal_producto + $costo_iva + $costo_ieps, 4, PHP_ROUND_HALF_EVEN);

            $sum_impuestos      += $impuestos_producto;
            $sum_iva            += $iva_producto;
            $sum_ieps           += $ieps_producto;
            $sum_total          += $total_producto;

            $rastreo[$id_producto]["costo_unitario"]=$costo_unitario;
            $rastreo[$id_producto]["cantidad"]=$cantidad;
            $rastreo[$id_producto]["subtotal_producto"]=$subtotal_producto;
            $rastreo[$id_producto]["impuestos_producto"]=$impuestos_producto;
            $rastreo[$id_producto]["iva_producto"]=$iva_producto;
            $rastreo[$id_producto]["ieps_producto"]=$ieps_producto;
            $rastreo[$id_producto]["total_producto"]=$total_producto;

            $cont++;
        }
        $totales=array(
            "rastreo"=>$rastreo,
            "subtotal"=>$sum_subtotal,
            "impuestos"=>$sum_impuestos,
            "iva"=>$sum_iva,
            "ieps"=>$sum_ieps,
            "total"=>$sum_total,
        );
        $this->actualizar_totales($id_pedidosaduana,$totales["subtotal"],$totales["iva"],$totales["ieps"],$totales["total"]);
        return $totales;
        /* SELECT 
            SUM(`costo_unitario`) as subtotal,
            SUM(`costo_total`) AS total,
            (SUM(`costo_total`)-SUM(`costo_unitario`)) as impuestos, 
            SUM(`costo_unitario`)*(AVG(`IVA`)/100) AS iva,
            SUM(`costo_unitario`)*(AVG(`IEPS`)/100) AS ieps 
            FROM `pedidosaduana_detalle` 
            return $this->db->query("SELECT 
                SUM(`costo_unitario`) as subtotal,
                SUM(`costo_total`) AS total,
                (SUM(`costo_total`)-SUM(`costo_unitario`)) as impuestos, 
                SUM(`costo_unitario`)*(AVG(`IVA`)/100) AS iva,
                SUM(`costo_unitario`)*(AVG(`IEPS`)/100) AS ieps 
                FROM `pedidosaduana_detalle`
                WHERE `id_pedidosaduana`=$id_pedidosaduana")->row();*/

    }//...calcular_totales

    public function actualizar_totales($id_pedidosaduana,$subtotal,$iva,$ieps,$total){
        $data["subtotal"]=$subtotal;
        $data["IVA"]=$iva;
        $data["IEPS"]=$ieps;
        $data["TOTAL"]=$total;

        return $this->Mgeneral->update_table_row("pedidosaduana",$data,"id",$id_pedidosaduana);
    }//...actualizar_totales

    public function importar_productos_de_cPedido_detalle($id_pedidosaduana,$data_import){
        $cont=0;
        $result=array();
        foreach($data_import as $data){
            $insert=$this->Mgeneral->save_register("pedidosaduana_detalle", $data);
            $result[$cont]=$insert;
            $cont++;

        }
        
        return $result;
    }//...importar_productos_de_cPedido_detalle

    public function actualizar_fecha_modificacion($id_pedidosaduana){
     $query="UPDATE `pedidosaduana` SET `fecha_modificacion` = CURRENT_TIMESTAMP WHERE `id` = '$id_pedidosaduana'";
       return $this->db->query($query);
    }//...actualizar_fecha_modificado

    public function get_fecha_modificacion($id_pedidosaduana){
        $this->db->select("fecha_modificacion");
        $this->db->where("id",$id_pedidosaduana);
        $row_pedido=$this->db->get("pedidosaduana")->row();
        $timestamp=$row_pedido->fecha_modificacion;
        $fecha=date('d/m/Y,H:i',strtotime($timestamp));
        return $fecha;
    }//...get_fecha_modificacion
}
