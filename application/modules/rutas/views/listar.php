<style>
  .tr-info{
    border-radius:4px;
    width:5px;
    height:5px;
  }
  .tr-info-in-process{
    color:transparent;
    border:1px solid black;
  }
  .tr-info-ok{
    color:rgba(0,255,0,.7);
  }
  .tr-info-alert{

    color:rgba(255,255,0,.7);
  }
  .tr-info-danger{

    color:rgba(255,0,0,.7);
  }
  .tr-in-process{
    background-color:transparent;
    border:1px solid black;
  }
  .tr-ok{
    background-color: rgba(0,255,0,.7)!important;
  }
  .tr-alert{
    background-color: rgba(255,255,0,.7)!important;
  }
  .tr-danger{
    background-color: rgba(255,0,0,.7)!important;
  }
  .span-invisible{
    position:absolute;
    display:none;
  }
  .icofont-checked{
      font-size:2em;
      padding-left: 2px;
  }
  .icofont-close-squared{
      font-size:2em;
      padding-left: 2px;
  }
  .td-check{
      text-align:-webkit-center;
      padding: .40rem 0rem 0rem 0rem!important;
  }
  .check-container{
      width: 35px;
      height: 35px;
      border-radius: 10%;
      background: white;
      border: 1px solid black;
      cursor: pointer;
      box-shadow: inset -4px -4px 8px rgba(255, 255, 255, 1), inset 4px 4px 8px rgba(0, 123, 255, 0.3);
  }
  .check-container:hover{
      /* background: #FAFFA8; */
      background: rgba(0, 123, 255, 0.3);
      box-shadow:none;
  }
</style>

<div class="card">
  <!--<div class="card-header row">
    <div class="col-md-6">
      <h3 class="card-title">Pedidos en Aduana</h3>
    </div>
    <div class="col-md-6">

       <button id="agregar_pedidosaduana" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar Pedido</button> 

    </div>

  </div>-->
  <!-- /.card-header -->
  <div class="card-body">
  <?php
  echo "Tu rol: ".$rol_name=strtolower($_SESSION["infouser"]["rol_name"])
  ?>
<span id="timer"></span>
    <table id="tabla_pedidosaduana" class="table table-bordered table-striped">
      <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Fecha </th>
        <th>Folio aduana</th>
        <th>Status</th>
        <th>Cliente</th>
        <!-- <th>Vehiculo</th>
        <th>Chofer</th> -->
        <th>En ruta</th>
        <th>Entregado</th>
        <th>Opciones</th>
        <th>Coordenadas</th>
      </tr>
      </thead>
      <tbody>
      <?php 
  /*     echo $this->db->last_query();
      echo serialize($con_rutas);*/ 



      function color_tr($chk_ruta,$chk_entregado){
          $arr=array();

          if($chk_entregado){
            $arr["class"]  ="tr-ok";
          }else{
            $arr["class"]  ="tr-in-process";

          }
          return $arr;

          /*if($hoy_bd==$dia_producto){
              $transcurrido_ms        = timeToMS($hora_bd)-timeToMS($hora_producto); //la hora actual de bd - la hora en que se pidió el producto
              $arr["transcurrido_ms"] = $transcurrido_ms;
              $arr["transcurrido"]    = msToTime($transcurrido_ms);
              
              if($chk_entregado){
                  $arr["class"]           ="tr-ok";
              }else{
                  if($transcurrido_ms<$limite_elab_ms){
                  
                      $arr["class"]           =(!$chk_ruta)?"tr-in-process":"tr-danger";
                  }
                  if($transcurrido_ms>=$limite_elab_ms && $transcurrido_ms<=$limite_entrega_ms){
                      $arr["class"]           =(!$chk_entregado)?"tr-alert":"tr-danger";
                  }
                  if($transcurrido_ms>=$limite_elab_ms && $transcurrido_ms>=$limite_entrega_ms){
                      $arr["class"]           =(!$chk_ruta)?"tr-alert":"tr-danger";
                  }
              }
          }else{
              $arr["transcurrido"]    ="ok";
              $arr["class"]           ="tr-in-process";
          } 
          $arr["class"].=" ".$chk_ruta." ".$chk_entregado;
          return $arr;*/

      }//...color_tr
      
      function make_check($input,$checked_icon,$ruta_id){
          $data_input="data-input='".$input."'";
          if($checked_icon!=""){
              $string         ="<td class='td-check'>
                                  <span class='span-invisible'>1</span>
                                  <div class='check-container' data-check='true' ".$data_input." id='pRutas_".$ruta_id."'>".$checked_icon."</div>
                              </td>";
          }else{
              $string         ="<td class='td-check'>
                                  <span class='span-invisible'>0</span>
                                  <div class='check-container' data-check='false' ".$data_input." id='pRutas_".$ruta_id."'></div>
                              </td>";
          }
          
          return $string;
      }//...make_check
      //=======================================================================================================
      $perm_chk_ruta=["almacenista","aduana","administrador","Administrador"];//roles que pueden checkear chk_ruta
      $perm_chk_entregado=["almacenista","aduana","administrador","Administrador"];//roles que pueden checkear chk_entregado
      //=======================================================================================================

      $checked_icon="<i class='icofont-checked'></i>";
      //$td_empty="<td><i class='icofont-close-squared'></i></td>";
      $td_empty="<td></td>";
      $td_checked_disabled    ="<td class='td-check'>
                                  <span class='span-invisible'>1</span>".$checked_icon.
                              "</td>";
//print_r($con_rutas);
      foreach ($con_rutas as $ruta_row) {

        $ruta_id              = $ruta_row->rutas_id;
        //$fecha_creacion             = explode(" ",$ruta_row->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
        $td_checked     ="<td class='td-check'>
        <div class='check-container' data-check='true' id='pRutas_".$ruta_id."'>".$checked_icon."</div>
            </td>";
        $td_no_checked  ="<td class='td-check'>
                <span class='span-invisible'>0</span>
                <div class='check-container' data-check='false' id='pRutas_".$ruta_id."'></div>
            </td>";

        $fecha_creacion             = $ruta_row->fecha_creacion;
        $folio_documento            = $ruta_row->folio_documento;
        //$folio_unico_documento      = $ruta_row->folio_unico_documento;

        $id_clientes                = $ruta_row->id_clientes;
        $nombre_cliente             = $ruta_row->nombre_cliente;       
        $cliente_location           = $ruta_row->cliente_latitud.", ".$ruta_row->cliente_longitud;
        //$vehiculo                   = $ruta_row->vehiculos_marca." ".$ruta_row->vehiculos_linea;
        //$chofer                     = $ruta_row->choferes_nombre;
        $vehiculo                   = $ruta_row->id_vehiculos;
        $chofer                     = $ruta_row->id_choferes;
        $status_ped                 = $ruta_row->cat_estatus_id;
        
        foreach($vehiculos as $vc){
          $vehiculo=($vc==$vehiculo)?$vc->vehiculos_marca." ".$vc->vehiculos_linea:"";
        }
        foreach($choferes as $chof){
          $chofer=($chof==$chofer)?$chof->choferes_nombre:"";
        }

        /* INNERS */
        $cat_estatus_nombre                = $ruta_row->cat_estatus_nombre;
        /* ...INNERS */

        $btn_cancelar='
        <button id="btn_update_status" type="button" flag="'.$ruta_id.'" data-st="6" data-url="'.base_url().'index.php/rutas/rutas/cambiar_status/'.$ruta_id.'/6" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i>Cancelar</button>';
        
        if($ruta_row->cat_estatus_nombre=="cancelado"){
          $btn_cancelar='
          <button id="btn_update_status" type="button" flag="'.$ruta_id.'" data-st="4" data-url="'.base_url().'index.php/rutas/rutas/cambiar_status/'.$ruta_id.'/4" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Abrir"><i class="fas fa-folder-open"></i>Abrir</button>';
          
        }

        $verif_producto =color_tr($ruta_row->chk_ruta,$ruta_row->chk_entregado);
      
        if($ruta_row->chk_ruta==1){

            $ruta     =(in_array($rol_name,$perm_chk_ruta))?make_check("chk_ruta",$checked_icon,$ruta_id):$td_checked_disabled;
            //$entregado  =(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado",$checked_icon,$ruta_id):$td_empty; 
            $ruta=($status_ped!=6)?$ruta:$td_checked_disabled;
            //$entregado=($status_ped!=6)?$entregado:$td_checked_disabled;

        }
        else{

            $ruta     =(in_array($rol_name,$perm_chk_ruta))?make_check("chk_ruta","",$ruta_id):$td_empty;
            $ruta=($status_ped!=6)?$ruta:$td_empty;
            
        }//...if ruta

        if($ruta_row->chk_entregado==1){

            $ruta=$td_checked_disabled;
            $entregado=(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado",$checked_icon,$ruta_id):$td_checked_disabled;
            $entregado=($status_ped!=6)?$entregado:$td_checked_disabled;

        }
        else{

            $entregado  =(in_array($rol_name,$perm_chk_entregado))?make_check("chk_entregado","",$ruta_id):$td_empty;
            $entregado=($status_ped!=6)?$entregado:$td_empty;
        }//...if entregado
        
        $verif_producto["class"]  =($status_ped!=6)?$verif_producto["class"]:"tr-in-process";

        echo
        '<tr id="borrar_'.$ruta_id.'" class="'.$verif_producto["class"].'">
          <td>'.$fecha_creacion.'</td>
          <td>'.$folio_documento.'</td>
          <td>'.$cat_estatus_nombre.'</td>
          <td>'.$nombre_cliente.'</td>
          <!--<td>'.$vehiculo.'</td>
          <td>'.$chofer.'</td>-->

          '.$ruta.'
          '.$entregado.'

          <td>
            <a href="'.base_url().'index.php/rutas/rutas/ver_detalle/'.$ruta_id.'" class="btn btn-success btn-sm" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Ver detalle"><i class="fas fa-file-alt"></i> Ver</a>'
            .$btn_cancelar.'

          </td>
          <td>'.$cliente_location.'</td>
        </tr>';
      } ?>
      </tbody>

    </table>
  </div>
  <!-- /.card-body -->
</div>

<script>

  $(document).ready(function() {

/*     setInterval(function() {
        //window.($(location).attr("href"));<span>No elaborado<span><i class="icofont-square "></i></span></span>
        location.reload();
        $("body").addClass("modal-open");
    }, 15000); */
    $('#menupedidosaduana').addClass('active-link');

    $('[data-toggle="popover"]').popover();

    $("#tabla_pedidosaduana").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": true
    });

    add_check_events();

    $("th").click(function(){
      $(".check-container").off();//evitar redundancia de eventlisteners
      add_check_events();
    });
    function add_check_events(){
      $(".check-container").on("click",function(){
            
        var check=$(this);
        var id_pRutas=check.attr("id");
        var checked=check.data("check");
        var input=check.data("input");
        var span_invisible  = check.parent().find( "span" );

        if(!checked){
          var input_val=1;
          var callback1=function(){
              let iconCheck=$.parseHTML("<i class='icofont-checked'></i>");
              check.append(iconCheck);
              check.data("check",true);
              span_invisible.text("1");

          };

        }

        if(checked){
          var input_val=0;
          var callback1=function(){
              check.empty();
              check.data("check",false);
              span_invisible.text("0");

          };

        }
        ajaxJson("<?php echo base_url()?>index.php/rutas/Rutas/check_pressed",
                    {
                    "id_pRutas":id_pRutas,
                    "input":input,
                    "value":input_val
                    }, 
                    "POST", 
                    "", 
                    callback1
            );
      });//...$(".check-container").click
    }//...add_check_events


        $("#btn_update_status").click(function(event){
        event.preventDefault();
        let button_status=$(this);
        let new_status=button_status.data("st");
        if (new_status=="6"){
          var btn_label="Cancelar";
          var btn_class="danger";
          var msg="Desea cancelar el pedido?";
        }else{
          var btn_label="Abrir";
          var btn_class="success";
          var msg="Desea abrir el pedido?";
        }
        bootbox.dialog({
        message: msg,
        closeButton: true,
        buttons:
                {
                  "danger":
                            {
                              "label": "<i class='icon-remove'></i>"+btn_label,
                              "className": "btn-"+btn_class,
                              "callback": function () {
                              id = button_status.attr('flag');
                              url = button_status.attr('data-url');
                              //$("#borrar_"+id).slideUp();

                                $.get(url);
                                location.reload();

                              }
                              },
                  "cancel":
                              {
                                  "label": "<i class='icon-remove'></i> Volver",
                                  "className": "btn-sm btn-info",
                                  "callback": function () {

                                  }
                              }

                            }
                        });
      });

    });//document ready

</script>
