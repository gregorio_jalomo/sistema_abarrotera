<?php
//...datos_pedido
$id_pedido                  = $rutas->rutas_id;
$fecha_creacion             = explode(" ",$rutas->fecha_creacion);//separa timestamp("aaaa/mm/dd hh:mm:ss") en array["aaaa/mm/dd","hh:mm:ss"]
$folio_documento            = $rutas->folio_documento;
$id_clientes                = $rutas->id_clientes;
$nombre_cliente             = $rutas->nombre_cliente;       
$cliente_location           = $rutas->cliente_latitud.", ".$rutas->cliente_longitud;
//$vehiculo                   = $rutas->vehiculos_marca." ".$rutas->vehiculos_linea;
//$chofer                     = $rutas->choferes_nombre;
$vehiculo                   = $rutas->id_vehiculos;
$chofer                     = $rutas->id_choferes;
$status                     = $rutas->cat_estatus_id;
$vehiculo                   = $rutas->id_vehiculos;
$chofer                     = $rutas->id_choferes;

foreach($vehiculos as $vc){
  $vehiculo=($vc==$vehiculo)?$vc->vehiculos_marca." ".$vc->vehiculos_linea:"";
}
foreach($choferes as $chof){
  $chofer=($chof==$chofer)?$chof->choferes_nombre:"";
}
/* INNERS */

  $cat_estatus_nombre                = $rutas->cat_estatus_nombre;
  $status_text=$cat_estatus_nombre;
/* ...INNERS */
switch ($status) {
  case 3:
    $badge="secondary";
    break;
  case 4:
    $badge="success";
    break;
  case 5:
    $badge="secondary";
    break;
  case 6:
    $badge="danger";
    break;

  break;
}
$checked_icon="<i class='icofont-checked my-icofont'></i>";
$en_ruta=($rutas->chk_ruta==1)?$checked_icon:" ";
$entregado=($rutas->chk_entregado==1)?$checked_icon:" ";
//...datos_pedido
?>
<style>
  .ul-costo{
    /* text-align:center; */
    padding-inline-start: 0px!important;
  }
  .badge{
    float:right;
  }
  .height-extended{
    height: 100%;
  }
  .div-info-cot-total{
      background-color: rgba(23,162,184,.2);
      border-radius: 5px;
  }
  .my-icofont{
    font-size:2em;
    text-align:-web-kit-center;
  }
</style>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> <b>pedido:</b> "<?=$folio_documento?>" <span class="badge bg-<?=$badge?>"><?=$status_text?></span></h5>
          <div>
            <b>Fecha de creacion:</b> <?=$fecha_creacion[0]." ".$fecha_creacion[1]?> 
          </div>
        </div>

        <!-- Main content -->
        <div class="invoice col-12 p-3 mb-3">
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-12 mb-3">
              <div class="row">
                <div class="col-12 border-right">
                  <!-- title row -->
                  <div class="row height-extended">

                    <div class="col-2 border-right ">
                      <h4>
                        <small><b>Cliente:</b></small>
                      </h4>
                      <span><?=$nombre_cliente?></span>
                    </div>

                    <div class="col-2 border-right ">
                      <h4>
                        <small><b>Chofer:</b></small>
                      </h4>
                      <span><?=($chofer==0)?"No asignado":$chofer;?></span>
                    </div>
                    <div class="col-2 border-right ">
                      <h4>
                        <small><b>Vehiculo:</b></small>
                      </h4>
                      <span><?=($vehiculo==0)?"No asignado":$vehiculo;?></span>
                    </div>
                    <div class="col-2 border-right ">
                      <h4>
                        <small><b>En ruta:</b></small>
                      </h4>
                      <span><?=$en_ruta?></span>
                    </div>
                    <div class="col-2 border-right ">
                      <h4>
                        <small><b>Entregado:</b></small>
                      </h4>
                      <span><?=$entregado?></span>
                    </div>
                  </div>

                </div>
                <!-- /.col-6 -->
                <!-- title row -->

              </div>
            </div>
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Unidad</th>
                    <th>Cant.</th>
                    <th>Precio</th>
                    <th>IVA</th>
                    <th>IEPS</th>
                    <th>Importe</th>
                </tr>
                </thead>
                <tbody id="productos_pedido">
                <?php
                  foreach ($rutas_detalle as $producto) {
                    echo "
                    <tr>
                      <td>$producto->codigo_barras_producto</td>
                      <td>$producto->nombre_producto</td>
                      <td>$producto->descripcion_de_la_unidad</td>
                      <td>$producto->cantidad</td>
                      <td>$producto->costo_unitario</td>
                      <td>$producto->IVA%</td>
                      <td>$producto->IEPS%</td>
                      <td>$producto->costo_total</td>
                    </tr>
                    ";
                  }
                ?>
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.invoice -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
  $('document').ready(function() {
    $('#menuClientes').addClass('active-link');


  });
</script>
