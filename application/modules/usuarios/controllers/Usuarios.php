<?php //Úτƒ-8 encoded 
ob_start();if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Usuarios extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      //$this->load->model('Mproductos', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));
      date_default_timezone_set('America/Mexico_City');
      $this->load->model('usuarios/Usuarios_model');
      $this->load->model('usuarios/Roles_model');
      $this->rol_usuario  = (isset($this->session->userdata['infouser']))?$this->session->userdata['infouser']['rol']:-1;
      $this->id_modulo    = 7;
      $this->is_admin     = ($this->rol_usuario==2 || $this->rol_usuario==1)?true:false;
      $this->opc_edt	    = array(1,2,4,5,7);
      $this->opc_del	    = array(6,3);//
  }

  public function cargar_vista($url,$data,$archivos_js){

    $data['force_landscape'] = (isset($data['force_landscape']) && $data['force_landscape'] != false) ? true : false;

    $contenido = $this->load->view($url, $data, TRUE);
    $header = $this->load->view('main_template/head', $data, TRUE);
    $menu = $this->load->view('main_template/menu', '', TRUE);
    $header_contenido = $this->load->view('main_template/header_contenido', $data, TRUE);
    $footer = $this->load->view('main_template/footer', '', TRUE);
    $this->load->view('main_template/main', array(
      'header'=>$header,
      'menu'=>$menu,
      'header_contenido'=>$header_contenido,
      'contenido'=>$contenido,
      'footer'=>$footer,
      'included_js'=>$archivos_js
    ));
  }//...cargar_vista

  public function p_denied_view($view_req,$back_url){
    $data['titulo_seccion']   = "No tienes permiso para $view_req";
    $data['flecha_ir_atras']  = ($back_url!="")?$back_url:"inicio/inicio";
    $this->cargar_vista('errors/permission_denied',$data,array());
  }//...p_denied_view

  //------------------------ USUARIOS ----------------------------//
    public function listar(){
      if(validar_permiso($this->rol_usuario,$this->id_modulo,32) || $this->is_admin ){

        $data['titulo_seccion'] = "Usuarios";
        
        $data['roles']=$this->Roles_model->get_roles();
        // $data['flecha_ir_atras'] = 'clientes/menu';
        $data['usuarios'] = $this->Mgeneral->get_result('usuario_status',1,'usuarios');

        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/listar',$data,$archivos_js);

      }else{
        $this->p_denied_view("listar usuarios","");
      }

    }//...alta

    public function alta(){
      if(validar_permiso($this->rol_usuario,$this->id_modulo,31) || $this->is_admin ){

        $data['titulo_seccion'] = "Usuarios";
        $data['flecha_ir_atras'] = "usuarios/usuarios/listar";

        $data['roles']=$this->Roles_model->get_roles();
        // $data['flecha_ir_atras'] = 'clientes/menu';
        $data['usuarios'] = $this->Mgeneral->get_result('id_roles',1,'usuarios');
        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/alta',$data,$archivos_js);        

      }else{
        $this->p_denied_view("agregar usuarios","usuarios/listar");
      }


    }//...alta

    public function guardar(){

      if(validar_permiso($this->rol_usuario,$this->id_modulo,31) || $this->is_admin ){

        $this->form_validation->set_rules('usuario_nombre_compl', 'Nombre completo', 'required');
        $this->form_validation->set_rules('id_roles', 'Rol de usuario', 'required');
        $this->form_validation->set_rules('usuario_username', 'Nombre de usuario', 'required');
        $this->form_validation->set_rules('usuario_pass', 'Contraseña', 'required');
        $this->form_validation->set_rules('usuario_celular', 'Celular', 'required');

        $response = validate($this);
        if($response['status']){
          $data['usuario_nombre_compl'] = $this->input->post('usuario_nombre_compl');
          $data['id_roles'] = $this->input->post('id_roles');
          $data['usuario_username'] = $this->input->post('usuario_username');
          $data['usuario_pass'] = $this->input->post('usuario_pass');
          $data['usuario_celular'] = $this->input->post('usuario_celular');
          $data["fecha_ralta"]         = date("Y-m-d H:i:s");

          //...
          $this->Mgeneral->save_register('usuarios', $data);

        }
        //  echo $response;
        echo json_encode(array('output' => $response));        
        
      }else{
        $this->p_denied_view("agregar usuarios","usuarios/listar");
      }

    }//...guardar

    public function editar($id_usuario = null){//vista

      if(validar_permiso($this->rol_usuario,$this->id_modulo,34) || $this->is_admin ){

        $data['titulo_seccion'] = "Usuarios";
        $data['flecha_ir_atras'] = "usuarios/listar";
        $data['usuarios'] = $this->Mgeneral->get_row('usuario_id',$id_usuario,'usuarios');
        $data['roles']=$this->Roles_model->get_roles();

        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/editar',$data,$archivos_js);        
        
      }else{
        $this->p_denied_view("editar usuarios","usuarios/listar");
      }

    }//...editar

    public function actualizar(){//consulta
      $this->form_validation->set_rules('usuario_nombre_compl', 'Nombre completo', 'required');
      $this->form_validation->set_rules('usuario_username', 'Nombre de usuario', 'required');
      $this->form_validation->set_rules('usuario_pass', 'Contraseña', 'required');
      $this->form_validation->set_rules('usuario_celular', 'Celular', 'required');
      $this->form_validation->set_rules('id_roles', 'Rol del usuario', 'required');
      
      $response = validate($this);
      if($response['status']){
        $data['usuario_id'] = $this->input->post('usuario_id');
        $data['usuario_nombre_compl'] = $this->input->post('usuario_nombre_compl');
        $data['usuario_username'] = $this->input->post('usuario_username');
        $data['usuario_pass'] = $this->input->post('usuario_pass');
        $data['usuario_celular'] = $this->input->post('usuario_celular');
        $data['id_roles'] = $this->input->post('id_roles');


        $this->Mgeneral->update_table_row('usuarios',$data,'usuario_id',$data['usuario_id']);
      }
      //  echo $response;
      echo json_encode(array('output' => $response));
      
    }//...actualizar

    public function cambiar_estatus($usuario_id, $usuario_status) {
      $data['usuario_status'] = $usuario_status;

      $go=false;
      if((validar_permiso($this->rol_usuario,$this->id_modulo,34) || $this->is_admin) && in_array($data['usuario_status'],$this->opc_edt)){
        $go=true;
      }
      if((validar_permiso($this->rol_usuario,$this->id_modulo,35) || $this->is_admin) && in_array($data['usuario_status'],$this->opc_del)){
        $go=true;
      }
      if($go){
        $data['usuario_id'] = $usuario_id;
        $this->Mgeneral->update_table_row('usuarios',$data,'usuario_id',$data['usuario_id']);              
      }
      echo json_encode($go);

    }//...cambiar_estatus

    public function proceso_login_usuario(){
      $this->form_validation->set_rules('user', 'Usuario', 'trim|required');
      $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
      $this->form_validation->set_message('required', 'El campo %s es requerido');

      if ($this->form_validation->run() == FALSE) {
        $error = array(
          'error_user' => form_error('user'),
          'error_pass' => form_error('password'),
      );
        redirect('login');
      } else {
          $postUsername=$this->input->post('user');
          $postPassword=$this->input->post('password');
          
          $datalog = array(
            'user' => $postUsername,
            'pass' => $postPassword
          );

          $result = $this->Usuarios_model->login($datalog);

          if ($result['bool'] == TRUE) {
            $user_id          = $result['login_result'][0]->usuario_id;
            $user_name        = $result['login_result'][0]->usuario_username;
            $user_name_compl  = $result['login_result'][0]->usuario_nombre_compl;
            $user_celular     = $result['login_result'][0]->usuario_celular;
            $user_rol         = $result['login_result'][0]->id_roles;
            $user_rol_name    = $this->Usuarios_model->get_rol_name($user_rol);
            $user_status      = $result['login_result'][0]->usuario_status;


            $session_data = array(
              'usuario' => $user_name,
              'nombre' => $user_name_compl,
              'telefono' => $user_celular,
              'id' => $user_id,
              'rol' => $user_rol,
              'rol_name' => $user_rol_name->roles_nombre,
              'status' => $user_status 
            );

            $this->session->set_userdata('infouser', $session_data);
            redirect('inicio');

          }else{
            $error = array(
              'error_message' => "Usuario o contraseña incorrectos",
          );
            $this->load->view('login/index',$error);}

      }
    }//...proceso_login_usuario

    public function logout(){

      session_destroy();

      $error = array(
        'logout_message' => 'Se ha cerrado la sesión'
      );
      $this->load->view('login/index',$error);
    }//...logout

    public function new_pass(){//modal
      $this->load->view('usuarios/modal_new_pass');
    }//...new_pass
    
    public function cambiar_pass(){//bd
      $this->form_validation->set_rules('user_modal', 'Usuario', 'trim|required');
      $this->form_validation->set_rules('password_modal', 'Contraseña', 'trim|required');
      $this->form_validation->set_rules('new_password', 'Nueva Contraseña', 'required');
      $this->form_validation->set_rules('new_password_confirm', 'Confirmar Contraseña', 'required');

      $this->form_validation->set_message('required', 'El campo %s es requerido');
      $response=validate($this);
      $error=array();
      if($response['status']){

        $postUsername=$this->input->post('user_modal');
        $postPassword=$this->input->post('password_modal');
        
        $datalog = array(
          'user' => $postUsername,
          'pass' => $postPassword
        );

        $result = $this->Usuarios_model->login($datalog);

        if ($result['bool']) {
          $data['usuario_id'] =$result['login_result'][0]->usuario_id;

          $data['usuario_pass'] = $this->input->post('new_password_confirm');

          $update_pass=$this->Mgeneral->update_table_row('usuarios',$data,'usuario_id',$data['usuario_id']);
          
        }else{
          $update_pass=false;
          $error = array(
                    'error_message_pass' => 'No fué posible cambiar la contraseña, Usuario o contraseña incorrectos '
                  );
          //$this->load->view('login/index',$error);
        }
       }else{
        $error = array(
          'error_user_pass' => form_error('user_modal'),
          'error_pass_pass' => form_error('password_modal'),
      );
        //$this->load->view('login/index',$error);
       }
      //echo $response;
      echo json_encode(array('output' => $response, 'found_user' => $result['bool'], 'error' => $error));
    }//...cambiar_pass
  //...------------------------ USUARIOS ----------------------------//

  //------------------------ ROLES ----------------------------//
    public function listar_roles(){
      
      if($this->is_admin){

        $data['titulo_seccion'] = "Roles";
        $data['flecha_ir_atras'] = "usuarios/usuarios/menu";
        
        $data['roles']=$this->Roles_model->get_roles();
        
        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/listar_roles',$data,$archivos_js);
        
      }else{
        $this->p_denied_view("listar roles","");
      }

    }//...listar_roles

    public function guardar_rol(){
      if($this->is_admin){

        $this->form_validation->set_rules('rol', 'funcion', 'required');

        $response = validate($this);
        if($response['status']){
          $data['roles_nombre'] = $this->input->post('rol');

          $this->Mgeneral->save_register('roles', $data);

        }
        //  echo $response;
        echo json_encode(array('output' => $response));
        
      }else{
        $this->p_denied_view("editar roles","");
      }

    }//...guardar_rol

    public function borrar_rol($roles_id){
      if($this->is_admin){

        $data['roles_id'] = $roles_id;
          $response = $this->Mgeneral->delete_row("roles","roles_id",$data['roles_id']);
      
        //  echo $response;
        echo json_encode(array('output' => $response));
        
      }else{
        $this->p_denied_view("eliminar roles","");
      }

    }//...borrar_rol
  //...------------------------ ROLES ----------------------------//

  //------------------------ PERMISOS ----------------------------//
    public function listar_permisos(){
      if($this->is_admin){

        $data['titulo_seccion'] = "Permisos";
        $data['flecha_ir_atras'] = "usuarios/usuarios/menu";

        $data['roles']=$this->Roles_model->get_roles();
        $data['funciones']=$this->Roles_model->get_funciones();
        $data['all_permisos']=array();
        foreach($data['roles'] as $rol){
          $permisos_de_rol=$this->Roles_model->get_permisos($rol->roles_id);
          if(count($permisos_de_rol)>0){
            array_push($data['all_permisos'],$permisos_de_rol);
          }
        }
        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/listar_permisos',$data,$archivos_js);
        
      }else{
        $this->p_denied_view("listar permisos","");
      }

    }//...listar_permisos

    public function editar_permisos($id_roles){//vista editar
      if($this->is_admin){

        $data['titulo_seccion'] = "Permisos";
        $data['flecha_ir_atras'] = "usuarios/usuarios/listar_permisos";
        $data['all_permisos'] = array();
        $data['roles']=$this->Roles_model->get_roles();
        $data['funciones']=$this->Roles_model->get_funciones();
        $data['all_permisos']=array();
        foreach($data['roles'] as $rol){
          $permisos_por_rol=$this->Roles_model->get_permisos($rol->roles_id);
          if(count($permisos_por_rol)>0){
            array_push($data['all_permisos'],$permisos_por_rol);
          }
        }

        $data['funciones_selected']=$this->Roles_model->get_permisos($id_roles);
        
        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/editar_permisos',$data,$archivos_js);
        
      }else{
        $this->p_denied_view("editar permisos","");
      }

    }//...editar_permisos

    public function modificar_permiso(){//consulta a bd
      if($this->is_admin){

        $this->form_validation->set_rules('rol', 'rol', 'required');
        $this->form_validation->set_rules('funciones[]', 'funciones', 'required');
        $this->form_validation->set_rules('id_roles_antiguo', 'id_roles_antiguo', 'required');

        $response = validate($this);
        if($response['status']){
          $rol = $this->input->post('rol');
          $funciones = $this->input->post('funciones[]');
          $rol_antiguo= $this->input->post('id_roles_antiguo');

          if($rol_antiguo==$rol){$this->Mgeneral->delete_row("permisos","id_roles",$rol_antiguo);}

          foreach($funciones AS $funcion){
            $data['id_roles'] = $rol;
            $data['id_modulo_funciones']=$funcion;
            $this->Mgeneral->save_register('permisos', $data);
          }
          

        }
        //  echo $response;
        echo json_encode(array('output' => $response,'rol_antiguo'=>$rol_antiguo,'rol_input'=>$rol));
        
      }else{
        $this->p_denied_view("editar permisos","");
      }

    }//...modificar_permiso

    public function guardar_permiso(){
      if($this->is_admin){

        $this->form_validation->set_rules('rol', 'rol', 'required');
        $this->form_validation->set_rules('funciones[]', 'funcion', 'required');

        $response = validate($this);
        if($response['status']){
          $rol = $this->input->post('rol');
          $funciones = $this->input->post('funciones[]');

          foreach($funciones AS $funcion){
            $data['id_roles'] = $rol;
            $data['id_modulo_funciones']=$funcion;
            $this->Mgeneral->save_register('permisos', $data);
          }
          
        }
        //  echo $response;
        echo json_encode(array('output' => $response));
        
      }else{
        $this->p_denied_view("agregar permisos","");
      }

    }//...guardar_permiso

    public function borrar_permiso($permisos_id){
      if($this->is_admin){

        $data['permisos_id'] = $permisos_id;
          $response = $this->Mgeneral->delete_row("permisos","permisos_id",$data['permisos_id']);
      
        //  echo $response;
        echo json_encode(array('output' => $response));
        
      }else{
        $this->p_denied_view("eliminar permisos","");
      }

    }//...borrar_permiso
  //...------------------------ PERMISOS ----------------------------//

  //------------------------ FUNCIONES ----------------------------//
    public function listar_funciones(){
      if($this->is_admin){

        $data['titulo_seccion'] = "Funciones";
        $data['flecha_ir_atras'] = "usuarios/usuarios/menu";

        $data['funciones']=$this->Roles_model->get_funciones();

        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/listar_funciones',$data,$archivos_js);
        
      }else{
        $this->p_denied_view("listar funciones","");
      }

    }//...listar_funciones

    public function consulta_modulo_accion($modulo_descripcion,$accion_descripcion){

        $session_data = array(
          'modulo' => $modulo_descripcion,
          'accion' => $accion_descripcion
        );

        $this->session->set_userdata('ask_for_access', $session_data);
        //echo json_encode($this->session->userdata['ask_for_access']);
      
      $existe_modulo=$this->Roles_model->existe_modulo($modulo_descripcion);
      $existe_accion=$this->Roles_model->existe_accion($accion_descripcion);
      $data=array(
        "existe_modulo"=>$existe_modulo,
        "existe_accion"=>$existe_accion
      );
      
      return $data;
    }

    public function guardar_funcion(){
      if($this->is_admin){

        $this->form_validation->set_rules('modulo_descripcion', 'modulo', 'trim|required');
        $this->form_validation->set_rules('accion_descripcion', 'funcion', 'trim|required');

        $response = validate($this);

        $message="";

        if($response['status']){
          $modulo_descripcion=$this->input->post('modulo_descripcion');
          $accion_descripcion=$this->input->post('accion_descripcion');
          
            //consultar modulo y accion
            $modulo_accion=$this->consulta_modulo_accion($modulo_descripcion,$accion_descripcion);
            /* echo "===modulo_accion()===<br>";
            echo json_encode($modulo_accion);
            echo "<br>=================<br>"; */

            //verificar si existe el modulo
            if($modulo_accion["existe_modulo"]["rows"] == 0 ){
              //insert tabla modulos
              $data_modulos['modulo_descripcion'] = $modulo_descripcion;
              $id_nuevo_modulo=$this->Mgeneral->save_register('modulos', $data_modulos);
              $message.=" módulo agregado,";

            }else{
              $id_nuevo_modulo=$modulo_accion["existe_modulo"]["result"][0]->id_modulo;
              $message.=" módulo existente,";
            }

            //verificar si existe la accion
            if($modulo_accion["existe_accion"]["rows"] == 0 ){
              //insert tabla acciones
              $data_acciones['accion_descripcion'] = $accion_descripcion;
              $id_nueva_accion=$this->Mgeneral->save_register('acciones', $data_acciones);
              $message.=" acción agregada,";
              
              }else{
                $message.=" acción existente,";
                $id_nueva_accion=$modulo_accion["existe_accion"]["result"][0]->id_accion;
              }

            //verificar si existe la funcion
            $existe_funcion=$this->Roles_model->existe_funcion($id_nuevo_modulo,$id_nueva_accion);
            $queryFuncion=$this->db->last_query();
            if($existe_funcion["rows"] == 1 ){

              $funcion_existente=true;
              $message.=" función existente";

            }else{
              $funcion_existente=false;
              //insert tabla funciones
              $message.=" función agregada";
              $data_funciones['id_modulo'] = $id_nuevo_modulo;
              $data_funciones['id_accion'] = $id_nueva_accion;
              $this->Mgeneral->save_register('funciones', $data_funciones);
            }

        }
        //  echo $response;
        echo json_encode(array('output' => $response,
                              'funcion_existente'=>$funcion_existente,
                              'message'=>$message,
                          )
                        );
        
      }else{
        $this->p_denied_view("agregar funciones","");
      }

    }//...guardar_funcion

    public function consultar_permisos_por_idfuncion($id_funcion){
      $data['usuario_id'] = $id_funcion;
      $response =$this->Roles_model->get_permisos_por_idfuncion($id_funcion);
    
      //  echo $response;
      echo json_encode(array('output' => $response));
    }//...consultar_permisos_por_idfuncion

    public function borrar_funcion($id_funcion){
      if($this->is_admin){

        $data['id_funcion'] = $id_funcion;
          $response = $this->Mgeneral->delete_row("funciones","id_funcion",$data['id_funcion']);
      
        //  echo $response;
        echo json_encode(array('output' => $response));
        
      }else{
        $this->p_denied_view("borrar funciones","");
      }

    }//...borrar_funcion
  //...------------------------ FUNCIONES ----------------------------//

  //------------------------ CATALOGOS USUARIOS ----------------------------//
    public function menu(){
      if($this->is_admin){

        $data['menu_derecho'] = 'Menú Usuarios';
        $data['flecha_ir_atras'] = "usuarios/listar";
        $opciones = $this->load->view('main_template/menu', '', True);
        $archivos_js=array(
          'statics/js/bootbox.min.js',
          'statics/js/general.js?v='.time(),
          'statics/bootstrap4/js/bootstrap.min.js'
        );
    
        $this->cargar_vista('usuarios/menu',$data,$archivos_js);
        
      }else{
        $this->p_denied_view("listar usuarios","");
      }

    }//...menu
  //...------------------------ CATALOGOS USUARIOS ----------------------------//

}