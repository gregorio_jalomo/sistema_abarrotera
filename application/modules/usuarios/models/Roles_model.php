<?php

class Roles_model extends CI_Model{


    public function __construct()
    {
        parent::__construct();
    }

    public function get_permisos($id_rol)
    {  
        /*"SELECT   permisos.*,
                funciones.id_modulo,
                funciones.id_accion,
                roles.roles_nombre,
                modulos.modulo_descripcion,
                acciones.accion_descripcion
        FROM permisos 
        INNER JOIN `funciones` ON funciones.id_funcion = permisos.id_modulo_funciones 
        INNER JOIN `roles` ON roles.roles_id = permisos.id_roles
        INNER JOIN `modulos` ON funciones.id_modulo = modulos.id_modulo
        INNER JOIN `acciones` ON funciones.id_accion = acciones.id_accion
        WHERE `id_roles`= $is_rol" */
          $this->db->select("permisos.*,funciones.id_modulo,funciones.id_accion,roles.roles_nombre,modulos.modulo_descripcion,acciones.accion_descripcion");
          $this->db->from("permisos");
          $this->db->join("funciones","funciones.id_funcion = permisos.id_modulo_funciones");
          $this->db->join("roles","roles.roles_id = permisos.id_roles");
          $this->db->join("modulos","funciones.id_modulo = modulos.id_modulo");
          $this->db->join("acciones","funciones.id_accion = acciones.id_accion");
          $this->db->where("id_roles",$id_rol);
          $this->db->order_by("modulo_descripcion","DESC");

        $result=$this->db->get()->result();
          return $result;
      }
    
    public function get_roles()
    {
        return $this->Mgeneral->get_table('roles');
    }

    public function get_permisos_por_idfuncion($id_funcion)
    {
        return $this->Mgeneral->get_result("id_modulo_funciones",$id_funcion,"permisos");
    }

    public function get_funciones()
    {
            /*"SELECT funciones.*,modulos.modulo_descripcion,acciones.accion_descripcion 
                FROM funciones 
                INNER JOIN `modulos` ON funciones.id_modulo = modulos.id_modulo 
                INNER JOIN `acciones` ON funciones.id_accion = acciones.id_accion
                */
        $this->db->select("funciones.*,modulos.modulo_descripcion,acciones.accion_descripcion");
        $this->db->from("funciones");
        $this->db->join("modulos","funciones.id_modulo = modulos.id_modulo");
        $this->db->join("acciones","funciones.id_accion = acciones.id_accion");

        $result=$this->db->get()->result();
        return $result;
    }

    public function existe_modulo($modulo_descripcion){

        $result=$this->Mgeneral->get_result("modulo_descripcion",$modulo_descripcion,"modulos");
        $rows=count($result);
        $data=array("result"=>$result,"rows"=>$rows);
        return $data;
    }
    public function existe_accion($accion_descripcion){

        $result=$this->Mgeneral->get_result("accion_descripcion",$accion_descripcion,"acciones");
        $rows=count($result);
        $data=array("result"=>$result,"rows"=>$rows);
        return $data;
    }
    public function existe_funcion($id_modulo,$id_funcion){
        $query=$this->db->get_where('funciones', array('id_modulo' => $id_modulo,"id_funcion"=>$id_funcion));
        $result=$query->result();
        $rows=count($result);
        $data=array("result"=>$result,"rows"=>$rows);
        return $data;
    }

    public function validar_permiso($rol_usuario,$id_modulo,$id_funcion)
    {
        $this->db->select("permisos.*,funciones.id_modulo,funciones.id_accion,roles.roles_nombre");
        $this->db->from("permisos");
        $this->db->join("funciones","funciones.id_funcion = permisos.id_modulo_funciones");
        $this->db->join("roles","roles.roles_id = permisos.id_roles");
        $this->db->where("id_roles",$rol_usuario);
        $this->db->where("id_modulo",$id_modulo);
        $this->db->where("id_accion",$id_funcion);
        $exec=$this->db->get()->result();
        $query=$this->db->last_query();
        $result="";
        if(count($exec) == 1){
            return true;
        }else{
            return false;
        }
    }

}
?>