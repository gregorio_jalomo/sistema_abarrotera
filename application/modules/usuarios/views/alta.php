<style>
      .select2-selection__choice {
        background-color: #17a2b8 !important;
        border-color: #17a2b8 !important;
      }
  </style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header row">
            <h3 class="card-title">Agregar Usuario</h3>
          </div>

          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_usuario">
            <div class="card-body">
              <div class="row form-group">
              
                  <div class="col-md-4">
                    <label for="">Nombre Completo</label>
                    <input type="text" class="form-control" name="usuario_nombre_compl" id="usuario_nombre_compl">
                  </div>

                  <div class="col-md-4">
                    <label for="">Usuario</label>
                    <input type="text" class="form-control" name="usuario_username" id="usuario_username">
                  </div>

                  <div class="col-md-4">
                    <label for="">Contraseña</label>
                    <input type="text" class="form-control" name="usuario_pass" id="usuario_pass">
                  </div>

                  <div class="col-md-4">
                    <label for="">Celular</label>
                    <input type="text" maxlength="10"  class="form-control" name="usuario_celular" id="usuario_celular" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                  </div>

                    <div class="col-md-4">
                      <label for="roles">Roles:</label>
                      <select name="roles" id="roles" 
                        class="form-control select2 select-roles">
                        <option value=""></option>
                        <?php
                          foreach ($roles as $row) {
                              echo 
                                '<option value="'.$row->roles_id.'">'.$row->roles_nombre.'</option>
                              ';
                            }
                        ?>
                      </select>
                    </div> 
                </div>
              

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->

  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');
  $('.select-roles').select2({
      dropdownParent: $('.form-group'),
      placeholder: "Elige un rol...",
    });

  $('#alta_usuario').submit(function(event){
    event.preventDefault();
    console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/guardar";
    ajaxJson(url,{
        'usuario_nombre_compl' : $('#usuario_nombre_compl').val(),
        'id_roles' : $('#roles').val(),
        'usuario_username' : $('#usuario_username').val(),
        'usuario_pass' : $('#usuario_pass').val(),
        'usuario_celular' : $('#usuario_celular').val(),

      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      //alert(json_response);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/listar");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });

});//document ready
</script>



