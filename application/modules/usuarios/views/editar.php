<?php
  $usuario_id = $usuarios->usuario_id;
  $usuario_nombre = $usuarios->usuario_nombre_compl;
  $usuario_username = $usuarios->usuario_username;
  $usuario_pass = $usuarios->usuario_pass;
  $usuario_celular = $usuarios->usuario_celular;
  $usuario_id_roles = $usuarios->id_roles;

?>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Usuario</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_usuario">
            <div class="card-body">
              <div class="row form-group">

                  <div class="col-md-4">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="usuario_nombre_compl" id="usuario_nombre_compl" value="<?php echo $usuario_nombre; ?>">
                  </div>

                  <div class="col-md-4">
                    <label for="">Usuario</label>
                    <input type="text" class="form-control" name="usuario_username" id="usuario_username" value="<?php echo $usuario_username; ?>">
                  </div>

                  <div class="col-md-4">
                    <label for="">Contraseña</label>
                    <input type="text" class="form-control" name="usuario_pass" id="usuario_pass" value="<?php echo $usuario_pass; ?>">
                  </div>

                  <div class="col-md-4">
                    <label for="">Celular</label>
                    <input type="text" maxlength="10" class="form-control" name="usuario_celular" id="usuario_celular" value="<?php echo $usuario_celular; ?>" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                  </div>

                  <div class="col-md-4">
                    <label for="usuario_rol">Roles:</label>
                    <select name="id_roles" id="id_roles" 
                    class="form-control select2 select-roles">
                    <option value=""></option>
                    <?php
                      foreach ($roles as $row) {
                        $selected="";
                        if($usuario_id_roles==$row->roles_id){
                          $selected="selected='selected'";
                        }
                          echo 
                            '<option value="'.$row->roles_id.'" '.$selected.' >'.$row->roles_nombre.' </option>
                          ';
                        }
                    ?>
                    </select>
                  </div> 

                </div>
                

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                  <i class="fas fa-save"></i>&nbsp;
                  <span id="payment-button-amount">Actualizar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                <a href="<?php echo base_url().'index.php/usuarios/listar'; ?>" class="btn btn-danger" id="enviar">Cancelar</a>
              </div>
            
            </div>
            <!-- /.card-body -->
          </form>
        </div>
        <!-- /.card -->

          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $('#menuusuarios').addClass('active-link');
  $("#cargando").hide();

  $('.select-roles').select2({
      dropdownParent: $('.form-group'),
      placeholder: "Elige un rol...",
    });

  $('#editar_usuario').submit(function(event){
    event.preventDefault();
    console.log($('#usuario_nombre').val());
    
    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/actualizar";
    ajaxJson(url,{
        'usuario_id' : '<?php echo $usuario_id; ?>',
        'usuario_nombre_compl' : $('#usuario_nombre_compl').val(),
        'usuario_username' : $('#usuario_username').val(),
        'usuario_pass' : $('#usuario_pass').val(),
        'usuario_celular' : $('#usuario_celular').val(),
        'id_roles' : $('#id_roles').val()
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/listar");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
});
</script>
