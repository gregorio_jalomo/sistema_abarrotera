
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Editar Permiso</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="editar_permisos">
            <div class="card-body">

              <div class="row form-group">

                    <div class="col-md-4">
                      <label for="roles">Roles:</label>
                      <select name="roles" id="roles" 
                      class="form-control select2 select-roles">
                      <option value=""></option>
                      <?php
                        foreach ($roles as $row) {
                          $selected="";
                          if($row->roles_id==$funciones_selected[0]->id_roles){$selected="selected='selected'";}
                            echo 
                              '<option value="'.$row->roles_id.'" '.$selected.'>'.$row->roles_nombre.' </option>
                            ';
                          }
                          
                      ?>
                      </select>
                      
                    </div> 



                    <div class="col-md-4">
                      <label for="funciones">Funciones:</label>
                      <select name="funciones[]" id="funciones" 
                      class="form-control select2 select-multiple-funciones" multiple="multiple">
                      
                      <?php
                      $slctd_fn=array(
                        'modulo'=>array(),
                        'accion'=>array()
                      );
                      foreach($funciones_selected as $selected_fn){
                        array_push($slctd_fn['modulo'],$selected_fn->modulo_descripcion);
                        array_push($slctd_fn['accion'],$selected_fn->accion_descripcion);
                      }
                      foreach ($funciones as $row) {
                          $selected="";
                          if( in_array($row->modulo_descripcion,$slctd_fn['modulo']) && 
                              in_array($row->accion_descripcion,$slctd_fn['accion'])
                          ){$selected="selected='selected'";}
                          echo 
                            '<option value="'.$row->id_funcion.'" '.$selected.'>'.$row->modulo_descripcion."->".$row->accion_descripcion.'  </option>
                          ';
                        }
                      ?>

                      </select>
                    </div>
                    <input type="hidden" value="<?=$selected_fn->id_roles?>" id="id_roles_antiguo">
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div><!-- /.card-body -->
          </form>

        </div><!-- /.card -->
      </div><!--/.col (right) -->
    </div><!-- /.row -->

  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');

    $('.select-roles').select2({
      dropdownParent: $('.form-group'),
      placeholder: "Elige un rol...",
    });

    $('.select-multiple-funciones').select2({
      tags:true,dropdownParent: $('.form-group'),
      placeholder: "Elige una función...",
    });

  $('#editar_permisos').submit(function(event){
    event.preventDefault();
    //console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/usuarios/modificar_permiso";
    ajaxJson(url,{	
        'rol' : $('#roles').val(),
        'funciones' : $('#funciones').val(),
        'id_roles_antiguo': $('#id_roles_antiguo').val()
      },
      "POST","",function(result){
      console.log(result);
      json_response = JSON.parse(result);
      //alert(json_response);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/usuarios/listar_permisos");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });

});//document ready
</script>



