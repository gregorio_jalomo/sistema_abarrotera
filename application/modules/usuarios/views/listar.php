<style>
      .select2-selection__choice {
        background-color: #17a2b8 !important;
        border-color: #17a2b8 !important;
      }
  </style>
<section class="content">

    <div class="card">
        <div class="card-header row">
            <div class="col-md-6">
              <h3 class="card-title">Usuarios agregados</h3>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url().'index.php/usuarios/usuarios/menu'?>"><button class="btn btn-dark float-right"><i class="fas fa-archive"></i> Catálogos usuarios</button></a>
                <a href="<?php echo base_url().'index.php/usuarios/usuarios/alta'?>"><button class="btn btn-primary float-right"><i class="fas fa-plus"></i> Agregar usuario</button></a>
            </div>
            
            <div class="card-body">
              <div class="table-responsive p-0">
                <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Opciones</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($usuarios as $row ) {
                        $usuario_id = $row->usuario_id;
                        $usuario_nombre = $row->usuario_nombre_compl;

                        echo '
                          <tr id="borrar_'.$usuario_id.'">
                            <td>'.$usuario_nombre.'</td>
                            <td>
                              <a href="'.base_url().'index.php/usuarios/editar/'.$usuario_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                              <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusUsuario('.$usuario_id.',2);"><i class="fa fa-times"></i></button> -->
                              <a href="'.base_url().'index.php/usuarios/cambiar_estatus/'.$usuario_id.'/3" class="eliminar_relacion" flag="'.$usuario_id.'" id="delete'.$usuario_id.'">
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fas fa-trash-alt"></i> Eliminar</button>
                              </a>
                              <!--<a href="'.base_url().'index.php/usuarios/roles/'.$usuario_id.'" class="" flag="'.$usuario_id.'" id="roles'.$usuario_id.'">
                                <button type="button" class="btn btn-secondary btn-sm" ><i class="fas fa-user-check"></i> Roles</button>
                              </a>-->
                            </td>
                          </tr>
                        ';
                      }
                    ?>
                  </tbody>

                </table>
              </div>
            </div><!-- /.card-body-->
        </div><!-- /.card-header-->
    </div><!-- /.card -->

</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');

  $('#alta_usuario').submit(function(event){
    event.preventDefault();
    console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/guardar";
    ajaxJson(url,{
        'usuario_nombre' : $('#usuario_nombre').val(),
        'usuario_a_paterno' : $('#usuario_a_paterno').val(),
        'usuario_a_materno' : $('#usuario_a_materno').val(),
      },
      "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      alert(json_response);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/alta");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $.get(url,{},function(result){
                                if(result=="true"){
                                  $("#borrar_"+id).slideUp();
                                  ExitoCustom("Eliminado");
                                }else{
                                  ErrorCustom("No tienes permiso para borrar usuarios");
                                }
                              });
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
  });

});//document ready
</script>



