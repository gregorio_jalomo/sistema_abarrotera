<section class="content">
  <div class="container-fluid">

  <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Añadir Funcion</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_funcion">
            <div class="card-body">

              <div class="row form-group">
                    
                    <div class="col-md-4">
                      <label for="funcion">Módulo:</label>
                      <input type="text" style="text-transform: capitalize;" name="modulo" id="modulo" placeholder="Ej. Productos">
                    </div> 
                    
                    <div class="col-md-4">
                      <label for="funcion">Funcion:</label>
                      <input type="text" style="text-transform: capitalize;" name="funcion" id="funcion" placeholder="Ej. Agregar">
                    </div> 
                   
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div><!-- /.card-body -->
          </form>

        </div><!-- /.card -->
      </div><!--/.col (right) -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-12">
        <div class="card card-info"><!-- card -->
          <div class="card-header">
            <h3 class="card-title">Funciones agregadas</h3>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Funciones</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    
                    foreach ($funciones as $funcion ) {
                      echo '
                        <tr id="borrar_'.$funcion->id_funcion.'">

                          <td>'.$funcion->modulo_descripcion."->".$funcion->accion_descripcion.'</td>

                          <td> 
                            <a href="'.base_url().'index.php/usuarios/usuarios/borrar_funcion/'.$funcion->id_funcion.'" id="delete'.$funcion->id_funcion.'" ></a>
                            <button type="button" class="btn btn-danger btn-sm eliminar_funcion" flag="'.$funcion->id_funcion.'" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fa fa-times"></i></button>
                          </td>

                        </tr>
                      ';

                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div><!-- /.card-body -->
        </div><!-- /.card -->
      </div>
    </div>


  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');

//alta funcion
  $('#alta_funcion').submit(function(event){
    event.preventDefault();
    //console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/usuarios/guardar_funcion";
    ajaxJson(url,{	
        'modulo_descripcion' : capitalizeWord($('#modulo').val()),
        'accion_descripcion' : capitalizeWord($('#funcion').val())
      },
      "POST","",function(result){
      console.log(result);
      json_response = JSON.parse(result);
      //alert(json_response);
      obj_status = json_response.output.status;
      obj_message= json_response.message
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO("+obj_message+")","success","<?php echo base_url()?>index.php/usuarios/listar_funciones");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });
//...alta funcion

//borrar funcion
  $(".eliminar_funcion").click(function(event){

      var id_funcion=$(event.currentTarget).attr('flag');
      var link=$("#delete"+id_funcion).attr('href');
      var nombre_funcion=$("#borrar_"+id_funcion);
      nombre_funcion=nombre_funcion[0].outerText;

      event.preventDefault();
      bootbox.dialog({
        message: "You want to delete '"+nombre_funcion+"' function?",
        closeButton: true,
        buttons:
          {
            "danger":
                      {
                        "label": "<i class='icon-remove'></i>Eliminar ",
                        "className": "btn-danger",
                        "callback": function () {
                        id = id_funcion;
                        url = link;
                        $("#borrar_"+id).slideUp();
                          $.get(url);
                        }
                        },
                          "cancel":
                          {
                              "label": "<i class='icon-remove'></i> Cancelar",
                              "className": "btn-sm btn-info",
                              "callback": function () {

                              }
                          }

                      }
        });

  });
//...borrar funcion

});//document ready
</script>