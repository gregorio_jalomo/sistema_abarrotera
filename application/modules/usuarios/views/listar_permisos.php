<section class="content">
  <div class="container-fluid">

  <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Agregar Permisos</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_permisos">
            <div class="card-body">

              <div class="row form-group">

                    <div class="col-md-4">
                      <label for="roles">Roles:</label>
                      <select name="roles" id="roles" 
                      class="form-control select2 select-roles">
                      <option value=""></option>
                      <?php
                        foreach ($roles as $row) {
                            echo 
                              '<option value="'.$row->roles_id.'" >'.$row->roles_nombre.' </option>
                            ';
                          }

                      ?>
                      </select>
 
                    </div> 

                    <div class="col-md-4">
                      <label for="funciones">Funciones:</label>
                      <select name="funciones[]" id="funciones" 
                      class="form-control select2 select-multiple-funciones" multiple="multiple">
                    
                      <?php
                      foreach ($funciones as $row) {
                          echo 
                            '<option value="'.$row->id_funcion.'" >'.$row->modulo_descripcion."->".$row->accion_descripcion.'  </option>
                          ';
                        }
                      ?>

                      </select>
                    </div>
                   
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div><!-- /.card-body -->
          </form>

        </div><!-- /.card -->
      </div><!--/.col (right) -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-12">
        <div class="card card-info"><!-- card -->
          <div class="card-header">
            <h3 class="card-title">Permisos agregados</h3>
          </div><!-- /.card-header -->
          <div class="card-body">

            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Rol</th>
                  <th>Funciones</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    //if(count($all_permisos >0)){
                      foreach ($all_permisos as $rol ) {
                      $list_permisos=array();
                      $list_permisos2="<ul>";
                        foreach($rol as $permisos_rol){
                          $roles_id = $permisos_rol->id_roles;
                          $roles_nombre = $permisos_rol->roles_nombre;
                          $funcion_nombre=$permisos_rol->modulo_descripcion."->".$permisos_rol->accion_descripcion;
                          $funcion_id = $permisos_rol->id_accion;
                          array_push($list_permisos,$funcion_nombre);
                          $list_permisos2.="<li>- ".$funcion_nombre."</li>";
                        }
                        $list_permisos2.="</ul>";
                        echo '
                          <tr id="borrar_'.$roles_id.'">
                            <td>'.$roles_nombre.'</td>
                            <td>
                              <div>'.$list_permisos2.'</div>

                            </td>
                            <td>
                              
                            <a href="'.base_url().'index.php/usuarios/usuarios/editar_permisos/'.$roles_id.'" class="btn btn-primary btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Editar"><i class="fas fa-pen"></i> Editar</a>
                            <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Desactivar" onclick="estatusUsuario('.$roles_id.',2);"><i class="fa fa-times"></i></button> -->
                            <!--<a href="'.base_url().'index.php/usuarios/roles/'.$roles_id.'" class="" flag="'.$roles_id.'" id="roles'.$roles_id.'">
                            <button type="button" class="btn btn-secondary btn-sm" ><i class="fas fa-user-check"></i> Roles</button>
                            </a>-->

                            </td>

                          </tr>
                        ';
                        unset($list_permisos);
                        unset($list_permisos2);
                      }
                    //}
                  ?>
                </tbody>

              </table>
            </div>
          </div><!-- /.card-body -->
        </div><!-- /.card -->
      </div>
    </div>


  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');

  $('.select-roles').select2({
      dropdownParent: $('.form-group'),
      placeholder: "Elige un rol...",
    });

    $('.select-multiple-funciones').select2({
      tags:true,dropdownParent: $('.form-group'),
      placeholder: "Elige una función...",
    });

  $('#alta_permisos').submit(function(event){
    event.preventDefault();
    //console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/usuarios/guardar_permiso";
    ajaxJson(url,{	
        'rol' : $('#roles').val(),
        'funciones' : $('#funciones').val(),
      },
      "POST","",function(result){
      console.log(result);
      json_response = JSON.parse(result);
      //alert(json_response);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/usuarios/listar_permisos");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });

  $(".eliminar_relacion").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el registro?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                        $.get(url);
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
  });
  


});//document ready
</script>



