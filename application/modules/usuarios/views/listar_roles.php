
<section class="content">
  <div class="container-fluid">

  <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Añadir Rol</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="" method="post" novalidate="novalidate" id="alta_rol">
            <div class="card-body">

              <div class="row form-group">

                    <div class="col-md-4">
                      <label for="rol">Rol:</label>
                      <input type="text" name="rol" id="rol" placeholder="Ej. Cajero">
                    </div> 
                   
              </div>

              <div align="right">
                <button id="enviar" type="submit" class="btn  btn-info ">
                <i class="fas fa-save"></i>&nbsp;
                    <span id="payment-button-amount">Guardar</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
                <div align="right">
                  <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                </div>
              </div>

            </div><!-- /.card-body -->
          </form>

        </div><!-- /.card -->
      </div><!--/.col (right) -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-12">
        <div class="card card-info"><!-- card -->
          <div class="card-header">
            <h3 class="card-title">Roles agregados</h3>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive p-0">
              <table  class="table table-head-fixed text-nowrap" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Roles</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    
                    foreach ($roles as $rol ) {
                      echo '
                        <tr id="borrar_'.$rol->roles_id.'">

                          <td>'.$rol->roles_nombre.'</td>

                          <td> 
                            <a href="'.base_url().'index.php/usuarios/usuarios/borrar_rol/'.$rol->roles_id.'" id="delete'.$rol->roles_id.'" ></a>
                            <button type="button" class="btn btn-danger btn-sm eliminar_rol" flag="'.$rol->roles_id.'" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Eliminar"><i class="fa fa-times"></i></button>
                          </td>

                        </tr>
                      ';

                    }
                  ?>
                </tbody>

              </table>
            </div>
          </div><!-- /.card-body -->
        </div><!-- /.card -->
      </div><!--/.col-s12-->
    </div><!--/.row-->


  </div><!-- /.container-fluid -->
</section>

<script>
$(document).ready(function(){
  $("#cargando").hide();
  $('#menuUsuarios').addClass('active-link');

  $('#alta_rol').submit(function(event){
    event.preventDefault();
    //console.log($('#usuario_nombre').val());

    $("#enviar").hide();
    $("#cargando").show();
    var url ="<?php echo base_url()?>index.php/usuarios/usuarios/guardar_rol";
    ajaxJson(url,{	
        'rol' : $('#rol').val(),
      },
      "POST","",function(result){
      console.log(result);
      json_response = JSON.parse(result);
      //alert(json_response);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
        $("#enviar").show();
        $("#cargando").hide();
      }
      if(obj_status == true){
        exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/listar_roles");
        $("#enviar").show();
        $("#cargando").hide();
      }
    });
  });

  $(".eliminar_rol").click(function(event){
      event.preventDefault();
      bootbox.dialog({
      message: "Desea eliminar el rol?",
      closeButton: true,
      buttons:
        {
          "danger":
                    {
                      "label": "<i class='icon-remove'></i>Eliminar ",
                      "className": "btn-danger",
                      "callback": function () {
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                        $.get(url);
                      }
                      },
                        "cancel":
                        {
                            "label": "<i class='icon-remove'></i> Cancelar",
                            "className": "btn-sm btn-info",
                            "callback": function () {

                            }
                        }

                    }
                });
  });
  


});//document ready
</script>



