<div class=""><!--submenu Roles-->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Roles</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div> <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" >
        <div class="container-fluid" >
                <!-- Info boxes -->

                <div class="row" >
                        <div class="col-12 col-sm-6 col-md-3">

                            <a href="<?php echo base_url()?>index.php/usuarios/listar_roles">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-success elevation-1">
                                    <i class="fas fa-user-tag"></i>
                                    </span>

                                    <div class="info-box-content">

                                        <span class="info-box-text">Roles</span>
                                        <span class="info-box-number"></span>

                                    </div><!-- /...info-box-content -->
                                </div><!-- /...info-box -->
                            </a>
                        </div><!-- /...col -->

                </div><!-- /...row -->
        </div><!-- /...container-fluid -->
    </section>
</div><!-- /...submenu roles-->

<br>

<div class=""><!--submenu funciones-->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Permisos</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div> <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" >
        <div class="container-fluid" >
                <!-- Info boxes -->

                <div class="row" >

                        <div class="col-12 col-sm-6 col-md-3">
                            <a href="<?php echo base_url()?>index.php/usuarios/listar_permisos">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-success elevation-1">
                                            <i class="fas fa-user-tag"></i>
                                        </span>

                                        <div class="info-box-content">

                                            <span class="info-box-text">Permisos</span>
                                            <span class="info-box-number"></span>

                                        </div>
                                        <!-- /...info-box-content -->
                                    </div>
                                <!-- /...info-box -->
                            </a>
                        </div><!-- /...col -->
                </div><!-- /...row -->
        </div><!-- /...container-fluid -->
    </section>
</div><!-- /...submenu funciones-->

<br>

<div class=""><!--submenu funciones2-->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Funciones</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div> <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" >
        <div class="container-fluid" >
                <!-- Info boxes -->

                <div class="row" >

                        <div class="col-12 col-sm-6 col-md-3">
                            <a href="<?php echo base_url()?>index.php/usuarios/listar_funciones">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-success elevation-1">
                                            <i class="fas fa-user-tag"></i>
                                        </span>

                                        <div class="info-box-content">

                                            <span class="info-box-text">Funciones</span>
                                            <span class="info-box-number"></span>

                                        </div>
                                        <!-- /...info-box-content -->
                                    </div>
                                <!-- /...info-box -->
                            </a>
                        </div><!-- /...col -->
                </div><!-- /...row -->
        </div><!-- /...container-fluid -->
    </section>
</div><!-- /...submenu funciones2-->
<script>
  $('document').ready(function() {
      var header_flecha_atras=$(".title-arrow");
      header_flecha_atras.css("position","fixed");
      header_flecha_atras.css("left","90%");
    $('#menucatalogos').addClass('active-link');

  });
</script>
