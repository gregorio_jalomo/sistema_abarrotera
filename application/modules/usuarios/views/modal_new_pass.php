<?php ?>
<form id="form-new-pass" action="" method="post">




    <div class="input-group mb-3">
      <input type="user" id="user_modal" name="user_modal" class="form-control" placeholder="Usuario">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-user"></span>
        </div>
      </div>
    </div>

    <div class="input-group mb-3">
      <input type="password" id="password_modal" name="password_modal" class="form-control" placeholder="Contraseña actual">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>
    <div class="input-group mb-3">
      <input type="password" id="new_password" name="new_password" class="form-control" placeholder="Nueva contraseña">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>
    <div class="input-group mb-3">
      <input type="password" id="new_password_confirm" name="new_password_confirm" class="form-control" placeholder="Confirma la nueva contraseña">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>

</form>