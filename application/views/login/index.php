<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GROVA Iniciar sesión</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/dist/css/adminlte.min.css">
    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo base_url()?>statics/assets/css/custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .modal-title{
      text-align:left;
    }
  </style>
</head>
<body class="hold-transition login-page"
style="background-color:#1A4A60">
<div class="login-box">
  <div class="login-logo">
    <img width="90px" height="90px" src="<?php echo base_url()?>statics/tema/dist/img/logo.png">
    <br>
    <a href="#"><b style="color:white;">ABARROTERA GROVA</b></a>
  </div>

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicio de sesión</p>

      <form action="<?php echo site_url('/usuarios/proceso_login_usuario') ?>" method="post">

        <?php if (isset($logout_message)) { ?>
              <div class="text-center p-t-12">
                <span class="txterror"><?php echo $logout_message;?>
                </span>
              </div>

        <?php }?>
            <?php if (isset($error_message)) { ?>
              <div class="text-center p-t-12">
                <span class="txterror"><?php echo $error_message;?>
                </span>
              </div>
            <?php }?>

        <?php if (isset($error_user)) { ?>
            <div class="text-center">
              <span class="txterror"><?php echo $error_user;?>
              </span>
            </div>
          <?php } ?>

        <?php if (isset($error_pass)) { ?>
            <div class="text-center">
              <span class="txterror"><?php echo $error_pass;?>
              </span>
            </div>
          <?php } ?>

          <div class="input-group mb-3">
            <input type="user" id="user" name="user" class="form-control" placeholder="Usuario">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
<!-- 
          <div class="input-group">
              <?php /* echo form_error('password', '<div class="text-center col-12"> <span class="txterror">', '</span></div>');  */?>
          </div> -->
          <div class="input-group mb-3">
            <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-8">
              <div class="a-login">
              <!--<div class="icheck-primaryn">
                <input type="checkbox" id="remember">
                <label for="remember">
                  Recordarme
                </label> -->
                <a class="cambiar-pass" href="#">Cambiar contraseña</a>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <a href="<?php echo base_url()?>index.php/inicio"><button type="submit" class="btn btn-primary btn-block">Entrar</button>
              </a>
            </div>
            <!-- /.col -->
          </div>
      </form>

      <!--div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div-->
      <!-- /.social-auth-links -->

      <!--p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p-->
      <!--p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p-->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
<!-- jQuery -->
<script src="<?php echo base_url()?>statics/tema/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()?>statics/tema/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>statics/tema/dist/js/adminlte.min.js"></script>

<script src="<?php echo base_url('statics/js/bootbox.min.js') ?>"></script>
<!-- general.js -->
<script src="<?php echo base_url()?>statics/js/general.js?v=')<?=time()?> "></script>
<script>
var site_url = "<?php echo site_url() ?>";

          $(".cambiar-pass").click(function(){

                customModal(site_url+'/usuarios/new_pass',
                  {},
                  "POST",
                  "md",
                  confirmNewPass,
                  "callbackCancel",
                  "callbackExtra",
                  "Confirmar",
                  "Cerrar",
                  "txtBtnExtra",
                  "Cambiar contraseña",
                  "modal_new_pass",
                  false
                );

          });

confirmNewPass = function(){
  var newPass=$("#new_password").val();
  var newPassConfirm=$("#new_password_confirm").val();
  if(newPass.length >=4 ){
    if( newPass == newPassConfirm){
      confirmar_new_pass();
      }else{
        ErrorCustom("El campo 'Nueva contraseña' y 'Confirmar contraseña' deben coincidir");
      }  
  }else{
      ErrorCustom("El campo 'Nueva contraseña' debe tener al menos 4 caracteres");
    }
}

function confirmar_new_pass(){


  var url = site_url+'/usuarios/cambiar_pass';

  ajaxJson(
    url,
    {
      user_modal:$("#user_modal").val(),
      password_modal:$("#password_modal").val(),
      new_password:$("#new_password").val(),
      new_password_confirm:$("#new_password_confirm").val(),
    },
    "POST",
    "async",
    function(result){
      //alert(JSON.stringify(JSON.parse(result)));
      result=JSON.parse(result);
      if(result.output.status){
        //Errores form modal
        if(result.error.error_user_pass){
          ErrorCustom(result.error.error_user_pass);
        }
        if(result.error.error_pass_pass){
          ErrorCustom(result.error.error_pass_pass);
        }

        if(result.found_user){
              ExitoCustom("¡La contraseña se actualizó correctamente! Ahora puedes iniciar sesión",
                              function(){
                                $('.modal_new_pass').modal('toggle');
                                $('#user').focus();
                                //window.location.href = site_url;
                              },
                              "Ok!"
              );

        }else{
          //error usuario/contraseña incorrectos
          if(result.error.error_message_pass){
            ErrorCustom(result.error.error_message_pass);
            $('#user_modal').val("");
            $('#password_modal').val("");
            $('#new_password').val("");
            $('#new_password_confirm').val("");

          }
        }
        
      }else{
          ErrorCustom('Asegurate de llenar todos los campos.');
      }
  });//ajaxJson


}

</script>
</body>
</html>
