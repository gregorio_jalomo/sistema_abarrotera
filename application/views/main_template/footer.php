<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b></b> 
    </div>
    <strong>Copyright &copy;VargasGroup 2020  All rights
    reserved.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- <script src="<?php echo base_url()?>statics/tema/plugins/jquery/jquery.min.js"></script> -->
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()?>statics/tema/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url()?>statics/tema/plugins/select2/js/select2.full.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url()?>statics/tema/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url()?>statics/tema/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- DataTables buttons -->
<script src="<?php echo base_url()?>statics/tema/plugins/datatables-buttons/js/dataTables.buttons.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url()?>statics/tema/plugins/datatables-buttons/js/buttons.html5.min.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url()?>statics/tema/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>statics/tema/dist/js/demo.js"></script>
<script>
var site_url = "<?php echo site_url() ?>";
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      
    });

    //Initialize Select2 Elements
    $('.select2').select2({
      language: {
        noResults: function() {
          return "No se encontraron resultados";        
        },
        searching: function() {
          return "Buscando..";
        }
      }
    });

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  });
  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()
</script>
