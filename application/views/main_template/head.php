<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <link rel="icon" type="image/ico" sizes="64x64" src="<?=base_url()?>statics/tema/dist/img/logo_cut.ico"> -->
  <!-- <title>

  Abarrotera
  </title> -->
  <link rel="icon" type="image/png" href="<?=base_url()?>statics/tema/dist/img/favicon/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?=base_url()?>statics/tema/dist/img/favicon/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?=base_url()?>statics/tema/dist/img/favicon/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?=base_url()?>statics/tema/dist/img/favicon/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?=base_url()?>statics/tema/dist/img/favicon/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="Abarrotera GROVA"/>
<!-- <meta name="msapplication-TileColor" content="#FFFFFF" /> -->
<meta name="msapplication-TileImage" content="mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php
  
  if(isset($force_landscape) && $force_landscape != false){
    //echo '<link rel="manifest" href="'.base_url().'statics/js/manifest.json">';
?>
    <style type="text/css">
        #force-landscape-message{
          color:#3c8dbc;
          display: none; 
      }
        @media only screen and (orientation:portrait){
            .wrapper { 
              display:none; 
            }
            .content-wrapper { 
              display:none; 
            }
            #force-landscape-message { 
              display:block; 
            }
        }
        @media only screen and (orientation:landscape){
            .wrapper { 
              display:revert; 
            }
            .content-wrapper { 
              display:revert; 
            }
            #force-landscape-message{
              display:none; 
            }
        }
    </style>

<?php


  }
  
  ?>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/datatables-buttons/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/tema/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/assets/css/custom.css?v=<?php echo time();?>">
  <link rel="stylesheet" href="<?php echo base_url()?>statics/assets/css/main-style.css?v=<?php echo time();?>">
  
  <!-- icofont -->
  <link rel="stylesheet" href="<?php echo base_url() ?>/statics/assets/css/icofont.css?v=<?php echo time();?>" >
  <!-- ContextMenu CSS -->
  <link rel="stylesheet" href="<?php echo base_url()?>statics/assets/css/contextMenu/jquery.contextMenu.min.css">


  <script src="<?php echo base_url()?>statics/tema/plugins/jquery/jquery.min.js"></script>

<?php
    
  if(isset($force_landscape) && $force_landscape != false){

?>
<div id="force-landscape-message">
  <img src="<?=base_url("statics/tema/dist/img/force-landscape.png")?>" alt="" width="50%" height="50%">
  <br>Por favor gire su dispositivo horizontalmente.
</div>
<?php

  }