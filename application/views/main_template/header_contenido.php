<section class="content-header title-arrow">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>
          <?php if(isset($titulo_seccion)): ?>
                <?php echo $titulo_seccion; ?>
          <?php endif; ?>
        </h1>
      </div>
      <!-- <div class="col-sm-6">
        <?php if(isset($menu_derecho)): ?>
                <?php echo $menu_derecho; ?>
        <?php endif; ?>
      </div> -->
      <?php 
        if(isset($flecha_ir_atras)) {
          echo '
            <div class="col-sm-6">
              <span class="arrow-r-header-secondary float-right">
                <a href="'.base_url().'index.php/'.$flecha_ir_atras.'"><i class="fas fa-arrow-circle-left"></i></a>
              </span>
            </div>
          ';
        }
      ?>
    
    </div>
  </div><!-- /.container-fluid -->
</section>
