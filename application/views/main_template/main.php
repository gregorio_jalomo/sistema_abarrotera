<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['infouser'])) {
  $username = ($this->session->userdata['infouser']['usuario']);
  $telefono = ($this->session->userdata['infouser']['telefono']);

} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<!DOCTYPE html>
<html>
<head>
 <!--  <link rel = "stylesheet" type = "text/css"
         href = "<?php echo base_url(); ?>css/style.css">
      <script type = 'text/javascript' src = "<?php echo base_url();
         ?>js/sample.js"></script> -->
  <?php if(isset($header)): ?>
      <?php echo $header; ?>
  <?php endif; ?>
   <?php if(isset($included_css)): ?>
      <?php foreach($included_css as $files_css): ?>
        <link rel="stylesheet" href="<?php echo base_url().$files_css; ?>">

      <?php endforeach; ?>
  <?php endif; ?>
  <?php if(isset($included_js)): ?>
      <?php foreach($included_js as $files_js): ?>
          <script type="text/javascript" src="<?php echo base_url().$files_js; ?>"></script>
      <?php endforeach; ?>
  <?php endif; ?>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper" "skin-blue .main-header .navbar" >
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color:#3c8dbc">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <div class="navbar navbar-static-top">


      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url()?>index.php/inicio" > <span style="color:white;">Inicio</span> </a>
      </li>
        </div>

    </ul>

    <!-- SEARCH FORM -->



    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?php echo base_url()?>statics/tema/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?php echo base_url()?>statics/tema/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?php echo base_url()?>statics/tema/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>

      <li class="dropdown user user-menu open"  >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <img height="40" width="40" src="<?php echo base_url()?>statics/tema/dist/img/usuario.jpg"
                class="img-circle">
              <span class="hidden-xs" style="color:white;"><?=$this->session->userdata['infouser']['nombre']?></span>
            </a>
            <ul  class="dropdown-menu" style="background-color:#d2d6de">
              <!-- User image -->
              <li class="user-header">

                <img height="40" width="40" src="<?php echo base_url()?>statics/tema/dist/img/usuario.jpg"
                class="img-circle">

                <p class="usuario2">
              <h6>  <span style="color:black; " ><?=$this->session->userdata['infouser']['nombre']?></span> </h6>
                  <h6>  <span style="color:black;" ><?=$this->session->userdata['infouser']['rol_name']?></span>  </h6>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->




              <li class="user-footer" >
                <div class="pull-right" align="center" >
                <a href="#" class="btn btn-success" class="boton1">Perfil</a>

                <a href="<?php echo base_url() ?>index.php/usuarios/usuarios/logout" class="btn btn-success" class="boton2">Salir</a>

                </div>
              </li>
                </div>

              </div>
            </ul>
          </li>

    </ul>

  </nav>



  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url()?>index.php/inicio" class="brand-link" style="background-color:#3c8dbc">
      <img src="<?php echo base_url()?>statics/tema/dist/img/logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="logo-mini" >Abarrotera</span>

    </a>



 <?php if(isset($menu)): ?>
        <?php echo $menu; ?>
  <?php endif; ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper content-overflow">
    <!-- Content Header (Page header) -->
    <?php if(isset($header_contenido)): ?>
        <?php echo $header_contenido; ?>
    <?php endif; ?>

    <!-- Main content -->
    <section class="content">

    <?php if(isset($contenido)): ?>
        <?php echo $contenido; ?>
    <?php endif; ?>



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php if(isset($footer)): ?>
        <?php echo $footer; ?>
    <?php endif; ?>


</body>
</html>
