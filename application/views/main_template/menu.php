<?php

    $rol_usuario=$this->session->userdata['infouser']['rol'];
    $is_admin=($rol_usuario==2 || $rol_usuario==1)?true:false;
?>
<!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex" >
        <div class="image">

          <img src="<?php echo base_url()?>statics/tema/dist/img/usuario.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block" ><?=$this->session->userdata['infouser']['usuario']?></a>



        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <?php

                    if(validar_permiso($rol_usuario,1,2) || $is_admin){
 
                        ?>
                          <li class="nav-item has-treeview">
                            <a href="<?php echo base_url().'index.php/productos/Productos/listar_productos'?>" class="nav-link" id="menuProductos">

                              <i class="nav-icon fas fa-edit"></i>
                              <p>
                                Productos
                              </p>
                            </a>

                          </li>
                        <?php
                    }else{
                      ?>
                       
                      <?php
                    }

                ?>
                    
                <?php

                  if(validar_permiso($rol_usuario,1,1) || $is_admin){

                      ?>
                      
                      <?php
                  }else{
                    ?>
                      
                    <?php
                  }

                ?>

<?php
    //listar_proveedoresaduana
    if(validar_permiso($rol_usuario,13,61) || $is_admin){

        ?>
          <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="<?php echo base_url().'index.php/proveedoresaduana/proveedoresaduana/listar_proveedoresaduana'?>" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Aduana</p>
              </a>
            </li>
          </ul>
    <?php
    }else{
    ?>
    
  <?php 
  }

?>


                    <li class="nav-item has-treeview">
                      <a href="#" class="nav-link" id="menuClientes">
                        <i class="nav-icon fas fa-user-tag"></i>
                        <p>
                          Clientes
                          <i class="right fas fa-angle-left"></i>
                        </p>
                      </a>

                      <?php
                          //listar_clientes
                          if(validar_permiso($rol_usuario,6,27) || $is_admin){

                              ?>
                                  <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="<?php echo base_url().'index.php/clientes/clientes/listar_clientes'?>" class="nav-link" id="menuClientes">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Catálogo</p>
                                      </a>
                                    </li>
                                  </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }
                      ?>

                      <?php
                          //listar_clientescotizacion
                          if(validar_permiso($rol_usuario,9,41) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                    <a href="<?php echo base_url().'index.php/clientescotizacion/clientescotizacion/listar_clientescotizacion'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Cotizaciones</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }
                      ?>

                      <?php
                          //listar_clientespedido
                          if(validar_permiso($rol_usuario,34,116) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                    <a href="<?php echo base_url().'index.php/clientespedido/clientespedido/listar_clientespedido'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Pedidos</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }
                      ?>

                      <?php
                          //listar_pedidosaduana
                          if(validar_permiso($rol_usuario,35,122) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                    <a href="<?php echo base_url().'index.php/pedidosaduana/pedidosaduana/listar_pedidosaduana'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Pedidos aduana</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }
                      ?>

                      <?php
                          //listar_rutas
                          if(validar_permiso($rol_usuario,14,159) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                    <a href="<?php echo base_url().'index.php/rutas/rutas/listar_rutas'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Pedidos en ruta</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }
                      ?>

                    </li>

                    <li class="nav-item has-treeview">
                      <a href="<?php echo base_url().'index.php/catalogos/catalogos/menu'?>" class="nav-link " id="menuCatalogos">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                          Catálogos
                        </p>
                      </a>
                    </li>

                    <?php
                        //listar_existencias
                        if(validar_permiso($rol_usuario,15,68) || $is_admin){

                            ?>
                              <li class="nav-item has-treeview">
                                <a href="<?php echo base_url().'index.php/existencias/listar'?>" class="nav-link " id="menuEntradasSalidas">
                                  <i class="nav-icon fas fa-arrows-alt-v"></i>
                                  <p>
                                    Entradas y salidas
                                  </p>
                                </a>
                              </li>
                        <?php
                        }

                    ?>


                    <?php
                        //listar_promociones
                        if(validar_permiso($rol_usuario,3,12) || $is_admin){

                            ?>
                              <li class="nav-item has-treeview">
                                <a href="<?php echo base_url().'index.php/promociones/promociones/listar_promociones'?>" class="nav-link " id="menuPromociones">
                                  <i class="nav-icon fas fa-edit"></i>
                                  <p>
                                    Promociones
                                  </p>
                                </a>
                              </li>
                        <?php
                        }else{
                        ?>
                        
                      <?php 
                      }

                    ?>

                    <?php
                        //alta_agentes_venta
                        if(validar_permiso($rol_usuario,14,64) || $is_admin){

                            ?>
                              <li class="nav-item has-treeview">
                                    <a href="<?php echo base_url().'index.php/agentesventa/alta'?>" class="nav-link" id="menuAgentesventa">
                                  <i class="nav-icon fas fa-edit"></i>
                                  <p>
                                    Agentes Venta
                                  </p>
                                </a>
                              </li>
                        <?php
                        }else{
                        ?>
                        
                      <?php 
                      }

                    ?>



                  

                    <li class="nav-item has-treeview">
                      <a href="#" class="nav-link" id="menuProveedores">
                        <i class="nav-icon fas fa-user-tag"></i>
                        <p>
                          Proveedores
                          <i class="right fas fa-angle-left"></i>
                        </p>
                      </a>

                      <?php
                          //listar_proveedores
                          if(validar_permiso($rol_usuario,8,37) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                      <a href="<?php echo base_url().'index.php/proveedores/proveedores/listar_proveedores'?>" class="nav-link" id="menuProveedores">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Catálogo</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }

                      ?>

                      
                      <?php
                          //listar_proveedorescotizacion
                          if(validar_permiso($rol_usuario,10,47) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                      <a href="<?php echo base_url().'index.php/proveedorescotizacion/proveedorescotizacion/listar_proveedorescotizacion'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Cotizaciones</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }

                      ?>

                      <?php
                          //listar_proveedorescompra
                          if(validar_permiso($rol_usuario,12,58) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                      <a href="<?php echo base_url().'index.php/proveedorescompra/proveedorescompra/listar_proveedorescompra'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Compras</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }

                      ?>

                      <?php
                          //listar_proveedoresaduana
                          if(validar_permiso($rol_usuario,13,61) || $is_admin){

                              ?>
                                <ul class="nav nav-treeview">
                                  <li class="nav-item">
                                      <a href="<?php echo base_url().'index.php/proveedoresaduana/proveedoresaduana/listar_proveedoresaduana'?>" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Aduana</p>
                                    </a>
                                  </li>
                                </ul>
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }

                      ?>

                    </li>


                    <?php
                          //usuarios
                          if(validar_permiso($rol_usuario,7,32) || $is_admin){

                              ?>
                                  <li class="nav-item has-treeview">

                                    <a href="<?php echo base_url()?>index.php/usuarios/Usuarios/listar" class="nav-link" id="menuUsuarios">

                                          <i class="fas fa-users-cog nav-icon"></i>
                                          <p>
                                            Usuarios
                                          </p>
                                    </a>

                                  </li>                                
                          <?php
                          }else{
                          ?>
                          
                        <?php 
                        }

                      ?>


          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Entradas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nueva Entrada</p>
                </a>
              </li>

            </ul>
          </li> -->

          <!-- fin de entradas -->


          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Utilerías
                <i class="right fas fa-angle-left"></i>
              </p>


            </a>


            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Exportar Productos</p>
                </a>
              </li>

            </ul>

          </li> -->

         <!-- fin de utilerias -->


          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Devoluciones
                <i class="right fas fa-angle-left"></i>
              </p>


            </a>


            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nueva Devolución</p>
                </a>
              </li>

            </ul>

          </li> -->

          <!-- fin de devoluciones-->


          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Facturas
                <i class="right fas fa-angle-left"></i>
              </p>


            </a>


            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nueva Factura</p>
                </a>
              </li>

            </ul>

          </li> -->
          <!-- fin de facturas -->

          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Aperturas/Cortes
                <i class="right fas fa-angle-left"></i>
              </p>


            </a>


            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Exportar Aperturas Cortes</p>
                </a>
              </li>

            </ul>

          </li> -->
          <!-- fin aperturas cortes -->



          <!-- fin de clientes -->




        <!--  <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Layout Options
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../layout/top-nav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/top-nav-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation + Sidebar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/boxed.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Boxed</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/fixed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Sidebar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/fixed-topnav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Navbar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/fixed-footer.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Footer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../layout/collapsed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Collapsed Sidebar</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Gráficas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ChartJS</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../charts/flot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Flot</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../charts/inline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inline</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                UI Elements
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../UI/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/icons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Icons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/buttons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Buttons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/sliders.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sliders</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/modals.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Modals & Alerts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/navbar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Navbar & Tabs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/timeline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Timeline</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../UI/ribbons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ribbons</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Forms
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../forms/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/advanced.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Advanced Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/editors.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Editors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/validation.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Validation</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../tables/simple.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../tables/data.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../tables/jsgrid.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>jsGrid</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">EXAMPLES</li>
          <li class="nav-item">
            <a href="../calendar.html" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendar
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Mailbox
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../mailbox/mailbox.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../mailbox/compose.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compose</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../mailbox/read-mail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Read</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/profile.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/e-commerce.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>E-commerce</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/projects.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Projects</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/project-add.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/project-edit.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Edit</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/project-detail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Detail</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/contacts.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Extras
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/login.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Login</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/register.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Register</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/forgot-password.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Forgot Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/recover-password.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Recover Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/lockscreen.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/legacy-user-menu.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Legacy User Menu</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/language-menu.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Language Menu</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/404.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Error 404</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/500.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Error 500</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/pace.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pace</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../examples/blank.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blank Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../../starter.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Starter Page</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">MISCELLANEOUS</li>
          <li class="nav-item">
            <a href="https://adminlte.io/docs/3.0" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Documentation</p>
            </a>
          </li>
          <li class="nav-header">MULTI LEVEL EXAMPLE</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Level 1</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Level 1
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Level 2</p>
                </a>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Level 2
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Level 2</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Level 1</p>
            </a>
          </li>
          <li class="nav-header">LABELS</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Important</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>Warning</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Informational</p>
            </a>
          </li>
        </ul>
      </nav> -->
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
