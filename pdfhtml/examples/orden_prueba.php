<?php
/**
 * Template para generar PDF de CFDI 3.3
 * Soporta complementos de Pagos 1.0 y Comercio Exterior 1.1
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.1.0 (10/05/2018)
 */
 $piePagina = "hola";
$colorFondo = "#78e08f";
$colorTexto = "#000";
$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$tipoComprobante ="este es el tipo de comprovante";
$cce11 = "otro campo";
?>
<style type="text/css">


.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#777;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
	padding-left: 1.6mm;
	padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
	padding-top: 1.3mm;
	padding-bottom: 1.3mm;
}
.cell-padding-narrow{
	padding-top: 1mm;
	padding-bottom: 1mm;
}
.cell-padding-big{
	padding-top: 2.6mm;
	padding-bottom: 2.6mm;
}

.border-gray{
	border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{

}
table.sat-info h5{
	line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }


</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>

	<table class="page-head">
		<tr>
			<td style="width:28%;text-align:center">
				<img src="logo.png" style="height:86px">
		</td>
			<td style="width:43%;">
				<h1 style="margin-top:2mm" class="font-large text-center">ENCABEZADO DE LA PAGINA</h1>

				<p style="margin-top:1mm" class="text-center">direccion del usuario</p>

			</td>
			<td style="width:1%"></td>
			<td style="width:28%">
				<table class="text-center">
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Serie - Folio</th>
						<th style="width:45%" class="border-gray cell-padding-v">Tipo</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v">folio</td>
						<td style="width:45%" class="border-gray cell-padding-v">comprobante<br/></td>
					</tr>
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Fecha</th>
						<th style="width:45%" class="border-gray cell-padding-v">Lugar Expedición</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v">fecha<br/></td>
						<td style="width:45%" class="border-gray cell-padding-v">lugar expedicion><br/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>





  	<table class="spacing-top-2mm">
  		<tr>
  			<td style="width:49.5%;vertical-align:top">
  				<table>
  					<tr><th style="width:100%" class="text-center cell-padding-v">EMISOR</th></tr>
  					<tr>
  						<td style="width:100%" class="cell-padding border-gray">
  							<table>
  								<tr>
  									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo 'cfdi:EmisorNombre'; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>RFC:</b> <span><?php echo 'cfdi:EmisorRfc'; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo "getRegimenFiscal"; ?></span></p></td>
  								</tr>
  							</table>
  						</td>
  					</tr>
  				</table>
  			</td>
  			<td style="width:1%">
  			</td>
  			<td style="width:49.5%;vertical-align:top">
  				<table>
  					<tr><th style="width:100%" class="text-center cell-padding-v">RECEPTOR</th></tr>
  					<tr>
  						<td style="width:100%" class="cell-padding border-gray">
  							<table>
  								<tr>
  									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo 'cfdi:ReceptorNombre'; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>RFC:</b> <span><?php echo 'cfdi:ReceptorRfc'; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>Uso del CFDI:</b> <span><?php echo "getUsoCfdi"; ?></span></p></td>
  								</tr>

  							</table>
  						</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>







  <table class="productos" class="spacing-top-2mm">
		<thead>
			<tr><th style="width:100%" colspan="6" class="text-center cell-padding-v">CONCEPTOS</th></tr>
			<tr>
				<th class="cell-padding" style="width: 8%">Clave</th>
				<th class="cell-padding" style="width:10%">Cantidad</th>
				<th class="cell-padding" style="width: 9%">Unidad</th>
				<th class="cell-padding" style="width:51%">Descripción</th>
				<th class="cell-padding text-right" style="width:10%">Precio</th>
				<th class="cell-padding text-right" style="width:12%">Importe</th>
			</tr>
		</thead>
		<tbody>

			<tr>
				<td style="width: 8%" class="cell-padding-narrow border-left"><?php echo "NoIdentificacion"; ?></td>
				<td style="width:10%" class="cell-padding-narrow"><?php echo "Cantidad"; ?></td>
				<td style="width: 9%" class="cell-padding-narrow"><?php echo 'Unidad'; ?></td>
				<td style="width:51%" class="cell-padding-narrow"><?php echo 'Descripcion'; ?></td>
				<td style="width:10%" class="cell-padding-narrow text-right">$<?php echo "ValorUnitario"; ?></td>
				<td style="width:12%" class="cell-padding-narrow text-right border-right">$<?php echo 'Importe'; ?></td>
			</tr>
			<tr class="last-row">
				<td style="width:100%" colspan="6" class="border-left border-bottom cell-padding-narrow border-right text-muted">
					<b>ClaveProdServ:</b> <?php echo 'ClaveProdServ'; ?>.
					<b>ClaveUnidad:</b> <?php echo 'ClaveUnidad'; ?>.
					<b>Impuestos:</b> <?php echo '-'; ?>.
					<?php

					echo '<b>Descuento:</b> ';
					?>
					<?php

				 echo '<b>C. Predial:</b> ';
					?>
					<?php

					echo '<b>No. Pedimento:</b> ';
					?>
				</td>
			</tr>

		</tbody>
	</table>






	<table class="spacing-top-2mm">
		<tr>
			<td style="width:74%">
				<table>
					<tr><th colspan="2" class="100p text-center cell-padding-v">DATOS GENERALES</th></tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">dfdfd</td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">metodo pago</td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">T.C. / Moneda</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">tipo cambio/moneda</td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Total con Letra</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">letra</td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:25%">
				<table>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Subtotal</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$SubTotal</td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Descuento</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$Descuento</td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Traslados</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$traslado</td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Retenciones</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$total retenidos</td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">TOTAL</th>
						<td style="width:55%" class="text-right text-bold cell-padding-narrow border-gray">$total</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>




	<table class="spacing-top-2mm">
		<tr>
			<td class="100p cell-padding border-gray">
				<table class="sat-info">
					<tr>
						<td style="width:16%;vertical-align:top">

							<h5 class="text-center spacing-top-2mm"">Versión de CFDI</h5>
							<p class="text-center font-system">version</p>
						</td>
						<td style="width:84%">
							<table class="100p">
								<tr>
									<td style="width:21%">
										<h5>Fecha de Timbrado</h5>
										<p class="font-system">tres</p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado SAT</h5>
										<p class="font-system">cuatro</p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado Emisor</h5>
										<p class="font-system">cinco</p>
									</td>
									<td style="width:35%">
										<h5>Folio Fiscal</h5>
										<p class="font-system">seis</p>
									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2" class="100p">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


	<?php
  $mensajeFactura = "mensaje inferior";
	if(!empty($mensajeFactura)){ ?>
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">NOTA</th></tr>
		<tr>
			<td style="width:100%" class="text-center cell-padding border-gray">
				<?php echo $mensajeFactura ?>
			</td>
		</tr>
	</table>
<?php
}
?>


	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una representación impresa de un CFDI.</p>
	</div>


</page>
