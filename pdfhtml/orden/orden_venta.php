<?php
$id_orden = $_GET['id'];
//$con=mysqli_connect("localhost","root","Passw0rd$$","elastillero");
$con=mysqli_connect("localhost","xcanpetc_astilla","astilla2018","xcanpetc_elastillero");
          if (mysqli_connect_errno($con)) {
             echo "Error al conectarse a MySQL: " . mysqli_connect_error();
          }
          $result = mysqli_query($con,"SELECT ordenFechaCreacion,
                                              ordenReferencia,
                                              ordenFechaLimite,
                                              ordenComentario,
                                              ordenRepresentante,
                                              orden_total,
                                              ordenIdProveedor,
                                              ordenId FROM orden_compra where
          orden_id='".$id_orden."'");
          $row = mysqli_fetch_array($result);
          $fecha_creacion = $row[0];
          $ordenReferencia = $row[1];
          $ordenFechaLimite = $row[2];
          $ordenComentario = $row[3];
          $ordenRepresentante = $row[4];
          $orden_total = $row[5];
          $ordenIdProveedor = $row[6];
          $ordenId = $row[7];


          //Proveedor
          $result_prove = mysqli_query($con,"SELECT `id`,
                                                    `rfc`,
                                                     `razon_social`,
                                                      `calle`,
                                                      `numero`,
                                                      `colonia`,
                                                      `cp`,
                                                      `municipio`,
                                                      `ciudad`,
                                                      `estado`,
                                                      `pais`,
                                                      `telefono`,
                                                      `email`,
                                                      `tados_bancarios`,
                                                      `nombre`,
                                                      `fecha_actualizacion`,
                                                      `regimen` FROM `proveedores`
                                                      WHERE id = '".$ordenIdProveedor."'");
          $row_pro = mysqli_fetch_array($result_prove);
          $prove_id = $row_pro[0];
          $prove_rfc = $row_pro[1];
          $prove_razon_social = $row_pro[2];
          $prove_calle = $row_pro[3];
          $prove_numero = $row_pro[4];
          $prove_colonia = $row_pro[5];
          $prove_cp = $row_pro[6];
          $prove_municipio = $row_pro[7];
          $prove_ciudad = $row_pro[8];
          $prove_estado = $row_pro[9];
          $prove_pais = $row_pro[10];
          $prove_telefono = $row_pro[11];
          $prove_email = $row_pro[12];
          $prove_tados_bancarios = $row_pro[13];
          $prove_nombre = $row_pro[14];
          $prove_fecha_actualizacion = $row_pro[15];
          $prove_regimen = $row_pro[16];





          //mysqli_close($con);
?>

<?php
/**
 * Template para generar PDF de CFDI 3.3
 * Soporta complementos de Pagos 1.0 y Comercio Exterior 1.1
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.1.0 (10/05/2018)
 */
$piePagina = "Orden de Compra";
$colorFondo = "#78e08f";
$colorTexto = "#000";
$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$tipoComprobante ="este es el tipo de comprovante";
$cce11 = "otro campo";
?>
<style type="text/css">


.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#777;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
	padding-left: 1.6mm;
	padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
	padding-top: 1.3mm;
	padding-bottom: 1.3mm;
}
.cell-padding-narrow{
	padding-top: 1mm;
	padding-bottom: 1mm;
}
.cell-padding-big{
	padding-top: 2.6mm;
	padding-bottom: 2.6mm;
}

.border-gray{
	border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{

}
table.sat-info h5{
	line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }


</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>

	<table class="page-head">
		<tr>
			<td style="width:28%;text-align:center">
				<img src="logo.png" style="height:86px">
		</td>
			<td style="width:43%;">
				<h2 style="margin-top:2mm; color:#353b48" class="font-large text-center">EL ASTILLERO HIGIENE AMBIENTAL S.A. DE C.V.</h2>

				<p style="margin-top:1mm; color:#353b48" class="text-center">www.fumigacioneselastillero.com</p>

			</td>
			<td style="width:1%"></td>
			<td style="width:28%">
        <table>
          <tr>
            <td style="width:100%" class="cell-padding border-gray">
              <table>
                <tr>
                  <td style="width:100%"><p><span><?php echo 'Av. Tecoman, Rinconada San Pablo'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo '256, CP:28050'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Colima Col."; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Tel: (01 312)158 14 00"; ?></span></p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
			</td>
		</tr>
	</table>





  	<table class="spacing-top-2mm">
  		<tr>
  			<td style="width:49.5%;vertical-align:top">
  				<table>
  					<tr><th style="width:100%" class="text-center cell-padding-v">VENDIDO A:</th></tr>
  					<tr>
  						<td style="width:100%" class="cell-padding border-gray">
  							<table>
  								<tr>
  									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo $prove_razon_social; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>RFC:</b> <span><?php echo $prove_rfc; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo $prove_regimen; ?></span></p></td>
  								</tr>
  							</table>
  						</td>
  					</tr>
  				</table>
  			</td>
  			<td style="width:1%">
  			</td>
  			<td style="width:49.5%;vertical-align:top">
  				<table>
  					<tr>
              <td style="width:100%" class="cell-padding border-gray">
  							<table>
  								<tr>
  									<td style="width:100%"><p><b>ORDEN COMPRA NO.:</b> <span><?php echo $ordenId; ?></span></p></td>
  								</tr>


  							</table>
  						</td>
            </tr>
  					<tr>
  						<td style="width:100%" class="cell-padding border-gray">
  							<table>
  								<tr>
  									<td style="width:100%"><p><b>Fecha:</b> <span><?php echo $fecha_creacion; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>Fecha vencimiento:</b> <span><?php echo $ordenFechaLimite; ?></span></p></td>
  								</tr>
  								<tr>
  									<td style="width:100%"><p><b>L.A.B.:</b> <span><?php echo "hola"; ?></span></p></td>
  								</tr>

  							</table>
  						</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>







  <table class="productos" class="spacing-top-2mm">
		<thead>
			<tr><th style="width:100%" colspan="6" class="text-center cell-padding-v">CONCEPTOS</th></tr>
			<tr>
				<th class="cell-padding" style="width: 8%">Clave</th>
				<th class="cell-padding" style="width:10%">Cantidad</th>
				<th class="cell-padding" style="width: 9%">Unidad</th>
				<th class="cell-padding" style="width:51%">Descripción</th>
				<th class="cell-padding text-right" style="width:10%">Precio</th>
				<th class="cell-padding text-right" style="width:12%">Importe</th>
			</tr>
		</thead>
		<tbody>

      <?php
      $total_venta = 0;
      $result_productos = mysqli_query($con,"SELECT * FROM orden_compra_productos
                                                      join productos on orden_compra_productos.ordenP_idProducto = productos.productoId
                                                      WHERE orden_compra_productos.ordenP_idOrden =$ordenId");

     while($productos = mysqli_fetch_assoc($result_productos)){
          $productoNombre = $productos['productoNombre'];
          $ordenP_cantidad = $productos['ordenP_cantidad'];
          $productoPresantacion = $productos['productoPresantacion'];
          $productoPrecioPublico = $productos['productoPrecioPublico'];
          $total_venta += $productos['ordenP_precio']*$productos['ordenP_cantidad'];
      ?>
			<tr>
				<td style="width: 8%" class="cell-padding-narrow border-left"><?php echo $productos['productoId']; ?></td>
				<td style="width:10%" class="cell-padding-narrow"><?php echo $productos['ordenP_cantidad']; ?></td>
				<td style="width: 9%" class="cell-padding-narrow"><?php echo 'Unidad'; ?></td>
				<td style="width:51%" class="cell-padding-narrow"><?php echo $productos['productoNombre']; ?></td>
				<td style="width:10%" class="cell-padding-narrow text-right">$<?php echo $productos['ordenP_precio']; ?></td>
				<td style="width:12%" class="cell-padding-narrow text-right border-right">$<?php echo $productos['ordenP_precio']*$productos['ordenP_cantidad']; ?></td>
			</tr>
      <tr class="last-row">
				<td style="width:100%" colspan="6" class="border-left border-bottom cell-padding-narrow border-right text-muted">
					<!--b>ClaveProdServ:</b> <?php echo 'ClaveProdServ'; ?>.
					<b>ClaveUnidad:</b> <?php echo 'ClaveUnidad'; ?>.
					<b>Impuestos:</b> <?php echo '-'; ?>.
					<?php

					echo '<b>Descuento:</b> ';
					?>
					<?php

				 echo '<b>C. Predial:</b> ';
					?>
					<?php

					echo '<b>No. Pedimento:</b> ';
					?>-->
				</td>
			</tr>
    <?php }

    ?>


		</tbody>
	</table>






	<table class="spacing-top-2mm">
		<tr>
			<td style="width:74%">
				<table>
					<tr><th colspan="2" class="100p text-center cell-padding-v">DATOS GENERALES</th></tr>
					<!--tr>
						<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">dfdfd</td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">metodo pago</td>
					</tr-->
					<tr>
						<th style="width:18%" class="cell-padding-narrow">T.C. / Moneda</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">MXN</td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Total con Letra</th>
						<td style="width:82%" class="cell-padding-narrow border-gray">
                <?php
                $iva_total = $total_venta * .16;
                $can_letra = $iva_total+ $total_venta;
                //require_once '/cantidadletra.php';
                //$letras = new CantidadConLetra();
                echo $can_letra;//$letras->convertir($can_letra,'PESO', $sufijo = 'MXN');
                ?>
            </td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:25%">
				<table>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Subtotal</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $total_venta;?></td>
					</tr>
					<!--tr>
						<th style="width:45%" class="cell-padding-narrow">Descuento</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$Descuento</td>
					</tr-->
					<tr>
						<th style="width:45%" class="cell-padding-narrow">IVA</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray"><?php echo "$".$iva_total;?></td>
					</tr>
					<!--tr>
						<th style="width:45%" class="cell-padding-narrow">Retenciones</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$total retenidos</td>
					</tr-->
					<tr>
						<th style="width:45%" class="cell-padding-narrow">TOTAL</th>
						<td style="width:55%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $iva_total+ $total_venta;?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>




	<!--table class="spacing-top-2mm">
		<tr>
			<td class="100p cell-padding border-gray">
				<table class="sat-info">
					<tr>
						<td style="width:16%;vertical-align:top">

							<h5 class="text-center spacing-top-2mm"">Versión de CFDI</h5>
							<p class="text-center font-system">version</p>
						</td>
						<td style="width:84%">
							<table class="100p">
								<tr>
									<td style="width:21%">
										<h5>Fecha de Timbrado</h5>
										<p class="font-system">tres</p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado SAT</h5>
										<p class="font-system">cuatro</p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado Emisor</h5>
										<p class="font-system">cinco</p>
									</td>
									<td style="width:35%">
										<h5>Folio Fiscal</h5>
										<p class="font-system">seis</p>
									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2" class="100p">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table-->


	<?php
  $mensajeFactura = $ordenComentario;
	if(!empty($mensajeFactura)){ ?>
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">COMENTARIOS</th></tr>
		<tr>
			<td style="width:100%" class="text-center cell-padding border-gray">
				<?php echo $mensajeFactura ?>
			</td>
		</tr>
	</table>
<?php
}
?>


	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una orden de compra.</p>
	</div>


</page>
