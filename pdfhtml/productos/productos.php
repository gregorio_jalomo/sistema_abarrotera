<?php
$tipo = $_GET['tipo'];
//$con=mysqli_connect("localhost","root","","elastillero");
$con=mysqli_connect("localhost","xcanpetc_astilla","astilla2018","xcanpetc_elastillero");
          if (mysqli_connect_errno($con)) {
             echo "Error al conectarse a MySQL: " . mysqli_connect_error();
          }
?>
<style type="text/css">


.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#777;}

*{
    font-size: 7pt;
    line-height: 125%;
}
.font-large{
    font-size: 12pt;
}
.font-medium,
.font-medium *{
    font-size: 9pt;
}
.font-system{
    font-family:courier;
    line-height: 110%;
}

p{
    margin:0;
}
h1{
    margin:0;
}
h2{
    margin:0;
}
h5{
    margin:0;
}
table{
    border-spacing: 0;
    border-collapse: collapse;
}

.spacing{
    height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
    margin-top:0.5mm;
}
.spacing-top-1mm{
    margin-top:1mm;
}
.spacing-top-2mm{
    margin-top:2mm;
}
.spacing-top-3mm{
    margin-top:3mm;
}
.spacing-bottom{
    margin-top:1mm;
}
.spacing-bottom-2mm{
    margin-bottom:2mm;
}


.100p{
    width:100%;
}
.99p{
    width:99%;
}
.80p{
    width:80%;
}
.75p{
    width:75%;
}
.60p{
    width:60%;
}
.50p{
    width:50%;
}
.40p{
    width:40%;
}
.33p{
    width:33%;
}
.34p{
    width:34%;
}
.25p{
    width:25%;
}

th,
.bg-gray{
    background: <?php echo $colorFondo; ?>;
    color: <?php echo $colorTexto; ?>;
    font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
    padding-left: 1.6mm;
    padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
    padding-top: 1.3mm;
    padding-bottom: 1.3mm;
}
.cell-padding-narrow{
    padding-top: 1mm;
    padding-bottom: 1mm;
}
.cell-padding-big{
    padding-top: 2.6mm;
    padding-bottom: 2.6mm;
}

.border-gray{
    border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
    border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
    border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
    border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
    padding-top: 1.2mm;
    padding-bottom: 0mm;
}
table.productos tr.last-row td {
    padding-bottom: 1.1mm;
}
table.sat-info{

}
table.sat-info h5{
    line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }


</style>

<table class="page-head">
        <tr>
            <td style="width:28%;text-align:center">
                <img src="logo.png" style="height:86px">
        </td>
            <td style="width:43%;">
                <h2 style="margin-top:2mm; color:#353b48" class="font-large text-center">EL ASTILLERO HIGIENE AMBIENTAL S.A. DE C.V.</h2>

                <p style="margin-top:1mm; color:#353b48" class="text-center">www.fumigacioneselastillero.com</p>

            </td>
            <td style="width:1%"></td>
            <td style="width:28%">
        <table>
          <tr>
            <td style="width:100%" class="cell-padding border-gray">
              <table>
                <tr>
                  <td style="width:100%"><p><span><?php echo 'Av. Tecoman, Rinconada San Pablo'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo '256, CP:28050'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Colima Col."; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Tel: (01 312)158 14 00"; ?></span></p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
            </td>
        </tr>
    </table>
<span style="font-size: 20px; font-weight: bold">Lista productos<br></span>
<br>
<br>
<table>
    <col style="width: 20%" >
    <col style="width: 20%">
    <col style="width: 20%">
    <col style="width: 20%">
    <thead>
        
        <tr>
            <th style="font-size: 14px; font-weight: bold">Producto</th>
            <th style="font-size: 14px; font-weight: bold">Referencia</th>
            <th style="font-size: 14px; font-weight: bold">Marca</th>
            <th style="font-size: 14px; font-weight: bold">Contenido</th>
            <th style="font-size: 14px; font-weight: bold">Precio</th>
        </tr>
    </thead>
<?php
      $total_venta = 0;
      $result_productos = mysqli_query($con,"SELECT * FROM productos 
                                             join medidas 
                                             on medidas.medidaId = productos.productoMedida
                                             join marca 
                                             on marca.marcaId = productos.productoIdMarca");

     while($productos = mysqli_fetch_assoc($result_productos)){
          $productoNombre = $productos['productoNombre'];
          $productoReferencia = $productos['productoReferencia'];
           $precio = 0;
          $marca = $productos['marcaNombre'];
          $contenido = $productos['procuctoContenido']." ".$productos['medidaNombre'];
          if($tipo == 1){
            $precio =  $productos['productoPrecioPublico'];
          }
          if($tipo == 2){
            $precio =  $productos['productoPrecioCredito'];
          }

          if($tipo == 3){
            $precio =  $productos['productoPrecioMayoreo'];
          }
          
      ?>
<tr>
    <td style="font-size: 14px; "><?php echo $productoNombre; ?></td>
    <td style="font-size: 14px; "><?php echo $productoReferencia; ?></td>
    <td style="font-size: 14px; "><?php echo $marca; ?></td>
    <td style="font-size: 14px; "><?php echo $contenido; ?></td>
    <td style="font-size: 14px; "><?php echo $precio; ?></td>

</tr>
<?php
}
?>
    
</table>

<end_last_page end_height="30mm">
    <div>
        
    </div>
</end_last_page>