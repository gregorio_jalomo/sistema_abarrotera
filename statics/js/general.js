var code400 = function () {
    ErrorCustom('No tiene permisos de acceder!', "");
}
var code404 = function () {
    ErrorCustom('La petición realizada no se encuentra, favor de intentarlo de nuevo mas tarde.', "");
}
var code500 = function () {
    ErrorCustom('El servidor no se encuentra disponible, intenta de nuevo', "" );
}
var code409 = function () {
    ErrorCustom('Sesión ha expirado por inactividad en el sistema', function () {
        window.location.href = '/Account/LogOff';
    });
}

//mensaje y tipo: success, danger, etc
var exito = function(mensaje,tipo){
    //usageExample: exito("Hecho correctamente","success");
  bootbox.dialog({
      message: mensaje,
      closeButton: false,
      buttons:
      {
          "danger":
          {
              "label": "<i class='icon-remove'></i> Cerrar",
              "className": "btn-sm btn-"+tipo+"",

          }
      }
  });
  setTimeout(function(){$(".auto_close").click();},1500);
}

var exito_redirect = function(mensaje,tipo,redirect){
  bootbox.dialog({
      message: mensaje,
      closeButton: false,
      buttons:
      {
          "danger":
          {
              "label": "<i class='icon-remove'></i> Cerrar",
              "className": "btn-sm btn-"+tipo+"",
              "callback": function () {
                  window.location.href = redirect;
              }

          }
      }
  });
  setTimeout(function(){$(".auto_close").click();},1000);
}

var ajaxJson = function (url, data, metodo, asincrono, callback) {
    //usageExample: ajaxJson("<?php echo base_url()?>index.php/controller/function/"+<?=$some_variable?>, {}, "POST", "", function(){});
    if (metodo == "")
        metodo = "POST";
    $.ajax({
        type: metodo,
        url: url,
        enctype: 'multipart/form-data',
        datatype: "JSON",
        async: asincrono,
        cache: false,
        data: data,
        statusCode: {
            200: function (result) {
            	//console.log(result);
                if (callback != "") {
                    var call = $.Callbacks();
                    call.add(callback);
                    call.fire(result);
                }
            },
            401: code400,
            404: code404,
            500: code500,
            409: code409
        }
    });

}


var ajaxJsonFiles = function (url, data, metodo, asincrono, callback) {
    if (metodo == "")
        metodo = "POST";
    $.ajax({
        type: metodo,
        url: url,
        enctype: 'multipart/form-data',
        datatype: "JSON",
        async: asincrono,
        cache: false,
        data: data,
        contentType : false,
        processData : false,
        statusCode: {
            200: function (result) {
              //console.log(result);
                if (callback != "") {
                    var call = $.Callbacks();
                    call.add(callback);
                    call.fire(result);
                }
            },
            401: code400,
            404: code404,
            500: code500,
            409: code409
        }
    });

}
var ConfirmCustom = function (mensaje, callbackOk, callbackCancel, TxtBtnOk, TxtBtnFail) {
    //usage: ConfirmCustom(mensaje, callbackOk, callbackCancel, TxtBtnOk, TxtBtnFail);
    if (TxtBtnOk == "" || TxtBtnOk == undefined)
        TxtBtnOk = "Aceptar";
    if (TxtBtnFail == "" || TxtBtnFail == undefined)
        TxtBtnFail = "Cancelar";
    bootbox.dialog({
        message: mensaje,
        closeButton: false,
        buttons:
        {
            "success":
            {
                "label": TxtBtnOk,
                "className": "btn-sm btn-success",
                "callback": function () {
                    if (callbackOk != "") {
                        var call = $.Callbacks();
                        call.add(callbackOk);
                        call.fire();
                    }
                }
            },
            "danger":
            {
                "label": TxtBtnFail,
                "className": "btn-sm btn-danger",
                "callback": function () {
                    if (callbackCancel != "") {
                        var call = $.Callbacks();
                        call.add(callbackCancel);
                        call.fire();
                    }
                }
            }
        }
    });
}
var ExitoCustom = function (mensaje, callback,nombre_boton='') {
    if(nombre_boton==''){
        var cerrar = 'Cerrar';
    }else{
        cerrar = nombre_boton;
    }
    if (mensaje == "" || mensaje == undefined)
        mensaje = '<span>Los datos han sido guardados con éxito</span>';
    else
        mensaje = "<span>" + mensaje + "</span>";
    bootbox.dialog({
        message: mensaje,
        closeButton: false,
        buttons:
        {
            "success":
            {
                "label": "<i class='icon-remove'></i> "+cerrar,
                "className": "btn-sm btn-success",
                "callback": function () {
                    if (callback != "") {
                        var call = $.Callbacks();
                        call.add(callback);
                        call.fire();
                    }
                }
            }
        }
    });
    setTimeout(function(){$(".icon-remove").click();},1000);
}
var ErrorCustom = function (mensaje, callback) {
    if (mensaje == "" || mensaje == undefined)
        mensaje = '<span>Ocurrió un error al procesar la petición</span>';
    else
        mensaje = "<span>" + mensaje + "</span>";
    bootbox.dialog({
        message: mensaje,
        closeButton: false,
        buttons:
        {
            "danger":
            {
                "label": "<i class='icon-remove'></i> Cerrar",
                "className": "btn-sm btn-danger",
                "callback": function () {
                    if (callback != "") {
                        var call = $.Callbacks();
                        call.add(callback);
                        call.fire();
                    }
                }
            }
        }
    });
    setTimeout(function(){$(".auto_close").click();},1500);
}


var customHTMLModal = function (html, size, callbackOk, callbackCancel, txtBtnOk, txtBtnCancel, txtTitulo, idModal) {
    var btns = {};
    if (txtBtnOk != "") {
        btns.success = {
            "label": txtBtnOk,
            "className": "btn-sm btn-success",
            "callback": function () {
                if (callbackOk != "") {
                    var call = $.Callbacks();
                    call.add(callbackOk);
                    call.fire();
                    return false;
                }
            }
        };
    }
    if (txtBtnCancel != "") {
        btns.danger = {
            "label": txtBtnCancel,
            "className": "btn-sm btn-danger",
            "callback": function () {
                if (callbackCancel != "") {
                    var call = $.Callbacks();
                    call.add(callbackCancel);
                    call.fire();
                }
            }
        };
    }
    bootbox.dialog({
        message: html,
        closeButton: true,
        title: txtTitulo,
        className: idModal,
        buttons: btns
    });
    if (size != "")
        $("." + idModal).children().addClass("modal-" + size);
}
totalModal=0;

//customModal(site_url+"/usuarios/info_camioneta",{"data":value},"POST","lg","callbackOk","callbackCancel","callbackExtra","txtBtnOk","txtBtnCancel","txtBtnExtra","modalTitle","modalId","closeBtnBool");
var customModal = function (url, data, metodo, size, callbackOk, callbackCancel,callbackExtra, txtBtnOk, txtBtnCancel,txtBtnExtra, txtTitulo, idModal,closeBtnBool) {
    totalModal++;
    var btns = {};
    if (txtBtnOk != "txtBtnOk" && txtBtnOk != "") {
        btns.success = {
            "label": txtBtnOk,
            "className": "btn btn-primary",
            "callback": function () {
                if (callbackOk != "callbackOk" && callbackOk != "") {
                    var call = $.Callbacks();
                    call.add(callbackOk);
                    call.fire();
                    return false;
                }
            }
        };
    }

    if (txtBtnCancel != "txtBtnCancel" && txtBtnCancel != "") {
        btns.danger = {
            "label": txtBtnCancel,
            "className": "btn btn-secundario",
            "callback": function () {
                totalModal--;
                if (callbackCancel != "callbackCancel" && callbackCancel != "") {
                    var call = $.Callbacks();
                    call.add(callbackCancel);
                    call.fire();
                }
                if (totalModal > 0) {
                    setTimeout(function () { $("body").addClass("modal-open modal-panel") }, 500);
                }
            }
        };
    }

    if (txtBtnExtra != "txtBtnExtra" && txtBtnExtra != "") {
        btns.extra = {
            "label": txtBtnExtra,
            "className": "btn btn-primary btn-ticket",
            "callback": function () {
                totalModal++;
                if (callbackExtra != "callbackExtra" && callbackExtra != "") {
                    var call = $.Callbacks();
                    call.add(callbackExtra);
                    /*return false;
                     call.fire();
                     */
                }

            }
        };
    }
    if(url != "url" && url != ""){
        $.ajax({
            url: url,
            type: metodo,
            data: data,
            datatype: "html",
            cache: false,
            statusCode: {
                200: function (response) {
                    if (typeof response == "string") {
                        bootbox.dialog({
                            message: response,
                            closeButton: closeBtnBool,
                            title: txtTitulo,
                            className: idModal,
                            buttons: btns
                        });
                        if (size != "")
                            $("." + idModal).children().addClass("modal-" + size);
                    }
                    else {
                        if (response.Exito == false) {
                            ErrorCustom(response.Mensaje, "");
                        }
                        else {
                            llamarMaestro(response.Url, response.Parametros);
                        }
                    }
                },
                401: code400,
                404: code404,
                500: code500,
                409: code409
            }
        });
    }//if url
    else{
        bootbox.dialog({
            message: "mensaje",
            closeButton: closeBtnBool,
            title: txtTitulo,
            className: idModal,
            buttons: btns
        });
        if (size != "")
            $("." + idModal).children().addClass("modal-" + size);
    }
}

var llamarMaestro = function (frm, parametros) {
    if (typeof parametros != "undefined") {
        $.each(parametros, function (key, value) {
            if (typeof value != "object") {
                $("#" + frm).append('<input type="hidden" name="' + key + '" value="' + value + '" />');
            }
            else {
                $("#" + frm).append("<input type='hidden' name='" + key + "' value='" + JSON.stringify(value) + "' />");
            }
        });
        $("#" + frm).submit();
    }
}
var capitalizeWord = function (word) {  
    var newtext= word.trim()
                .replace("  ","").replace(" ","")
                .toLocaleLowerCase();
    newtext=newtext[0].replace(" ","").toLocaleUpperCase() + newtext.slice(1).replace(" ",""); 
    //alert(newtext);
    return newtext;
  } 



var valida_solo_float = function(HTMLInputObject){
        let x = HTMLInputObject;
        let correctFloat = x.val().replace(/[^0-9\.]/g,"");
        let floatSplitted = correctFloat.split(".");

        if (floatSplitted.length>=3) {
            correctFloat=correctFloat.slice(0, -1);//quitar el segundo punto decimal
        }

        x.val(correctFloat);
        return correctFloat;
    }

  var valida_solo_numeros = function (e){//sin punto decimal, usage:  onkeydown="return valida_solo_numeros(event)"
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla ==8 || tecla==9){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    
    patron =/[0-9]/;

    //alert(tecla);
    tecla_final = String.fromCharCode(tecla);
    //alert(tecla_final);
    return patron.test(tecla_final);
    
    /*var key = evt.keyCode;
    alert(key);
    return (key >= 48 && key <= 57);*/

}

var randomStr = function(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}//...randomStr


function clean_array(my_array) {
    /* 
        esta funcion remueve los valores 'null', '0', '""', 'false', 'undefined' and 'NaN' 
        util cuando se crea el array con indices numericos dinamicamente ya que si los indices 
        no son consecutivos automaticamente se definen en null los indices faltantes

        arr[                                arr[
            {"myobj":"1"}           \         {"myobj":"1"}
            null,             -----  \        {"myobj":"2"}
            null,             -----  /      ]
            {"myobj":"2"}           /
        ]
    */
    let index = -1;
    const arr_length = my_array ? my_array.length : 0;
    let resIndex = -1;
    const result = [];

    while (++index < arr_length) {
        const value = my_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

  /**
   * Ajuste decimal de un número.
   *
   * @param {String}  tipo  El tipo de ajuste.
   * @param {Number}  valor El numero.
   * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
   * @returns {Number} El valor ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }//...decimalAdjust

    // Decimal round
    if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }